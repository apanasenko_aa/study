function [y, dy, ddy] = function_4(x)
	y = exp(3 * x) + 5 * exp(-2 * x);
	dy = 3 * exp(3 * x) - 10 * exp(-2 * x);
	ddy = 9 * exp(3 * x) + 20 * exp(-2 * x);
endfunction;
