function [x_opt, iter] = conjugate_gradient(func, X, eps = 1e-5)
	n = length(X);
	x_opt = X;
	b = func(zeros(n, 1));
	P = zeros(n, 1);
	A = [zeros(n, 1), ones(n, 1)];
	iter = 0;
	Fx = func(x_opt);
	while (norm(Fx) > eps)
		iter += 1;
		A = [A(1:n, 2), Fx];
		P = A(1:n, 2) + norm(A(1:n, 2)) ** 2 / (norm(A(1:n, 1)) ** 2) * P;
		x_opt += (P' * (-A(1:n, 2)) / (P' * (func(P) - b))) * P;
		Fx = func(x_opt);
	endwhile;
endfunction;