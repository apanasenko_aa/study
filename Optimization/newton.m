function [x_opt, f_opt, num_oracle, time] = newton(func, interval, eps = 1e-5, max_iter = 500)
	a = interval(1);
	b = interval(2);
	num_oracle = 0;	
	h = 1;
	k = 0;
	x = (a + b) /2;
	df = 1;
	time = clock();
	while (abs(df) > eps && k < max_iter)
		k++;
		num_oracle++;
		[f df ddf] = func(x);
		y = x - step(h, k) * df / ddf;
		x = y;
	end;
	time = etime(clock(), time);
	x_opt = x;
	f_opt = func(x_opt)(1);	
	% printf("Метод Ньютона с адаптивным шагом\n");
	% printf("Точка минимума: %f\n", x_opt);
	% printf("Минимум: %f\n", f_opt);
	% printf("Кол-во обращений к оракулу: %d\n", num_oracle);
	% printf("Время работы: %d\n\n", time);
endfunction;

function res = step(h, k)
	res = h / sqrt(k + 1);
endfunction;
