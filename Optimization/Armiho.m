function [res, iter] = Armiho(func, x, eps = 1e-5)
   sigma = 0.4;
   betta = 0.8;
   
   lambda = 0.6;

   [f grad] = func(x);
   xk = x;
   fk = f;

   iter = 0;
   while (norm(grad) >= eps)
      iter += 1;
      x = xk;
      f = fk;

      m = 0;
      while (fk > f - sigma * lambda * betta**m * norm(grad)**2)
         m++;
         fk = func(x - lambda * betta**m * grad);
      endwhile

      lambda_k = sigma * lambda * betta**m;
      xk = x - lambda_k * grad;
      [fk grad] = func(xk);
   endwhile

   res = x;

endfunction
