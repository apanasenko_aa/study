function [y, dy, ddy] = function_5(x)
	y = 0.2 * x * log(x) + (x - 2.3) ** 2;
	dy = 0.2 * (log(x) + 1) + 2 * (x - 2.3);
	ddy = 0.2 / x + 2;
endfunction;
