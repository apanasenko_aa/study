function res = main_()


x0 = [-1 1];
printf("init point (-1, 1)\n");
xmin = gd_method(@Calcfg, x0, 1e-15);
xmin

x0 = [0.5 0.5];
printf("init point (0.5, 0.5)\n");
xmin = gd_method(@Calcfg, x0, 1e-15);
xmin

x0 = [0 0];
printf("init point (0, 0)\n");
xmin = gd_method(@Calcfg, x0, 1e-15);
xmin

endfunction