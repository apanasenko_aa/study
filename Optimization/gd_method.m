function x_min = gd_method(func, x0, eps = 1e-5)
   betta = 0.5;
   delta = 0.35;
   lambda_k = lambda = 0.6;
   xk = x0;
   [f0 grad_k] = func(x0);
   fk = f0;
   iter = 0;
   while (norm(grad_k) >= eps)
      iter++;
      x0 = xk;
      f0 = fk;
      xk = x0 - lambda_k * grad_k; %первая k-aя точка
      [fk tmp] = func(xk);         %вычисляем значение функции в к-ой точки
      m = 0;
      while (fk > f0 - delta * lambda * betta**m * norm(grad_k)**2) %армиха
         m++;
         [fk tmp] = func(x0 - lambda * betta**m * grad_k); % увеличили m посчитали значение в этой точки 
      endwhile
      lambda_k = delta * lambda * betta**m;
      xk = x0 - lambda_k * grad_k;
      [fk grad_k] = func(xk);
      % xk
      % printf("norm gradient = %f\n", norm(grad_k));
   endwhile
   x_min = x0;
   printf("iterations %d\n", iter);
endfunction
