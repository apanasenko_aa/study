function res = grad_Q(x)
	global N;
	res = zeros(length(x), 1);
	for i = 1 : length(x)
		for j = 1 : N
			t = pi * (j - 1) / (N - 1);
			res(i) -= (sin(t) - P(x, t)) * 2 * t ** (i - 1);
		endfor;
	endfor;
endfunction;

function res = P(x, t)
	res = 0;
	for i = 1 : length(x)
		res += t ** (i - 1) * x(i);
	endfor;
endfunction;