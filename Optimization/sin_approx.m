function res = sin_approx()
	global N = 100;
	x1 = zeros(5, 1);
	x2 = [1.0; 1.0; 0.5; 0.5; 0];
	[a1, i1] = conjugate_gradient(@grad_Q, x1)
	[a2, i2] = conjugate_gradient(@grad_Q, x2)
	[a3, i3] = qNewton(@grad_Q, x1)
	[a4, i4] = qNewton(@grad_Q, x2)
endfunction;