function [x_opt, iter] = qNewton(func, x, eps = 1e-5)
	n = length(x);
	b = func(zeros(n, 1));
	A = eye(n, n); 
	iter = 0;
	fx = func(x);
	d = fx;
	while (norm(fx) > eps)
		iter += 1;
		x_opt = x + ( d' * fx) / (d' * (func(-d) - b)) * d;
		Fx = func(x_opt);
		r = x_opt - x - A * (Fx - fx);
		A += r * r' / (r' * (Fx - fx));
		x = x_opt;
		fx = Fx
		d = A * fx;
	end;
endfunction;