function [x_opt, f_opt, num_oracle, time] = golden_ratio(func, interval, eps = 1e-5, max_iter = 500)
	a = interval(1);
	b = interval(2);
	k = (3 - sqrt(5)) / 2;
	x1 =  a + k * (b - a);
	x2 =  b - k * (b - a);
	y1 = func(x1)(1);
	y2 = func(x2)(1);
	num_oracle = 2;
	time = clock();
	while (abs(a - b) > eps && num_oracle < max_iter)
		if (y1 > y2)
			a = x1;
			x1 = x2;
			x2 = b - (x1 - a);
			y1 = y2;
			y2 = func(x2)(1);
		else
			b = x2;
			x2 = x1;
			x1 = a + (b - x2);
			y2 = y1;
			y1 = func(x1)(1);
		end
		num_oracle++;
	end
	time = etime(clock(), time);
	x_opt = (a + b) / 2;
	f_opt = func(x_opt)(1);	
	% printf("Метод золотого сечения\n");
	% printf("Точка минимума: %f\n", x_opt);
	% printf("Минимум: %f\n", f_opt);
	% printf("Кол-во обращений к оракулу: %d\n", num_oracle);
	% printf("Время работы: %d\n\n", time);
endfunction