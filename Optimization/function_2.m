function [y, dy, ddy] = function_2(x)
	y = log(x - 2) ** 2 + log(10 - x) ** 2 - x ** 0.2;
	dy = 2 * log(x - 2) / (x - 2) - 2 * log(10 - x) / (10 - x) - 0.2 * x ** -0.8;
	ddy = 2 * (1 - log(x - 2)) / (x - 2) ** 2 - 2 * (1 + log(10 - x)) / (10 - x) ** 2 + 0.16 * x ** -1.8;
endfunction;
