function [y, dy, ddy] = function_3(x)
	y = -3 * x * sin(0.75 * x) + exp(-2 * x);
	dy = -3 * (sin(0.75 * x) + 0.75 * x * cos(0.75 * x)) - 2 * exp(-2 * x);
	ddy = -3 *(cos(0.75 * x) + 0.75*(cos(0.75 * x) - x * sin(0.75 * x))) + 4 * exp(-2 * x);
endfunction;