function [x_opt, f_opt, num_oracle, time] = dihotomia(func, interval, eps = 1e-5, max_iter = 500)
	r = interval(1);
	l = interval(2);
	num_oracle = 0;
	time = clock();
	while (abs(r - l) > eps)
		num_oracle++;
		if (num_oracle > max_iter)
			break;
		end;
		x_opt = (r + l) / 2;
		[f, g] = func(x_opt);
		if (g >= 0)
			l = x_opt;
		else
			r = x_opt;
		end;
	end;
	time = etime(clock(), time); 	
	f_opt = func(x_opt);	
	% printf("Метод дихотомии\n");
	% printf("Точка минимума: %f\n", x_opt);
	% printf("Минимум: %f\n", f_opt);
	% printf("Кол-во обращений к оракулу: %d\n", num_oracle);
	% printf("Время работы: %d\n\n", time);
endfunction;