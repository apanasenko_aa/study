function [y, dy, ddy] = function_1(x)
	y = -5 * x ** 5 + 4 * x ** 4 - 12 * x ** 3 + 11 * x ** 2 - 2 * x + 1;
	dy = -25 * x ** 4 + 16 * x ** 3 - 36 * x ** 2 + 22 * x - 2;
	ddy = -100 * x ** 3 + 48 * x ** 2 - 72 * x + 22;
endfunction;
