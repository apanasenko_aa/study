function [x_opt, f_opt, num_oracle, time] = fibonachi(func, interval, eps = 1e-5, max_iter = 500)
	a = interval(1);
	b = interval(2);
	time = clock();
	f = F((b - a) / eps, max_iter);
	s = length(f);
	x1 =  a + (b - a) * f(s - 2) / f(s);
	x2 =  a + (b - a) * f(s - 1) / f(s);
	y1 = func(x1)(1);
	y2 = func(x2)(1);
	num_oracle = 2;
	while (s > 1)
		if (y1 > y2)
			a = x1;
			x1 = x2;
			x2 = b - (x1 - a);
			y1 = y2;
			y2 = func(x2)(1);
		else
			b = x2;
			x2 = x1;
			x1 = a + (b - x2);
			y2 = y1;
			y1 = func(x1)(1);
		end
		num_oracle++;
		s--;
	end
	time = etime(clock(), time);
	x_opt = (a + b) / 2;
	f_opt = func(x_opt)(1)
	% printf("Метод Фибоначи\n");
	% printf("Точка минимума: %f\n", x_opt);
	% printf("Минимум: %f\n", f_opt);
	% printf("Кол-во обращений к оракулу: %d\n", num_oracle);
	% printf("Время работы: %d\n\n", time);
endfunction


function ans = F(N, max_iter)
	n = 2;
	ans = [1, 2];
	while (ans(n) < N && n < max_iter)
		n += 1;
		ans = [ans, ans(n - 2) + ans(n - 1)];
	end
endfunction