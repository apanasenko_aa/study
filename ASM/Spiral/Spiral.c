#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main()
{
	int n, i, j, k, m, t, x = 0, y = -1;
	scanf("%d", &n);
	int a[n][n];
	t = n;
	k = 1;                     
	m = 0;                     
	int delta_x = 0;
	int delta_y = 1;
	while(n)
	{
		m++;
		for(i = 0; i < n; i++)
		{
			x += delta_x % 2 ? (delta_x - 1) % 4 ? -1 : 1 : 0;	
			y += delta_y % 2 ? (delta_y - 1) % 4 ? -1 : 1 : 0;
			a[x][y] = k++;			
		}
		n -= m % 2;
		delta_x++;
		delta_y++;
	}
	for(i = 0; i < t; i++)
	{
		for(j = 0; j < t; j++)
		{
			printf("%d ", a[i][j]);
		}
		printf("\n");
	}
	return 0;
}