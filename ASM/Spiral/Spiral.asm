.686
.model flat, stdcall

include    ..\masm32\include\msvcrt.inc
includelib ..\masm32\lib\msvcrt.lib

.data
	a dd 10 dup(10 dup(0))
	nn dd 10
	n dd 10
	m dd 0
	k dd 0
	x dd 0
	y dd -1
	i dd 0
	delta_x dd 0
	delta_y dd 1
	format db "%d ", 0
	endl db 13, 10, 0

.code

main:
	start_make_line:
	cmp n, 0
	je end_make_line
		inc m
		mov i, 0
		start_set_next_element:
		mov EBX, n
		cmp i, EBX
		je end_set_next_element		
			inc k
			mov EAX, delta_x
			mov EBX, 2
			xor EDX, EDX			
			div EBX
			cmp EDX, 0
			je break
				mov EAX, delta_x
				sub EAX, 1
				mov EBX, 4
				xor EDX, EDX				
				div EBX
				cmp EDX, 0
				je mod_4
					dec x
					jmp break
				mod_4:
					inc x
			break:
			mov EAX, delta_y
			mov EBX, 2
			xor EDX, EDX			
			div EBX
			cmp EDX, 0
			je break_
				mov EAX, delta_y
				sub EAX, 1
				mov EBX, 4
				xor EDX, EDX				
				div EBX
				cmp EDX, 0
				je mod_4_
					dec y
					jmp break_
				mod_4_:
					inc y
			break_:
			mov EAX, x
			mul nn
			add EAX, y
			mov EDX, k
			mov a[EAX*4], EDX
		inc i
		jmp start_set_next_element
		end_set_next_element:
		inc delta_x
		inc delta_y	
		mov EAX, m
		xor EDX, EDX
		MOV ESI, 2
		div ESI
		sub n, EDX
	jmp start_make_line
	end_make_line:

	mov EBX, 0
	mov x, 0
	print_arr:
	mov EAX, nn
	cmp x, EAX
	je end_print_arr	
		mov y, 0
		print_line:
		mov EAX, nn
		cmp y, EAX
		je end_print_line
			invoke crt_printf, addr format, a[EBX]
			add EBX, 4
		inc y
		jmp print_line
		end_print_line:
	invoke crt_printf, addr endl
	inc x
	jmp print_arr
	end_print_arr:
	ret 0

end main

