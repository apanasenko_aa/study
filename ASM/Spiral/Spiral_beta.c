#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main()
{
	int n;
	scanf("%d", &n);
	int i, j;
	int start = 1, end = n * n;
	for (i = 0; i < (n + 1) / 2; ++i)
	{
		int x = 0;
		for(j = 0; j < i; j++)
		{
			x += 4 * (n - j * 2 - 1);
			printf("%d ", x - i + 1 + j);
			
		}
		for(j = 0; j < n - 2 * i; j ++)
		{
			printf("%d ", start + j);
		}
		for(j = i - 1; j >= 0; j--)			
		{
			printf("%d ", x - 3 * (n - j * 2 - 1) + i - j + 1);
			x -= 4 * (n - j * 2 - 1);
		}
		printf("\n");
		start += 4 * (n - i * 2 - 1);
	}
	for (i = 0 ; i < n / 2; i++)
	{
		int x = 0;
		for(j = 0; j < n / 2 - i; j ++)
		{
			x += 4 * (n - j * 2 - 1);
			printf("%d ", x - i - n / 2 + 1 + j - n % 2);
			
		}
		for(j = 0; j < 2 * i + n % 2; j ++)
		{
			printf("%d ", x - j - i * 2 - 1 - n % 2);
		}
		for(j = n / 2 - i - 1; j >= 0; j--)			
		{
			printf("%d ", x - 3 * (n - j * 2 - 1) + i + n / 2 - j + 1 + n % 2);
			x -= 4 * (n - j * 2 - 1);			
		}
		printf("\n");
		end -= 4 * (n - (i + n / 2) * 2 - 1);
	}
	return 0;
}