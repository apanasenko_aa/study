#include <cstdio>
#include <cmath>
#include <iostream>

using namespace std;

int main() {
	int n;
	cin >> n;
	double ans = 0.0;
	double t, h;
	int iter = 1000 * 1000;;
	long double pi = acos(-1.0);
	h = 1.0 / iter;
	for (int i = 0; i < iter; i++) {
		t = i * 1.0 / iter + 0.5 / iter;
		ans += pi * sqrt(1.0 + 4.0 * n * n * sin(pi * t) * sin(pi * t)) * h;
	}

	cout.precision(20);
	cout << ans << endl;

	return 0;
}