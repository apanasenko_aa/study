 .686
 .model flat, stdcall
 option casemap :none

include ..\masm32\include\kernel32.inc
include ..\masm32\include\masm32.inc
include ..\masm32\include\msvcrt.inc
includelib ..\masm32\lib\kernel32.lib
includelib ..\masm32\lib\masm32.lib
includelib ..\masm32\lib\msvcrt.lib

.data
	n dq 0
	i dd 0
	k dd 1000000
	h dq 0.000001
	t dq 0.0
	x05 dq 0.5
	x40 dq 4.0
	ans dq 0.0
	f_in  db "%d", 0
	f_out db "%.30g", 13, 10, 0
	
.code

main:
	invoke crt_scanf, addr f_in, addr n
	mov i, 0
	FOR_1:
	mov EAX, i
	cmp EAX, k
	jae END_FOR_1
		finit
		;t = i * 1.0 / h + 0.5 / h;
		fild i
		fadd x05
		fmul h
		fstp t		

		;ans += pi * sqrt(1.0 + 4.0 * n * n * sin(pi * t) * sin(pi * t)) * h; 
		
		fldpi 
		fmul t
		fsin

		fldpi 
		fmul t		
		fsin

		fmul

		fmul x40
		fild n
		fild n
		
		fmul
		fmul

		fld1
		fadd

		fsqrt
		
		fmul h
		fldpi
		
		fmul

		fadd ans
		fstp ans

	inc i
	jmp FOR_1
	END_FOR_1:
	invoke crt_printf, addr f_out, ans
	ret 0

end main
