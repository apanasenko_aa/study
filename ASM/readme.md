Язык Ассемблера
===============

Для большинства заданий в текущей директории должен лежать ассемблер MASM (http://www.masm32.com/masmdl.htm).

1. [Быстрое вычисление чисел Фибоначчи.](Fibonacci/)
	* Алгоритм: http://e-maxx.ru/algo/fibonacci_numbers#8
2. [Обход матрицы спиралью.](Spiral/)
	* Задача: http://imcs.dvfu.ru/cats/static/problem_text-cpid-500003.html
	* Spiral.c - наивная реализация
	* Spiral_beta.c - построчный вывод без цикла за O(n)
3. [Программа, использующая рекурсию.](Recursion/)
	* Quicksort: https://en.wikipedia.org/wiki/Quicksort
4. [Ввод числа с плавающей точкой.](Float/)
5. [Вычисления с плавающей точкой.](Float_calc/)
	* Задача: http://imcs.dvfu.ru/cats/static/problem_text-cpid-911712.html
6. [Выявление кэш-эффектов.](Cache/)
7. [SSE + волновое уравнение.](SSE/)
	* не доделано
