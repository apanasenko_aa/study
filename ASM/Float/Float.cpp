#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <math.h>
#include <cstring>

using namespace std;

string int_to_bin(string x)
{
	if (x == "0")
		return string("0");
	string result = "";
	while (x != "")
	{
		string temp = "";
		int mod = 0;
		for (int i = 0; i < x.size(); i++)
		{
			temp += (char)((x[i] - '0' + 10 * mod) / 2 + '0');
			mod = (x[i] - '0') % 2;
		}
		result = (char)(mod + '0') + result;
		x = temp.substr((int)(temp[0] == '0'), temp.size());
	}
	return result;
}

string float_to_bin(string x)
{
	if (x == "0")
		return string("0");
	string result = "";
	while ((result.size() - result.find_first_of("1")) < 23 || result.find_first_of("1") == -1 )
	{
		int i = result.size();
		int k = result.find_first_of("1");
		string temp = string(x.size() + 1, '0');
		int mod = 0;
		for (int i = x.size(); i > 0; i--)
		{
			int k = (x[i - 1] - '0') * 2 + mod;
			temp[i] = (char)(k % 10 + '0');
			mod = k / 10;
		}
		result += (char)(mod + '0');
		x = temp.substr(1, temp.size());
	}
	return result;
}

int main()
{
	while (1)
	{
		//FILE* in = fopen("input.txt", "r"); 
		//FILE *out = fopen("output.txt", "w");
		int e = 0; 
		string in;
		cin >> in;
		int sign = in[0] == '-';
		int point = in.find_first_of(".");
		string int_part = in.substr(sign, point);
		string float_part = point != -1 ? in.substr(point + 1, in.size()) : "0";
		string bin_int = int_to_bin(int_part); 
		string bin_float = float_to_bin(float_part);
		
		std::string mantissa = "";
		if (bin_int != "0")
		{
			e = bin_int.size() - 1 + 127;
			mantissa = bin_int.substr(1,bin_int.size()) + bin_float;
		}
		else if (float_part == "0")
		{
			e = 0;
			mantissa = "0";
		}
		else
		{
			e = 127 - bin_float.find_first_of("1") - 1;
			mantissa = bin_float.substr(bin_float.find_first_of("1") + 1, bin_float.size()) + bin_float;
		}
		mantissa += std::string(23, '0');
		string ans = sign ? "1" : "0";
		string qwerty = "";
		while (e)
		{
			qwerty = (char)(e % 10 +'0') + qwerty;
			e /= 10;
		}	
		string exp = int_to_bin(qwerty);
		ans += string(8 - exp.size(), '0') + exp + mantissa.substr(0, 23);
		unsigned int ans_ = 0;
		for (int i = 0; i < 32; ++i)
		{
			cout << ans[i];
			ans_ |= ((ans[i] - '0') << 31 - i);
			
		}
		cout <<endl;
		float b = 0;
		memcpy(&b, &ans_, 32);
		cout << b << endl;
	}
	return 0;
}