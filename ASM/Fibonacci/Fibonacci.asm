.686
.model flat, stdcall

include    ..\masm32\include\msvcrt.inc
includelib ..\masm32\lib\msvcrt.lib

.data
	n dword 0
	fin db "%d", 0
	f db "%d %d %d ",13, 10, 0
	a1 dd 0
	a2 dd 1
	a3 dd 1
	b1 dd 0
	b2 dd 1
	b3 dd 1
	
	mod_ dd 1000000000
.code

MUL_BY_MOD MACRO a, b
; result in EDX
	MOV EAX, a
	MUL b
	DIV mod_
ENDM

MUL_AND_ADD MACRO c1, c2, c3, c4, res
	MUL_BY_MOD c1, c2
	MOV res, EDX
    
	MUL_BY_MOD c3, c4
	ADD res, EDX
	MOV EAX, res
	XOR EDX, EDX
	DIV mod_
	MOV res, EDX
			
ENDM

SWAP MACRO a, b
	MOV EAX, b
	MOV a, EAX
ENDM

main:
	invoke crt_scanf, addr fin, addr n
	BSR ESI, n
	DEC ESI
	matrix_power:
		BT   n, ESI

		
		MUL_AND_ADD a1, a1, a2, a2, b1
		MUL_AND_ADD a1, a2, a2, a3, b2
		MUL_AND_ADD a2, a2, a3, a3, b3
		SWAP a1, b1
		SWAP a2, b2
		SWAP a3, b3

		BT   n, ESI
		JNC not_mul
			MOV EAX, a2
			MOV EBX, a2
			MOV a1, EBX
			MOV EBX, a3
			MOV a2, EBX                
			ADD a3, EAX
		not_mul:        
		DEC ESI
		CMP ESI, 0
	JNS matrix_power	
	invoke crt_printf, addr fin, a2
	ret 0

end main
