#include <stdio.h>
#include <stdlib.h>

void qSort(int start, int end, int a[])
{
	int l = start;
	int r = end;
	int m = a[(r + l) / 2];
	int k;
	do {
		for (;(a[l] < m); l++);
		for (;(a[r] > m); r--);
		if (l > r) 
			break;
		k = a[l];
		a[l] = a[r];
		a[r] = k;
		l++;
		r--;
	} while (l < r);
	if (r > start)
		qSort(start, r, a);
	if (l < end)
		qSort(l, end, a);
}

int main() 
{
	int a[10] = {1, 232, 245,757, 53, 353, 76, 356, 53, 121, 213, 745, 34, 32, 24, 67, 765, 33, 90, 288};
	int i, n = 20;
	qSort(0, n - 1, a);
	for (i = 0; i < n; i++) 
		printf("%d ", a[i]);
	return 0;
}
