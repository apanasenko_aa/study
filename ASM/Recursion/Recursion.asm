.686
.model flat, stdcall

include    ..\masm32\include\msvcrt.inc
includelib ..\masm32\lib\msvcrt.lib

.data
	a dd 1, 232, 245,757, 53, 353, 76, 356, 53, 121, 213, 745, 34, 32, 24, 67, 765, 33, 90, 288	
	n dd 20
	i dd 0
	m dd 0
	format db "%d ", 0
	debag  db "_%d %d %d_", 13, 10, 0
	endl db 13, 10, 0

.code
;6.10 6.116 6.120

qSort PROTO :DWORD, :DWORD

qSort PROC l:DWORD, r:DWORD
	;invoke crt_printf, addr debag, l, r, 0
	;mov i, ESP
	;invoke crt_printf, addr format, i
	push l
	push r
	mov EAX, r
	add EAX, l
	xor EDX , EDX
	mov EBX, 2
	div EBX
	push a[EAX*4]
	pop m 
	L1:
		FOR_1:
			mov EBX, l
			mov EAX, a[EBX*4]
			cmp EAX, m
			jae END_FOR_1
			inc l
			jmp FOR_1
		END_FOR_1:
		FOR_2:
			mov EBX, r
			mov EAX, a[EBX*4]
			cmp EAX, m
			jbe END_FOR_2
			dec r
			jmp FOR_2
		END_FOR_2:
		mov EAX, l
		cmp EAX, r
		ja  END_L1
		mov EBX, r
		push a[EBX*4]
		mov EBX, l
		push a[EBX*4]
		mov EBX, r
		pop a[EBX*4]
		mov EBX, l
		pop a[EBX*4]
		inc l
		dec r
		mov EAX, l
		cmp EAX, r
		jb  L1		
	END_L1:
	;invoke crt_printf, addr debag, l, r, 0
	mov EAX, r
	cmp EAX, DWORD ptr [ESP + 4]
	jbe L2 
		push r
		push DWORD ptr [ESP + 8]		
		call qSort
		pop EAX
		pop EAX
	L2:
	mov EAX, l
	cmp EAX, DWORD ptr [ESP]
	jae L3 
		push DWORD ptr [ESP]
		push l		
		call qSort
		pop EAX
		pop EAX
	L3:
	ret 0
qSort ENDP


main:
	mov EAX, n
	dec EAX
	push EAX
	push DWORD ptr 0
	;mov i, ESP
	;invoke crt_printf, addr format, i
	call qSort
	pop EAX
	pop EAX
	mov i, 0
	print_arr:
	mov EAX, n
	sub EAX, i 
	jz end_print_arr
		mov EBX, i	
		invoke crt_printf, addr format, a[EBX*4] 
	inc i
	jmp print_arr
	end_print_arr:
	ret 0

end main
