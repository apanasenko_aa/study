﻿class Vine:
    """
        Params of vine

        0  Alcohol
        1  Malic acid
        2  Ash
        3  Alcalinity of ash
        4  Magnesium
        5  Total phenols
        6  Flavanoids
        7  Nonflavanoid phenols
        8  Proanthocyanins
        9  Color intensity
        10 Hue
        11 OD280/OD315 of diluted wines
        12 Proline
    """
    def __init__(self, params: list, real_class: int):
        self.params = params
        self.out_params = params
        self.real_class = real_class
        self.find_class = 0

    def print(self):
        return 'Class: %d\nParams: %s\n' % (self.real_class, ', '.join(map(str, self.params)))

    def normalize(self, norm):
        return Vine(
            list(map(lambda x: self.params[x] / norm[x], range(len(norm)))),
            self.real_class
        )

    def __str__(self):
        return '%d (%.3f)' % (self.real_class, self.params[0])


class DataSet:
    _data = []
    _norm = []

    def __init__(self, file_name):
        for line in open(file_name):
            x = line.strip().split(',')
            self._data.append(Vine(list(map(float, x[1:])), int(x[0])))

    def set_norm(self):
        self._norm = []
        for i in range(len(self._data[0].params)):
            self._norm.append(sum(map(lambda x: x.params[i], self._data)) / len(self._data))

    def get_norm(self):
        return self._norm

    def normalize(self):
        if not self._norm:
            self.set_norm()
        self._data = list(map(lambda x: x.normalize(self._norm), self._data))

    def get_data(self):
        return self._data

    def get_classes(self):
        return set(map(lambda x: x.real_class, self.get_data()))


class Parzen:

    @staticmethod
    def analysis_params(data_set: DataSet, norm: [float]):

        for i in data_set.get_classes():
            t = list(filter(lambda x: x.real_class == i, training_set))
            n = len(t)
            result = []
            for j in range(len(t[0].params)):
                m = sum(map(lambda x: x.params[j] / norm[j], t)) / n
                d = sum(map(lambda x: pow(m - (x.params[j] / norm[j]), 2), t)) / n
                result.append('%.3f(%.3f)' % (m, d))

            print(' | '.join(result))

    @staticmethod
    def p(x1: Vine, x2: Vine):
        #params = [0, 5, 6, 7, 9]
        #params = [5, 6, 7]
        params = list(range(13))
        return pow(sum(map(lambda k: pow(x1.params[k] - x2.params[k], 2), params)), 0.5)

    @staticmethod
    def a(x: Vine, X: DataSet, h: float, y: [int]):
        result = 0
        res_class = 0
        for i in y:
            r = sum(map(lambda x_i: Parzen.K(Parzen.p(x, x_i) / h) * int(x_i.find_class == i), X))
            if r > result:
                result = r
                res_class = i

        return res_class

    @staticmethod
    def K(r):
        return 3/4 * (1 - pow(r, 2)) * int(abs(r) <= 1)

    @staticmethod
    def find_k(data_set: DataSet, min_k = 1):
        classes = data_set.get_classes()
        errors = []
        for x in data_set.get_data():
            errors.append([])
            data = data_set.get_data().copy()
            data.remove(x)
            data = sorted(data, key=lambda i: Parzen.p(i, x))
            for i in range(min_k, len(data) - 1):
                x.find_class = Parzen.a(x, data[:i], Parzen.p(x, data[i]), classes)
                errors[-1].append(int(x.find_class != x.real_class))
                x.find_class = x.real_class

        e = {}
        for i in range(len(errors[0])):
            e[i + min_k] = sum(map(lambda j: j[i], errors))

        best_k = 1
        for k in e:
            if e[best_k] > e[k]:
                best_k = k

        return best_k


if __name__ == '__main__':
    training_set = DataSet('vine.training_set')
    training_set.set_norm()
    training_set.normalize()

    k = Parzen.find_k(training_set)

    for i in training_set.get_data():
        i.find_class = i.real_class

    all_data = training_set.get_data()
    classes = training_set.get_classes()

    for line in open('vine.test_set'):
        x = line.strip().split(',')
        v = Vine(list(map(float, x[1:])), int(x[0])).normalize(training_set.get_norm())
        data = sorted(all_data, key=lambda i: Parzen.p(i, v))
        v.find_class = Parzen.a(v, data[:k], Parzen.p(v, data[k]), classes)
        all_data.append(v)

    errors = {}
    for i in training_set.get_classes():
        errors[i] = len(list(filter(lambda x: x.real_class != x.find_class and x.real_class == i, all_data)))
        print(str(errors[i] / len(all_data)))
