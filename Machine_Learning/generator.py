﻿import random as _random

if __name__ == "__main__":

    TRAINING_SET_PERCENT = 0.2

    with \
            open('vine.data', 'r') as f_data, \
            open('vine.training_set', 'w') as f_training, \
            open('vine.test_set', 'w') as f_test:

        for line in f_data:
            if _random.random() < TRAINING_SET_PERCENT:
                f_training.write(line)
            else:
                f_test.write(line)
