#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_N 1000
#define INFINITY 10000000


int d[MAX_N][MAX_N];


int min (int a, int b)
{
	return a > b ? b : a;
}


int main ()
{
	FILE* in = fopen("input.txt", "r");
	FILE* out = fopen("output.txt", "w");

	int N, M, u, v, w, i, j, k;
	fscanf(in, "%d %d", &N, &M);

	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
			d[i][j] = INFINITY;
		d[i][i] = 0;
	}

	for (i = 0; i < M; i++)
	{
		fscanf(in, "%d %d %d", &u, &v, &w);
		u--; v--;
		d[u][v] = w;
	}

	for (i = 0; i < N; i++)
		for (j = 0 ; j < N; j++)
			for (k = 0 ; k < N; k++)
				if (d[j][i] < INFINITY && d[i][k] < INFINITY)
					d[j][k] = min(d[j][k], d[j][i] + d[i][k]);

	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
			if (d[i][j] < INFINITY)
				fprintf(out, "%d ", d[i][j]);
			fprintf(out, "\n");
	}

	return 0;
}
