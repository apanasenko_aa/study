#include <stdio.h>

#define MAX_N 20

int n, sum;
int matrix[MAX_N][MAX_N];
int masks[MAX_N];


int check(int combination)
{
	int i, j, cur_sum = 0;
	for (i = 0; i < n; i++)
	{
		if (combination & masks[i])
		{
			for (j = 0; j < n; j++)
			{
				cur_sum += !(combination & masks[j]) ? matrix[i][j] : 0;
			}
		}
	}
	sum = cur_sum > sum ? cur_sum : sum;
}


int main()
{
	int i, j;
	FILE* in = fopen("input.txt", "r");
	FILE* out = fopen("output.txt", "w");
	fscanf(in, "%d", &n);
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			fscanf(in, "%d", &matrix[i][j]);
		}
		masks[i] = 1 << i;
	}
	for (i = 0; i < (1 << n); i++)
	{
		check(i);
	}
	fprintf(out, "%d", sum);
	return 0;
}
