#include <stdio.h>
#include <string.h>

#define COUNT_CHAR 26
#define MAX_COUNT_CAP 10
#define MAX_LEN_CAP 20
#define NOT_USED_LINE 100

char str[MAX_COUNT_CAP + 1][MAX_LEN_CAP + 1];
int n, sum;
int used_char[COUNT_CHAR];
int orders[MAX_COUNT_CAP];
int result[MAX_COUNT_CAP];


void check()
{
	int i;
	sum = 0;
	for (i = 0; i < n; i++)
	{
		sum += orders[i];
		result[i] = orders[i];
	}

}


void findComb(int d, int max_inx, int cur_sum)
{
	if (d == n)
	{
		check();
		return;
	}
	int i, inx;
	for (i = 0; i < max_inx; i++)
	{
		if (cur_sum + i >= sum)
		{
			return;
		}
		if (!used_char[str[d][i] - 'a'])
		{
			used_char[str[d][i] - 'a'] = 1;
			orders[d] = i;
			inx = max_inx < strlen(str[d + 1]) && i != strlen(str[d]) - 1 ? max_inx : strlen(str[d + 1]);
			findComb(d + 1, inx, cur_sum + i);
			used_char[str[d][i] - 'a'] = 0;
		}
	}
	if (cur_sum + NOT_USED_LINE < sum)
	{
		orders[d] = NOT_USED_LINE;
		findComb(d + 1, strlen(str[d + 1]), cur_sum + NOT_USED_LINE);
	}
}


int main()
{
	int i, j;
	sum = 1000;

	FILE* in = fopen("input.txt", "r");
	FILE* out = fopen("output.txt", "w");
	fscanf(in, "%d", &n);
	for (i = 0; i < n; i++)
	{
		fscanf(in, "%s", str[i]);
	}
	findComb(0, strlen(str[0]), 0);

	for (i = 0; i < n; i++)
	{
		if (result[i] != NOT_USED_LINE)
		{
			for (j = strlen(str[i]); j >= result[i]; j--)
			{
				str[i][j + 1] = str[i][j];
			}
			str[i][j + 1] = '&';
		}
		fprintf(out, "%s\n", str[i]);
	}
	return 0;
}
