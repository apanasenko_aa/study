#include <stdio.h>
#include <string.h>

#define COUNT_LETTER 6
#define MAX_COUNT_CUBE 12
#define MAX_COUNT_CHAR 128

char str[MAX_COUNT_CUBE][COUNT_LETTER + 1];
char word[MAX_COUNT_CUBE + 1];
int orders[MAX_COUNT_CUBE + 1];
int need_letter[MAX_COUNT_CHAR];
int n, count_need_litter = 0;


int findComb(int d)
{
	if (d == n)
	{
		return 0;
	}
	int i, j;
	char ch;
	for (i = 0; i < COUNT_LETTER; i++)
	{
		j = 0;
		ch = str[d][i];
		if (ch != '.' && need_letter[ch])
		{
			--need_letter[ch];
			--count_need_litter;
			while (ch != word[j]) j++;
			orders[j] = d + 1;
			word[j] = '.';
			if (!count_need_litter || findComb(d + 1))
			{
				return 1;
			}
			orders[j] = 0;
			word[j] = ch;
			++need_letter[ch];
			++count_need_litter;
		}
	}
	return findComb(d + 1);
}


int main()
{
	int i, j, k;
	FILE* in = fopen("input.txt", "r");
	FILE* out = fopen("output.txt", "w");
	fscanf(in, "%d", &n);
	fscanf(in, "%s", word);
	for (i = 0; i < strlen(word); i++)
	{
		++need_letter[word[i]];
		++count_need_litter;
	}
	for (i = 0; i < n; i++)
	{
		fscanf(in, "%s", str[i]);
		for (j = 0; j < COUNT_LETTER; j++)    /**  delete duplicate letters  **/
		{
			for (k = j + 1; k < COUNT_LETTER; k++)
			{
				if (str[i][k] == str[i][j])
				{
					str[i][k] = '.';
				}
			}
		}
	}
	i = 0;
	findComb(0);
	do
	{
		fprintf(out, "%d ", orders[i]);
		i++;
	} while (orders[i] != 0);
	return 0;
}
