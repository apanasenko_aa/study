#include <stdio.h>
#include <string.h>

#define MAX_COUNT_MEN 1000

int p, q;
int answers[MAX_COUNT_MEN];
int results_men[MAX_COUNT_MEN];


int check(int solution)
{
	int ans, def, result = 1;
	int j, i = 0;
	while (i != p && result)
	{
		def = solution ^ answers[i];
		ans = q;
		for (j = 0; j < q; j++)
		{
			ans -= (def >> j) & 1;
		}
		result &= ans == results_men[i];
		i++;
	}
	return result;
}


int main()
{
	int i, j, result;
	char str[20];
	FILE* in = fopen("input.txt", "r");
	FILE* out = fopen("output.txt", "w");
	fscanf(in, "%d %d", &p, &q);
	for (i = 0; i < p; i++)
	{
		fscanf(in, "%s %d", str, &results_men[i]);
		for (j = q; j > 0; j--)
		{
			answers[i] = (answers[i] << 1) + (str[j - 1] == '+' ? 1 : 0);
		}
	}
	while (!check(result))
	{
		result++;
	}
	for (i = 0; i < q; i++)
	{
		fprintf(out, "%c", (result >> i & 1) == 1 ? '+' : '-');
	}
	return 0;
}
