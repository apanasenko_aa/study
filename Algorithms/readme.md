﻿* [Динамическое программирование I](Students_DynProg_I_19.04.2013/) ([Турнир](http://imcs.dvfu.ru/cats/main.pl?f=problems;sid=;cid=896577))
	* [Свинья-копилка](Students_DynProg_I_19.04.2013/A.c) ([Задача](http://imcs.dvfu.ru/cats/static/problem_text-cpid-896583.html))
	* [Ford-Bellman](Students_DynProg_I_19.04.2013/B.c) ([Задача](http://imcs.dvfu.ru/cats/static/problem_text-cpid-896584.html))
	Кратчайшие пути от одной вершины графа до всех остальных.
	* [Knapsack problem](Students_DynProg_I_19.04.2013/C.c) ([Задача](http://imcs.dvfu.ru/cats/static/problem_text-cpid-896585.html))
	Задача о рюкзаке.

* [Динамическое программирование II](Students_DynProg_II_03.05.2013/) ([Турнир](http://imcs.dvfu.ru/cats/main.pl?f=problems;sid=;cid=899950))
	* [Floyd-Warshall](Students_DynProg_II_03.05.2013/A.c) ([Задача](http://imcs.dvfu.ru/cats/static/problem_text-cpid-899956.html))
	Кратчайшие пути между всеми вершинами.

* [Графы I](Students_Graphs_I_29.03.2013/) ([Турнир](http://imcs.dvfu.ru/cats/main.pl?f=problems;sid=;cid=894115))
	* [Bipartite graph](Students_Graphs_I_29.03.2013/A_Bipartite_graph.c) ([Задача](http://imcs.dvfu.ru/cats/static/problem_text-cpid-894121.html))
	Проверка графа на двудольность.
	* [Biconnectivity](Students_Graphs_I_29.03.2013/B_Biconnectivity.cpp) ([Задача](http://imcs.dvfu.ru/cats/static/problem_text-cpid-894122.html))
	* [Бюрократия](Students_Graphs_I_29.03.2013/C_Bureaucracy.c) ([Задача](http://imcs.dvfu.ru/cats/static/problem_text-cpid-894123.html))
	* [Breadth First Search](Students_Graphs_I_29.03.2013/D_BFS.c) ([Задача](http://imcs.dvfu.ru/cats/static/problem_text-cpid-894124.html))

* [Графы II](Students_Graphs_II_and_Strings_25.05.2013/) ([Турнир](http://imcs.dvfu.ru/cats/main.pl?f=problems;sid=;cid=901970))
	* [Dijkstra](Students_Graphs_II_and_Strings_25.05.2013/A.cpp) ([Задача](http://imcs.dvfu.ru/cats/static/problem_text-cpid-901976.html))

* [Перебор](Students_Enums_01.03.2013/) ([Турнир](http://imcs.dvfu.ru/cats/main.pl?f=problems;sid=;cid=891432))
	* [Network Saboteur](Students_Enums_01.03.2013/NET.c) ([Задача](http://imcs.dvfu.ru/cats/static/problem_text-cpid-891438.html))
	* [Слово из кубиков](Students_Enums_01.03.2013/word.c) ([Задача](http://imcs.dvfu.ru/cats/static/problem_text-cpid-891439.html))
	* [Ответы к тесту](Students_Enums_01.03.2013/test.c) ([Задача](http://imcs.dvfu.ru/cats/static/problem_text-cpid-891440.html))
	* [Hot-keys](Students_Enums_01.03.2013/hot_keys.c) ([Задача](http://imcs.dvfu.ru/cats/static/problem_text-cpid-891441.html))
