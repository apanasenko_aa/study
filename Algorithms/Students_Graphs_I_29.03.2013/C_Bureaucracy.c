#pragma comment(linker, "/STACK:8000000")
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define N 100000

typedef struct Edge {
	int adjacent_vertex;
	struct Edge* next;
} Edge;

int iAns = 0;
int is_solved = 1;
int need_vertex[N];
int received[N];
int result[N];
int seq_number[N];
Edge* adjacency_matrix[N];


void Add_edge (int a, int b)
{
	Edge* new_edge = (Edge*) malloc(sizeof(Edge));
	if (!new_edge)
	{
		return; //bo-bo
	}
	new_edge->adjacent_vertex = a;
	new_edge->next = adjacency_matrix[b];
	adjacency_matrix[b] = new_edge;
	need_vertex[b]++;
}


void Search_Free_Vertex (int iVer, int d)
{
	if (seq_number[iVer] && seq_number[iVer] <= d)
	{
		is_solved = 0;
		return;
	}
	seq_number[iVer] = d;
	if (adjacency_matrix[iVer]){
		Edge* e = adjacency_matrix[iVer];
		do {
			if (!received[e->adjacent_vertex])
			{
				Search_Free_Vertex(e->adjacent_vertex, d + 1);
			}
			e = e->next;
		} while (e && is_solved);
	}
	result[iAns++] = iVer + 1;
	received[iVer] = 1;
}


int main ()
{
	FILE* in = fopen("input.txt", "r");
	FILE* out = fopen("output.txt", "w");

	int n, m, a, b, i, j;
	fscanf(in, "%d %d", &n, &m);
	for (i = 0; i < m; i++)
	{
		fscanf(in, "%d %d", &a, &b);
		a--; b--;
		Add_edge(a, b);
	}

	for (i = 0; i < n && is_solved; i++)
	{
		if (!received[i])
		{
			Search_Free_Vertex(i, 1);
		}
	}
	if (is_solved)
	{
		for (i = 0; i < n; i++)
		{
			fprintf(out, "%d ", result[i]);
		}
	} else {
		fprintf(out, "-1");
	}
	return 0;
}
