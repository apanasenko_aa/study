
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define N 100000


typedef struct Edge {
	int adjacent_vertex;
	struct Edge* next;
} Edge;


int result[N];
int is_used[N];
int count;
Edge* adjacency_matrix[N];


void add_edge (int a, int b)
{
	Edge* new_edge = (Edge*) malloc(sizeof(Edge));
	if (!new_edge)
	{
		return; //bo-bo
	}
	new_edge->adjacent_vertex = b;
	new_edge->next = adjacency_matrix[a];
	adjacency_matrix[a] = new_edge;
}


int bfs(int first_v, int n)
{
	int turn[N];
	turn[0] = first_v;
	is_used[first_v] = 1;
	int i_first = 0;
	int i_last = 1;
	while (i_first != i_last)
	{
		result[count++] = turn[i_first] + 1;
		Edge* e = adjacency_matrix[turn[i_first]];
		while (e)
		{
			if (!is_used[e->adjacent_vertex])
			{
				is_used[e->adjacent_vertex] = 1;
				turn[i_last++] = e->adjacent_vertex;
			}
			e = e->next;
		}
		i_first++;
	}
	//printf("%d\n", i_first);
	return (i_first == n);
}


int main ()
{
	FILE* in = fopen("input.txt", "r");
	FILE* out = fopen("output.txt", "w");

	int n, m, s, a, b, i;
	fscanf(in, "%d %d %d", &n, &m, &s);
	for (i = 0; i < m; i++)
	{
		fscanf(in, "%d %d", &a, &b);
		a--; b--;
		if (a == b) continue;
		add_edge(a, b);
		add_edge(b, a);
	}
	if (bfs(s - 1, n))
	{
		for (i = 0; i < count; i++)
		{
			fprintf(out, "%d ", result[i]);
		}
	} else {
		fprintf(out, "-1");
	}
	return 0;
}
