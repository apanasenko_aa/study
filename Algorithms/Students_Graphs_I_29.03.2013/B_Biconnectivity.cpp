
#include <cstdio>
#include <cstdlib>
#include <cstring>
#pragma comment(linker, "/STACK:8000000")

#define N 100000

typedef struct Edge {
	int adjacent_vertex;
	struct Edge* next;
} Edge;

int result[N];
int is_used[N];
int depths[N];
int is_cutvertex[N];
int count_cutvertex;
Edge* adjacency_matrix[N];


int min (int a, int b)
{
	return a > b ? b : a;
}


void add_edge (int a, int b)
{
	Edge* new_edge = (Edge*) malloc(sizeof(Edge));
	if (!new_edge)
	{
		return; //bo-bo
	}
	new_edge->adjacent_vertex = b;
	new_edge->next = adjacency_matrix[a];
	adjacency_matrix[a] = new_edge;
}


int dfs(int v, int d)
{
	depths[v] = d;
	int count_child = 0;
	int min_find_d = N;
	Edge* e = adjacency_matrix[v];
	do {
		int child = e->adjacent_vertex;
		int min_child_d;
		if (!depths[child])
		{
			count_child++;
			min_child_d = dfs(child, d + 1);
			if (min_child_d >= d && !is_cutvertex[v] && d > 1)
			{
				result[count_cutvertex++] = v + 1;
				is_cutvertex[v] = 1;
			}
		} else {
			min_child_d = depths[child];
		}
		min_find_d = min(min_find_d, min_child_d);
		e = e->next;
	} while (e);
	if (d == 1 && count_child > 1)
	{
		result[count_cutvertex++] = v + 1;
		is_cutvertex[v] = 1;
	}
	return min_find_d;
}


int main ()
{
	FILE* in = fopen("input.txt", "r");
	FILE* out = fopen("output.txt", "w");

	int n, m, a, b, i;
	fscanf(in, "%d %d", &n, &m);
	for (i = 0; i < m; i++)
	{
		fscanf(in, "%d %d", &a, &b);
		a--; b--;
		if (a == b) continue;
		add_edge(a, b);
		add_edge(b, a);
	}
	dfs(0, 1);
	fprintf(out, "%d ", count_cutvertex);
	for (i = 0; i < count_cutvertex; i++)
	{
		fprintf(out, "%d ", result[i]);
	}
	return 0;
}
