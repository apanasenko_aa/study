#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define N 100000

typedef struct Edge {
	int adjacent_vertex;
	struct Edge* next;
} Edge;

int count_edge[N];
int ver_in_set[N];                //vertices belonging to the set
Edge* adjacency_matrix[N];
int is_graph_bipartite = 1;


void add_edge (int a, int b)
{
	Edge* new_edge = (Edge*) malloc(sizeof(Edge));
	if (!new_edge || a == b)
	{
		return; //bo-bo
	}
	new_edge->adjacent_vertex = b;
	new_edge->next = count_edge[a] ? adjacency_matrix[a] : NULL;
	adjacency_matrix[a] = new_edge;
	count_edge[a]++;
}

int split_vertices (int setNo, int iVer)
{
	if (ver_in_set[iVer])
	{
		is_graph_bipartite &= ver_in_set[iVer] == setNo;
		return 0;
	}
	ver_in_set[iVer] = setNo;
	if (count_edge[iVer] && is_graph_bipartite){
		Edge* e = adjacency_matrix[iVer];
		do {
			split_vertices(setNo == 1 ? 2 : 1, e->adjacent_vertex);
			e = e->next;
		} while (e && is_graph_bipartite);
	}
}


int main ()
{
	FILE* in = fopen("input.txt", "r");
	FILE* out = fopen("output.txt", "w");

	int n, m, a, b, i, j;
	fscanf(in, "%d %d", &n, &m);
	for (i = 0; i < m; i++)
	{
		fscanf(in, "%d %d", &a, &b);
		a--; b--;
		add_edge(a, b);
		add_edge(b, a);
	}
	for (i = 0; i < n && is_graph_bipartite; i++)
	{
		if (!ver_in_set[i])
		{
			split_vertices(1, i);
		}
	}
	fprintf(out, "%s", is_graph_bipartite && n > 1 ? "BIPARTITE" : "NO");
	return 0;
}
