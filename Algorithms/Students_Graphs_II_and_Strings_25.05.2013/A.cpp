#include <ioctream>
#include <queue>
#include <vector>
#include <cstdio>
#include <string>
#include <algorithm>


using namespace std;


#define INFINITY 1000000
#define MAX_N 1000

struct edge
{
	int start;
	int end;
	int length
};

int adjacency_matrix[MAX_N][MAX_N];
int used[MAX_N];

int main() {

	FILE* in = fopen("input.txt", "r");
	FILE* out = fopen("output.txt", "w");

	int N, M, s, u, v, w, i, j, k;
	fscanf(in, "%d %d %d", &N, &M, &s);

	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
			adjacency_matrix[i][j] = INFINITY;
		adjacency_matrix[i][i] = 0;
	}

	int n;

	vector < vector < pair<int,int> > > g (n);

	int s = ...;

	vector<int> d (n, INF),  p (n);
	d[s] = 0;
	vector<char> u (n);
	for (int i=0; i<n; ++i) {
		int v = -1;
		for (int j=0; j<n; ++j)
			if (!u[j] && (v == -1 || d[j] < d[v]))
				v = j;
		if (d[v] == INF)
			break;
		u[v] = true;

		for (size_t j=0; j<g[v].size(); ++j) {
			int to = g[v][j].first,
				len = g[v][j].second;
			if (d[v] + len < d[to]) {
				d[to] = d[v] + len;
				p[to] = v;
			}
		}
	}
}
