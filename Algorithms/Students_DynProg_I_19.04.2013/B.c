#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_N 1000
#define INFINITY 10000000


typedef struct Edge
{
	int u;
	int v;
	int w;
} Edge;


Edge edges[MAX_N];
int result[MAX_N];


int min (int a, int b)
{
	return a > b ? b : a;
}


int main ()
{
	FILE* in = fopen("input.txt", "r");
	FILE* out = fopen("output.txt", "w");

	int N, M, S, u, v, w, i, j;
	fscanf(in, "%d %d %d", &N, &M, &S);
	S--;

	for (i = 0; i < N; i++)
		result[i] = INFINITY;

	for (i = 0; i < M; i++)
	{
		fscanf(in, "%d %d %d", &u, &v, &w);
		u--; v--;
		edges[i].u = u;
		edges[i].v = v;
		edges[i].w = w;
	}

	result[S] = 0;
	for (i = 0; i < N - 1; i++)
		for (j = 0 ; j < M; j++)
			if (result[edges[j].u] < INFINITY)
				result[edges[j].v] = min(result[edges[j].v], result[edges[j].u] + edges[j].w);

	for (i = 0; i < N; i++)
		if (result[i] < INFINITY)
			fprintf(out, "%d ", result[i]);

	return 0;
}
