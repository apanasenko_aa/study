#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_N 40
#define MAX_SUM 10000010

int a[MAX_N];
int cur_sum[MAX_SUM];
int result[MAX_N];


int min (int a, int b)
{
	return a > b ? b : a;
}


int main ()
{
	FILE* in = fopen("input.txt", "r");
	FILE* out = fopen("output.txt", "w");

	int N, w, i, j;
	fscanf(in, "%d %d", &N, &w);

	for (i = 0; i < N; i++)
	{
		fscanf(in, "%d ", &a[i]);
	}

	cur_sum[0] = -1;

	for (i = 0; i < N; i++)
	{
		for (j = w - a[i] ; j >= 0; j--)
		{
			if (cur_sum[j] != 0 && cur_sum[j + a[i]] == 0)
			{
				cur_sum[j + a[i]] = i + 1;
			}
		}
	}

	if (cur_sum[w])
	{
		i = w;
		while (i)
		{
			result[cur_sum[i] - 1] = 1;
			i -= a[cur_sum[i] - 1];
		}
		for (i = 0; i < N; i++)
		{
			fprintf(out, "%d ", result[i]);
		}
	} else {
		fprintf(out, "-1");
	}
	return 0;
}
