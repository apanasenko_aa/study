#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_N 100
#define MAX_WEIGHT 10000

int C[MAX_N];
int W[MAX_N];
int min_sum[MAX_WEIGHT];


int min (int a, int b)
{
	return a > b ? b : a;
}


int main ()
{
	FILE* in = fopen("input.txt", "r");
	FILE* out = fopen("output.txt", "w");

	int E, F, N, i, j;
	fscanf(in, "%d %d %d", &E, &F, &N);
	int k = F - E;

	for (i = 0; i < N; i++)
		fscanf(in, "%d %d", &C[i], &W[i]);

	min_sum[0] = 1;

	for (i = 0; i < N; i++)
		for (j = 0; j <= k; j++)
			if (j <= k - W[i] && min_sum[j])
				min_sum[j + W[i]] = min_sum[j + W[i]] ? min(min_sum[j + W[i]], min_sum[j] + C[i]) : min_sum[j] + C[i];

	fprintf(out, "%d", min_sum[k] - 1);
	return 0;
}
