use strict;

open STDIN, '<input.txt';
open STDOUT, '>output.txt';

my $n = <>;
my %aquarium;
for (1..$n) {
    my $m = <>;
    my %piece;
    for (1..$m) {
        my $s = <>;
        chomp $s;
        $piece{$s}++
    }
    foreach my $fish ( keys %piece ) {
        $aquarium{$fish} = $piece{$fish} if ($aquarium{$fish} < $piece{$fish})
    }
}
my $sum = 0;
$sum += $fish for my $fish ( values %aquarium );
print $sum;
