use strict;

open STDIN, '<input.txt';
open STDOUT, '>output.txt';
sub readln { my $s = <>; chomp $s; $s; }

my $n_subjects = <>;
my %lib; 
for (1..$n_subjects) {
    my $subject = readln();
    my $n_books = <>;
    for (1..$n_books) {
        my $book = readln();
        my $n_topics = <>;
        for (1..$n_topics) {
            my $topic = readln();
            push @{$lib{$topic}->{$subject}}, $book;
        }
    }
}
$, = "\n";
my $n_needs = <>;
while (my $need = readln()) {
    foreach my $sub_key ( keys %{$lib{$need}} ){
        print "Topic: $need","Subject: $sub_key", "Books:", @{$lib{$need}->{$sub_key}};
        print "\n";
    }
}
