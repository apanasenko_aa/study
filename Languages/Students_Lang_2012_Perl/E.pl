use strict;

open STDIN, '<input.txt';
open STDOUT, '>output.txt';

my $n = <>;
my @values = split(' ', <>);
my $max = 0;
map {$max = $max > $_ ? $max : $_} @values;
for my $i (reverse 0..$max + 1) {
    print map(['.+---+','......', '.|###|' ]->[$i <=> $_], @values), ".\n";
}
print '-', '+---+-' x $n;
