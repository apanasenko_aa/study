use strict;

open STDIN, '<format.in';
open STDOUT, '>format.out';

sub readln { my $s = <>; chomp $s; $s;  }
my @s = split('', <>);
my @str;
my @max;
my $i = 0;
while (<>) {
    chomp;
    @{$str[$i]} = split('&', $_);
    for my $j (0..(@s - 2)) {
        my $len = length $str[$i]->[$j];
        $max[$j] = $max[$j] > $len ? $max[$j] : $len;
    }
    $i++;
}
for my $i (0..(@str - 1)) {
    for my $j (0..(@s - 2)) {
        my $cur_s = $str[$i]->[$j];
        my $length = $max[$j] - length $cur_s;
        if ($s[$j] eq 'l') {
            $cur_s = '| ' . $cur_s . ' ' x $length . ' ';
        }
        elsif ($s[$j] eq 'r') {
            $cur_s = '| ' . ' ' x $length . $cur_s . ' ';
        }
        else {
            $cur_s = ' ' x ($length / 2) . $cur_s . ' ' x ($length / 2);
            $cur_s = length $cur_s == $max[$j] ? '| ' . $cur_s . ' ' : '| ' . $cur_s . '  ';
        }
        $str[$i]->[$j] = $cur_s;
    }
}
print map( '+' . '-' x ($_ + 2), @max), "+\n";
print @{$str[0]},"|\n";
print map( '+' . '-' x ($_ + 2), @max), "+\n";
for $i (1..(@str - 1)) {
    print @{$str[$i]},"|\n"}
print map( '+' . '-' x ($_ + 2), @max), "+";
