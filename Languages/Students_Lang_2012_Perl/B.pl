open STDIN, '<input.txt';
open STDOUT, '>output.txt';
my $dec1 = "//";
my $dec2 = "/*";
my $dec3 = "*/";
my $i = 0;
my $tmp = '';
my $v = 1;
while (<>) {
    my $s = $_;
    START:
    if ($s =~ m[\/\*]) {
        my $idec1 = index ($s, $dec1);
        my $idec2 = index ($s, $dec2);
        if (((index ($s, $dec1) > index ($s, $dec2)) || (index ($s, $dec1) == -1)) && ($v == 1)) {
            $v = 0;
            print substr($s,0, index ($s, $dec2) + 2);
            $s = substr($s, index ($s, $dec2) + 2);
        }
    }
    if (($v == 0) && (index ($s, $dec3) == -1)) {
        print $s;
        next;
    }
    if (($v == 0) && (index ($s, $dec3) != -1)) {
        print substr($s,0,index($s,$dec3) + 2);
        $v = 1;
        $s = substr($s, index($s,$dec3) + 2);
        goto START;
    }
    if ((($i = index($s, $dec1)) != -1 ) && ($v == 1)) {
        $tmp = substr($s, 0, $i);
        $s = substr($s, $i);
        $s =~s[\*\/][\* \/]g;
        $s =~s[\/\/][\/\*];
        $s =~s[\n][\*\/\n];
        $s .= "*/" if (index($s, "\n") == -1);
        $s = $tmp . $s;
    }
    print $s;
}
