use strict;

open STDIN, '<input.txt';
open STDOUT, '>output.txt';

my $n = <>;
my %files;
for (1..$n) {
    my $name = <>;
    $name =~ s/\s*//g;
    my $i = '';
    my $first = 1;
    while ($name) {
        if ($files{(lc $name) . $i} == undef) {
            $files{lc $name} = $i;                  # максимальный индекс для этого имени
            $files{(lc $name) . $i} = 1;
            print ($name . $i);
            print "\n";
            $name = 0;}
        else {
            $i = $files{lc $name} - 1 if ($first);   # для уменьшения кол-ва повторений цикла
            $first = 0;
            $i++
        }
    }
}
