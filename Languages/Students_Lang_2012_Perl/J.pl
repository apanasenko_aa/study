#use strict;
open STDIN, '<input.txt';
open STDOUT, '>output.txt';

sub readln { my $s = <>; chomp $s; $s; }

my ($n, $m) = split ' ', <>;
my @arr;
push (@{$arr[0]}, '.') for (0..($m + 1));
@{$arr[$_]} = ('.', split ('', readln()), '.') for (1..$n);
push (@{$arr[$n + 1]}, '.') for (0..($m + 1));
my $str = 'GOOD';
my %valency = ('H'=> 1, 'O'=> 2, 'C'=> 4);
my %cof = ("\\" => [-1, -1, 1,  1], 
           "\|" => [-1,  0, 1,  0],
           "\/" => [-1,  1, 1, -1],
           "\-" => [ 0, -1, 0,  1]);
my $p = 0;
my $fst = '';
for my $i (1..$n) {
    for my $j (1..$m) {
        if (index("\\\/\-\|", $arr[$i][$j]) > -1) {
            my $curstr = 'CHO' . $arr[$i][$j];
            unless ((index ($curstr, cur($i, 0, $j, 1, $arr[$i][$j])) > -1)
                and (index ($curstr, cur($i, 2, $j, 3, $arr[$i][$j])) > -1)){ 
                $str = 'BAD';
                last;
            }
        }
        if (index("CHO", $arr[$i][$j]) > -1) {
            $fst = "$i $j";
            my $k = 0;
            foreach my $key ( keys %cof )  {
                $k++ if (cur($i, 0, $j, 1, $key) eq $key);
                $k++ if (cur($i, 2, $j, 3, $key) eq $key);
            }
            unless ($k == $valency{$arr[$i][$j]}) {
                $str = 'VALENCY';
                $str = 'BAD' if (($k == 0) && ($p != 0));
                }
                $p++;
            }
        last if ($str eq 'BAD');
        }
    last if ($str eq 'BAD');
}
$str = 'BAD' if ($p == 0);
unless ($str eq 'BAD') {
    my ($i, $j) = split (' ', $fst);
    only_one($i, $j);
    for $i (0..$n+1) {
        for $j (0..$m+1) {
            $str = 'BAD'unless $arr[$i][$j] eq '.';
        }
    }
}
print $str;

sub only_one {
    local ($i, $j) = @_;
    $arr[$i][$j] = '.';
    local $s = "CHO\\\/\-\|";
    foreach my $key ( keys %cof )  {
        only_one($i + $cof{$key}->[0], $j + $cof{$key}->[1]) if (index ($s, cur($i, 0, $j, 1, $key)) > -1);
        only_one($i + $cof{$key}->[2], $j + $cof{$key}->[3]) if (index ($s, cur($i, 2, $j, 3, $key)) > -1);
    }
}

sub cur {$arr[$_[0] + $cof{$_[4]}->[$_[1]]][$_[2] + $cof{$_[4]}->[$_[3]]];}
