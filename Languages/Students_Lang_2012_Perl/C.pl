#use strict;
#use warning;

open STDIN, '<format.in';
open STDOUT, '>format.out';

$/ = " ";
my $n = <>;
$/ = "\n";
my $m = <>;
$/ = undef;
my $text = <>;
$n1 = $n;
$n++;
my $tmp = '';
my $i = 0;
my $s = '';

$s .= ' ' while ($i++ < $m);

$text =~ s[^(\s*)][];
$text =~ s[(\s*)$][];
$text =~ s[\n+\s*\n+][\t]g;
$text =~ s[\n][ ]g;
$text =~ s/\s*\t+\s*/\t/g;
$text =~ s/ +/ /g;
$text =~ s/ *([,.?!\-:']) */$1 /g;
$text =~ s/ +([,.?!\-:'])/$1/g;

$text =~ s[ *\t][\n$s]g;
$text = $s . $text . "\t";

while (index($text, "\t") > $n1) {
    $s = substr($text, 0, $n);
    $i = rindex($s, "\n");
    $i = rindex($s, " ") if ($i < 0); 
    $tmp .= substr($text, 0, $i);
    $tmp .= "\n";
    $text = substr($text, ++$i)}
$text =~ s[(\s*)$][];
$tmp .= $text;
$tmp =~ s[\t][];
print $tmp;
