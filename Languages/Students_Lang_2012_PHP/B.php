<?php
    $outFile = fopen('output.txt', 'w');
    $lines = file("input.txt");
    list($n, $p) = split(' ',trim($lines[0]));
    for ($i = 1; $i <= $n; $i++){
        $arr = split(' ',trim($lines[$i]));
        $cur_min = min($arr);
        $cur_max = max($arr);
        for ($j = 0; $j < $p; $j++){
            $ans[$j] -= $arr[$j] == $cur_max ? 500 : 0;
            $ans[$j] += $arr[$j] == $cur_min ? 1 : 0;
        }
    }
    $str_ans = '';
    ksort ($ans);
    foreach($ans as $key => $val){
        $str_ans .= $val * 2 > $n ? ($key + 1) . ' ' : '' ;
    }
    $str_ans = $str_ans == '' ? 0 : $str_ans;
    fwrite($outFile, trim($str_ans));
    fclose($outFile);
?>
