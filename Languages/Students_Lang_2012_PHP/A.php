<?php
    $outFile = fopen('output.txt', 'w');
    $lines = file("input.txt");
    $n += $lines[0];
    $ans_ar = array();
    for ($i = 1; $i <= $n; $i++){
        $s = count_chars(trim($lines[$i]), 3);
        if (strlen($s) == 1)
            array_push($ans_ar, $s);
        $cur_col = '';
        for($j = 1; $j <= $n; $j++) $cur_col .= $lines[$j]{$i - 1};
        $s = count_chars(trim($cur_col), 3);
        if (strlen($s) == 1)
            array_push($ans_ar, $s);
    }
    sort ($ans_ar);
    $ans = count($ans_ar) ? join('',array_unique($ans_ar)) : 'NO';
    fwrite($outFile, trim($ans));
    fclose($outFile);
?>
