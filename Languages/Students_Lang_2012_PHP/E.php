<?php
    $outFile = fopen('output.txt', 'w');
    $lines = file("input.txt");
    list($k, $n) = split(' ',trim($lines[0]));
    #$text = preg_replace("(^\*+)", "", trim($lines[1]));
    #$text = preg_replace("(\*+$)", "", $text);
    $text = preg_replace("(\*)", "(.*?)", trim($lines[1]));
    $ans_ar = array();
    for ($i = 0; $i < $n; $i++){
        preg_match("<$text>", trim($lines[2]), $out, 0, $i);
        if (count($out[0]) == 0) break;
        array_push($ans_ar, $out[0]);
    }
    $ans = count($ans_ar) ? min(array_map(strlen, $ans_ar)) : -1;
    fwrite($outFile, $ans);
    fclose($outFile);
?>
