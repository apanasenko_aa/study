inFile = open('input.txt', 'r')
outFile = open('output.txt', 'w')
n = inFile.readline.to_i
inArray = inFile.readline.split.map{|i| i.to_i}
ansArray = Array.new(101){|i| i = 0}
0.step(n * 2 - 1, 2) {|i|
    startLight = [(inArray[i] - inArray[i + 1]), 0].max
    endLight = [(inArray[i] + inArray[i + 1] - 1), 100].min
    for j in startLight..endLight
        ansArray[j] = ansArray[j].to_i + 1
    end
}
outFile.write(ansArray.max)
