inFile = open('input.txt', 'r')
outFile = open('output.txt', 'w')
inArray = inFile.read.split.map{|i| i.to_i}
n = inArray.shift
cnt_str = inArray.max
outFile.write("#{'.' * cnt_str + '#' + '.' * cnt_str}\n")
for i in 0..(n - 1)
    l = cnt_str
    increase = true
    elm = (i % 2) == 0 ? ['#','*'] : ['*','#']
    len = (inArray[i] - 1) * 2
    r = -1
    for j in 0..len
        increase = false if (r + 1) == inArray[i]
        l += increase ? -1 : 1
        r = cnt_str - l - 1
        e = '.'
        if i != 0 && j == 0 || j == len && i != (n - 1)  then
            e = elm[1]
        end
        outFile.write("#{'.'*l + elm[0] + '.'*r + e + '.'*r + elm[0] + '.'*l}\n")
    end
end
outFile.write("#{'.' * cnt_str + elm[0] + '.' * cnt_str}")
