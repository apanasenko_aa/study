with open('input.txt', 'r') as inFile, open('output.txt', 'w') as outFile:

    def noDuplicateItems(l = []):
        '''The function takes a list of type integer,
        return 1 if there are no duplicate items,
        return 0 otherwise'''
        l.sort()
        while 0 in l : l.remove(0)
        for i in range (len(l) - 1):
            if l[i] == l[i + 1] :
                return 0
        return 1

    n = int(inFile.readline())
    ans = 1
    mat = []
    for i in range (n*n) :
        mat.append(list(map(int, inFile.readline().strip().split())))
    for i in range (n*n) :
        curcol = []
        curstr = []
        for j in range (n*n) :
            curcol.append(mat[j][i])
            curstr.append(mat[i][j])
        ans *= noDuplicateItems(curcol)
        ans *= noDuplicateItems(curstr)
    for i in range (n) :
        for j in range (n) :
            cursqr = []
            for k in range (n) :
                for m in range (n) :
                    cursqr.append(mat[i*n+m][j*n+k])
            ans *= noDuplicateItems(cursqr)
    if ans : outFile.write('CORRECT')
    else : outFile.write('INCORRECT')
