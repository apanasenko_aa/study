inFile = open('input.txt', 'r')
outFile = open('output.txt', 'w')
n = int(inFile.readline())
values = list(map(int, inFile.readline().strip ().split(' ')))
max_value = max(values) + 1
for i in range (max_value + 1):
    for j in range (n):
        if (max_value - i) > values[j] : a = '......'
        if (max_value - i) == values[j] : a = '.+---+'
        if (max_value - i) < values[j] : a = '.|###|'
        outFile.write(a)
    outFile.write(".\n")
outFile.write('-+---+' * n + "-")
inFile.close()
outFile.close()
