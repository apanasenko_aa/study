inFile = open('input.txt', 'r')
outFile = open('output.txt', 'w')
s = inFile.readline().strip ()
while s.find('3') != -1 or int(s) % 3 == 0 :
    if s.find('3') != -1:
        s = s[0: s.find('3')] + '4' + '0' * (len(s) - s.find('3') - 1)
    if int(s) % 3 == 0 :
        s = str(int(s) + 1)
outFile.write(s)
inFile.close()
outFile.close()
