from re import findall, sub
with open('xml.in', 'r') as inFile, open('xml.out', 'w') as outFile:
    s_ans = s = inFile.readline().strip()
    if s :
        if s[0] != '<' : s_ans = '<' + s_ans[1:]
        if s[-1] != '>': s_ans = s_ans[:len(s_ans) - 1] + '>'
        s = '0' + s[1:len(s) - 1] + '0'
        s = sub('><', '00', s)
        teg_list = findall('0[a-z]+0', s)
        for in_teg in teg_list :
            out_teg = sub('0', '0/', in_teg, 1)
            if s.find(out_teg) != -1 :
                s = sub(in_teg, '0'* len(in_teg), s, 1)
                s = sub(out_teg, '0'* len(out_teg), s, 1)
        teg_bad = findall('0\D+0', s)
        if (len(teg_bad) == 3) or (len(teg_bad) == 1):
            ind = s.find('>')
            if ind != -1 : s_ans = s_ans[:ind+1] + '<' + s_ans[ind+2:]
            ind = s.rfind('<')
            if ind != -1 : s_ans = s_ans[:ind-1] + '>' + s_ans[ind:]
        if len(teg_bad) == 2 :
            ind = s.find(teg_bad[1][1:])
            s_ans = s_ans[:ind] + '/' + teg_bad[0][1:len(teg_bad[0])-1] + s_ans[ind+len(teg_bad[1][1:])-1:]
    outFile.write(s_ans)
