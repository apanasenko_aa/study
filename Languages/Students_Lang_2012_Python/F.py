with open('input.txt', 'r') as inFile, open('output.txt', 'w') as outFile:
    n = int(inFile.readline())
    files = {}
    for i in range(n) :
    	name = inFile.readline().strip()
    	l_name = name.lower()
    	if l_name in files:
    		while (l_name + str(files[l_name])) in files :
    			files[l_name] += 1
    		outFile.write(name + str(files[l_name]) + "\n")
    		files[l_name + str(files[l_name])] = 1
    		files[l_name] += 1
    	else :
    		outFile.write(name + "\n")
    		files[l_name] = 1