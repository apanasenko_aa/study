import itertools
with open('input.txt', 'r') as inFile, open('output.txt', 'w') as outFile:
    a = inFile.read().strip().split("\n")
    list_ip = {}
    lst = list(itertools.permutations(a))
    for i in lst:
        ip = ''.join(i)
        for s in ip.split('.'):
            if (s == '') or (int(s) > 255) or (len(s) != len(str(int(s)))) : ip = ''
        if ip : list_ip[ip] = 1
    print("\n".join(sorted(list_ip)))
    outFile.write("\n".join(sorted(list_ip)))
