inFile = open('input.txt', 'r')
outFile = open('output.txt', 'w')
Birds = list(map(int, inFile.read().strip().split(' ')))
AllBirds = PastBirds = Speed = 0
for i in range (1, Birds[0] + 1):
    AllBirds = PastBirds + Birds[i]
    if Birds[i] != 0 :
        CurSpeed = max(i - PastBirds - 1, AllBirds - i)
        Speed = max(Speed, CurSpeed)
    PastBirds += Birds[i]
outFile.write(str(Speed))
inFile.close()
outFile.close()
