with open('input.txt', 'r') as inFile, open('output.txt', 'w') as outFile:
    values = list(map(int, inFile.readline().strip().split(' ')))
    i = 0
    mas = {}
    while values[i] != 0 :
        if values[i] > 0 : mas[values[i]] = 1
        else : mas[-values[i]] = 0
        i += 1
    for k in mas.keys():
        if mas[k] == 1 : outFile.write(str(k) + ' ')
