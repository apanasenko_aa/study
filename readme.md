Список курсов
=============

* [Параллельное программирование](Parallel_Programming/)
* [Логическое программирование](Logic_Programming/)
* [Язык Ассемблера](ASM/)
* [Теория графов](Graph_Theory/)
* [Алгоритмы](Algorithms/)
* [Языки программирования](Languages/)
* [Векторный графический редактор](Graphical_Editor/)
* [Калькулятор](Calculator/)
* [Расписание](Database/)
* [ТВиМС](tvims/)
* [Алгоритмы: дополнительные главы](Algorithms_extra/)
* [Функциональное программирование](Functional_Programming/)
* [Cистемное и прикладное ПО](SPPO/)
* [Структуры данных](Data_Structures/)
* [Методы машинного обучения](Machine_Learning/)
* [Операционные системы](Operating_Systems/) TODO
* [SQL](SQL/) TODO
* [Методы оптимизации](Optimization/) TODO
* [Параллельное программирование (Octave)](Parallel_Programming_Octave/) TODO
* [Курсовая работа (3 курс)](Coursework/) TODO

[Турниры](Contests/)  
[Всякая мелочь](Other/)
