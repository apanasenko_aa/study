import math

def fack(n):
	result = 1
	for i in range (1, n + 1):
		result *= i
	return result

def C(n, k):
	if n < k or k < 0 :
		return 0
	return fack(n) / (fack(k) * fack(n - k))

def P(n, p):
	result = []
	for k in range (n + 1):
		result.append(C(n, k) * math.pow(p, k) * math.pow(1 - p, n - k)) 
	return result

def M(n, in_p):
	p = P(n, in_p)
	result = 0
	for i in range(n + 1):
		result += i * p[i]
	return result

def D(n, in_p):
	p = P(n, in_p)
	m_ = M(n, in_p)
	result = 0
	for i in range(n + 1):
		result += p[i] * math.pow(i - m_, 2)
	return result

def F(n, in_p):
	result = []
	for i in P(n, in_p):
		back = 0
		if len(result) > 0 :
			back = result[len(result) - 1]
		result.append(back + i)
	return result 
