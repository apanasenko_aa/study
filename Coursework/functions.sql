
n: traffic.create_segments_vectors()

-- DROP FUNCTION traffic.create_segments_vectors();

CREATE OR REPLACE FUNCTION traffic.create_segments_vectors()
  RETURNS integer AS
$BODY$
  BEGIN
    ---
    --- Разбивает сегменты дорожной сети на элементарные векторы и заполняет таблицу traffic.vectors
    ---
    DELETE FROM traffic.vectors;
    INSERT INTO traffic.vectors(id_segment, the_geom, bearing)
      SELECT
        s.id,
        ST_MakeLine(ST_PointN(s.the_geom, n - 1), ST_PointN(s.the_geom, n)),
        ST_Azimuth(ST_PointN(s.the_geom, n - 1), ST_PointN(s.the_geom, n)) * 180 / pi()
      FROM (
             SELECT id, the_geom, generate_series(2, ST_NPoints(the_geom)) n
             FROM road.traffic_segments
             ORDER BY id, n
           ) s;
    RETURN (SELECT COUNT(*) FROM traffic.vectors);
  END;
  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION traffic.create_segments_vectors()
  OWNER TO vlmaps;
COMMENT ON FUNCTION traffic.create_segments_vectors() IS 'Разбивает сегменты дорожной сети на элементарные векторы и заполняет таблицу traffic.vectors';



-- Function: bus.point_to_path()

-- DROP FUNCTION bus.point_to_path();

CREATE OR REPLACE FUNCTION bus.point_to_path()
  RETURNS trigger AS
$BODY$
DECLARE
	v_point geometry;
	v_line geometry;
	v_id_segment integer;
	v_affected integer;
	rec record;
BEGIN

	SELECT * INTO rec FROM bus.snap_to_segments(ST_SetSRID(ST_Point(NEW.lon, NEW.lat), 4326), NEW.bearing, NEW.udid);

	v_point := rec.snapped_point;
	v_id_segment := rec.id_segment;

	IF v_point IS NOT NULL THEN

	---
	--- Точка притянулась
	---
	NEW.norm_lon := ST_X(v_point);
	NEW.norm_lat := ST_Y(v_point);
	NEW.id_segment := v_id_segment;
	
	---
	--- Обновление последних координат самих устройств
	---
	SELECT the_geom INTO v_line FROM road.traffic_segments s WHERE s.id = v_id_segment;
	UPDATE bus.devices SET
		lon = NEW.norm_lon,
		lat = NEW.norm_lat,
		last_update = now(),
		id_segment = v_id_segment,
		length_to_end = ST_Length(ST_Transform(ST_SetSRID(ST_Line_Substring(v_line, ST_Line_Locate_Point(v_line, v_point), 1), 4326), 3857))
	WHERE udid = NEW.udid;

	ELSE

	---
	--- Точка НЕ притянулась
	---
	UPDATE bus.devices SET
		lon = NEW.norm_lon,
		lat = NEW.norm_lat,
		id_segment = NULL,
		length_to_end = NULL,
		last_update = now()
	WHERE udid = NEW.udid;

	END IF;

	RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION bus.point_to_path()
  OWNER TO vlmaps;


-- Function: bus.snap_to_segments(geometry, double precision, character varying)

-- DROP FUNCTION bus.snap_to_segments(geometry, double precision, character varying);

CREATE OR REPLACE FUNCTION bus.snap_to_segments(IN p_raw_point geometry, IN p_bearing double precision, IN p_udid character varying, OUT snapped_point geometry, OUT id_segment integer)
  RETURNS record AS
$BODY$
	---
	--- Функция по точке и направлению возвращает притянутую с учётом направления к ближайшему сегменту точку и ID самого сегмента
	---
	--- Множитель к функции LEAST в ORDER BY позволяет повысить значение направления при попадании в нужный сегмент
	---
	SELECT
		ST_ClosestPoint(v.the_geom, p_raw_point)  AS snapped_point,
		v.id_segment                              AS id_segment
	FROM
		(SELECT * FROM road.traffic_vectors vec WHERE ST_Distance(vec.the_geom, p_raw_point) < 0.005 /* около 300 метров */) AS v
		INNER JOIN bus.devices d ON (d.udid = p_udid)
--		INNER JOIN bus.routes_segments rs ON (rs.id_segment = v.id_segment AND d.id_route = rs.id_route)
	WHERE (d.udid = p_udid)
	ORDER BY
		ST_Distance(p_raw_point, v.the_geom) * 95278.3289025413 + 5 * LEAST(abs(v.bearing - p_bearing), 360 - abs(v.bearing - p_bearing))
	LIMIT 1;
	$BODY$
  LANGUAGE sql VOLATILE
  COST 100;
ALTER FUNCTION bus.snap_to_segments(geometry, double precision, character varying)
  OWNER TO vlmaps;
COMMENT ON FUNCTION bus.snap_to_segments(geometry, double precision, character varying) IS 'Функция по точке и направлению возвращает притянутую с учётом направления к ближайшему сегменту указанного маршрута точку и ID самого сегмента';

