﻿-- Table: road.		

-- DROP TABLE road.traffic;

CREATE TABLE road.traffic
(
  gid serial NOT NULL,
  id_road integer, -- Уникальный идентификатор (gid) дороги. Ссылка на таблицы road_vl и road_vl_points
  the_geom geometry,
  id_first_node integer NOT NULL, -- ID первого узла (первой точки линии)
  id_last_node integer NOT NULL, -- ID последнего узла (последней точки линии)
  speed double precision, -- Скорость сегмента
  CONSTRAINT road_traffic_pk PRIMARY KEY (gid),
  CONSTRAINT road_traffic_first_node_fk FOREIGN KEY (id_first_node)
      REFERENCES road.nodes (gid) MATCH SIMPLE
      ON UPDATE SET NULL ON DELETE CASCADE,
  CONSTRAINT road_traffic_last_node_fk FOREIGN KEY (id_last_node)
      REFERENCES road.nodes (gid) MATCH SIMPLE
      ON UPDATE SET NULL ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE road.traffic
  OWNER TO vlmaps;
COMMENT ON COLUMN road.traffic.id_road IS 'Уникальный идентификатор (gid) дороги. Ссылка на таблицы road_vl и road_vl_points';
COMMENT ON COLUMN road.traffic.id_first_node IS 'ID первого узла (первой точки линии)';
COMMENT ON COLUMN road.traffic.id_last_node IS 'ID последнего узла (последней точки линии)';
COMMENT ON COLUMN road.traffic.speed IS 'Скорость сегмента';


-- Index: road.traffic_the_geom_idx

-- DROP INDEX road.traffic_the_geom_idx;

CREATE INDEX traffic_the_geom_idx
  ON road.traffic
  USING gist
  (the_geom);

