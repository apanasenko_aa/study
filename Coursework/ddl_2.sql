-- Schema: road

-- DROP SCHEMA road;

CREATE SCHEMA road
  AUTHORIZATION vlmaps;

GRANT ALL ON SCHEMA road TO vlmaps;
GRANT ALL ON SCHEMA road TO public;
COMMENT ON SCHEMA road
  IS 'Представление дорожной сети';



-- Schema: bus

-- DROP SCHEMA bus;

CREATE SCHEMA bus
  AUTHORIZATION vlmaps;

GRANT ALL ON SCHEMA bus TO vlmaps;
GRANT ALL ON SCHEMA bus TO public;
COMMENT ON SCHEMA bus
  IS 'Данные о движении автобусов';



-- Table: road.nodes

-- DROP TABLE road.nodes;

CREATE TABLE road.nodes
(
  gid integer NOT NULL, -- ID
  node_type character(1) NOT NULL DEFAULT 'c'::bpchar, -- Тип узла: ...
  the_geom geometry, -- Представление узла
  visual_geom geometry, -- Визуальное представление узла (у остановок данное поле отличается от поле the_geom)
  CONSTRAINT road_nodes_pk PRIMARY KEY (gid)
)
;
ALTER TABLE road.nodes
  OWNER TO vlmaps;
COMMENT ON COLUMN road.nodes.gid IS 'ID';
COMMENT ON COLUMN road.nodes.node_type IS 'Тип узла: 
  "c" - перекресток 
  "s" - остановка
  "e" - начало/конец дороги (если это не перекресток)';
COMMENT ON COLUMN road.nodes.the_geom IS 'Представление узла';
COMMENT ON COLUMN road.nodes.visual_geom IS 'Визуальное представление узла (у остановок данное поле отличается от поле the_geom)';


-- Index: road.nodes_the_geom_idx

-- DROP INDEX road.nodes_the_geom_idx;

CREATE INDEX nodes_the_geom_idx
  ON road.nodes
  USING gist
  (the_geom);


-- Table: road.segments

-- DROP TABLE road.segments;

CREATE TABLE road.segments
(
  gid serial NOT NULL,
  id_road integer, -- Уникальный идентификатор (gid) дороги. Ссылка на таблицы road_vl и road_vl_points
  the_geom geometry,
  id_first_node integer NOT NULL, -- ID первого узла (первой точки линии)
  id_last_node integer NOT NULL, -- ID последнего узла (последней точки линии)
  length double precision, -- Длина сегмента в метрах
  is_two_way boolean NOT NULL DEFAULT false, -- Сегмент двухсторонней дороги?
  CONSTRAINT road_segments_pk PRIMARY KEY (gid),
  CONSTRAINT road_segments_first_node_fk FOREIGN KEY (id_first_node)
      REFERENCES road.nodes (gid) MATCH SIMPLE
      ON UPDATE SET NULL ON DELETE CASCADE,
  CONSTRAINT road_segments_last_node_fk FOREIGN KEY (id_last_node)
      REFERENCES road.nodes (gid) MATCH SIMPLE
      ON UPDATE SET NULL ON DELETE CASCADE
)
;
ALTER TABLE road.segments
  OWNER TO vlmaps;
COMMENT ON COLUMN road.segments.id_road IS 'Уникальный идентификатор (gid) дороги. Ссылка на таблицы road_vl и road_vl_points';
COMMENT ON COLUMN road.segments.id_first_node IS 'ID первого узла (первой точки линии)';
COMMENT ON COLUMN road.segments.id_last_node IS 'ID последнего узла (последней точки линии)';
COMMENT ON COLUMN road.segments.length IS 'Длина сегмента в метрах';
COMMENT ON COLUMN road.segments.is_two_way IS 'Сегмент двухсторонней дороги?';


-- Index: road.segments_the_geom_idx

-- DROP INDEX road.segments_the_geom_idx;

CREATE INDEX segments_the_geom_idx
  ON road.segments
  USING gist
  (the_geom);

-- Table: bus.points

-- DROP TABLE bus.points;

CREATE TABLE bus.points
(
  id serial NOT NULL, -- ID записи
  udid character varying(16) NOT NULL, -- Уникальный идентификатор устройства
  lon double precision, -- Долгота
  lat double precision, -- Широта
  alt double precision, -- Высота
  accuracy double precision, -- Точность
  speed double precision, -- Скорость
  bearing double precision, -- Направление
  ts timestamp with time zone, -- Время регистрации точки
  id_event integer,
  norm_lon double precision, -- Долгота скорректированной (притянутой) точки
  norm_lat double precision, -- Широта скорректированной (притянутой) точки
  id_segment integer, -- ID сегмента, в который попадает точка с нормализованными координатами
  CONSTRAINT points_pk PRIMARY KEY (id)
)
;
ALTER TABLE bus.points
  OWNER TO vlmaps;
COMMENT ON TABLE bus.points
  IS 'Точки, регистрируемые устройствами';
COMMENT ON COLUMN bus.points.id IS 'ID записи';
COMMENT ON COLUMN bus.points.udid IS 'Уникальный идентификатор устройства';
COMMENT ON COLUMN bus.points.lon IS 'Долгота';
COMMENT ON COLUMN bus.points.lat IS 'Широта';
COMMENT ON COLUMN bus.points.alt IS 'Высота';
COMMENT ON COLUMN bus.points.accuracy IS 'Точность';
COMMENT ON COLUMN bus.points.speed IS 'Скорость';
COMMENT ON COLUMN bus.points.bearing IS 'Направление';
COMMENT ON COLUMN bus.points.ts IS 'Время регистрации точки';
COMMENT ON COLUMN bus.points.norm_lon IS 'Долгота скорректированной (притянутой) точки';
COMMENT ON COLUMN bus.points.norm_lat IS 'Широта скорректированной (притянутой) точки';
COMMENT ON COLUMN bus.points.id_segment IS 'ID сегмента, в который попадает точка с нормализованными координатами';


-- Index: bus.points_id_event_idx

-- DROP INDEX bus.points_id_event_idx;

CREATE INDEX points_id_event_idx
  ON bus.points
  USING btree
  (id_event);

-- Index: bus.points_id_segment_idx

-- DROP INDEX bus.points_id_segment_idx;

CREATE INDEX points_id_segment_idx
  ON bus.points
  USING btree
  (id_segment);

-- Index: bus.points_ts_idx

-- DROP INDEX bus.points_ts_idx;

CREATE INDEX points_ts_idx
  ON bus.points
  USING btree
  (ts);

-- Index: bus.points_udid_idx

-- DROP INDEX bus.points_udid_idx;

CREATE INDEX points_udid_idx
  ON bus.points
  USING btree
  (udid COLLATE pg_catalog."default");

