﻿-- DROP FUNCTION update_speed (timestamp with time zone, timestamp with time zone);

SELECT update_speed('2014-04-29 13:00:00+11'::timestamp with time zone, '2014-04-29 14:00:00+11'::timestamp with time zone);

CREATE FUNCTION update_speed (s timestamp with time zone, e timestamp with time zone)
RETURNS INTEGER 
AS $$
BEGIN
	UPDATE road.traffic SET speed = -1;

	UPDATE road.traffic t
	SET speed = (
		SELECT AVG(bp.speed) 
		FROM bus.points bp
		WHERE t.gid = bp.id_segment
		  AND bp.ts >= s
		  AND bp.ts < e
		);
	RETURN 1;
END;
$$  LANGUAGE plpgsql;
