﻿DROP FUNCTION Dijkstra (integer, integer);
CREATE FUNCTION Dijkstra (first_node integer, last_node integer)
RETURNS INTEGER[] AS $$
DECLARE
	cur_node_id         integer;
	length_to_cur_node  double precision;
	way_to_cur_node     integer[];
BEGIN
        DELETE FROM road.way;

	CREATE TEMP TABLE LENGTH_TO_NODES(
		id              integer           PRIMARY KEY, 
		length_to_node  double precision  NOT NULL,
		way             integer[],	
		done            boolean           NOT NULL
	) ON COMMIT DROP;

	INSERT INTO LENGTH_TO_NODES (id, length_to_node, way, done)
		SELECT road.nodes.gid, 99999999999, NULL, FALSE FROM road.nodes;

	UPDATE LENGTH_TO_NODES SET length_to_node = 0 WHERE LENGTH_TO_NODES.id = first_node;

	LOOP
		SELECT id, length_to_node, way INTO cur_node_id, length_to_cur_node, way_to_cur_node
			FROM LENGTH_TO_NODES 
			WHERE done = FALSE
			ORDER BY length_to_node LIMIT 1;

		IF cur_node_id = last_node 
		THEN EXIT; 
		END IF;

		UPDATE LENGTH_TO_NODES 
			SET done = TRUE 
			WHERE LENGTH_TO_NODES.id = cur_node_id;

		UPDATE LENGTH_TO_NODES ln
		SET 
			length_to_node = length_to_cur_node + s.length, 
			way = (SELECT array_append(way_to_cur_node, s.gid))
		FROM road.segments AS s
		WHERE cur_node_id = s.id_first_node
		  AND ln.id = s.id_last_node 
		  AND ln.done = FALSE 
		  AND (length_to_cur_node + s.length) < ln.length_to_node;

	END LOOP;

	INSERT INTO road.way(gid, id_road, the_geom, id_first_node, id_last_node, length, is_two_way)
		SELECT gid, id_road, the_geom, id_first_node, id_last_node, length, is_two_way
		FROM road.segments
		WHERE road.segments.gid = ANY (SELECT * FROM unnest((SELECT way FROM LENGTH_TO_NODES WHERE id = last_node)::integer[]));

        
	RETURN (SELECT way FROM LENGTH_TO_NODES WHERE id = last_node)::integer[];
END;
$$  LANGUAGE plpgsql