import postgresql
from datetime import tzinfo, timedelta, datetime
from time import sleep

class TZ(tzinfo):
	def utcoffset(self, dt): return timedelta(minutes=660)

db = postgresql.open('pq://vlmaps:123456@localhost:5432/layers')
trans = db.xact()
trans.start()
db.execute('UPDATE road.traffic SET speed = -1')
db.execute('DELETE FROM bus.points')
insert_bp = db.prepare(
"""INSERT INTO bus.points(id, udid, lon, lat, alt
						, accuracy, speed, bearing
						, ts, id_event, norm_lon
						, norm_lat, id_segment)
		SELECT id, udid, lon, lat, alt
			, accuracy, speed, bearing
			, ts, id_event, norm_lon
			, norm_lat, id_segment
		FROM bus.points_tmp
		WHERE ts >= $1 
			AND ts < $2 
			AND speed > 1
""")

update_speed = db.prepare('SELECT update_speed($1::timestamp with time zone, $2::timestamp with time zone)')

time = datetime(2014, 4, 29, 13, tzinfo=TZ())
end = datetime(2014, 5, 17, 4, tzinfo=TZ())

k = 0
while time < end:
	insert_bp(time, time + timedelta(minutes=60))
	# for i in range(6):
	# 	update_speed(time, time + timedelta(minutes=10))
	# 	trans.commit()
	# 	print(k)
	# 	k +=1
	# 	time += timedelta(minutes=10)
	# 	sleep(2)
	update_speed(time, time + timedelta(minutes=60))
	trans.commit()
	time += timedelta(minutes=60)

