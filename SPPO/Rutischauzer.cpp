#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include <algorithm>
#include <map>

using namespace std;

struct Token
{
	int type;
	int level;
	string value;
	Token(int t, int l, string v) : type(t), level(l), value(v) {}
};

Token* parse(const string& text, int& pos, int& level)
{
	while (pos < text.length())
	{
		char c = text[pos++];
		if (c == '(' || c == ')') 
			level += c == '(' ? 1 : -1;
		else if (c == '+' || c == '-' || c == '/' || c == '*')
			return new Token(1, level, string(1, c));
		else {
			string digit = "";
			while (c >= '0' & c <= '9' && pos < text.length())
			{
				digit += string(1, c);
				c = text[pos++];
			}
			pos--;
			if (digit == "") throw string("unknown symbol");
			return new Token(2, level, digit);
		}
	}
	if (level > 0) throw string("Need )");
	if (level < 0) throw string("Need (");
	return new Token(0, level, "");
}

void calc_level(vector<Token*>& tokens, int pos)
 {
	 if (tokens[pos]->type != 2 
		 || tokens[pos + 2]->type != 2 
		 || tokens[pos + 1]->type != 1
		 || tokens[pos]->level != tokens[pos + 1]->level
		 || tokens[pos]->level != tokens[pos + 2]->level)
 		throw string("Invalid expression");
	 int result;
	 if (tokens[pos + 1]->value == "+")
		 result = stoi(tokens[pos]->value) + stoi(tokens[pos + 2]->value);
 	 else if (tokens[pos + 1]->value == "-")
 		result = stoi(tokens[pos]->value) - stoi(tokens[pos + 2]->value);
 	 else if (tokens[pos + 1]->value == "*")
 		result = stoi(tokens[pos]->value) * stoi(tokens[pos + 2]->value);
 	 else if (tokens[pos + 1]->value == "/")
 		result = stoi(tokens[pos]->value) / stoi(tokens[pos + 2]->value);
	 else 
		throw string("Unknown operation"); 
	 tokens[pos] = new Token(2, tokens[pos + 1]->level - 1, to_string(result));
	 for (int i = pos + 3; i < tokens.size(); i++)
		 tokens[i - 2] = tokens[i];
	 tokens.resize(tokens.size() - 2);
 }


int Rutischauzer(string text)
{
	vector<Token*> tokens;
	int pos = 0;
	int level = 0;
	while (pos < text.length())
		tokens.push_back(parse(text, pos, level));
	tokens.pop_back();
	while (tokens.size() > 1)
	{
		int token_index = 0;
		for (int i = 1; i < tokens.size(); i++)
			token_index = tokens[token_index]->level < tokens[i]->level ? i : token_index;
		calc_level(tokens, token_index);
	}
	return stoi(tokens[0]->value);
}

int main()
{
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	string text;
	cin >> text;
	try {
		cout << Rutischauzer(text) << endl;
	} catch (string e){
		cout << e << endl;
	}
	return 0;
}