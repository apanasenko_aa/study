﻿Cистемное и прикладное ПО
=========================

* Сжатие и кодирование
	* [RLE](http://algolist.manual.ru/compress/standard/rle.php) ([кодирование](RLE_compress.py)/[декодирование](RLE_uncompress.py))
	* [Арифметическое кодирование](http://algolist.manual.ru/compress/standard/arithm.php) ([кодирование](arithm_compress.py)/[декодирование](arithm_uncompress.py))
* [Разбор арифметического выражения](http://algolist.manual.ru/syntax/parsear.php)
	* [Алгоритм Рутисхаузера](Rutischauzer.cpp)
	* [Алгоритм Бауэра и Замельзона](Bauer_Zamelzon.cpp)
* [Генетический алгоритм](http://algolist.manual.ru/ai/ga/index.php)
	* [Генетический алгоритм](Genetic_algorithm/Genetic_algorithm.cpp)
