#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include <algorithm>
#include <map>

using namespace std;

typedef struct Task 
{
	int complexity;
	float time;
} Task;

typedef struct Solve 
{
	vector<int> solve;
	float fitness;
	bool operator < (Solve &s) {return fitness < s.fitness;}
	bool operator <= (Solve &s) {return fitness <= s.fitness;}
} Solve;

vector<Task> tasks;
vector<vector<float>> people;
Solve ans;


float fitness(const vector<int> &solve)
{
	float result = 0;
	for (int j = 0; j < people.size(); j++)
	{
		float t = 0;
		for (int i = 0; i < solve.size(); i++)
			t += solve[i] == j ? tasks[i].time * people[j][tasks[i].complexity] : 0;
		result = t > result ? t :result;
	}
	return result;
}

Solve getNewSolve(int count, int range)
{
	Solve result;
	for (int i = 0; i < count; i++)
		result.solve.push_back(rand() % range);
	result.fitness = fitness(result.solve);
	return result;
}

Solve getNewSolve(Solve &p1, Solve &p2)
{
	Solve result;
	for (int i = 0; i < p1.solve.size(); i++)
		if (rand() % 100 < 20)
			result.solve.push_back(rand() % people.size());
		else
			result.solve.push_back(rand() % 2 == 1 ? p1.solve[i] : p2.solve[i]); 
	result.fitness = fitness(result.solve);
	return result;
}



vector<int> genetic(int count_solves = 100, int max_iter = 100)
{
	vector<Solve> solves(count_solves);
	for (int i = 0; i < count_solves; i++)
		solves[i] = getNewSolve(tasks.size(), people.size());		
	for (int i = 0; i < max_iter; i++)
	{
		sort(solves.begin(), solves.end());
		vector<float> p(count_solves, 0); 
		for(int j = 0; j < count_solves; j++)
			p[j] = (solves[0].fitness + solves.back().fitness) - solves[j].fitness;
		float sum = 0;
		for(int j = 0; j < count_solves; j++)
			sum += p[j];
		vector<int> parents(count_solves); 
		for(int j = 0; j < count_solves; j++)
		{
			int c = int(p[j] / sum * 100);
			for (int k = 0; k < c; k++)
				parents.push_back(j);
		}
		for (int j = 0; j < count_solves && parents.size() > 2; j++)
		{
			int i = rand() % parents.size();
			int p1 = parents[i];
			parents[i] = parents.back();
			parents.pop_back();
			i = rand() % parents.size();
			int p2 = parents[i];
			parents[i] = parents.back();
			parents.pop_back();
			Solve newSolve = getNewSolve(solves[p1], solves[p2]);
			int k = 0;
			for (; k < count_solves && solves[k] <= newSolve; k++);
			if (k < count_solves)
				solves[k] = newSolve;
		}
		solves[count_solves - 1] = getNewSolve(tasks.size(), people.size());
	}
	return solves[0].solve;
}
//
//void gen_ans(vector<int> v, int size)
//{
//	if (v.size() == size){
//		Solve s;
//		s.solve = v;
//		s.fitness = fitness(v);
//		ans = ans < s ? ans : s;
//	} else {
//		for (int i = 0; i < people.size(); i++)
//		{
//			v.push_back(i);
//			gen_ans(v, size);
//			v.pop_back();
//		}
//	}
//}

int main()
{
	//freopen("input.txt", "r", stdin);
	//freopen("output.txt", "w", stdout);
	FILE* inFile = fopen("input.txt", "r");
	FILE* outFile = fopen("output1.txt", "w");
	int n;
	fscanf(inFile,"%d", &n);

	tasks.resize(n);
	for (int i = 0; i < n - 1; i++)
	{
		fscanf(inFile,"%d,", &tasks[i].complexity);
		tasks[i].complexity--;
	}
	fscanf(inFile,"%d", &tasks[n - 1].complexity);
	tasks[n - 1].complexity--;

	for (int i = 0; i < n - 1; i++)
		fscanf(inFile,"%f,", &tasks[i].time);
	fscanf(inFile,"%f", &tasks[n - 1].time);
	

	fscanf(inFile,"%d", &n);
	people.resize(n);
	float x;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			fscanf(inFile,"%f,", &x);
			people[i].push_back(x);
		}
		fscanf(inFile,"%f", &x);
		people[i].push_back(x);
	}
	//ans.fitness = 1000000000000;
	//vector<int> v;
	//gen_ans(v, tasks.size());
	//FILE* ansFile = fopen("ans.txt", "w");
	//for (int i = 0; i < ans.solve.size(); i++) {
	//	fprintf(ansFile, "%d ", ans.solve[i] + 1);
	//}
	//

	vector<int> ans_ = genetic(50, 1000);
	vector<float> res(people.size(), 0);
	for (int i = 0; i < ans_.size(); i++) {
		fprintf(outFile, "%d ", ans_[i] + 1);
		res[ans_[i]] += tasks[i].time * people[ans_[i]][tasks[i].complexity];
	}
	for (int j = 0; j < people.size(); j++)
		fprintf(outFile, "\n%f", res[j]);
	return 0;
}

//
//������ ������������� �������� ��� ��������. ���� ������ ������ �� ������, ��� 
//������ ������ ����������� �� ��������� ��������� (1, 2, 3 ��� 4), � ����� ��������� 
//����� ���������� ������ � �����. ������ ��������� �����������, ���� ��������� ��� 
//������. ��� ������� ������������ � ��� ������ ��������� ��������� ������ ����������� 
//�����������, � �������, ��� ���������, ����� ������������ �������� ����� ���������� 
//������ ������ ������������� � ���������� �������. ���������, ��� ��� ������������ 
//�������� �������� � �������� � ���� � ���� ����� � �������� ��� ������ ���������� 
//�����. ���������� ����������� ���������, �������������� ������ �� �������������, 
//� ����� �������������� ����� ���������� ������� (�������� ������� ������ �� 
//����������� ���������� �������). ����� ������� ���������� ����������� � ������� 
//������������� ���������.
// ��� ���������� ������, ��� �������� ���� ����� ������� �� ������������ 
//� ����� ����� ��������� ������������� ���������, �������������� ���������� 
//���������� ����������� ������������� � ���������� ������� �� ���������� ����� 
//�� ��������� ������� ������� ������, ��������� ����� ����� � ��������� �����, 
//���������� ������������� � �� ������.
//
//�� ����� � ��������� ���� � �������� �������.
//
//������ ������ � ����� �����.
//������ ������ � ������ ��������� ��������� �����, ����� ����� �� 1 �� 4, ����������� �������.
//������ ������ � ������ ���������� ������� ��� �����, ������������ �����, ����������� �������.
//��������� ������ � ����� �������������, ����� ������������� �����
//����� ������ � ������ ������������� ��� 1-�� ������������
//�
//N+4 ������ - ������ ������������� ��� N-�� ������������
//�� ������ � ��������� ���� � ������������, 
//������ ������ � ������, ����������� n-�� ������ � m-�� �������������.
//����� ���� ���� n �����, ��� n � ���������� �������������. � ������ ������ 
//��������� ������������ ����� � ���������� ����� ������ ������� ������������ ��� 
//������������� ��� ��������.
//
//������:
//
//Input.txt
//3
//1, 1, 4
//5.2, 3.4, 4
//2
//1, 1, 2, 5
//0.7, 1, 1.2, 1.5
//
//Output.txt
//1 2 2
//5.2
//8.38