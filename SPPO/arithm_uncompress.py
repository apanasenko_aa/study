#inFileName = input('Input file name: ')
#outFileName = input('Output file name: ')
inFileName = '1.out'
outFileName = '1.ans'

with open(inFileName, 'r') as inFile, open(outFileName, 'w') as outFile :
	outText = ''
	n, m = list(map(int, inFile.readline().split()))
	chars = []
	begins = []
	ends = []
	for i in range(m):
		c, s, e = inFile.readline().split()
		chars.append(chr(int(c)))
		begins.append(float(s))
		ends.append(float(e))
	pos = float(inFile.read())
	print(chars)

	for j in range(n):
		i = 0
		while pos > ends[i]:
			i += 1
		outText += chars[i]
		pos = (pos - begins[i]) / (ends[i] - begins[i])

	outFile.write(outText)
	print(outText)
