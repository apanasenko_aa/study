#inFileName = input('Input file name: ')
#outFileName = input('Output file name: ')
inFileName = '1.out'
outFileName = '1.ans'

with open(inFileName, 'r') as inFile, open(outFileName, 'w') as outFile :
	inText = inFile.read()
	outText = inText[0]
	i = 1
	canIterate = True
	while i < len(inText) :
		if inText[i - 1] == inText[i]  and canIterate :
			outText += inText[i] * int(inText[i + 1])
			i += 1
			canIterate = False
		else :
			outText += inText[i]
			canIterate = True
		i += 1
	outFile.write(outText)
	print(outText)
