#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include <algorithm>
#include <map>

using namespace std;

int opToInt(char c)
{
	 if (c == '\0') return 0;
	 if (c == '(') return 1;
	 if (c == '+') return 2;
	 if (c == '-') return 3;
	 if (c == '*') return 4;
	 if (c == '/') return 5;
	 if (c == ')') return 6;
}

int Bauer_Zamelzon(string text)
{
	vector<int> E;
	vector<char> T;

	int table[6][7] = {{6, 1, 1, 1, 1, 1, 5},
					   {5, 1, 1, 1, 1, 1, 3},
					   {4, 1, 2, 2, 1, 1, 4},
					   {4, 1, 2, 2, 1, 1, 4},
					   {4, 1, 4, 4, 2, 2, 4},
	                   {4, 1, 4, 4, 2, 2, 4}};
	int pos = 0;
	while (1)
	{
		string digit = "";
		while (pos < text.length() && text[pos] >= '0' && text[pos] <= '9')
			digit += text[pos++];
		
		if (digit != "")
			E.push_back(stoi(digit));
		char topOp = T.empty() ? '\0' : T.back();
		char inOp = pos < text.length() ? text[pos] : '\0';
		int r, l, f = table[opToInt(topOp)][opToInt(inOp)];

		switch (f)
		{
		case 1:
			T.push_back(inOp);
			pos++;
			break;
		case 2:
		case 4:
			l = E.back();
			E.pop_back();
			r = E.back();
			E.pop_back();
			switch (topOp)
			{
			case '+':
				E.push_back(l + r);
				break;
			case '-':
				E.push_back(r - l);
				break;
			case '*':
				E.push_back(l * r);
				break;
			case '/':
				E.push_back(r / l);
				break;
			}	
			T.pop_back(); 
			if (f == 2)
			{
				T.push_back(inOp);
				pos++;
			}
			break;
		case 3:
			T.pop_back();
			pos++;
			break;
		case 5:
			throw string("error");
			break;
		case 6:
			return E.back();
			break;
		}
	}
	return E.back();
}

int main()
{
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	string text;
	cin >> text;
	try {
		cout << Bauer_Zamelzon(text) << endl;	
	} catch (string e){
		cout << e << endl;
	}
	return 0;
}