#inFileName = input('Input file name: ')
#outFileName = input('Output file name: ')
inFileName = '1.in'
outFileName = '1.out'

with open(inFileName, 'r') as inFile, open(outFileName, 'w') as outFile :
	inText = inFile.read()
	letters = {}
	for i in inText :
		letters[i] = inText.count(i)
	start = {}
	end = {}
	cur_pos = 0
	for i in sorted(letters.keys()):
		start[i] = cur_pos
		cur_pos += letters[i] / len(inText)
		end[i] = cur_pos

	left = 0
	right = 1
	for i in inText :
		nLeft = left + (right - left) * start[i]
		nRight = left + (right - left) * end[i]
		left = nLeft
		right = nRight
	# print
	outFile.write(str(len(inText)) + ' ' + str(len(start)) + '\n')
	for i in sorted(start.keys()):
		outFile.write(str(ord(i)) + ' ' + str(start[i]) + ' ' + str(end[i]) + '\n')
	outFile.write(str((left + right) / 2))
