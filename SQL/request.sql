SELECT G."Name", SUM(S.HOURS) FROM TEACHERS_SUBJECTS T_S
    INNER JOIN SUBJECTS S
    INNER JOIN  GROUPS G  
ON T_S."Subject_id" = S."id"
ON T_S."Group_id" = G."id"
GROUP BY G."Name"
ORDER BY SUM(S.HOURS)
    DESC

SELECT T."Name", COUNT(DISTINCT S."id") AS "Студентов" FROM TEACHERS_SUBJECTS T_S
    INNER JOIN TEACHERS T 
    INNER JOIN STUDENTS S 
ON T_S."Teacher_id" = t."id"
ON T_S."Group_id" = s."Group_id"
WHERE 
    EXTRACT(YEAR FROM S."Birhday") = 1993 AND EXTRACT(MONTH FROM S."Birhday") = 6
GROUP BY (T."Name")
ORDER BY ("Студентов")
    DESC


UPDATE SUBJECTS S SET HOURS = (SELECT AVG(T_S."Hours") FROM TEACHERS_SUBJECTS T_S 
WHERE T_S."Subject_id" = S."id")

UPDATE TEACHERS_SUBJECTS T_S SET "Hours" = (SELECT S.HOURS FROM SUBJECTS s 
WHERE S."id" = T_S."Subject_id");
