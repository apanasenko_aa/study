CREATE DATABASE 'C:\university.fdb' 
USER 'SYSDBA' PASSWORD 'masterkey';

CREATE TABLE groups(
		"id" INT NOT NULL,
		"Name" VARCHAR(150) NOT NULL,
		PRIMARY KEY("id"));
		
CREATE TABLE students(
		"id" INT NOT NULL,
		"Name" VARCHAR(150) NOT NULL,
		"Group_id" INT REFERENCES  groups("id")
			ON DELETE CASCADE,
		"Birhday" DATE NOT NULL,
		PRIMARY KEY ("id"));

CREATE TABLE teachers(
		"id" INT NOT NULL,
		"Name" VARCHAR(150) NOT NULL,
		PRIMARY KEY ("id"));

CREATE TABLE subjects(
			"id" INT NOT NULL,
			"Name" VARCHAR(150) NOT NULL,
			PRIMARY KEY ("id"));
			
CREATE TABLE teachers_subjects(
			"Teacher_id" INT REFERENCES teachers("id"),
			"Subject_id" INT REFERENCES subjects("id"),
			"Group_id" INT REFERENCES groups("id"),
			"Hours" INT NOT NULL);

INSERT INTO groups VALUES(1, '�8103a-1');
INSERT INTO groups VALUES(2, '�8103a-2');
INSERT INTO groups VALUES(3, '�8103�'); 
INSERT INTO groups VALUES(4, '�8113'); 
INSERT INTO groups VALUES(5, '�8105'); 
INSERT INTO groups VALUES(6, '�8107'); 
INSERT INTO groups VALUES(7, '�8108'); 
INSERT INTO groups VALUES(8, '�8503'); 

INSERT INTO students VALUES (1, '��������� ������', 2, '02/04/93');
INSERT INTO students VALUES (2, '���������� ���������',  1,  '06/28/93');
INSERT INTO students VALUES (3, '������ �������', 2, '08/01/94');
INSERT INTO students VALUES (4, '������ ����', 2, '01/17/93');
INSERT INTO students VALUES (5, '�������� ������', 1, '02/26/92');
INSERT INTO students VALUES (6, '������ ���������', 2, '12/17/91');
INSERT INTO students VALUES (7, '����� ������', 2, '09/15/93');
INSERT INTO students VALUES (8, '������ �����', 1, '07/18/93');
INSERT INTO students VALUES (9, '���� ��������', 2, '05/09/93');
INSERT INTO students VALUES (10, '����������� �����', 2, '05/19/93');
INSERT INTO students VALUES (11, '����� ��������', 2, '07/03/93');
INSERT INTO students VALUES (12, '�������� ���������', 2, '04/24/93');
INSERT INTO students VALUES (13, '����� ������', 1,  '06/21/93');
INSERT INTO students VALUES (14, '��������� �����', 1, '07/02/93');
INSERT INTO students VALUES (15, '������� ��������', 2, '08/29/93');
INSERT INTO students VALUES (16, '�������� ����������', 1, '09/02/93');
INSERT INTO students VALUES (17, '���������� �����', 1, '03/01/93');
INSERT INTO students VALUES (19, '��������� ������', 2, '02/03/93');
INSERT INTO students VALUES (20, '������� ����', 1, '09/26/93');
INSERT INTO students VALUES (21, '����� �������', 1, '09/20/93');
INSERT INTO students VALUES (22, '���������� ����', 2, '10/10/93');
INSERT INTO students VALUES (23, '���������� ����������', 2, '05/13/93');
INSERT INTO students VALUES (24, '�������� �������', 2, '12/06/93');
INSERT INTO students VALUES (25, '��������� ����������', 2, '03/16/93');
INSERT INTO students VALUES (26, '�������� ���������', 1, '12/03/92');
INSERT INTO students VALUES (27, '��������� �����', 2, '05/25/93');
INSERT INTO students VALUES (28, '��������� ����', 1, '09/01/93');
INSERT INTO students VALUES (29, '����������� ������', 1, '06/23/93');
INSERT INTO students VALUES (30, '������� �����', 1, '02/22/93');
INSERT INTO students VALUES (31, '������ ����', 1, '05/30/93');
INSERT INTO students VALUES (32, '����� ������', 2, '10/20/90');

INSERT INTO teachers VALUES(1, '�������� ���� ����������');
INSERT INTO teachers VALUES(2, '��������� ������ ����������');
INSERT INTO teachers VALUES(3, '������ ����� ���������');
INSERT INTO teachers VALUES(4, '������ ��������� ���������');
INSERT INTO teachers VALUES(5, '������� ������� ����������');
INSERT INTO teachers VALUES(6, '���������� ���� ��������');
INSERT INTO teachers VALUES(7, '����� ����� �������');
INSERT INTO teachers VALUES(8, '���������� ������� ������������');
INSERT INTO teachers VALUES(9, '��� �������� ��������������');
INSERT INTO teachers VALUES(10, '���������� ������ ��������');
INSERT INTO teachers VALUES(11, '������� �����');
INSERT INTO teachers VALUES(12, '������ ������� ����������');

INSERT INTO subjects VALUES(1, '������� � ���������');
INSERT INTO subjects VALUES(2, '���� ������');
INSERT INTO subjects VALUES(3, '���������� ����������');
INSERT INTO subjects VALUES(4, '����������� ����');
INSERT INTO subjects VALUES(5, '�������������� ������');
INSERT INTO subjects VALUES(6, '�������� �� ���');
INSERT INTO subjects VALUES(7, '�����������');
INSERT INTO subjects VALUES(8, '����� � ������ ����������������');

INSERT INTO teachers_subjects VALUES(1,5,1,128);
INSERT INTO teachers_subjects VALUES(1,5,2,128);
INSERT INTO teachers_subjects VALUES(1,5,3,128);
INSERT INTO teachers_subjects VALUES(2,7,1,128);
INSERT INTO teachers_subjects VALUES(2,7,2,128);
INSERT INTO teachers_subjects VALUES(3,6,1,128);
INSERT INTO teachers_subjects VALUES(3,2,2,128);
INSERT INTO teachers_subjects VALUES(4,2,1,256);
INSERT INTO teachers_subjects VALUES(4,2,2,128);
INSERT INTO teachers_subjects VALUES(4,8,1,128);
INSERT INTO teachers_subjects VALUES(4,8,2,128);
INSERT INTO teachers_subjects VALUES(5,6,2,128);
INSERT INTO teachers_subjects VALUES(6,1,1,128);
INSERT INTO teachers_subjects VALUES(6,1,2,128);
INSERT INTO teachers_subjects VALUES(6,1,3,128);
INSERT INTO teachers_subjects VALUES(7,8,1,128);
INSERT INTO teachers_subjects VALUES(7,8,2,128);
INSERT INTO teachers_subjects VALUES(8,4,1,128);
INSERT INTO teachers_subjects VALUES(8,4,2,128);
INSERT INTO teachers_subjects VALUES(8,4,3,128);
INSERT INTO teachers_subjects VALUES(9,1,1,128);
INSERT INTO teachers_subjects VALUES(9,1,2,128);
INSERT INTO teachers_subjects VALUES(9,1,3,128);
INSERT INTO teachers_subjects VALUES(9,3,1,128);
INSERT INTO teachers_subjects VALUES(9,3,2,128);
INSERT INTO teachers_subjects VALUES(9,3,3,128);
INSERT INTO teachers_subjects VALUES(10,8,1,128);
INSERT INTO teachers_subjects VALUES(10,8,2,128);
INSERT INTO teachers_subjects VALUES(10,6,3,128);
INSERT INTO teachers_subjects VALUES(11,8,1,128);
INSERT INTO teachers_subjects VALUES(12,5,1,128);
INSERT INTO teachers_subjects VALUES(12,5,2,128);
INSERT INTO teachers_subjects VALUES(12,5,3,128);