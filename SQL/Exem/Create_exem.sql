create database 'exem.fdb' user 'sysdba' password 'masterkey';

create table books (
	id integer primary key,
	name varchar(200),
	page_count integer);

create table writers (
	id integer primary key,
	name varchar(200));

create table authors (
	book_id integer references books (id),
	writer_id integer references writers (id),
	unique(book_id, writer_id));

create table publications (
	id integer primary key,
	book_id integer references books (id),
	pub_year integer,
	copies integer not null);