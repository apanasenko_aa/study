create table Erdos_Number (
		id integer primary key,
		E_num integer not null);

SET TERM ^ ;
recreate procedure F_Erdos (id_Erdos integer)
returns (w_name varchar(200), Erdos_num integer)
as
declare variable k integer;
begin
    delete from ERDOS_NUMBER;
	insert into ERDOS_NUMBER values (:id_Erdos, 0);
	k = -1;
	while (:k < (select max(e.e_num) from Erdos_Number e)) do
	begin
		k = k + 1;
		insert into ERDOS_NUMBER (id, E_num)
			select distinct aut.writer_id, (:k + 1) from authors aut
			where aut.book_id in (
				select a.book_id from authors a
				where a.writer_id in (
					select e.id from Erdos_Number e
                    where e.E_num = :k))
			and aut.writer_id not in (select er.id from ERDOS_NUMBER er);
	end
	for select w.name, e.E_num from ERDOS_NUMBER e
		inner join writers w on w.id = e.id
		into w_name, Erdos_num
	do
		suspend;
END^

SET TERM ; ^

select * from F_Erdos(3) e
order by e.ERDOS_NUM;