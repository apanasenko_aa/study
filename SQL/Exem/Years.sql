select w.name, count(p.pub_year) + 1 from(
    SELECT a.id wi, p.pub_year y from authors a
        inner join publications p on p.book_id = a.book_id
) wy1
    inner join writers w on w.id = a.writer_id
    where exists (
        select p.pub_year, t.y from authors a
            inner join publications p on p.book_id = a.book_id
            inner join (
                select a.writer_id wi, p.pub_year y from authors a 
                inner join publications p on p.book_id = a.book_id
            ) t on t.wi = a.writer_id and t.wi = wy1.wi and t.y = p.pub_year + 1
    ) 
    group by w.name
    having count(p.pub_year) = max(count(p.pub_year))
