SET TERM ^ ;

create procedure F_Erdos (id_Erdos)
returns (w_name varchar(200), Erdos_num integer)
as
declare variable k integer;
begin
	create table Erdos_Number (
		id integer primary key,
		E_num integer not null);
	insert into Edros_Number values (id_Erdos, 0);
	k = -1;
	while k < (select max(e.e_num) from Erdos_Number e) do
	begin
		k = k + 1;
		insert into Edros_Number (id, E_num)
			select aut.writer_id, (k + 1) from authors aut
			while aut.book_id in (
				select a.book_id from authors a
				while a.writer_id in (
					select e.id from Erdos_Number e
                    where e.E_num = k))
			and aut.book_id not in (
				select er.id from Erdos_Number er)
	end;
	for select w.name, e.E_num from Edros_Number e
		inner join writers w on w.id = e.id
		into w_name, Erdos_num
	do
		suspend;
	drop table Edros_Number;
END^

SET TERM ; ^

select * from F_Erdos(id_Edros);