SET TERM ^ ;
recreate procedure y_count (now integer, old integer) 
returns (w_name varchar(200), y_count integer)
as
declare variable k integer;
begin
    for select distinct w.name from(
            SELECT distinct a.WRITER_ID wi, p.pub_year y from authors a
            inner join publications p on p.book_id = a.book_id) wy1
        inner join writers w on w.id = wy1.wi
        order by w.NAME
    into :w_name
	do begin
        old = 0;
        y_count = 1;
        for select wy1.y from(
                SELECT distinct a.WRITER_ID wi, p.pub_year y from authors a
                inner join publications p on p.book_id = a.book_id) wy1
            inner join writers w on w.id = wy1.wi
            where w.NAME = :w_name
            order by wy1.y
        into :now
        do begin
            if (old <> 0)
            then begin
                if (old + 1 = now)
                then k = k + 1;
                else begin 
                    if (k > y_count) then y_count = k;
                    k = 1;
                end               
            end
            else k = 1;
            old = now;
		end
		if (k > y_count) then y_count = k;
        suspend; 
    end
END^
SET TERM ; ^

select * from y_count(0,0)