#!/bin/sh
LD_PRELOAD="$LD_PRELOAD /usr/lib64/libmpi.so"
LD_PRELOAD="$LD_PRELOAD /usr/lib64/libopen-rte.so"
LD_PRELOAD="$LD_PRELOAD /usr/lib64/libopen-pal.so"

export LD_PRELOAD
mpirun -np $2 octave -q $1
