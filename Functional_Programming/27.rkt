#lang racket
(define (fold_right op init l)
  (if (null? l)
      init
      (op (car l) (fold_right op init (cdr l)))
   )
)

(fold_right (lambda (x y) 
              (
               if (list? y)
                  (append y (list x))
                  (list y x)
              )    
            ) 
            null (list 1 2 3 4 5 6 7 8 9 10)
)


(define (fold_left op init l)
  (define (iter result rest)
    (if (null? rest)
        result
        (iter (op result (car rest)) (cdr rest))))
  (iter init l)
)

(fold_left (lambda (x y) 
              (
               if (list? x)
                  (append (list y) x)
                  (list y x)
              )    
            ) 
            null (list 1 2 3 4 5 6 7 8 9 10)
)
