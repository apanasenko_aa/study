#include <mpi.h>
#include <stdio.h>
#include <math.h>
#include <iostream>

double f(double a)
{
	return (4.0 / (1.0 + a*a));
}

int main(int argc, char *argv[])
{
	const double PI = 3.141592653589793238462643;
	int proc_id, count_procs;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &count_procs);
	MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);

	while (true) {
		int n;
		double local_pi, global_pi;
		double start;

		if (!proc_id) {
			std::cout << "Enter the number of intervals:  ";
			std::cin >> n;
			start = MPI_Wtime();
		}
		MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
		if (!n) break;
		local_pi = 0.0;
		for (int i = proc_id; i < n; i += count_procs) {
			double x = (i + 0.5) / n;
			local_pi += f(x) / n;
		}
		MPI_Reduce(&local_pi, &global_pi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

		if (!proc_id)
			printf("pi ~ %.16f | eps ~ %.16f | time = %f\n", global_pi, fabs(global_pi - PI), MPI_Wtime() - start);
	}
	MPI_Finalize();
	return 0;
}
