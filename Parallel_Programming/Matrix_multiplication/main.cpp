#include <mpi.h>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <string>
#include <ctime>
#include <iostream>
#include <cassert>

using namespace std;

typedef vector<vector<int>> Matrix;

Matrix newMatrix(int m, int n)
{
	Matrix result(m, vector<int>(n));
	for (int i = 0; i < m; i++)
		for (int j = 0; j < n; j++)
			result[i][j] = 0;
	return result;
}


Matrix randMatrix(int m, int n)
{
	Matrix result(m, vector<int>(n));
	for (int i = 0; i < m; i++)
		for (int j = 0; j < n; j++)
			result[i][j] = (rand() % 5 ? 1 : -1) * (rand() % 10);
	return result;
}


Matrix transposeMatrix(Matrix& M)
{
	Matrix result(M[0].size(), vector<int>(M.size()));
	for (int i = 0; i < M[0].size(); i++)
		for (int j = 0; j < M.size(); j++)
			result[i][j] = M[j][i];
	return result;
}


Matrix operator * (Matrix& A, Matrix& B)
{
	if (A[0].size() != B.size()) {
		cout << "Error in multiplay matrix" << endl;
		return Matrix();
	}
	Matrix result(A.size(), vector<int>(B[0].size()));
	for (int i = 0; i < A.size(); i++) {
		for (int j = 0; j < B[0].size(); j++) {
			result[i][j] = 0;
			for (int k = 0; k < A[i].size(); k++) {
				result[i][j] += A[i][k] * B[k][j];
			}
		}
	}
	return result;
}


int operator * (vector<int> a, vector<int> b)
{
	if (a.size() != b.size()) {
		cout << "Error in multiplay vector" << endl;
		return 0;
	}
	int result = 0;
	for (int i = 0; i < a.size(); i++) {
		result += a[i] * b[i];
	}
	return result;
}


void print(Matrix& M, string name)
{
	cout << "Matrix " << name << endl;
	for (int i = 0; i < M.size(); i++) {
		for (int j = 0; j < M[i].size(); j++)
			cout << M[i][j] << ' ';
		cout << endl;
	}
	cout << endl;
}


int main(int argc, char *argv[])
{
	int proc_id, count_procs;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &count_procs);
	MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);
	srand(time(NULL));

	while (true) {
		Matrix A, B, C, BT;
		MPI_Status status;
		double start;
		int m = 5, n = 4, l = 6;
		if (!proc_id) {
			cout << "Enter the number M, N, L :";
			cin >> m >> n >> l;
			if (!(n * m * l)) return 0 ;
			start = MPI_Wtime();

			A = randMatrix(m, n);
			B = randMatrix(n, l);
			C = newMatrix(m, l);
			BT = transposeMatrix(B);

			for (int i = 1; i < count_procs; i++) {
				MPI_Send(&m, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
				MPI_Send(&n, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
				MPI_Send(&l, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
			}
			for (int i = 0; i < l * m; i += count_procs) {
				for (int j = 1; j < count_procs && i + j < l * m; j++) {
					MPI_Send(&A[(i + j) % m][0], n, MPI_INT, j, i + j, MPI_COMM_WORLD);
					MPI_Send(&BT[(i + j) / m][0], n, MPI_INT, j, i + j, MPI_COMM_WORLD);
				}
				C[i % m][i / m] = A[i % m] * BT[i / m];
				for (int j = 1; j < count_procs && i + j < l * m; j++) {
					MPI_Recv(&C[(i + j) % m][(i + j) / m], 1, MPI_INT, j, i + j, MPI_COMM_WORLD, &status);
				}
			}
			printf("Time = %f\n", MPI_Wtime() - start);
			//print(A, "A");
			//print(B, "B");
			//print(C, "A x B");
			//print(A * B, "A * B");
		} else {
			MPI_Recv(&m, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
			MPI_Recv(&n, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
			MPI_Recv(&l, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
			vector<int> a(n);
			vector<int> b(n);
			for (int i = proc_id; i < l * m; i += count_procs) {
				MPI_Recv(&a[0], n, MPI_INT, 0, i, MPI_COMM_WORLD, &status);
				MPI_Recv(&b[0], n, MPI_INT, 0, i, MPI_COMM_WORLD, &status);
				int result = a * b;
				MPI_Send(&result, 1, MPI_INT, 0, i, MPI_COMM_WORLD);
			}
		}
	}
	MPI_Finalize();
	return 0;
}
