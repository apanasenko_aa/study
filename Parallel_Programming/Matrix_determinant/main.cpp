#include <mpi.h>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <string>
#include <ctime>
#include <iostream>
#include <cassert>

using namespace std;

typedef vector<vector<int>> Matrix;

Matrix newMatrix(int m, int n)
{
	Matrix result(m, vector<int>(n));
	for (int i = 0; i < m; i++)
		for (int j = 0; j < n; j++)
			result[i][j] = 0;
	return result;
}


Matrix randMatrix(int m, int n)
{
	Matrix result(m, vector<int>(n));
	for (int i = 0; i < m; i++)
		for (int j = 0; j < n; j++)
			result[i][j] = (rand() % 5 ? 1 : -1) * (rand() % 10);
	return result;
}


Matrix getMinor(Matrix& M, int r, int c)
{
	Matrix result = newMatrix(M.size() - 1, M.size() - 1);
	int offset_i = 0;
	for (int i = 0; i < M.size(); i++) {
		offset_i |= i == r;
		if (i == r) continue;
		int offset_j = 0;
		for (int j = 0; j < M.size(); j++) {
			offset_j |= j == c;
			if (j == c) continue;
			result[i - offset_i][j - offset_j]= M[i][j];
		}
	}
	return result;
}


int determinant(Matrix& M)
{
	int result = 0;
	if (M.size() == 1) {
		result = M[0][0];
	} else {
		int row = 1;
		for (int col = 1; col <= M.size(); col++) {
			result += pow(-1, row + col) * M[row - 1][col - 1] * determinant(getMinor(M, row - 1, col - 1));
		}
	}
	return result;
}


vector<int> matrix2vector(Matrix& M)
{
	vector<int> result(M.size() * M.size());
	for (int i = 0; i < result.size(); i++)
		result[i] = M[i / M.size()][i % M.size()];
	return result;
}


Matrix vector2matrix(vector<int> v)
{
	int n = (int)sqrt(v.size());
	Matrix result(n, vector<int>(n));
	for (int i = 0; i < v.size(); i++)
		result[i / n][i % n] = v[i];
	return result;
}


void print(Matrix& M, string name)
{
	cout << "matrix " << name << endl;
	for (int i = 0; i < M.size(); i++) {
		for (int j = 0; j < M[i].size(); j++)
			cout << M[i][j] << ' ';
		cout << endl;
	}
	cout << endl;
}


int main(int argc, char *argv[])
{
	int proc_id, count_procs;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &count_procs);
	MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);
	srand(time(NULL));

	while (true) {
		Matrix M;
		vector<int> m;
		MPI_Status status;
		double start;
		int d, n = 4;
		if (!proc_id) {
			cout << "Enter the rang of matrix :";
			cin >> n;
			if (!n) return 0;
			start = MPI_Wtime();
			M = randMatrix(n, n);
			for (int i = 1; i < count_procs; i++) {
				MPI_Send(&n, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
			}
			int answer = 0;
			int count = pow(n - 1, 2);
			for (int i = 0; i < n; i += count_procs) {
				for (int j = 1; j < count_procs && i + j < n; j++) {
					m = matrix2vector(getMinor(M, 0, i + j));
					MPI_Send(&m[0], count, MPI_INT, j, i + j, MPI_COMM_WORLD);
				}
				answer += pow(-1.0, 2.0 + i + 0) * M[0][i] * determinant(getMinor(M, 0, i));
				for (int j = 1; j < count_procs && i + j < n; j++) {
					MPI_Recv(&d, 1, MPI_INT, j, i + j, MPI_COMM_WORLD, &status);
					answer += pow(-1.0, 2.0 + i + j) * M[0][i + j] * d;
				}
			}
			cout << "Time = " << MPI_Wtime() - start << endl << endl;
			print(M, "M");
			cout << determinant(M) << endl;
			cout << answer << endl;
		} else {
			MPI_Recv(&n, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
			for (int i = proc_id; i < n; i += count_procs) {
				m = vector<int>((int)pow(n - 1, 2));
				MPI_Recv(&m[0], m.size(), MPI_INT, 0, i, MPI_COMM_WORLD, &status);
				d = determinant(vector2matrix(m));
				MPI_Send(&d, 1, MPI_INT, 0, i, MPI_COMM_WORLD);
			}
		}
	}
	MPI_Finalize();
	return 0;
}
