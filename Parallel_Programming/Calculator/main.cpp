#include <mpi.h>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <string>
#include <ctime>
#include <iostream>
#include <cassert>
#include <fstream>

using namespace std;


int opToInt(char c)
{
	if (c == '\0') return 0;
	if (c == '(') return 1;
	if (c == '+') return 2;
	if (c == '-') return 3;
	if (c == '*') return 4;
	if (c == '/') return 5;
	if (c == ')') return 6;
}


int Bauer_Zamelzon(string text)
{
	vector<int> E;
	vector<char> T;

	int table[6][7] = {
	{ 6, 1, 1, 1, 1, 1, 5 },
	{ 5, 1, 1, 1, 1, 1, 3 },
	{ 4, 1, 2, 2, 1, 1, 4 },
	{ 4, 1, 2, 2, 1, 1, 4 },
	{ 4, 1, 4, 4, 2, 2, 4 },
	{ 4, 1, 4, 4, 2, 2, 4 }
};
	int pos = 0;
	while (1)
	{
		string digit = "";
		while (pos < text.length() && text[pos] >= '0' && text[pos] <= '9')
			digit += text[pos++];

		if (digit != "")
			E.push_back(stoi(digit));
		char topOp = T.empty() ? '\0' : T.back();
		char inOp = pos < text.length() ? text[pos] : '\0';
		int r, l, f = table[opToInt(topOp)][opToInt(inOp)];

		switch (f)
		{
		case 1:
			T.push_back(inOp);
			pos++;
			break;
		case 2:
		case 4:
			l = E.back();
			E.pop_back();
			r = E.back();
			E.pop_back();
			switch (topOp)
			{
			case '+':
				E.push_back(l + r);
				break;
			case '-':
				E.push_back(r - l);
				break;
			case '*':
				E.push_back(l * r);
				break;
			case '/':
				E.push_back(r / l);
				break;
			}
			T.pop_back();
			if (f == 2)
			{
				T.push_back(inOp);
				pos++;
			}
			break;
		case 3:
			T.pop_back();
			pos++;
			break;
		case 5:
			throw string("error");
			break;
		case 6:
			return E.back();
			break;
		}
	}
	return E.back();
}


string calcalculate(string line)
{
	try {
		return to_string(Bauer_Zamelzon(line));
	} catch (string e){
		return e;
	}
}


void print(vector<string> v){
	for (int i = 0; i < v.size(); i++) {
		cout << v[i] << endl;
	}
}


int main(int argc, char *argv[])
{
	int proc_id, count_procs;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &count_procs);
	MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);

	MPI_Status status;
	double start;
	int buffer_size;

	if (!proc_id) {
		ifstream input("input.txt");
		if (!input.is_open()) {
			cout << "File not found :(" << endl;
			MPI_Finalize();
			return 0;
		}
		start = MPI_Wtime();
		vector<string> result;
		string line;
		int lineNumber = 0;
		int countProcsWorks = 0;
		for (int i = 1; i < count_procs && !input.eof(); i++) {
			getline(input, line);
			MPI_Send(&line[0], line.size(), MPI_CHAR, i, lineNumber++, MPI_COMM_WORLD);
			countProcsWorks++;
			result.push_back("");
		}
		int id_cur_proc;
		int tag_cur_proc;
		while (countProcsWorks) {
			MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			id_cur_proc = status.MPI_SOURCE;
			tag_cur_proc = status.MPI_TAG;
			MPI_Get_count(&status, MPI_CHAR, &buffer_size);
			line = string(buffer_size, '\0');
			MPI_Recv(&line[0], buffer_size, MPI_CHAR, id_cur_proc, tag_cur_proc, MPI_COMM_WORLD, &status);
			result[tag_cur_proc] = to_string(id_cur_proc) + " " + line;
			countProcsWorks--;
			if (!input.eof()) {
				getline(input, line);
				MPI_Send(&line[0], line.size(), MPI_CHAR, id_cur_proc, lineNumber++, MPI_COMM_WORLD);
				countProcsWorks++;
				result.push_back("");
			}
		}
		cout << "Time = " << MPI_Wtime() - start << endl << endl;
		print(result);
		MPI_Abort(MPI_COMM_WORLD, 0);
	} else {
		string buffer;
		int tag_cur_proc;
		while (true) {
			buffer = "";
			MPI_Probe(0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			tag_cur_proc = status.MPI_TAG;
			MPI_Get_count(&status, MPI_CHAR, &buffer_size);
			buffer = string(buffer_size, '\0');
			MPI_Recv(&buffer[0], buffer_size, MPI_CHAR, 0, tag_cur_proc, MPI_COMM_WORLD, &status);
			buffer = calcalculate(buffer);
			MPI_Send(&buffer[0], buffer.size(), MPI_CHAR, 0, tag_cur_proc, MPI_COMM_WORLD);
		}
	}
	MPI_Finalize();
	return 0;
}
