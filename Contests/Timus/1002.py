def read():
    global inF
    if inF:
        return inF.readline().strip()
    else:
        return input().strip()


charToInt = {
    'j': '1',
    'a': '2',
    'b': '2',
    'i': '1',
    'c': '2',
    'd': '3',
    'e': '3',
    'f': '3',
    'g': '4',
    'h': '4',
    'k': '5',
    'l': '5',
    'm': '6',
    'n': '6',
    'p': '7',
    's': '7',
    'r': '7',
    't': '8',
    'u': '8',
    'v': '8',
    'w': '9',
    'x': '9',
    'y': '9',
    'o': '0',
    'q': '0',
    'z': '0'
}

inF = 0
# inF = open("input.txt", "r")
phone = read()

while phone != "-1":
    phone_dict = [[] for _ in range(len(phone))]
    checked = {}
    for i in range(int(read())):
        word = read()
        number = ""
        for c in word:
            number += charToInt[c]

        if number in checked:
            continue

        checked[number] = word

        if len(number) > len(phone):
            continue

        for j in range(len(phone) - len(number) + 1):
            if number == phone[j:len(number)+j]:
                phone_dict[j].append(len(number))

    best_solution = []
    visits = {}
    queue = [[]]
    while queue:
        solution = queue.pop(0)
        position = sum(solution)
        if position == len(phone):
            best_solution = solution
            break

        if position in visits:
            continue

        visits[position] = True

        for step in phone_dict[position]:
            queue.append(solution + [step])

    if not best_solution:
        print('No solution.')
    else:
        words = []
        for step in best_solution:
            words.append(checked[phone[:step]])
            phone = phone[step:]

        print(' '.join(words))

    phone = read()
