#include <iostream>
#include <cstdio>

using namespace std;

int main()
{
	int n;
	cin >> n;
	if ((n >= 1) and (n <= 4))
		printf("few");
	if ((n >= 5) and (n <= 9))
		printf("several");
	if ((n >= 10) and (n <= 19))
		printf("pack");
	if ((n >= 20) and (n <= 49))
		printf("lots");
	if ((n >= 50) and (n <= 99))
		printf("horde");
	if ((n >= 100) and (n <= 249))
		printf("throng");
	if ((n >= 250) and (n <= 499))
		printf("swarm");
	if ((n >= 500) and (n <= 999))
		printf("zounds");
	if (n >= 1000)
		printf("legion");
	return 0;
}