#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>

using namespace std;

vector <string> words, w_d;
string phone_num = "";
vector <int> result;

string convert_char_to_dig(string word)
{
	string res = "";
	for (int i = 0; i < word.size(); i++)
	{
		switch (word[i]) {
			case 'j' : res += "1"; break;
			case 'a' : res += "2"; break;
			case 'b' : res += "2"; break;
			case 'i' : res += "1"; break;
			case 'c' : res += "2"; break;
			case 'd' : res += "3"; break;
			case 'e' : res += "3"; break;
			case 'f' : res += "3"; break;
			case 'g' : res += "4"; break;
			case 'h' : res += "4"; break;
			case 'k' : res += "5"; break;
			case 'l' : res += "5"; break;
			case 'm' : res += "6"; break;
			case 'n' : res += "6"; break;
			case 'p' : res += "7"; break;
			case 's' : res += "7"; break;
			case 'r' : res += "7"; break;
			case 't' : res += "8"; break;
			case 'u' : res += "8"; break;
			case 'v' : res += "8"; break;
			case 'w' : res += "9"; break;
			case 'x' : res += "9"; break;
			case 'y' : res += "9"; break;
			case 'o' : res += "0"; break;
			case 'q' : res += "0"; break;
			case 'z' : res += "0"; break;
		}
	}
	return res;
}


void Find(vector<int> cur_seq, string rem_string)
{
	if (result.size() != 0 && cur_seq.size() >= result.size()) return;
	if (rem_string == "")
	{
		if (result.size() == 0 || result.size() > cur_seq.size())
			result = cur_seq;
	}
	for (int i = 0; i < w_d.size(); i++)
	{
		if (w_d[i] == rem_string.substr(0, w_d[i].length()))
		{
			cur_seq.push_back(i);
			Find(cur_seq, rem_string.substr(w_d[i].length(), rem_string.length() - w_d[i].length()));
			cur_seq.pop_back();
		}

	}

}


int main()
{
	//#ifndef ONLINE_JUDGE
 	freopen("input.txt", "r", stdin);
	//	freopen("output.txt", "w", cout);
	//#endif
	string word;
	int count_words;
	while (cin >> phone_num && phone_num != "-1")
	{
		cin >> count_words;
		for (int i = 0; i < count_words; i++)
		{
			cin >> word;
			words.push_back(word);
			w_d.push_back(convert_char_to_dig(word));
		}
		vector<int> v;
		Find(v, phone_num);
		if (result.size() > 0)
		{
			for (int i = 0; i < result.size(); i++)
			{
				cout << words[result[i]] << ' ';
			}
		} else {
			cout << "No solution.";
		}
		cout << endl;
		words.clear();
		w_d.clear();
		result.clear();
	}
	return 0;
}