#include <iostream>
#include <math.h>
#include <cstdio>
#include <vector>

using namespace std;

int main()
{
	vector <double> a;
	int n = 0;
	double k;
	while (cin >> k) {
		a.push_back(k);
		n++;
	}
	for(; n > 0; n--)
		printf("%.5f\n", pow(a[n - 1], 0.5));
	return 0;
}