﻿<?php
    $outFile = fopen('A-small-attempt2.out', 'w');
    $lines = file("A-small-attempt2.in");
    $n += $lines[0];
    $rev_str = array('a' => 'y',
                     'b' => 'h',
                     'c' => 'e',
                     'd' => 's',
                     'e' => 'o',
                     'f' => 'c',
                     'g' => 'v',
                     'h' => 'x',
                     'i' => 'd',
                     'j' => 'u',
                     'k' => 'i',
                     'l' => 'g',
                     'm' => 'l',
                     'n' => 'b',
                     'o' => 'k',
                     'p' => 'r',
                     'q' => 'z',
                     'r' => 't',
                     's' => 'n',
                     't' => 'w',
                     'u' => 'j',
                     'v' => 'p',
                     'w' => 'f',
                     'x' => 'm',
                     'y' => 'a',
                     'z' => 'q');
   	for ($i = 1; $i <= $n; $i++){
        $str = strtr($lines[$i], $rev_str);
        fwrite($outFile, "Case #$i: $str");
    }
    fclose($outFile);
?>