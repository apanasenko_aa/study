import itertools
with open('input.txt', 'r') as inFile, open('output.txt', 'w') as outFile:
	ar = []
	for i in range (0, 4) :
		ar.append(list(map(float, inFile.readline().strip().split())))
	a = [0, 1, 2, 3]
	lst = list(itertools.permutations(a))
	curmin = ar[0][0] + ar[1][1] + ar[2][2] + ar[3][3]
	ans = '1 2 3 4'
	for i in lst:
		sum = ar[0][i[0]] + ar[1][i[1]] + ar[2][i[2]] + ar[3][i[3]]
		if curmin > sum :
			ans = str(i[0] + 1) + ' ' + str(i[1] + 1) + ' ' + str(i[2] + 1) + ' ' + str(i[3] + 1)
			curmin = sum
	outFile.write(str(ans))


