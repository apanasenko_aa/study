with open('input.txt', 'r') as inFile, open('output.txt', 'w') as outFile:
	values = list(map(int, inFile.read().strip().split()))
	n = values.pop(0)
	m = values.pop(0)
	k = 0
	for i in range (1, n + 1) :
		if values.count(i) == m :
			ans = i
			k += 1
	if not(k == 1) : 
		ans = -1	
	outFile.write(str(ans))