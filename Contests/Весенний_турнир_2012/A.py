with open('input.txt', 'r') as inFile, open('output.txt', 'w') as outFile:
	(n, l, a, b) = list(map(int, inFile.readline().strip().split(' ')))
	i = 0
	if (a * n) <= l <= (b * n):
		k = l // n
		h = l % n
		outFile.write((str(k + 1) + ' ') * h)
		outFile.write((str(k) + ' ') * (n - h))
	else :
		outFile.write('0')	        
