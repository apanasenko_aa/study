program Lazarus_;
var
  f1, f2: Text;
  a: array [0..100000] of integer;
  n, m, i, k, ans: integer;
begin
	Assign(f1, 'input.txt');
	Assign(f2, 'output.txt');
  	reset(f1);
  	rewrite(f2);
  	read(f1, n, m);
  	ans := -1;
  	for i:= 1 to n do
  	begin
  		read(f1, k);
  		a[k] := a[k] + 1; 
  	end;
  	for i:= 1 to n do
  	begin
  		if a[i] = m then
  		begin
  			ans := i;
  			k := k + 1;
  		end;
  		if k = 2 then
  		begin
  			ans := -1;
  			break;
  		end;
  	end;
  	write(f2, ans);
  	Close(f1);
  	Close(f2);
end.

