﻿Список турниров
===============

* [Timus](http://acm.timus.ru/author.aspx?id=148945)
* [VK Cup 2015 (Квалификация 2)](VK_2015_qualification_2/)
	* [A. Поворот, отражение и масштабирование](VK_2015_qualification_2/A.py) ([Задачa](http://codeforces.ru/contest/523/problem/A))
	* [B. Усреднение обращений](VK_2015_qualification_2/B.py) ([Задачa](http://codeforces.ru/contest/523/problem/B))
	* [C. В поисках имени](VK_2015_qualification_2/C.py) ([Задачa](http://codeforces.ru/contest/523/problem/C))
	* [D. Статистика обработки роликов](VK_2015_qualification_2/D.cpp) ([Задачa](http://codeforces.ru/contest/523/problem/D))
* [Google Code Jam 2013](Google_Jam_2013_qualification/) (#6436)
	* [A. Tic-Tac-Toe-Tomek](Google_Jam_2013_qualification/A.py) ([Задачa](https://code.google.com/codejam/contest/2270488/dashboard#s=p0))
	* [B. Lawnmower](Google_Jam_2013_qualification/B.py) ([Задачa](https://code.google.com/codejam/contest/2270488/dashboard#s=p1))
	* [C. Fair and Square](Google_Jam_2013_qualification/C.py) ([Задачa](https://code.google.com/codejam/contest/2270488/dashboard#s=p2))
* [Google Code Jam 2012](Google_Jam_2012_qualification/) (#7248)
	* [A. Speaking in Tongues](Google_Jam_2012_qualification/A.php) ([Задачa](https://code.google.com/codejam/contest/1460488/dashboard#s=p0))
	* [B. Dancing With the Googlers](Google_Jam_2012_qualification/B.py) ([Задачa](https://code.google.com/codejam/contest/1460488/dashboard#s=p1))
	* [C. Recycled Numbers](Google_Jam_2012_qualification/C.py) ([Задачa](https://code.google.com/codejam/contest/1460488/dashboard#s=p2))
* [Весенний турнир 2012](Весенний_турнир_2012/)
	* [A. Расписные салфеточки](Весенний_турнир_2012/A.py) ([Задачa](http://imcs.dvfu.ru/cats/static/problem_text-cpid-852338.html))
	* [C. Кто не списал? (Pascal)](Весенний_турнир_2012/C.pas) [(Python)](Весенний_турнир_2012/C.py) ([Задачa](http://imcs.dvfu.ru/cats/static/problem_text-cpid-853862.html))
	* [E. Комбинированная эстафета](Весенний_турнир_2012/E.py) ([Задачa](http://imcs.dvfu.ru/cats/static/problem_text-cpid-852846.html))
* [ACM](ACM/) TODO
