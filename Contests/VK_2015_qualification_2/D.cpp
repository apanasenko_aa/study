﻿#include <functional>
#include <queue>
#include <vector>
#include <stdio.h>
#include <stdlib.h>


int main()
{
	unsigned long long int n, k;
	scanf("%llu %llu", &n, &k);
	std::priority_queue<unsigned long long int, std::vector<unsigned long long int>, std::greater<unsigned long long int> > a;
	unsigned long long int s, m, i, t;
	for (i = 0; i < k; i++) a.push(0);
	for (i = 0; i < n; i++) {
		scanf("%llu %llu", &s, &m);
		t = std::max(s, a.top()) + m;
		a.pop();
		printf("%llu\n", t);
		a.push(t);
	}
	return 0;
}
