#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main() {
	FILE *in = fopen("input.txt", "r");
	FILE *out = fopen("output.txt", "w");
	int N, M, i, w, l, max, k = 0, ans = -1;
	int players[1000];
	int fistPlay[1000];
    fscanf(in, "%d%d", &N, &M);
    memset(fistPlay, 0, sizeof(fistPlay));
    for (i = 0; i < N; i++)
        players[i] = N - 1;
	for (i = 0; i < M; i++){
		fscanf(in, "%d%d", &w, &l);
		w--;
		l--;
		players[l]--;
		if (w == 0) {
			k++;
			fistPlay[l] = 1;
		}
		if (l == 0)
			fistPlay[w] = 1;
	}
	max = players[1];
	for (i = 2; i < N; i++)
        max = players[i] > max ? players[i] : max;
	if (players[0] > max)
		ans = max + 1 - k;
	w = 0;
	if (players[0] == max) {
		for (i = 0; i < N; i++){
			if (fistPlay[i] == 1 && players[i] == max){
				w = 1;
				break;
			}
		}
   		if (w == 0)
   			ans = players[0] - k;
   	}
   	fprintf (out, "%d", ans);
   	return 0;
}
