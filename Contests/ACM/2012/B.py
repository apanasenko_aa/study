with open('input.txt', 'r') as inFile, open('output.txt', 'w') as outFile :
	a = list(map(int, inFile.read().strip().split()))
	m = max(a)
	n = min(a)
	while n != 0 :
		if n == m :
			outFile.write('1' * n)
			break
		m -= n
		if m < n :
			m, n = n, m
	outFile.write(0)