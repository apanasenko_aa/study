with open('input.txt', 'r') as inFile, open('output.txt', 'w') as outFile :
	a = list(map(int, inFile.read().strip().split()))
	n = a.pop(0)
	m = a.pop(0)
	ans = -1
	t =[]
	k = 0
	p = [n - 1 for _ in range (n)]
	for i in range (m) :
		w = a[i * 2] - 1
		l = a[i * 2 + 1] - 1
		if w == 0 : 
			k += 1
			t.append(l)
		if l == 0:
			t.append(w)
		p[l] -= 1      		
	x = p.pop(0)
	m = max(p)
	w = 0
	if x > m:
		ans = m + 1 - k 
	if x == m:
		for i in t:			
			if p[i - 1] == m :
				w = 1
				break
		if w == 0:
			ans = x - k
	outFile.write(str(ans))						