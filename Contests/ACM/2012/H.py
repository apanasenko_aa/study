with open('input.txt', 'r') as inFile, open('output.txt', 'w') as outFile :
	w, h = list(map(int, inFile.readline().strip().split()))
	mc, v, x, r = [], [], [], []
	k = 0
	n = 0
	mc.append('.' * (w + 2))
	for i in range (h) :
		mc.append('.' + inFile.readline().strip() + '.')	
	mc.append('.' * (w + 2))
	mi = [[-1 for i in range (w + 2)] for j in range (h + 2)]
	def x_1(di, dj) :
		if mc[di][dj] == '#' and mi[di][dj] == -1 : 
			x.append([di, dj])
			mi[di][dj] = 0
	def x_2(di, dj) :
		if mi[di][dj] not in r :
			r.append(mi[di][dj])
			return v[mi[di][dj]]
		return 0
	for i in range (1, h + 1) :
		for j in range (1, w + 1) :
			if (mc[i][j] == '#') and  (mi[i][j] == -1) : 
				x.append([i, j])
				while x : 
					xi, xj = x.pop(0)
					mi[xi][xj] = k
					n += 1
					x_1(xi + 1, xj)
					x_1(xi - 1, xj)
					x_1(xi, xj + 1)
					x_1(xi, xj - 1)
				v.append(n)
				n = 0
				k += 1	
	maxa, maxi, maxj = 0, 1, 1
	for i in range (1, h + 1) :
		for j in range (1, w + 1) :
			sum = 0
			r = [-1]
			if mc[i][j] == '.' :
				sum += x_2(i + 1, j)
				sum += x_2(i - 1, j)
				sum += x_2(i, j + 1)
				sum += x_2(i, j - 1)
			if sum >= maxa : 
				maxa, maxi, maxj = sum, i, j  
	outFile.write(str(maxj) + ' ' + str(maxi))
	print(v)