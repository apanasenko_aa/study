with open('input.txt', 'r') as inFile, open('output.txt', 'w') as outFile :
	k = int(inFile.readline())
	ans = 'DISAPPROVED'
	r = [0, 0]
	for i in range(2) :
		(s, R) = inFile.readline().strip().split('R')
		(a, b) = list(map(int, s.split('/')))
		R = int(R) * 12.7
		r[i] = a * b / 100 + R
	if abs(r[0] - r[1]) / r[0] * 100 <= k :
		ans = 'APPROVED'	
	outFile.write(ans)