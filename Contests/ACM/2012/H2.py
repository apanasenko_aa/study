with open('input.txt', 'r') as inFile, open('output.txt', 'w') as outFile :
    w, h = list(map(int, inFile.readline().strip().split()))
    mc, v, x = [], [], []
    k = 0
    n = 0
    mc.append('.' * (w + 2))
    for i in range (h) :
        mc.append('.' + inFile.readline().strip() + '.')    
    mc.append('.' * (w + 2))
    mi = [[-1 for i in range (w + 2)] for j in range (h + 2)]
    for i in range (1, h + 1) :
        for j in range (1, w + 1) :
            if (mc[i][j] == '#') and  (mi[i][j] == -1) : 
                x.append([i, j])
                while x :
                    xi, xj = x.pop(0)
                    mi[xi][xj] = k
                    n += 1
                    if mc[xi + 1][xj] == '#' and  mi[xi + 1][xj] == -1 : 
                        x.append([xi + 1, xj])
                    if mc[xi - 1][xj] == '#' and  mi[xi - 1][xj] == -1 : 
                        x.append([xi - 1, xj])
                    if mc[xi][xj + 1] == '#' and  mi[xi][xj + 1] == -1 : 
                        x.append([xi, xj + 1])
                    if mc[xi][xj - 1] == '#' and  mi[xi][xj - 1] == -1 : 
                        x.append([xi, xj - 1])
                v.append(n)
                n = 0
                k += 1 
    maxa = 0
    maxi = 1
    maxj = 1
    for i in range (1, h + 1) :
        for j in range (1, w + 1) :
            sum = 0
            r = [-1]
            if mc[i][j] == '.' :
                if mi[i + 1][j] not in r :
                    r.append(mi[i + 1][j])
                    sum += v[mi[i + 1][j]]
                if mi[i - 1][j] not in r :
                    r.append(mi[i - 1][j])
                    sum += v[mi[i - 1][j]]
                if mi[i][j - 1] not in r :
                    r.append(mi[i][j - 1])
                    sum += v[mi[i][j - 1]]
                if mi[i][j + 1] not in r :
                    r.append(mi[i][j + 1])
                    sum += v[mi[i][j + 1]]
            if sum > maxa : 
                maxa, maxi, maxj = sum, i, j  
    outFile.write(str(maxj) + ' ' + str(maxi))
    