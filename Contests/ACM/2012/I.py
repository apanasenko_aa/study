with open('input.txt', 'r') as inFile, open('output.txt', 'w') as outFile :
	a = list(map(int, inFile.read().strip().split()))
	n = a[0]
	s = a[1: 2 * n + 1]
	m = a[2 * n + 1]
	t = a[2 * n + 2:]
	r = []
	d = []
	ans = []
	for i in range (n) :
		r.append(int(s[i * 2]))
		d.append(int(s[i * 2 + 1]))
	r.append(0)
	d.append(0)
	for i in range (m) :
		for j in range (n):
			if (t[i] ==	r[j]) or (t[i] < r[j + 1]) : 
				k = j
				break
		ans.append(d[k] + (t[i] - r[k]) * (d[k + 1] - d[k]) // (r[k + 1] - r[k]))
	outFile.write(' '.join(map(str, ans)))						