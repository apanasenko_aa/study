def findLevel(stack : list, key : str):
	level = len(stack) - 1
	while level + 1 :
		if key in stack[level] :
			return level
		level -= 1
	return -1

with open("input.txt", "r") as inF, open("output.txt", "w") as outF:
	n  = int(inF.readline())
	stack = [{}]
	for i in range(n):
		line = inF.readline().strip()
		line_arr = line.split()
		if len(line_arr) > 1 :
			keyWord, value = line_arr
			if keyWord == "int" :
				stack[-1][value] = 0;
			elif keyWord == "print" :
				outF.write((value if value.isnumeric() else stack[findLevel(stack, value)][value]) + "\n")
			continue
		line_arr = line.split("=")
		if len(line_arr) > 1 :
			key, value = list(map(str, line_arr))
			stack[findLevel(stack, key)][key] = value if value.isnumeric() else stack[findLevel(stack, value)][value]
			continue
		if line == "{" :
			stack.append({})
			continue
		if line == "}" :
			stack.pop()
			continue