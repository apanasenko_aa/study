import math
with open("input.txt", "r") as inF, open("output.txt", "w") as outF:
	n, a, b = list(map(int, inF.read().split()))
	counts = [0] * 11
	counts[a] = n
	sum_n = n
	sum_marks = n * a
	cur_a = a
	while (cur_a < b):
		k = int(cur_a / 0.5)
		a = min(10, int(cur_a * 2))
		x = math.ceil(sum_n * (k + 1 - 2 * cur_a) / (2 * a - k  - 1) - 0.00000001)
		counts[a] += x
		sum_n += x
		sum_marks += x * a
		cur_a = sum_marks / sum_n
	outF.write(str(int(sum_n - n)))