#include "linear_sequence_assoc.h"

typedef enum {
    BLACK,
    RED
} LSQ_ColorNode;

typedef struct Node {
	LSQ_IntegerIndexT key;
	LSQ_BaseTypeT value;
	LSQ_ColorNode color;
	struct Node* left;
	struct Node* right;
	struct Node* parent;
} Node;

typedef struct Tree {
	Node* root;
	LSQ_IntegerIndexT size;
} Tree;

typedef struct Iterator {
	Tree* container;
	Node* node;
} Iterator;

static void RotateLeft (Tree*, Node*);
static void RotateRight (Tree*, Node*);
static void DestroySubTree (Node*);
static void Swap(Node*, Node*);
static void RBDelete(Tree*, Node*);
static void RBDeleteFixup(Tree*, Node*);
static Node* FindNode(Node*, LSQ_IntegerIndexT);
static Node* NodeSuccessor(Node*);
static Node* CreateNode(LSQ_IntegerIndexT, LSQ_BaseTypeT, LSQ_ColorNode, Node*);
static Iterator* CreateIterator(Tree*, Node*);

LSQ_HandleT LSQ_CreateSequence (void){
    Tree* curTree = (Tree*) malloc(sizeof(Tree));
    if (NULL == curTree){
        return LSQ_HandleInvalid;
    }
    curTree->root = NULL;
    curTree->size = 0;
    return curTree;
}

void LSQ_DestroySequence (LSQ_HandleT handle){
    if (NULL != handle){
        DestroySubTree(((Tree*)handle)->root);
        free(handle);
    }
    return;
}

LSQ_IntegerIndexT LSQ_GetSize (LSQ_HandleT handle){
    return NULL != handle ? ((Tree*)handle)->size : 0;
}

int LSQ_IsIteratorDereferencable (LSQ_IteratorT iterator){
    return (NULL != iterator && NULL != ((Iterator*)iterator)->node) && !LSQ_IsIteratorBeforeFirst(iterator) && !LSQ_IsIteratorPastRear(iterator);
}

int LSQ_IsIteratorPastRear(LSQ_IteratorT iterator){
    if (NULL == iterator){
        return NULL;
    }
    Iterator* curIterator = (Iterator*) iterator;
    return NULL != curIterator->node ? curIterator->node == curIterator->node->right : 1;
}

int LSQ_IsIteratorBeforeFirst (LSQ_IteratorT iterator){
    if (NULL == iterator){
        return NULL;
    }
    Iterator* curIterator = (Iterator*) iterator;
    return NULL != curIterator->node ? curIterator->node == curIterator->node->left : 1;
}

LSQ_BaseTypeT* LSQ_DereferenceIterator (LSQ_IteratorT iterator){
    return LSQ_IsIteratorDereferencable(iterator) ? &((Iterator*)iterator)->node->value : NULL;
}

LSQ_IntegerIndexT LSQ_GetIteratorKey (LSQ_IteratorT iterator){
    return LSQ_IsIteratorDereferencable(iterator) ? ((Iterator*)iterator)->node->key : NULL;
}

LSQ_IteratorT LSQ_GetElementByIndex(LSQ_HandleT handle, LSQ_IntegerIndexT index){
    return NULL != handle ? CreateIterator((Tree*)handle, FindNode(((Tree*)handle)->root, index)) : NULL;
}

LSQ_IteratorT LSQ_GetFrontElement(LSQ_HandleT handle){
    if (NULL == handle){
        return NULL;
    }
    Tree* curTree = (Tree*)handle;
    Node* curNode = curTree->root;
    while (NULL != curNode && curNode->left != curNode->left->left){
        curNode = curNode->left;
    }
    return CreateIterator(curTree, curNode);
}

LSQ_IteratorT LSQ_GetPastRearElement(LSQ_HandleT handle){
    if (NULL == handle){
        return NULL;
    }
    Tree* curTree = (Tree*)handle;
    Node* curNode = curTree->root;
    while (NULL != curNode && curNode != curNode->right){
        curNode = curNode->right;
    }
    return CreateIterator(curTree, curNode);
}

void LSQ_DestroyIterator(LSQ_IteratorT iterator){
    free(iterator);
    return;
}

void LSQ_AdvanceOneElement(LSQ_IteratorT iterator){
    if (NULL == iterator){
        return;
    }
    Iterator* curIterator = (Iterator*) iterator;
    if (LSQ_IsIteratorPastRear(curIterator)){
        return;
    }
    Node* curNode = curIterator->node;
    if (NULL != curNode->right){
        curNode = curNode->right;
        while (NULL != curNode->left){
            curNode = curNode->left;
        }
    } else {
        while (curNode == curNode->parent->right){
            curNode = curNode->parent;
        }
        curNode = curNode->parent;
    }
    curIterator->node = curNode;
    return;
}

void LSQ_RewindOneElement(LSQ_IteratorT iterator){
    if (NULL == iterator){
        return;
    }
    Iterator* curIterator = (Iterator*) iterator;
    if (NULL == curIterator->container->root || LSQ_IsIteratorBeforeFirst(curIterator)){
        return;
    }
    Node* curNode = curIterator->node;
    if (NULL != curNode->left){
        curNode = curNode->left;
        while (NULL != curNode->right){
            curNode = curNode->right;
        }
    } else {
        while (curNode == curNode->parent->left){
            curNode = curNode->parent;
        }
        curNode = curNode->parent;
    }
    curIterator->node = curNode;
    return;
}

void LSQ_ShiftPosition(LSQ_IteratorT iterator, LSQ_IntegerIndexT shift){
    for (; shift > 0; shift--) LSQ_AdvanceOneElement(iterator);
    for (; shift < 0; shift++) LSQ_RewindOneElement(iterator);
    return;
}

void LSQ_SetPosition(LSQ_IteratorT iterator, LSQ_IntegerIndexT pos){
    if(NULL == iterator){
        return;
    }
    Iterator* curIterator = (Iterator*) iterator;
    Node* curNode = curIterator->container->root;
    while (NULL != curNode && curNode->left != curNode->left->left){
        curNode = curNode->left;
    }
    curIterator->node = curNode;
    LSQ_ShiftPosition(curIterator, pos);
    return;
}

void LSQ_InsertElement(LSQ_HandleT handle, LSQ_IntegerIndexT key, LSQ_BaseTypeT value){
    if (NULL == handle){
        return;
    }
    Tree* curTree = (Tree*) handle;
    if (0 == curTree->size) {
        curTree->root = CreateNode(key, value, BLACK, NULL);
        curTree->root->left = CreateNode(NULL, NULL, BLACK, curTree->root);
        curTree->root->right = CreateNode(NULL, NULL, BLACK, curTree->root);
        curTree->root->left->left = curTree->root->left;
        curTree->root->right->right = curTree->root->right;
        curTree->size++;
        return;
    }
    Node* curNode = curTree->root;
    while (1){
        if (curNode->key == key) {
            curNode->value = value;
            return;
        }
        if (curNode->key > key) {
            if (NULL == curNode->left || curNode->left == curNode->left->left){
                break;
            }
            curNode = curNode->left;
        }
        if (curNode->key < key) {
            if (NULL == curNode->right || curNode->right == curNode->right->right){
                break;
            }
            curNode = curNode->right;
        }
    }
    Node* newNode = CreateNode(key, value, RED, curNode);
    if (key < curNode->key) {
        if (NULL != curNode->left) {
            curNode->left->parent = newNode;
            newNode->left = curNode->left;
        }
        curNode->left = newNode;
    } else {
        if (NULL != curNode->right) {
            curNode->right->parent = newNode;
            newNode->right = curNode->right;
        }
        curNode->right = newNode;
    }
    curTree->size++;
    Node* x = newNode;
    while (curTree->root != x && RED == x->parent->color){
        if (x->parent == x->parent->parent->left) {
            if (NULL != x->parent->parent->right && RED == x->parent->parent->right->color) {
                x->parent->parent->right->color = BLACK;
                x->parent->color = BLACK;
                x = x->parent->parent;
                x->color = RED;
            } else {
                if (x == x->parent->right) {
                    x = x->parent;
                    RotateLeft(curTree, x);
                }
                x->parent->color = BLACK;
                x->parent->parent->color = RED;
                RotateRight(curTree, x->parent->parent);
            }
        } else {
            if (NULL != x->parent->parent->left && RED == x->parent->parent->left->color){
                x->parent->parent->left->color = BLACK;
                x->parent->color = BLACK;
                x = x->parent->parent;
                x->color = RED;
            } else {
                if (x == x->parent->left){
                    x = x->parent;
                    RotateRight(curTree, x);
                }
                x->parent->color = BLACK;
                x->parent->parent->color = RED;
                RotateLeft(curTree, x->parent->parent);
            }
        }
    }
    curTree->root->color = BLACK;
    return;
}

void LSQ_DeleteFrontElement(LSQ_HandleT handle){
    LSQ_IteratorT iterator = LSQ_GetFrontElement(handle);
    LSQ_DeleteElement(handle, LSQ_GetIteratorKey(iterator));
    LSQ_DestroyIterator(iterator);
    return;
}

void LSQ_DeleteRearElement(LSQ_HandleT handle){
    LSQ_IteratorT iterator = LSQ_GetPastRearElement(handle);
    LSQ_RewindOneElement(iterator);
    LSQ_DeleteElement(handle, LSQ_GetIteratorKey(iterator));
    LSQ_DestroyIterator(iterator);
    return;
}

void LSQ_DeleteElement(LSQ_HandleT handle, LSQ_IntegerIndexT key){
    if (NULL == handle){
        return;
    }
    Tree* curTree = (Tree*) handle;
    Iterator* curIterator = (Iterator*) LSQ_GetElementByIndex(curTree, key);
    if (NULL == curIterator->node){
        return;
    }
    RBDelete(curTree, curIterator->node);
    LSQ_DestroyIterator(curIterator);
    return;
}

static void RotateLeft (Tree* handle, Node* x){
    Node* y = x->right;
    x->right = y->left;
    if (NULL != y->left){
        y->left->parent = x;
    }
    y->parent = x->parent;
    if (NULL == x->parent){
        handle->root = y;
    } else {
        Swap(x, y);
    }
    y->left = x;
    x->parent = y;
    return;
}

static void RotateRight (Tree* handle, Node* x){
    Node* y = x->left;
    x->left = y->right;
    if (NULL != y->right){
        y->right->parent = x;
    }
    y->parent = x->parent;
    if (NULL == x->parent) {
        handle->root = y;
    } else {
        Swap(x, y);
    }
    y->right = x;
    x->parent = y;
    return;
}

static void Swap(Node* x, Node* y){
    if (x == x->parent->left){
        x->parent->left = y;
    } else {
        x->parent->right = y;
    }
    return;
}

static void DestroySubTree (Node* root){
    if (NULL == root || root->left == root || root->right == root){
        free(root);
        return;
    }
    DestroySubTree(root->left);
    DestroySubTree(root->right);
    free(root);
    return;
}

static Iterator* CreateIterator(Tree* curTree, Node* curNode){
    Iterator* curIterator = (Iterator*) malloc(sizeof(Iterator));
    if (NULL == curIterator){
        return NULL;
    }
    curIterator->container = curTree;
    curIterator->node = curNode;
    return curIterator;
}

static Node* FindNode(Node* root, LSQ_IntegerIndexT key){
    if (NULL == root || root == root->left || root == root->right){
        return NULL;
    }
    if (NULL != root && root->key != key) {
        root = key > root->key ? FindNode(root->right, key) : FindNode(root->left, key);
    }
    return root;
}

static Node* CreateNode(LSQ_IntegerIndexT key, LSQ_BaseTypeT value, LSQ_ColorNode color, Node* parent){
    Node* result = (Node*)malloc(sizeof(Node));
    if (NULL == result){
        return NULL;
    }
    result->key = key;
    result->value = value;
    result->color = color;
    result->parent = parent;
    result->left = NULL;
    result->right = NULL;
    return result;
}

static Node* NodeSuccessor(Node* x){
    if (x->right == x->right->right){
        x = x->left;
        while (NULL != x->right){
            x = x->right;
        }
    } else {
        x = x->right;
        while (NULL != x->left){
            x = x->left;
        }
    }
    return x;
}

static void RBDelete(Tree* handle, Node* z){
    handle->size--;
    if (0 == handle->size){
        free(handle->root->left);
        free(handle->root->right);
        free(handle->root);
        handle->root = NULL;
        return;
    }
    Node *y = NULL;
    Node *x = NULL;
    y = (NULL == z->left || NULL == z->right) ? z : NodeSuccessor(z);
    int isCreateX = (NULL == y->left && NULL == y->right);
    x = isCreateX ? CreateNode(NULL, NULL, BLACK, y) : (NULL != y->left) ? y->left : y->right;
    x->parent = y->parent;
    if (NULL == y->parent){
        handle->root = x;
    } else {
        Swap(y, x);
    }
    z->key = y->key;
    z->value = y->value;
    if (BLACK == y->color) {
        RBDeleteFixup(handle, x);
    }
    if (isCreateX){
        Swap(x, NULL);
        free(x);
    }
    free(y);
    return;
}

static void RBDeleteFixup(Tree* handle, Node* x){
    Node* w = NULL; //brother x
    while (x != handle->root && BLACK == x->color){
        if (x == x->parent->left){
            w = x->parent->right;
            if (RED == w->color){
                w->color = BLACK;
                x->parent->color = RED;
                RotateLeft(handle, x->parent);
                w = x->parent->right;
            }
            if ((NULL == w->left || BLACK == w->left->color) && (NULL == w->right || BLACK == w->right->color)) {
                w->color = RED;
                x = x->parent;
            } else {
                if (NULL == w->right || BLACK == w->right->color) {
                    w->left->color = BLACK;
                    w->color = RED;
                    RotateRight(handle, w);
                    w = x->parent->right;
                }
                w->color = x->parent->color;
                x->parent->color = BLACK;
                w->right->color = BLACK;
                RotateLeft(handle, x->parent);
                x = handle->root;
            }
        } else {
            w = x->parent->left;
            if (RED == w->color){
                w->color = BLACK;
                x->parent->color = RED;
                RotateRight(handle, x->parent);
                w = x->parent->left;
            }
            if ((NULL == w->left || BLACK == w->left->color) && (NULL == w->right || BLACK == w->right->color)) {
                w->color = RED;
                x = x->parent;
            } else {
                if (NULL == w->left || BLACK == w->left->color) {
                    w->right->color = BLACK;
                    w->color = RED;
                    RotateLeft(handle, w);
                    w = x->parent->left;
                }
                w->color = x->parent->color;
                x->parent->color = BLACK;
                w->left->color = BLACK;
                RotateRight(handle, x->parent);
                x = handle->root;
            }
        }
    }
    x->color = BLACK;
    return;
}
