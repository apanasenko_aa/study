// #include <stdlib.h>
#include <assert.h>
//#include "linear_sequence_assoc.h"
#include "RB_Tree.c"

int main() {
    LSQ_HandleT h = LSQ_CreateSequence();
    assert(LSQ_GetSize(h) == 0);
    LSQ_IteratorT i = LSQ_GetFrontElement(h);
    assert(LSQ_IsIteratorPastRear(i));
    LSQ_DestroyIterator(i);
    LSQ_InsertElement(h, 1, 2);
    i = LSQ_GetElementByIndex(h, 1);
    assert(LSQ_IsIteratorDereferencable(i));
    LSQ_BaseTypeT *p = LSQ_DereferenceIterator(i);
    assert(*p == 2);
    assert(LSQ_GetIteratorKey(i) == 1);
    LSQ_DestroyIterator(i);
    i = LSQ_GetPastRearElement(h);
    assert(!LSQ_IsIteratorDereferencable(i));
    assert(LSQ_IsIteratorPastRear(i));
    LSQ_RewindOneElement(i);
    assert(LSQ_IsIteratorDereferencable(i));
    LSQ_RewindOneElement(i);
    assert(LSQ_IsIteratorBeforeFirst(i));
    LSQ_DestroyIterator(i);
    LSQ_DestroySequence(h);

   //----------------------------------------------------------------

    h = LSQ_CreateSequence();

    LSQ_InsertElement(h, 1, 1);
    LSQ_InsertElement(h, 2, 2);
    i = LSQ_GetPastRearElement(h);
    LSQ_RewindOneElement(i);
    p = LSQ_DereferenceIterator(i);
    assert(*p == 2);
    assert(LSQ_GetIteratorKey(i) == 2);
    LSQ_RewindOneElement(i);
    assert(LSQ_IsIteratorDereferencable(i));
    p = LSQ_DereferenceIterator(i);
    assert(*p == 1);
    assert(LSQ_GetIteratorKey(i) == 1);
    LSQ_RewindOneElement(i);
    assert(LSQ_IsIteratorBeforeFirst(i));
    LSQ_DestroyIterator(i);

    LSQ_InsertElement(h, 0, 0);
    i = LSQ_GetFrontElement(h);
    LSQ_ShiftPosition(i, 2);
    p = LSQ_DereferenceIterator(i);
    assert(*p == 2);
    assert(LSQ_GetIteratorKey(i) == 2);
    LSQ_SetPosition(i, 0);
    p = LSQ_DereferenceIterator(i);
    assert(*p == 0);
    assert(LSQ_GetIteratorKey(i) == 0);
    LSQ_SetPosition(i, 1);
    p = LSQ_DereferenceIterator(i);
    assert(*p == 1);
    assert(LSQ_GetIteratorKey(i) == 1);
    LSQ_DestroyIterator(i);

    assert(LSQ_GetSize(h) == 3);
    LSQ_DestroySequence(h);

   //--------------------------------------------

    h = LSQ_CreateSequence();
    LSQ_InsertElement(h, 1, 1);
    LSQ_DeleteElement(h, 1);
    i = LSQ_GetFrontElement(h);
    assert(LSQ_IsIteratorPastRear(i));
    LSQ_DestroyIterator(i);
    assert(!LSQ_GetSize(h));
    LSQ_DestroySequence(h);

    //---------------------------------------------
    h = LSQ_CreateSequence();
    LSQ_InsertElement(h, 1, 1);
    LSQ_InsertElement(h, 666, 666);
    LSQ_InsertElement(h, 5, 5);
    LSQ_InsertElement(h, 43, 43);

    i = LSQ_GetFrontElement(h);
    LSQ_RewindOneElement(i);
    assert(LSQ_IsIteratorBeforeFirst(i));
    LSQ_AdvanceOneElement(i);
    assert(LSQ_IsIteratorDereferencable(i));
    p = LSQ_DereferenceIterator(i);
    assert(*p == 1);
    LSQ_AdvanceOneElement(i);
    assert(LSQ_IsIteratorDereferencable(i));
    p = LSQ_DereferenceIterator(i);
    assert(*p == 5);
    LSQ_AdvanceOneElement(i);
    assert(LSQ_IsIteratorDereferencable(i));
    p = LSQ_DereferenceIterator(i);
    assert(*p == 43);
    LSQ_AdvanceOneElement(i);
    assert(LSQ_IsIteratorDereferencable(i));
    p = LSQ_DereferenceIterator(i);
    assert(*p == 666);
    LSQ_DestroyIterator(i);
    LSQ_DeleteElement(h, 43);
    LSQ_DeleteElement(h, 666);
    i = LSQ_GetPastRearElement(h);
    LSQ_InsertElement(h, 0, 0);
    LSQ_InsertElement(h, -1, -1);
    LSQ_InsertElement(h, 6, 6);
    LSQ_DeleteElement(h, 0);
    LSQ_DeleteElement(h, 5);
    LSQ_DeleteElement(h, 1);
    i = LSQ_GetFrontElement(h);
    assert(LSQ_IsIteratorDereferencable(i));
    p = LSQ_DereferenceIterator(i);
    assert(*p == -1);
    LSQ_AdvanceOneElement(i);
    assert(LSQ_IsIteratorDereferencable(i));
    p = LSQ_DereferenceIterator(i);
    assert(*p == 6);
    LSQ_AdvanceOneElement(i);
    assert(LSQ_IsIteratorPastRear(i));
    LSQ_RewindOneElement(i);
    assert(LSQ_IsIteratorDereferencable(i));
    int j;
    for (j = 0; j < 666; j++) LSQ_RewindOneElement(i);
    LSQ_AdvanceOneElement(i);
    assert(LSQ_IsIteratorDereferencable(i));
    LSQ_DestroyIterator(i);
    LSQ_DeleteElement(h, -1);
    LSQ_DeleteElement(h, 6);
    LSQ_DeleteElement(h, 666);
    LSQ_InsertElement(h, 666, 666);
    LSQ_InsertElement(h, 333, 333);
    LSQ_InsertElement(h, 111, 111);
    LSQ_InsertElement(h, 444, 444);
    LSQ_DeleteElement(h, 333);
    i = LSQ_GetFrontElement(h);
    assert(LSQ_IsIteratorDereferencable(i));
    p = LSQ_DereferenceIterator(i);
    assert(*p == 111);
    LSQ_AdvanceOneElement(i);
    assert(LSQ_IsIteratorDereferencable(i));
    p = LSQ_DereferenceIterator(i);
    assert(*p == 444);
    LSQ_AdvanceOneElement(i);
    assert(LSQ_IsIteratorDereferencable(i));
    p = LSQ_DereferenceIterator(i);
    assert(*p == 666);
    LSQ_DestroyIterator(i);
    LSQ_DeleteElement(h, 666);
    i = LSQ_GetFrontElement(h);
    assert(LSQ_IsIteratorDereferencable(i));
    p = LSQ_DereferenceIterator(i);
    assert(*p == 111);
    LSQ_AdvanceOneElement(i);
    assert(LSQ_IsIteratorDereferencable(i));
    p = LSQ_DereferenceIterator(i);
    assert(*p == 444);
    LSQ_DestroyIterator(i);
    LSQ_DestroySequence(h);

    //---------------------

    h = LSQ_CreateSequence();
    for (j = 0; j < 1000; j++) LSQ_InsertElement(h, j, j);
    assert(LSQ_GetSize(h) == 1000);
    for (j = 0; j < 1000; j++) LSQ_DeleteElement(h, j);
    assert(!LSQ_GetSize(h));
    for (j = 100; j > 0; j--) LSQ_InsertElement(h, j, j);
    assert(LSQ_GetSize(h) == 100);
    for (j = 100; j > 0; j--) LSQ_DeleteElement(h, j);
    assert(!LSQ_GetSize(h));
    i = LSQ_GetFrontElement(h);
    assert(LSQ_IsIteratorPastRear(i));
    LSQ_DeleteRearElement(h);
    LSQ_InsertElement(h, 2, 2);
    LSQ_InsertElement(h, 1, 1);
    LSQ_DeleteRearElement(h);
    i = LSQ_GetFrontElement(h);
    assert(LSQ_IsIteratorDereferencable(i));
    p = LSQ_DereferenceIterator(i);
    assert(*p == 1);
    LSQ_DestroyIterator(i);
    i = LSQ_GetElementByIndex(h, 6666);
    assert(LSQ_IsIteratorPastRear(i));
    LSQ_DestroyIterator(i);
    LSQ_DestroySequence(h);

  //----------------------------------------------
    int k;
    for (k = 0; k < 10; k++) {
        h = LSQ_CreateSequence();
        int cnt = 0;
        for (j = 0; j < 1000; j++) {
            int ind;
            LSQ_IteratorT ii = LSQ_GetElementByIndex(h, ind = rand());
            if (LSQ_IsIteratorPastRear(ii)) cnt++;
            LSQ_DestroyIterator(ii);
            LSQ_InsertElement(h, ind, 0);
            assert(LSQ_GetSize(h) == cnt);
        }
        if (cnt > 1) {
            i = LSQ_GetFrontElement(h);
            int pkey = LSQ_GetIteratorKey(i);
            LSQ_AdvanceOneElement(i);
            for (j = 1; j < cnt; j++) {
                assert(LSQ_IsIteratorDereferencable(i));
                int key = LSQ_GetIteratorKey(i);
                assert(key > pkey);
                LSQ_AdvanceOneElement(i);
            }
            LSQ_IsIteratorPastRear(i);
            LSQ_DestroyIterator(i);
        }
        LSQ_DestroySequence(h);
    }

 //----------------------------------

    h = LSQ_CreateSequence();
    int a[10000];
    int x, y, z = 0;
    x = 2;
    srand(time(NULL));
    for (j = 0; j < 10000; j++){
        x = (x * 29) % 30011;
        if (!LSQ_IsIteratorDereferencable(LSQ_GetElementByIndex(h, x))){
            a[LSQ_GetSize(h)] = x;
            z++;
            LSQ_InsertElement(h, a[j], a[j]);}
    }
    assert(z == LSQ_GetSize(h));
    for (j = 0; j < z; j++){
        y = rand() % z;
        LSQ_DeleteElement(h, a[y]);
        }
    printf("%d",LSQ_GetSize(h));
    LSQ_DestroySequence(h);

    //==================== case  7 8;

    LSQ_InsertElement(h, 1, 1);
    LSQ_InsertElement(h, 10, 10);
    LSQ_InsertElement(h, 2, 2);
    LSQ_InsertElement(h, 8, 8);
    LSQ_InsertElement(h, 3, 3);
    LSQ_InsertElement(h, 5, 5);
    LSQ_InsertElement(h, 6, 6);
    LSQ_InsertElement(h, 7, 7);
    LSQ_InsertElement(h, 4, 4);
    LSQ_InsertElement(h, 9, 9);
    LSQ_DeleteElement(h, 1);
    LSQ_DeleteElement(h, 10);
    LSQ_DeleteElement(h, 2);
    LSQ_DeleteElement(h, 4);
    LSQ_DeleteElement(h, 8);


    return EXIT_SUCCESS;
}
