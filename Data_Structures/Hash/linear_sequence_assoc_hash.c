#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include "linear_sequence_assoc_hash.h"

//static const int TABLE_SIZE = 256;
#define TABLE_SIZE 256
typedef struct Node {
    LSQ_BaseTypeT value;
    LSQ_KeyT key;
    struct Node* next;
} Node;

typedef struct Hash {
    LSQ_SizeT size;
    Node* keys[TABLE_SIZE];
    LSQ_Callback_CloneFuncT* F_valueClone;
    LSQ_Callback_CloneFuncT* F_keyClone;
    LSQ_Callback_CompareFuncT* F_Compare;
    LSQ_Callback_SizeFuncT* F_Size;
} Hash;

typedef struct Iterator {
    Hash* handle;
    Node* element;
    int index;
} Iterator;



static uint_least32_t Crc32(unsigned char *buf, size_t len)
{
    uint_least32_t crc_table[TABLE_SIZE];
    uint_least32_t crc;
    int i;
    int j;
    for (i = 0; i < TABLE_SIZE; i++){
        crc = i;
        for (j = 0; j < 8; j++)
            crc = crc & 1 ? (crc >> 1) ^ 0xEDB88320UL : crc >> 1;
        crc_table[i] = crc;
    };
    crc = 0xFFFFFFFFUL;
    while (len--)
        crc = crc_table[(crc ^ *buf++) & 0xFF] ^ (crc >> 8);
    return crc ^ 0xFFFFFFFFUL;
}



LSQ_HandleT LSQ_CreateSequence(LSQ_Callback_CloneFuncT keyCloneFunc, LSQ_Callback_SizeFuncT keySizeFunc,
                               LSQ_Callback_CompareFuncT keyCompFunc, LSQ_Callback_CloneFuncT valCloneFunc)
{
    int i;
    Hash* container = (Hash*) malloc(sizeof(Hash));
    container->size = 0;
    for (i = 0; i < TABLE_SIZE; i++)
        container->keys[i] = NULL;
    container->F_keyClone = keyCloneFunc;
    container->F_Compare = keyCompFunc;
    container->F_Size = keySizeFunc;
    container->F_valueClone = valCloneFunc;
    return container;
}


static void DestroyKeyNode(Node* header)
{
    Node* node = (Node*) header;
    if (node->next != NULL)
        DestroyKeyNode(node->next);
    free(node);
    return;
}



void LSQ_DestroySequence(LSQ_HandleT handle)
{
    int i;
    Hash* container = (Hash*) handle;
    for (i = 0; i < TABLE_SIZE; i++){
        DestroyKeyNode(container->keys[i]);
        container->keys[i] = NULL;
    }
    container->size = NULL;
    container->F_keyClone = NULL;
    container->F_Compare = NULL;
    container->F_Size = NULL;
    container->F_valueClone = NULL;
    free(container);
    return;
}



LSQ_SizeT LSQ_GetSize(LSQ_HandleT handle)
{
    Hash* container = (Hash*) handle;
    return container->size;
}


int LSQ_IsIteratorDereferencable(LSQ_IteratorT iterator)
{
    Iterator* curIter = (Iterator*) iterator;
    return (curIter->index >= 0) && (curIter->index < TABLE_SIZE);
}


int LSQ_IsIteratorPastRear(LSQ_IteratorT iterator)
{
    Iterator* curIter = (Iterator*) iterator;
    return curIter->index == TABLE_SIZE;
}


int LSQ_IsIteratorBeforeFirst(LSQ_IteratorT iterator)
{
    Iterator* curIter = (Iterator*) iterator;
    return curIter->index == -1;
}


LSQ_BaseTypeT LSQ_DereferenceIterator(LSQ_IteratorT iterator)
{
    Iterator* curIter = (Iterator*) iterator;
    return curIter->element->value;
}


LSQ_KeyT LSQ_GetIteratorKey(LSQ_IteratorT iterator)
{
    Iterator* curIter = (Iterator*) iterator;
    return curIter->element->key;
}


LSQ_IteratorT LSQ_GetElementByIndex(LSQ_HandleT handle, LSQ_KeyT key)
{
    Iterator* curIter = (Iterator*) malloc(sizeof(Iterator));
    Hash* curHash = (Hash*) handle;
    curIter->handle = curHash;
    curIter->index = Crc32(key, curHash->F_Size(key)) % TABLE_SIZE;
    curIter->element = curHash->keys[curIter->index];
    while (curIter->element != NULL && curHash->F_Compare(key, curIter->element->key))
        curIter->element = curIter->element->next;
    if (curIter->element != NULL)
        return curIter;
    else
        return LSQ_GetPastRearElement(curHash);
}


LSQ_IteratorT LSQ_GetFrontElement(LSQ_HandleT handle)
{
    Iterator* curIter = (Iterator*) malloc(sizeof(Iterator));
    Hash* curHash = (Hash*) handle;
    curIter->handle = curHash;
    curIter->element = NULL;
    if (LSQ_GetSize(curHash) == 0)
        curIter->index = TABLE_SIZE;
    else{
        curIter->index = -1;
        LSQ_AdvanceOneElement(curIter);
    }
    return curIter;
}


LSQ_IteratorT LSQ_GetPastRearElement(LSQ_HandleT handle)
{
    Iterator* curIter = (Iterator*) malloc(sizeof(Iterator));
    Hash* curHash = (Hash*) handle;
    curIter->handle = curHash;
    curIter->element = NULL;
    curIter->index = TABLE_SIZE;
    return curIter;
}


void LSQ_DestroyIterator(LSQ_IteratorT iterator)
{
    free(iterator);
    return;
}


void LSQ_AdvanceOneElement(LSQ_IteratorT iterator)
{
    Iterator* curIter = (Iterator*) iterator;
    if (curIter->element->next != NULL)
        curIter->element = curIter->element->next;
    else{
        int index;
        index = curIter->index + 1;
        while (curIter->handle->keys[index] != NULL && index != curIter->index)
            index = (index++) % TABLE_SIZE;
        curIter->element = curIter->handle->keys[index];
        curIter->index = index;
    }
    return;
}


void LSQ_InsertElement(LSQ_HandleT handle, LSQ_KeyT key, LSQ_BaseTypeT value)
{
    Iterator* curIter = (Iterator*) LSQ_GetElementByIndex(handle, key);
	if(LSQ_IsIteratorDereferencable(curIter)) {
		free (curIter->element->value);
		curIter->element->value = curIter->handle->F_valueClone(value);
	}
	else {
		Node* node = (Node*) malloc(sizeof(Node));
		Hash* container = (Hash*) handle;
        int index = Crc32(key, container->F_Size(key)) % TABLE_SIZE;
		node->key = curIter->handle->F_keyClone(key);
		node->value = curIter->handle->F_valueClone(value);
		node->next = curIter->handle->keys[index];
		curIter->handle->keys[index] = node;
		++curIter->handle->size;
	}
	LSQ_DestroyIterator(curIter);
    return;
}

void LSQ_DeleteElement(LSQ_HandleT handle, LSQ_KeyT key)
{
	Iterator* curIter = (Iterator*) LSQ_GetElementByIndex(handle, key);
	if (LSQ_IsIteratorDereferencable(curIter)) {
      Node* prev = curIter->handle->keys[curIter->index];
      if (prev == curIter->element)
         curIter->handle->keys[curIter->index] = curIter->element->next;
      else {
         while (prev->next != curIter->element)
            prev = prev->next;
         prev->next = curIter->element->next;
      }
      free(curIter->element->key);
      free(curIter->element->value);
      free(curIter->element);
      --curIter->handle->size;
	}
	LSQ_DestroyIterator(curIter);
    return;
}

