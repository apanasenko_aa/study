#include <stdio.h>
#include <stdlib.h>
#include "linear_sequence.h"

typedef struct Node {
    LSQ_BaseTypeT value;
    struct Node* next;
    struct Node* previous;
} Node;

typedef struct List {
    Node* first;
    Node* last;
} List;

typedef struct Iterator {
    Node* item;
} Iterator;


LSQ_HandleT LSQ_CreateSequence(void)
{
    List* result = (List*) malloc (sizeof (List));
    Node* firstItem = (Node*) malloc (sizeof (Node));
    Node* lastItem = (Node*) malloc (sizeof (Node));
    if ((result == NULL) || (firstItem == NULL) || (lastItem == NULL)) {
        free(result);
        free(firstItem);
        free(lastItem);
        return LSQ_HandleInvalid;
    }
    firstItem->previous = NULL;
    lastItem->next = NULL;
    firstItem->next = lastItem;
    lastItem->previous = firstItem;
    result->first = firstItem;
    result->last = lastItem;
    return result;
}


void LSQ_DestroySequence(LSQ_HandleT handle)
{
    if (handle == LSQ_HandleInvalid) return;
    List* curList = (List*) handle;
    while (curList->first != curList->last){
        Node* curItem = curList->first;
        curList->first = curList->first->next;
        free(curItem);
    }
    free(curList->last);
    free(curList);
    curList = LSQ_HandleInvalid;
    return;
}


LSQ_IntegerIndexT LSQ_GetSize(LSQ_HandleT handle)
{
    LSQ_IntegerIndexT count = 0;
    if (handle != LSQ_HandleInvalid){
        List* curList = (List*) handle;
        Node* temp = curList->first;
        while (temp->next != curList->last){
            count++;
            temp = temp->next;
        }
    }
    return count;
}


int LSQ_IsIteratorDereferencable(LSQ_IteratorT iterator)
{
    if (iterator == NULL) return 0;
    Iterator* curIterator = (Iterator*) iterator;
    return curIterator->item != NULL && curIterator->item->next != NULL && curIterator->item->previous != NULL;
}


int LSQ_IsIteratorPastRear(LSQ_IteratorT iterator)
{
    if (iterator == NULL) return 0;
    Iterator* curIterator = (Iterator*) iterator;
    return curIterator->item->next == NULL;
}


int LSQ_IsIteratorBeforeFirst(LSQ_IteratorT iterator)
{
    if (iterator == NULL) return 0;
    Iterator* curIterator = (Iterator*) iterator;
    return curIterator->item->previous == NULL;
}


LSQ_BaseTypeT* LSQ_DereferenceIterator(LSQ_IteratorT iterator)
{
    if (!LSQ_IsIteratorDereferencable(iterator)) return NULL;
    Iterator* curIterator = (Iterator*) iterator;
    return &curIterator->item->value;
}


LSQ_IteratorT LSQ_GetElementByIndex(LSQ_HandleT handle, LSQ_IntegerIndexT index)
{
    Iterator* result = (Iterator*) malloc(sizeof(Iterator));
    if (handle == LSQ_HandleInvalid || result == NULL) return NULL;
    List* curList = (List*) handle;
    Node* curItem = curList->first;
    LSQ_IntegerIndexT i;
    for (i = 0; i <= index; i++)
        curItem = curItem->next;
    result->item = curItem;
    return result;
}


LSQ_IteratorT LSQ_GetFrontElement(LSQ_HandleT handle)
{
    Iterator* result = (Iterator*) malloc(sizeof(Iterator));
    List* curList = (List*) handle;
    result->item = curList->first->next;
    return result;
}


LSQ_IteratorT LSQ_GetPastRearElement(LSQ_HandleT handle)
{
    Iterator* result = (Iterator*) malloc(sizeof(Iterator));
    List* curList = (List*) handle;
    result->item = curList->last;
    return result;
}


void LSQ_DestroyIterator(LSQ_IteratorT iterator)
{
    free(iterator);
    return;
}


void LSQ_AdvanceOneElement(LSQ_IteratorT iterator)
{
    Iterator* curIterator = (Iterator*) iterator;
    curIterator->item = curIterator->item->next;
    return;
}


void LSQ_RewindOneElement(LSQ_IteratorT iterator)
{
    Iterator* curIterator = (Iterator*) iterator;
    curIterator->item = curIterator->item->previous;
    return;
}


void LSQ_ShiftPosition(LSQ_IteratorT iterator, LSQ_IntegerIndexT shift)
{
    LSQ_IntegerIndexT i;
    for (i = 1; i <= abs(shift); i++)
        if (shift > 0)
            LSQ_AdvanceOneElement(iterator);
        else LSQ_RewindOneElement(iterator);
    return;
}


void LSQ_SetPosition(LSQ_IteratorT iterator, LSQ_IntegerIndexT pos)
{
    Iterator* result = (Iterator*) iterator;
    LSQ_IntegerIndexT i;
    while (result->item->previous != NULL)
        LSQ_RewindOneElement(iterator);
    for (i = 0; i <= pos; i++)
        LSQ_AdvanceOneElement(iterator);
    return;
}


void LSQ_InsertFrontElement(LSQ_HandleT handle, LSQ_BaseTypeT element)
{
    LSQ_IteratorT iterator = LSQ_GetFrontElement(handle);
    LSQ_InsertElementBeforeGiven(iterator, element);
    LSQ_DestroyIterator(iterator);
    return;
}


void LSQ_InsertRearElement(LSQ_HandleT handle, LSQ_BaseTypeT element)
{
    LSQ_IteratorT iterator = LSQ_GetPastRearElement(handle);
    LSQ_InsertElementBeforeGiven(iterator, element);
    LSQ_DestroyIterator(iterator);
    return;
}


void LSQ_InsertElementBeforeGiven(LSQ_IteratorT iterator, LSQ_BaseTypeT newElement)
{
    if ((iterator == NULL) || (LSQ_IsIteratorBeforeFirst(iterator))) return;
    Iterator* curIterator = (Iterator*) iterator;
    Node* curItem = (Node*) malloc(sizeof(Node));
    if (curItem == NULL) return;
    curItem->value = newElement;
    curItem->next = curIterator->item;
    curItem->previous = curIterator->item->previous;
    curIterator->item->previous->next = curItem;
    curIterator->item->previous = curItem;
    curIterator->item = curItem;
    return;
}


void LSQ_DeleteFrontElement(LSQ_HandleT handle)
{
    LSQ_IteratorT curIterator = LSQ_GetFrontElement(handle);
    LSQ_DeleteGivenElement(curIterator);
    LSQ_DestroyIterator(curIterator);
    return;
}


void LSQ_DeleteRearElement(LSQ_HandleT handle)
{
    LSQ_IteratorT curIterator = LSQ_GetPastRearElement(handle);
    LSQ_RewindOneElement(curIterator);
    LSQ_DeleteGivenElement(curIterator);
    LSQ_DestroyIterator(curIterator);
    return;
}


void LSQ_DeleteGivenElement(LSQ_IteratorT iterator)
{
    if ((iterator == NULL) || (!LSQ_IsIteratorDereferencable(iterator))) return;
    Iterator* curIterator = (Iterator*) iterator;
    LSQ_AdvanceOneElement(curIterator);
    curIterator->item->previous = curIterator->item->previous->previous;
    free(curIterator->item->previous->next);
    curIterator->item->previous->next = NULL;
    curIterator->item->previous->next = curIterator->item;
    return;
}