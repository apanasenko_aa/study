#include <stdio.h>
#include <stdlib.h>
#include "BitSet.h"
#include "BitSet.c"

int main()
{
	BitSetT bs = BS_Create(40);
	printf("%d\n", bs);
	BS_Set(bs, 0);
	BS_Set(bs, 2);

	if (BS_isExist(bs, 2)) printf("2 bit exists\n");
	BS_Set(bs, 3);
	BS_Set(bs, 8);
	BS_Set(bs, 15);
	BS_Set(bs, 25);
	BS_Set(bs, 38);
	BS_Print(bs);
	//BS_Print(bs);
	BS_Remove(bs, 25);
	BS_Print(bs);
	BS_Destroy(&bs);
	if (bs == BS_HandleInvalid) printf("BS killed!\n");
	bs = BS_Create(50);
	BS_SetRange(bs, 6, 12);
	BS_SetRange(bs, 16, 44);
	BS_Print(bs);
	BS_RemoveRange(bs, 10, 20);
	BS_Print(bs);
	BitSetT bs2 = BS_Create(5);
	BS_SetRange(bs2, 1, 4);
	BS_Print(bs2);
	BitSetT bs3 = BS_Merge(bs,bs2);
	BS_Print(bs3);
	BS_Destroy(&bs2);
	BS_Destroy(&bs3);
    BS_Print(bs);

    return 0;

}
