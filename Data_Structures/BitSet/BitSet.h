#include <stdbool.h>

#ifndef BITSET_H
#define BITSET_H

#define BS_HandleInvalid NULL

typedef void* BitSetT;

extern BitSetT BS_Create(int bitset_len);
extern int BS_GetSize(BitSetT bs);
extern bool BS_isExist(BitSetT bs, int index);
extern bool BS_Set(BitSetT bs, int index);
extern bool BS_Remove(BitSetT bs, int index);
extern bool BS_SetRange(BitSetT bs, int first_index, int last_index);
extern bool BS_RemoveRange(BitSetT bs, int first_index, int last_index);
extern bool BS_isRange(BitSetT bs, int first_index, int last_index);
extern BitSetT BS_Merge(BitSetT bs1, BitSetT bs2);
extern void BS_Print(BitSetT bs);
extern void BS_Destroy(BitSetT* bs);

#endif // BITSET_H
