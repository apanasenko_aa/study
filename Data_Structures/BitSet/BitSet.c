#include <stdbool.h>
#include "BitSet.h"

const int SIZE_PARTITION = 32;

typedef struct BitSet {
	int* bsArr;
	int bsLen;
} BitSet;

BitSetT BS_Create(int bs_len)
{
	BitSet* curBS = malloc(sizeof(BitSet));
	if (curBS == NULL)
		return BS_HandleInvalid;
	curBS->bsLen = bs_len;
	int sizeArr = bs_len / SIZE_PARTITION + 1;
    curBS->bsArr = (int*) malloc(sizeArr * sizeof(int));
	int i;
	for(i = 0; i < sizeArr; i++)
		curBS->bsArr[i] = 0;
	return curBS;
}

void BS_Destroy(BitSetT* bs)
{
	if (bs == NULL && *bs == BS_HandleInvalid)
        return;
	BitSet* curBS = (BitSet*)(*bs);
	free(curBS->bsArr);
	free(curBS);
	*bs = BS_HandleInvalid;
}

int BS_GetSize(BitSetT bs)
{
    return bs != NULL ? ((BitSet*)bs)->bsLen : BS_HandleInvalid;
}

bool BS_isExist(BitSetT bs, int index)
{
    if (bs == NULL)
        return false;
    BitSet* curBS = (BitSet*) bs;
    if ((curBS->bsLen - 1 < index) || ( 1 > index))
        return false;
    int i = index / SIZE_PARTITION;
    return (curBS->bsArr[i] & (1 << index % SIZE_PARTITION));

}

bool BS_Set(BitSetT bs, int index)
{
    if (bs == NULL)
        return false;
    BitSet* curBS = (BitSet*) bs;
    if ((curBS->bsLen - 1 < index) || ( 1 > index))
        return false;
    int i = index / SIZE_PARTITION;
    curBS->bsArr[i] |= 1 << (index % SIZE_PARTITION);
    return true;
}

bool BS_Remove(BitSetT bs, int index)
{
    if (bs == NULL)
        return false;
    BitSet* curBS = (BitSet*) bs;
    if ((curBS->bsLen - 1 < index) || ( 1 > index))
        return false;
    int i = index / SIZE_PARTITION;
    curBS->bsArr[i] &= ~(1 << (index % SIZE_PARTITION));
    return true;
}

bool BS_SetRange(BitSetT bs, int first_index, int last_index)
{
    if (bs == NULL)
        return false;
    BitSet* curBS = (BitSet*) bs;
    if ((curBS->bsLen - 1 < first_index) || ( 1 > first_index) || (first_index > last_index) ||
        ( 1 > last_index) || ( curBS->bsLen - 1 < last_index))
        return false;
    int fd = first_index / SIZE_PARTITION;
    int ld = last_index / SIZE_PARTITION;
    int fm = first_index % SIZE_PARTITION;
    int lm = last_index % SIZE_PARTITION;
    int i;
    fm = ~((1 << fm) - 1);
    lm = 0xFFFFFFFF >> (SIZE_PARTITION - lm - 1);
    if (fd == ld)
        curBS->bsArr[fd] |= (fm & lm);
    else {
        curBS->bsArr[fd] |= fm;
        for (i = fd + 1; i < ld; i++)
            curBS->bsArr[i] = 0xFFFFFFFF;
        curBS->bsArr[ld] |= lm;
    }
    return true;
}
bool BS_RemoveRange(BitSetT bs, int first_index, int last_index)
{
    if (bs == NULL)
        return false;
    BitSet* curBS = (BitSet*) bs;
    if ((curBS->bsLen - 1 < first_index) || ( 1 > first_index) || (first_index > last_index) ||
        ( 1 > last_index) || ( curBS->bsLen - 1 < last_index))
        return false;
    int fd = first_index / SIZE_PARTITION;
    int ld = last_index / SIZE_PARTITION;
    int fm = first_index % SIZE_PARTITION;
    int lm = last_index % SIZE_PARTITION;
    int i;
    fm = ((1 << fm) - 1);
    lm = ~(0xFFFFFFFF >> (SIZE_PARTITION - lm - 1));
    if (fd == ld)
        curBS->bsArr[fd] &= (fm | lm);
    else {
        curBS->bsArr[fd] &= fm;
        for (i = fd + 1; i < ld; i++)
            curBS->bsArr[i] = 0;
        curBS->bsArr[ld] &= lm;
    }
    return true;
}
bool BS_isRange(BitSetT bs, int first_index, int last_index)
{
    if (bs == NULL)
        return false;
    BitSet* curBS = (BitSet*) bs;
    if ((curBS->bsLen - 1 < first_index) || ( 1 > first_index) || (first_index > last_index) ||
        ( 1 > last_index) || ( curBS->bsLen - 1 < last_index))
        return false;
    int fd = first_index / SIZE_PARTITION;
    int ld = last_index / SIZE_PARTITION;
    int fm = first_index % SIZE_PARTITION;
    int lm = last_index % SIZE_PARTITION;
    int i, res = 0;
    fm = ~((1 << fm) - 1);
    lm = 0xFFFFFFFF >> (SIZE_PARTITION - lm - 1);
    if (fd == ld)
        res |= curBS->bsArr[fd] & fm & lm;
    else {
        res |= curBS->bsArr[fd] & fm ;
        for (i = fd + 1; i < ld; i++)
            res |= curBS->bsArr[i];
        res |= curBS->bsArr[ld] & lm;
    }
    return res != 0;
}
BitSetT BS_Merge(BitSetT bs1, BitSetT bs2)
{
    if ((bs1 == NULL) || (bs2 == NULL))
        return BS_HandleInvalid;
    BitSet* curBS1 = (BitSet*) bs1;
    BitSet* curBS2 = (BitSet*) bs2;
    BitSet* curBS = BS_Create(curBS1->bsLen > curBS2->bsLen ? curBS1->bsLen : curBS2->bsLen);
    int i;
    for (i = 0; i < curBS1->bsLen / SIZE_PARTITION + 1; i++)
        curBS->bsArr[i] |= curBS1->bsArr[i];
    for (i = 0; i < curBS2->bsLen / SIZE_PARTITION + 1; i++)
        curBS->bsArr[i] |= curBS2->bsArr[i];
    return curBS;
}
void BS_Print(BitSetT bs)
{
    if (bs == NULL)
        return;
    BitSet* curBS = (BitSet*) bs;
    int n = curBS->bsLen / SIZE_PARTITION + 1;
    int i, j;
    for (i = 0; i < n; i++)
    {
        for(j = 0; j < SIZE_PARTITION; j++)
            printf("%d", (curBS->bsArr[i] >> j & 1));
        printf(" | ");
    }
    printf("\n");
}

