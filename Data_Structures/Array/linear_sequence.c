#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "linear_sequence.h"

typedef struct {
    LSQ_BaseTypeT* data;
    LSQ_IntegerIndexT length;
    LSQ_IntegerIndexT size;
} Array;

typedef struct {
    Array* handle;
    LSQ_IntegerIndexT index;
} Iterator;

LSQ_HandleT LSQ_CreateSequence(void)
{
    Array* result = malloc (sizeof(Array));
    result->length = 0;
    result->data = 0;
    result->size = 0;
    return result;
}

void LSQ_DestroySequence(LSQ_HandleT handle)
{
    Array* curArr = (Array*) handle;
    free(curArr->data);
    free(handle);
    return;
}

LSQ_IntegerIndexT LSQ_GetSize(LSQ_HandleT handle)
{
    Array* curArr = (Array*) handle;
    return curArr->length;
}

int LSQ_IsIteratorDereferencable(LSQ_IteratorT iterator)
{
    Iterator* curIter = (Iterator*) iterator;
    return (curIter->index >= 0) && (curIter->index < curIter->handle->length);
}

int LSQ_IsIteratorPastRear(LSQ_IteratorT iterator)
{
    Iterator* curIter = (Iterator*) iterator;
    return (curIter->index == curIter->handle->length);
}

int LSQ_IsIteratorBeforeFirst(LSQ_IteratorT iterator)
{
    Iterator* curIter = (Iterator*) iterator;
    return (curIter->index == -1);
}

LSQ_BaseTypeT* LSQ_DereferenceIterator(LSQ_IteratorT iterator)
{
    Iterator* curIter = (Iterator*) iterator;
    return (curIter->handle->data + curIter->index);
}

LSQ_IteratorT LSQ_GetElementByIndex(LSQ_HandleT handle, LSQ_IntegerIndexT index)
{
    Array* curArr = (Array*) handle;
    Iterator* result = (Iterator*) malloc(sizeof(Iterator));
    if (result != NULL){
        result->handle = curArr;
        result->index = index;
    }
    return result;
}

LSQ_IteratorT LSQ_GetFrontElement(LSQ_HandleT handle)
{
    return LSQ_GetElementByIndex(handle, 0);
}

LSQ_IteratorT LSQ_GetPastRearElement(LSQ_HandleT handle)
{
    Array* curArr = (Array*) handle;
    return LSQ_GetElementByIndex(handle, curArr->length);
}

void LSQ_DestroyIterator(LSQ_IteratorT iterator)
{
    free(iterator);
    return;
}

void LSQ_AdvanceOneElement(LSQ_IteratorT iterator)
{
    Iterator* curIter = (Iterator*) iterator;
    curIter->index ++;
    return;
}

void LSQ_RewindOneElement(LSQ_IteratorT iterator)
{
    Iterator* curIter = (Iterator*) iterator;
    curIter->index --;
    return;
}

void LSQ_ShiftPosition(LSQ_IteratorT iterator, LSQ_IntegerIndexT shift)
{
    Iterator* curIter = (Iterator*) iterator;
    curIter->index += shift;
    return;
}

void LSQ_SetPosition(LSQ_IteratorT iterator, LSQ_IntegerIndexT pos)
{
    if (iterator != NULL){
        Iterator* curIter = (Iterator*) iterator;
        curIter->index = pos;
    }
    return;
}

void LSQ_InsertFrontElement(LSQ_HandleT handle, LSQ_BaseTypeT element)
{
    LSQ_IteratorT curIter = LSQ_GetFrontElement(handle);
    LSQ_InsertElementBeforeGiven(curIter, element);
    free(curIter);
    return;
}

void LSQ_InsertRearElement(LSQ_HandleT handle, LSQ_BaseTypeT element)
{
    LSQ_IteratorT curIter = LSQ_GetPastRearElement(handle);
    LSQ_InsertElementBeforeGiven(curIter, element);
    free(curIter);
    return;
}

void LSQ_InsertElementBeforeGiven(LSQ_IteratorT iterator, LSQ_BaseTypeT newElement)
{
    Iterator* curIter = (Iterator*) iterator;
    Array* curArr = curIter->handle;
    int i;
    curArr->data = realloc(curArr->data, curArr->size + sizeof(int));
    curArr->size += sizeof(int);
    for (i = curArr->length; i >= curIter->index; i--) {
        curArr->data[i] = curArr->data[i-1];
    }
    curArr->data[curIter->index] = newElement;
    curArr->length ++;
    return;
}

void LSQ_DeleteFrontElement(LSQ_HandleT handle)
{
    LSQ_IteratorT curIter = LSQ_GetFrontElement(handle);
    LSQ_DeleteGivenElement(curIter);
    free(curIter);
    return;
}

void LSQ_DeleteRearElement(LSQ_HandleT handle)
{
    LSQ_IteratorT curIter = LSQ_GetPastRearElement(handle);
    LSQ_RewindOneElement(curIter);
    LSQ_DeleteGivenElement(curIter);
    free(curIter);
    return;
}

void LSQ_DeleteGivenElement(LSQ_IteratorT iterator)
{
    int i;
    Iterator* curIter = (Iterator*) iterator;
    Array* curArr = curIter->handle;
    for (i = curIter->index + 1; i <= curArr->length; i++) {
        curArr->data[i-1] = curArr->data[i];
    }
    curArr->data = realloc(curArr->data, curArr->size - sizeof(int));
    curArr->length --;
    return;
}
