#include "linear_sequence_assoc.h"

typedef struct Node {
   LSQ_IntegerIndexT key;
   LSQ_BaseTypeT value;
   LSQ_IntegerIndexT height;
   struct Node* left;
   struct Node* right;
   struct Node* parent;
} Node;

typedef struct Tree{
   LSQ_IntegerIndexT size;
   Node* root;
} Tree;

typedef struct Iterator{
   Tree* container;
   Node* node;
} Iterator;

LSQ_HandleT LSQ_CreateSequence (void){
    Tree* curTree = (Tree*) malloc(sizeof(Tree));
    if (NULL == curTree)
        return LSQ_HandleInvalid;
    curTree->root = NULL;
    curTree->size = 0;
    return curTree;
}

static void DestroyNode(Node* handle){
    if (handle == NULL)
        return;
    if (handle->left == handle || handle->right == handle) {
        free(handle);
        return;}
    DestroyNode(handle->left);
    DestroyNode(handle->right);
    free(handle);
    return;
}

void LSQ_DestroySequence(LSQ_HandleT handle){
    if (handle == NULL)
        return;
    DestroyNode(((Tree*) handle)->root);
    free(handle);
    return;
}

LSQ_IntegerIndexT LSQ_GetSize (LSQ_HandleT handle){
    if (NULL == handle)
        return 0;
    else
        return ((Tree*)handle)->size;
}

int LSQ_IsIteratorDereferencable(LSQ_IteratorT iterator){
    return ((Iterator*) iterator)->node != NULL && !LSQ_IsIteratorPastRear(iterator) && !LSQ_IsIteratorBeforeFirst(iterator);
}

int LSQ_IsIteratorPastRear(LSQ_IteratorT iterator){
    Iterator* curIterator = (Iterator*) iterator;
    if (NULL == curIterator->node)
        return 1;/////////////////////////////////////////////////////////////////
    return curIterator->node == curIterator->node->right;
}

int LSQ_IsIteratorBeforeFirst(LSQ_IteratorT iterator){
    Iterator* CurIt = (Iterator*) iterator;
    if (CurIt->node == NULL)
        return 1;
    return CurIt->node == CurIt->node->left;
}

LSQ_BaseTypeT* LSQ_DereferenceIterator (LSQ_IteratorT iterator){
    if (!LSQ_IsIteratorDereferencable(iterator))
        return NULL;
    return &((Iterator*)iterator)->node->value;
}

LSQ_IntegerIndexT LSQ_GetIteratorKey(LSQ_IteratorT iterator){
    if (!LSQ_IsIteratorDereferencable(iterator))
        return NULL;
    return ((Iterator*) iterator)->node->key;
}

static Iterator* CreateIterator(Tree* curTree, Node* curNode){
    Iterator* curIterator = (Iterator*) malloc(sizeof(Iterator));
    if (NULL == curIterator)
        return NULL;
    curIterator->container = curTree;
    curIterator->node = curNode;
    return curIterator;
}

static Node* FindNode(Node* curNode, LSQ_IntegerIndexT key){
    if (NULL == curNode || curNode == curNode->left || curNode == curNode->right)
        return NULL;//////////////////////
    if (NULL != curNode && curNode->key != key)
        curNode = key > curNode->key ? FindNode(curNode->right, key) : FindNode(curNode->left, key);
    return curNode;
}

LSQ_IteratorT LSQ_GetElementByIndex(LSQ_HandleT handle, LSQ_IntegerIndexT index){
    if (NULL == handle)
        return NULL;
    return CreateIterator((Tree*)handle, FindNode(((Tree*)handle)->root, index)); /* ������ ���� ��������� �������� PastRear */
}

LSQ_IteratorT LSQ_GetFrontElement(LSQ_HandleT handle){
    if (NULL == handle)
        return NULL;
    Tree* curTree = (Tree*)handle;
    Node* curNode = curTree->root;
    while (NULL != curNode && curNode->left != curNode->left->left)
        curNode = curNode->left;
    return CreateIterator(curTree, curNode);
}

LSQ_IteratorT LSQ_GetPastRearElement(LSQ_HandleT handle){
    Tree* CurTree = (Tree*) handle;
    Node* CurNode = CurTree->root;
    while (CurNode != NULL && CurNode->right != NULL && CurNode != CurNode->right)
        CurNode = CurNode->right;
    return CreateIterator(CurTree, CurNode);
}

void LSQ_DestroyIterator(LSQ_IteratorT iterator){
    free(iterator);
    return;
}

void LSQ_AdvanceOneElement(LSQ_IteratorT iterator){
    Iterator* CurIt = (Iterator*) iterator;
    if (LSQ_IsIteratorBeforeFirst(CurIt)) {
        CurIt->node = CurIt->node->parent;
        return;}
    if (LSQ_IsIteratorPastRear(CurIt))
        return;
    //
    Iterator* iter = LSQ_GetPastRearElement(CurIt->container);
    if (CurIt->node == CurIt->node->parent) {
        CurIt->node = iter->node;
        LSQ_DestroyIterator(iter);
        return;}
    //
    if (CurIt->node->right != NULL) {
        CurIt->node = CurIt->node->right;
        while (CurIt->node->left != NULL)
            CurIt->node = CurIt->node->left;}
    else {
        Node* lastNode = CurIt->node;
        CurIt->node = CurIt->node->parent;
        while (CurIt->node->parent != NULL && CurIt->node->left != lastNode) {
            lastNode = CurIt->node;
            CurIt->node = CurIt->node->parent;}
        CurIt->node = CurIt->node->left == lastNode ? CurIt->node : NULL;}
    return;
}

void LSQ_RewindOneElement(LSQ_IteratorT iterator){
    Iterator* CurIt = (Iterator*) iterator;
    //
    Iterator* iter = LSQ_GetFrontElement(CurIt->container);
    if (NULL == iter->node) return;
    if (CurIt->node == iter->node->left) return;
    if (CurIt->node == iter->node) {
        CurIt->node = iter->node->left;
        LSQ_DestroyIterator(iter);
        return;}
    //
    if (LSQ_IsIteratorPastRear(CurIt)) {
        CurIt->node = CurIt->node->parent;
        return;}
    if (CurIt->node->left != NULL) {
        CurIt->node = CurIt->node->left;
        while (CurIt->node->right != NULL)
            CurIt->node = CurIt->node->right;}
    else {
        Node* lastNode = CurIt->node;
        CurIt->node = CurIt->node->parent;
        while (CurIt->node->parent != NULL && CurIt->node->right != lastNode) {
            lastNode = CurIt->node;
            CurIt->node = CurIt->node->parent;}
        CurIt->node = CurIt->node->right == lastNode ? CurIt->node : NULL;}
    return;
}

void LSQ_ShiftPosition(LSQ_IteratorT iterator, LSQ_IntegerIndexT shift){
    int i;
    for (i = 0; i < abs(shift); i++)
        if (shift > 0) LSQ_AdvanceOneElement(iterator);
        else LSQ_RewindOneElement(iterator);
    return;
}

void LSQ_SetPosition(LSQ_IteratorT iterator, LSQ_IntegerIndexT pos){
    Iterator* CurIt = (Iterator*) iterator;
    Node* CurNode = CurIt->container->root;
    while (CurNode != NULL && CurNode->left != NULL && CurNode->left != CurNode->left->left)
        CurNode = CurNode->left;
    CurIt->node = CurNode;
    LSQ_ShiftPosition(iterator, pos);
}

static Node* CreateNode(LSQ_IntegerIndexT key, LSQ_BaseTypeT value, Node* parent){
    Node* result = (Node*)malloc(sizeof(Node));
    if (NULL == result)
        return NULL;
    result->key = key;
    result->value = value;

    result->parent = parent;
    result->left = NULL;
    result->right = NULL;
    return result;
}

static int HeightNode (Node* handle){
    return handle == NULL ? 0 : handle->height;
}

static int BalansFactor(Node* root){
    return HeightNode(root->right) - HeightNode(root->left);
}

static void ChangeChild(Tree* handle, Node* root, Node* ChildNode) {
    if (ChildNode != NULL)
        ChildNode->parent = root->parent;
    if (root->parent == NULL)
        handle->root = ChildNode;
    else if (root->parent->left == root)
        root->parent->left = ChildNode;
    else if (root->parent->right == root)
        root->parent->right = ChildNode;
    return;
}

static void LeftRotation(Tree* handle, Node* root) {
    ChangeChild(handle, root, root->right);
    root->parent = root->right;
    if (root->right->left != NULL)
        root->right->left->parent = root;
    Node* Child = root->right->left;
    root->right->left = root;
    root->right = Child;
    return;
}

static void RightRotation(Tree* handle, Node* root) {
    ChangeChild(handle, root, root->left);
    root->parent = root->left;
    if (root->left->right != NULL)
        root->left->right->parent = root;
    Node* Child = root->left->right;
    root->left->right = root;
    root->left = Child;
    return;
}

static void BalansTree(Tree* handle, Node* root) {
    int dif = BalansFactor(root);
    if (dif == 2) {
        if (BalansFactor(root->right) < 0)
            RightRotation(handle, root->right);
        LeftRotation(handle, root);}
    if (dif == -2) {
        if (BalansFactor(root->left) > 0)
            LeftRotation(handle, root->left);
        RightRotation(handle, root);}
    root->height = (HeightNode(root->right) > HeightNode(root->left) ? HeightNode(root->right) : HeightNode(root->left)) + 1;
    if (root != handle->root)
        BalansTree(handle, root->parent);
    return;
}

void LSQ_InsertElement(LSQ_HandleT handle, LSQ_IntegerIndexT key, LSQ_BaseTypeT value){
    if (NULL == handle)
        return;
    Tree* curTree = (Tree*) handle;
    if (0 == curTree->size) {
        curTree->root = CreateNode(key, value, NULL);
        curTree->root->left = CreateNode(NULL, NULL, curTree->root);
        curTree->root->right = CreateNode(NULL, NULL, curTree->root);
        curTree->root->left->left = curTree->root->left;
        curTree->root->right->right = curTree->root->right;
        curTree->size++;
        return;}
    Node* curNode = curTree->root;
    return;
}


void LSQ_DeleteFrontElement(LSQ_HandleT handle){
   LSQ_IteratorT iterator = LSQ_GetFrontElement(handle);
   LSQ_DeleteElement(handle, LSQ_GetIteratorKey(iterator));
   LSQ_DestroyIterator(iterator);
}

void LSQ_DeleteRearElement(LSQ_HandleT handle){
    LSQ_IteratorT iterator = LSQ_GetPastRearElement(handle);
    LSQ_RewindOneElement(iterator);
    LSQ_DeleteElement(handle, LSQ_GetIteratorKey(iterator));
    LSQ_DestroyIterator(iterator);
}

static void DeleteNode(Tree* handle, Node* CurNode, Node* ChangeNode){
    if (handle->size == 1) {
        free(handle->root->left);
        free(handle->root->right);
        free(handle->root);
        handle->root = NULL;
        handle->size--;
        return;}
    if (ChangeNode == NULL){
        if (CurNode->left != NULL){
            CurNode->left->parent = CurNode->parent;
            ChangeChild(CurNode->parent, CurNode, CurNode->left);}
        else if (CurNode->right != NULL){
            CurNode->right->parent = CurNode->parent;
            ChangeChild(CurNode->parent, CurNode, CurNode->right);}
        else
            ChangeChild(CurNode->parent, CurNode, NULL);
        Node* LastNode = CurNode->parent;
        free(CurNode);
        handle->size--;
        BalansTree(handle, LastNode);
        }
    else {
        CurNode->key = ChangeNode->key;
        CurNode->value = ChangeNode->value;
        DeleteNode(handle, ChangeNode, NULL);}
    return;
}

void LSQ_DeleteElement(LSQ_HandleT handle, LSQ_IntegerIndexT key){
    Tree* CurTree = (Tree*) handle;
    Iterator* CurIt = (Iterator*) LSQ_GetElementByIndex(CurTree, key);
    Node* CurNode = CurIt->node;
    if (CurNode == NULL)
        return;
    if (CurNode->left == NULL || CurNode->right == NULL)
        DeleteNode(CurTree, CurNode, NULL);
    else {
        if (CurNode->right != CurNode->right->right){
            Node* ChangeNode = CurNode->right;
            while (ChangeNode->left != NULL)
                ChangeNode = ChangeNode->left;
                DeleteNode(CurTree, CurNode, ChangeNode);}
        else{
            Node* ChangeNode = CurNode->left;
            while (ChangeNode->right != NULL)
                ChangeNode = ChangeNode->right;
                DeleteNode(CurTree, CurNode, ChangeNode);}
    }
    return;
}

