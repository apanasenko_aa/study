#include "linear_sequence_assoc.h"
#include <assert.h>
#include <limits.h>

#define MIN_SIZE 4
#define MAX_SIZE 8

typedef enum {
    FALSE,
    TRUE
} boolean;

typedef struct Node {
    LSQ_IntegerIndexT key[MAX_SIZE];
    LSQ_BaseTypeT value[MAX_SIZE];
    struct Node* child [MAX_SIZE];
    struct Node* parent;
    boolean leaf;
    LSQ_IntegerIndexT size;
} Node;

typedef struct Tree {
	Node* root;
	LSQ_IntegerIndexT size;
} Tree;

typedef struct Iterator {
	Tree* container;
	Node* node;
	int index;
} Iterator;


static Iterator* CreateIterator(Tree*, Node*, int);
static Iterator* SubTreeSearch(Tree*, Node*, LSQ_IntegerIndexT);
static Node* CreateNode();
static void TreeSplitChild(Node*, Node*, int);
static void TreeInsertNotFull(Node*, LSQ_IntegerIndexT, LSQ_BaseTypeT);
static void TreeInsert(Tree*, LSQ_IntegerIndexT, LSQ_BaseTypeT);
static void DestroySubTree (Node*);
static void DeleteElementInSubTree(Tree*, Node*, LSQ_IntegerIndexT);


LSQ_HandleT LSQ_CreateSequence (void){
    Tree* curTree = (Tree*) malloc(sizeof(Tree));
    if (NULL == curTree){
        return LSQ_HandleInvalid;
    }
    curTree->root = CreateNode();
    curTree->root->parent = NULL;
    curTree->root->size = 0;
    curTree->root->leaf = TRUE;
    LSQ_InsertElement(curTree, INT_MAX, INT_MAX);
    LSQ_InsertElement(curTree, INT_MIN, INT_MIN);
    curTree->size = 0;
    return curTree;
}

void LSQ_DestroySequence (LSQ_HandleT handle){
    if (NULL != handle){
        DestroySubTree(((Tree*)handle)->root);
        free(handle);
    }
    return;
}

LSQ_IntegerIndexT LSQ_GetSize (LSQ_HandleT handle){
    return NULL != handle ? ((Tree*)handle)->size : 0;
}

int LSQ_IsIteratorDereferencable (LSQ_IteratorT iterator){
    return (NULL != iterator && NULL != ((Iterator*)iterator)->node) && !LSQ_IsIteratorBeforeFirst(iterator) && !LSQ_IsIteratorPastRear(iterator);
}

int LSQ_IsIteratorPastRear(LSQ_IteratorT iterator){
    if (NULL == iterator){
        return NULL;
    }
    Iterator* curIterator = (Iterator*) iterator;
    return 0 != curIterator->container->size ? curIterator->node->key[curIterator->index] == INT_MAX : 1;
}

int LSQ_IsIteratorBeforeFirst (LSQ_IteratorT iterator){
    if (NULL == iterator){
        return NULL;
    }
    Iterator* curIterator = (Iterator*) iterator;
    return 0 != curIterator->container->size ? curIterator->node->key[curIterator->index] == INT_MIN : 1;
}

LSQ_BaseTypeT* LSQ_DereferenceIterator (LSQ_IteratorT iterator){
    return LSQ_IsIteratorDereferencable(iterator) ? &((Iterator*)iterator)->node->value[((Iterator*)iterator)->index] : NULL;
}

LSQ_IntegerIndexT LSQ_GetIteratorKey (LSQ_IteratorT iterator){
    return LSQ_IsIteratorDereferencable(iterator) ? ((Iterator*)iterator)->node->key[((Iterator*)iterator)->index] : NULL;
}

LSQ_IteratorT LSQ_GetElementByIndex(LSQ_HandleT handle, LSQ_IntegerIndexT index){
    return NULL != handle ? SubTreeSearch((Tree*)handle, ((Tree*)handle)->root, index) : NULL;
}

LSQ_IteratorT LSQ_GetFrontElement(LSQ_HandleT handle){
    if (NULL == handle){
        return NULL;
    }
    Tree* curTree = (Tree*)handle;
    Node* curNode = curTree->root;
    Iterator* result = SubTreeSearch(curTree, curNode, INT_MIN);
    result->index++;
    return result;
}

LSQ_IteratorT LSQ_GetPastRearElement(LSQ_HandleT handle){
    if (NULL == handle){
        return NULL;
    }
    Tree* curTree = (Tree*)handle;
    Node* curNode = curTree->root;
    Iterator* result = SubTreeSearch(curTree, curNode, INT_MAX);
    return result;
}

void LSQ_DestroyIterator(LSQ_IteratorT iterator){
    free(iterator);
    return;
}

void LSQ_InsertElement(LSQ_HandleT handle, LSQ_IntegerIndexT key, LSQ_BaseTypeT value){
    if (NULL == handle){
        return;
    }
    Tree* curTree = (Tree*) handle;
    Iterator* curIterator = SubTreeSearch(curTree, curTree->root, key);
    if (NULL != curIterator){
        curIterator->node->value[curIterator->index] = value;
        return;
    }
    LSQ_DestroyIterator(curIterator);
    TreeInsert(curTree, key, value);
    curTree->size++;
    return;
}

void LSQ_AdvanceOneElement(LSQ_IteratorT iterator){
    if (NULL == iterator){
        return;
    }
    Iterator* curIterator = (Iterator*) iterator;
    if (LSQ_IsIteratorPastRear(curIterator)){
        return;
    }
    Node* curNode = curIterator->node;
    if (TRUE == curNode->leaf){
        if (curIterator->index < curNode->size - 1){
            curIterator->index++;
            return;
        } else {
            while (curNode->parent->child[curNode->parent->size] == curNode){
                curNode = curNode->parent;
            }
            int i = 0;
            while (curNode->parent->child[i] != curNode){
                i++;
            }
            curIterator->index = i;
            curIterator->node = curNode->parent;
        }
    } else {
        curIterator->node = curNode->child[curIterator->index + 1];
        while (TRUE != curIterator->node->leaf){
            curIterator->node = curIterator->node->child[0];
        }
        curIterator->index = 1;
    }
    return;
}

void LSQ_RewindOneElement(LSQ_IteratorT iterator){
    if (NULL == iterator){
        return;
    }
    Iterator* curIterator = (Iterator*) iterator;
    if (NULL == curIterator->container->root || LSQ_IsIteratorBeforeFirst(curIterator)){
        return;
    }
    Node* curNode = curIterator->node;
    if (TRUE == curNode->leaf){
        if (curIterator->index > 0){
            curIterator->index--;
            return;
        } else {
            while (curNode->parent->child[0] == curNode){
                curNode = curNode->parent;
            }
            int i = 0;
            while (curNode->parent->child[i] != curNode){
                i++;
            }
            curIterator->index = i - 1;
            curIterator->node = curNode->parent;
        }
    } else {
        curIterator->node = curNode->child[curIterator->index];
        while (TRUE != curIterator->node->leaf){
            curIterator->node = curIterator->node->child[curIterator->node->size];
        }
        curIterator->index = curIterator->node->size - 1;
    }
    return;
}

void LSQ_ShiftPosition(LSQ_IteratorT iterator, LSQ_IntegerIndexT shift){
    for (; shift > 0; shift--) LSQ_AdvanceOneElement(iterator);
    for (; shift < 0; shift++) LSQ_RewindOneElement(iterator);
    return;
}

void LSQ_SetPosition(LSQ_IteratorT iterator, LSQ_IntegerIndexT pos){
    if(NULL == iterator){
        return;
    }
    Iterator* curIterator = (Iterator*) iterator;
    Node* curNode = curIterator->container->root;
    while (NULL != curNode && TRUE != curNode->leaf){
        curNode = curNode->child[0];
    }
    curIterator->node = curNode;
    curIterator->index = 1;
    LSQ_ShiftPosition(curIterator, pos);
    return;
}

void LSQ_DeleteElement(LSQ_HandleT handle, LSQ_IntegerIndexT key){
    if (NULL == handle){
        return;
    }
    Tree* curTree = (Tree*) handle;
    Iterator* curIterator = SubTreeSearch(curTree, curTree->root, key);
    if (NULL != curIterator){
        DeleteElementInSubTree(curTree, curTree->root, key);
        curTree->size--;
    }
    LSQ_DestroyIterator(curIterator);
    return;
}

void LSQ_DeleteFrontElement(LSQ_HandleT handle){
    LSQ_IteratorT iterator = LSQ_GetFrontElement(handle);
    LSQ_DeleteElement(handle, LSQ_GetIteratorKey(iterator));
    LSQ_DestroyIterator(iterator);
    return;
}

void LSQ_DeleteRearElement(LSQ_HandleT handle){
    LSQ_IteratorT iterator = LSQ_GetPastRearElement(handle);
    LSQ_RewindOneElement(iterator);
    LSQ_DeleteElement(handle, LSQ_GetIteratorKey(iterator));
    LSQ_DestroyIterator(iterator);
    return;
}

void LP(LSQ_HandleT h){
    Tree* T = (Tree*) h;
    int i, j;
    printf("\n%d \n", T->size);
    for (i = 0; i < T->root->size; i++){
        printf("%d ", T->root->key[i]);
    }
    for (j = 0; j <=T->root->size; j++){
        printf("\n %d \n", T->root->child[j]->size);
        for (i = 0; i < T->root->child[j]->size; i++){
            printf("%d ", T->root->child[j]->key[i]);
        }
    }
    //printf("\n %d \n", T->root->child[1]->size);
    //for (i = 0; i < T->root->child[1]->size; i++){
    //    printf("%d ", T->root->child[1]->key[i]);
    //}
}

//================================ static ================================//

static Iterator* CreateIterator(Tree* curTree, Node* curNode, int index){
    Iterator* curIterator = (Iterator*) malloc(sizeof(Iterator));
    if (NULL == curIterator){
        return NULL;
    }
    curIterator->container = curTree;
    curIterator->node = curNode;
    curIterator->index = index;
    return curIterator;
}

static Iterator* SubTreeSearch(Tree* curTree, Node* curNode, LSQ_IntegerIndexT key){
    int i = 0;
    while (i < curNode->size && key > curNode->key[i]){
        i++;
    }
    if (i < curNode->size && key == curNode->key[i]){
        return CreateIterator(curTree, curNode, i);
    }
    return (TRUE == curNode->leaf) ? NULL : SubTreeSearch(curTree, curNode->child[i], key);
    //return i < curNode->N && key == curNode->key[i] ? CreateIterator(curTree, curNode, i) : (TRUE == curNode->leaf) ? NULL : SubTreeSearch(curTree, curNode->children[i], key);
}

static Node* CreateNode(){
    Node* result = (Node*)malloc(sizeof(Node));
    assert(result);
    return result;
}

static void TreeSplitChild(Node* parent, Node* curNode, int index){
    Node* newNode = CreateNode();
    int i;
    newNode->leaf = curNode->leaf;
    newNode->parent = parent;
    newNode->size = MIN_SIZE - 1;
    for (i = 0; i < (MIN_SIZE - 1); i++){
        newNode->key[i] = curNode->key[i + MIN_SIZE];
        newNode->value[i] = curNode->value[i + MIN_SIZE];
        newNode->child[i] = curNode->child[i + MIN_SIZE];
    }
    newNode->child[MIN_SIZE - 1] = curNode->child[MAX_SIZE - 1];
    curNode->size = MIN_SIZE - 1;
    for (i = parent->size + 1; i > index; i--){
        parent->key[i] = parent->key[i - 1];
        parent->value[i] = parent->value[i - 1];
        parent->child[i + 1] = parent->child[i];
    }
    parent->child[index + 1] = newNode;
    parent->key[index] = curNode->key[MIN_SIZE - 1];
    parent->value[index] = curNode->value[MIN_SIZE - 1];
    parent->size++;
    return;
}

static void TreeInsertNotFull(Node* curNode, LSQ_IntegerIndexT key, LSQ_BaseTypeT value){
    int i = curNode->size - 1;
    if (TRUE == curNode->leaf) {
        while (i >= 0 && key < curNode->key[i]) {
            curNode->key[i + 1] = curNode->key[i];
            curNode->value[i + 1] = curNode->value[i];
            i--;
        }
        curNode->key[i + 1] = key;
        curNode->value[i + 1] = value;
        curNode->size++;
    } else {
        while (i >= 0 && key < curNode->key[i]) {
            i--;
        }
        i++;
        if ((MAX_SIZE - 1) == curNode->child[i]->size){
            TreeSplitChild(curNode, curNode->child[i], i);
            if (key > curNode->key[i]){
                i++;
            }
        }
        TreeInsertNotFull(curNode->child[i], key, value);
    }
    return;
}

static void TreeInsert(Tree* curTree, LSQ_IntegerIndexT key, LSQ_BaseTypeT value){
    Node* root = curTree->root;
    if (MAX_SIZE - 1 == root->size){
        Node* newRoot = CreateNode();
        curTree->root = newRoot;
        newRoot->leaf = FALSE;
        newRoot->size = 0;
        newRoot->child[0] = root;
        TreeSplitChild(newRoot, root, 0);
        TreeInsertNotFull(newRoot, key, value);
    } else {
        TreeInsertNotFull(root, key, value);
    }
    return;
}

static void DestroySubTree (Node* root){
    if (FALSE == root->leaf){
        int i;
        for (i = 0; i <= root->size; i++){
            DestroySubTree(root->child[i]);
        }
    }
    free(root);
    return;
}

static void DeleteElementInSubTree(Tree* curTree, Node* root, LSQ_IntegerIndexT key){
    int j, i = 0;
    if (TRUE == root->leaf){
        root->size--;
        while (key != root->key[i]){
            i++;
        }
        while (i < root->size){
            root->key[i] = root->key[i + 1];
            root->value[i] = root->value[i + 1];
            i++;
        }
    } else {
        //
        i = root->size - 1;
        while (i > 0 && key < root->key[i]){
            i--;
        }
        if (MIN_SIZE - 1 == root->child[i]->size && MIN_SIZE - 1 == root->child[i + 1]->size) {
            Node* curNode = root->child[i];
            curNode->key[MIN_SIZE - 1] = root->key[i];
            curNode->value[MIN_SIZE - 1] = root->value[i];
            for (j = MIN_SIZE; j < MAX_SIZE - 1; j++){
                curNode->key[j] = root->child[i + 1]->key[j - MIN_SIZE];
                curNode->value[j] = root->child[i + 1]->value[j - MIN_SIZE];
                curNode->child[j] = root->child[i + 1]->child[j - MIN_SIZE];
            }
            curNode->child[MAX_SIZE - 1] = root->child[i + 1]->child[MIN_SIZE - 1];
            free(root->child[i + 1]);
            root->size--;
            curNode->size += MIN_SIZE;
            if (0 == root->size){
                curTree->root = curNode;
                curNode->parent = NULL;
                free(root);
            } else {
                for(; i < root->size; i++){
                    root->key[i] = root->key[i + 1];
                    root->value[i] = root->value[i + 1];
                    root->child[i + 1] = root->child[i + 2];
                }
            }
            DeleteElementInSubTree(curTree, curNode, key);
        } else {
            if (key == root->key[i]){
                if (root->child[i]->size > root->child[i + 1]->size){
                    root->key[i] = root->child[i]->key[root->child[i]->size - 1];
                    root->value[i] = root->child[i]->value[root->child[i]->size - 1];
                    DeleteElementInSubTree(curTree, root->child[i], root->child[i]->key[root->child[i]->size - 1]);
                } else {
                    root->key[i] = root->child[i + 1]->key[0];
                    root->value[i] = root->child[i + 1]->value[0];
                    DeleteElementInSubTree(curTree, root->child[i + 1], root->child[i + 1]->key[0]);
                }
            } else {
                if (key > root->key[i]){
                    if (MIN_SIZE - 1 == root->child[i + 1]->size){
                        for (j = root->child[i + 1]->size - 1; j >= 0; j--){
                            root->child[i + 1]->key[j + 1] = root->child[i + 1]->key[j];
                            root->child[i + 1]->value[j + 1] = root->child[i + 1]->value[j];
                            root->child[i + 1]->child[j + 2] = root->child[i + 1]->child[j + 1];
                        }
                        root->child[i + 1]->key[0] = root->key[i];
                        root->child[i + 1]->value[0] = root->value[i];
                        root->child[i + 1]->child[1] = root->child[i + 1]->child[0];
                        root->child[i + 1]->child[0] = root->child[i]->child[root->child[i]->size];
                        root->key[i] = root->child[i]->key[root->child[i]->size - 1];
                        root->value[i] = root->child[i]->value[root->child[i]->size - 1];
                        root->child[i + 1]->size++;
                        root->child[i]->size--;
                    }
                    DeleteElementInSubTree(curTree, root->child[i + 1], key);
                } else {
                    if (MIN_SIZE - 1 == root->child[i]->size){
                        root->child[i]->key[root->child[i]->size] = root->key[i];
                        root->child[i]->value[root->child[i]->size] = root->value[i];
                        root->child[i]->child[root->child[i]->size + 1] = root->child[i + 1]->child[0];
                        root->key[i] = root->child[i + 1]->key[0];
                        root->value[i] = root->child[i + 1]->value[0];
                        for (j = 0; j < root->child[i + 1]->size - 1; j++){
                            root->child[i + 1]->key[j] = root->child[i + 1]->key[j + 1];
                            root->child[i + 1]->value[j] = root->child[i + 1]->value[j + 1];
                            root->child[i + 1]->child[j] = root->child[i + 1]->child[j + 1];
                        }
                        root->child[i + 1]->child[root->child[i + 1]->size - 1] = root->child[i + 1]->child[root->child[i + 1]->size];
                        root->child[i + 1]->size--;
                        root->child[i]->size++;
                    }
                    DeleteElementInSubTree(curTree, root->child[i], key);
                }
            }
        }
    }
}
