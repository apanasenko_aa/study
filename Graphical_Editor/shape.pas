unit shape;

{$mode objfpc}{$H+}

interface

uses
  Windows, Classes, SysUtils, Graphics, Transform, Math, Clipbrd, DOM, XMLWrite,
    types;

type

  TControlPoint = record
    ConPoint: TDoublePoint;
    PerShape: Integer;
    Index   : Integer;
  end;

  { T_Shape }

  T_Shape = class(TObject)
  public
    StartPoint: TDoublePoint;
    EndPoint  : TDoublePoint;
    FPenWidth : Integer;
    FPenColor : TColor;
    SizeRect  : TRect;
    procedure GetControlPoints (NumShape: Integer); virtual; abstract;
    procedure Move(Old, Now: TPoint); virtual; abstract;
    procedure MovePoint(Ind: integer; Old, Now: TPoint); virtual; abstract;
    procedure Draw(ScreenCanvas: TCanvas); virtual; abstract;
    procedure DrawRect(ScreenCanvas: TCanvas);
    procedure GetRect; virtual; abstract;
    procedure AddPoint(ScreenCanvas: TCanvas); virtual;
    procedure SetPapameters(ScreenCanvas: TCanvas); virtual;
    function FindRegion(SelectRect: TRect): integer; virtual; abstract;
    function SaveToSVG(XML: TXMLDocument; MainGroup, defs: TDomElement): TDomElement; virtual; abstract;
    function Save: string; virtual;
    constructor Create; virtual;
    constructor Load; virtual;
    procedure ConvertShape(Index: integer); virtual; abstract;
    procedure TurnShape (Fi: Integer); virtual; abstract;
  end;

  { T_FreeHand }

  T_FreeHand = class(T_Shape)
  public
    Points: array of TDoublePoint;
    procedure GetControlPoints (NumShape: Integer);  override;
    procedure Move(Old, Now: TPoint); override;
    procedure MovePoint(Ind: integer; Old, Now: TPoint); override;
    procedure GetRect; override;
    procedure Draw(ScreenCanvas: TCanvas); override;
    procedure AddPoint(ScreenCanvas: TCanvas); override;
    function FindRegion(SelectRect: TRect): integer; override;
    function SaveToSVG(XML: TXMLDocument; MainGroup, defs: TDomElement): TDomElement; override;
    function Save: string; override;
    constructor Load; override;
    procedure ConvertShape(Index: integer);  override;
    procedure TurnShape (Fi: Integer); override;
  end;

  { T_Line }

  T_Line = class(T_Shape)
  public
    FPenStyle: TPenStyle;
    procedure GetControlPoints (NumShape: Integer);  override;
    procedure Move(Old, Now: TPoint); override;
    procedure MovePoint(Ind: integer; Old, Now: TPoint); override;
    procedure Draw(ScreenCanvas: TCanvas); override;
    procedure GetRect; override;
    function FindRegion(SelectRect: TRect): integer; override;
    function SaveToSVG(XML: TXMLDocument; MainGroup, defs: TDomElement): TDomElement; override;
    function Save: string; override;
    constructor Load; override;
    procedure SetPapameters(ScreenCanvas: TCanvas); override;
    procedure ConvertShape(Index: integer);  override;
    procedure TurnShape (Fi: Integer); override;
  end;

  { T_Rectangle }

  T_Rectangle = class(T_Line)
  public
    FBrushColor: TColor;
    FBrushStyle: TBrushStyle;
    procedure Draw(ScreenCanvas: TCanvas); override;
    function FindRegion(SelectRect: TRect): integer; override;
    function SaveToSVG(XML: TXMLDocument; MainGroup, defs: TDomElement): TDomElement; override;
    function Save: string; override;
    constructor Load; override;
    procedure SetPapameters(ScreenCanvas: TCanvas); override;
    procedure TurnShape (Fi: Integer); override;
  end;

  { T_RoundRect }

  T_RoundRect = class(T_Rectangle)
  public
    FCornerRadius: integer;
    procedure Draw(ScreenCanvas: TCanvas); override;
    function FindRegion(SelectRect: TRect): integer; override;
    function SaveToSVG(XML: TXMLDocument; MainGroup, defs: TDomElement): TDomElement; override;
    function Save: string; override;
    constructor Load; override;
  end;

  { T_Ellipse }

  T_Ellipse = class(T_Rectangle)
  public
    procedure Draw(ScreenCanvas: TCanvas); override;
    function FindRegion(SelectRect: TRect): integer; override;
    function SaveToSVG(XML: TXMLDocument; MainGroup, defs: TDomElement): TDomElement; override;
  end;

  { T_Spray }

  T_Spray = class(T_FreeHand)
  public
    FRadius: integer;
    FDensity: integer;
    FRandSeeds: array of integer;
    FCounts: array of integer;
    procedure Draw(ScreenCanvas: TCanvas); override;
    procedure AddPoint(ScreenCanvas: TCanvas); override;
    procedure IncCounts;
    procedure GetRect; override;
    function FindRegion(SelectRect: TRect): integer; override;
    function SaveToSVG(XML: TXMLDocument; MainGroup, defs: TDomElement): TDomElement; override;
    function Save: string; override;
    constructor Load; override;
  end;

type

  { T_ShapeList }

  T_ShapeList = class(TObject)
  public
    Shapes: array of T_Shape;
    AllRect: TRect;
    ControlPoints: array of TControlPoint;
    procedure NextShapes(CurShape: T_Shape);
    procedure DrawAll(ScreenCanvas: TCanvas);
    procedure DrawRect(ScreenCanvas: TCanvas);
    procedure SaveToFile(var Output: TextFile);
    procedure LoadFromFile(var Input: TextFile);
    procedure DeleteShapes;
    procedure DownShapes;
    procedure UpShapes;
    procedure AddHistory;
    procedure ReadHistory;
    function GetAllRect(CurShape: T_Shape): TRect;
    function UpdateAllRect: TRect;
    procedure CopyShapes;
    procedure PasteShapes;
    procedure DrawControlPoints(ScreenCanvas: TCanvas);
  end;

{  T_History = class (TObject)
  public
    PosHistory: integer;
    MaxHistory: integer;
    MinHistory: integer;
    procedure AddHistory;
    procedure ReadHistory;
  end; }

  ClassShape = class of T_Shape;

procedure SaveFileToSVG(Name: string);


const
  delim = '|';
  SizeMemory = 50;

var
  ShapeList: T_ShapeList;
  SelectList: array of integer;
  ListShapes: array of ClassShape;
  Parameters: string;
  PosHistory: integer;
  MaxHistory: integer;
  MinHistory: integer;
  Patr: TPenPattern;
  PatternNumber: integer;

implementation

operator / (A: TPoint; B: Double): TDoublePoint;
begin
  Result.X := A.X / B;
  Result.Y := A.Y / B;
end;

operator + (A, B: TDoublePoint): TDoublePoint;
begin
  Result.X := A.X + B.X;
  Result.Y := A.Y + B.Y;
end;

operator - (A, B: TDoublePoint): TDoublePoint;
begin
  Result.X := A.X - B.X;
  Result.Y := A.Y - B.Y;
end;

operator - (A, B: TPoint): TPoint;
begin
  Result.X := A.X - B.X;
  Result.Y := A.Y - B.Y;
end;

procedure T_ShapeList.ReadHistory;
var
  Output: TextFile;
begin
  if not InRange(PosHistory, MinHistory, MaxHistory) then
    exit;
  SelectList := nil;
  Assign(Output, IntToStr(PosHistory));
  ShapeList.LoadFromFile(Output);
end;

procedure T_ShapeList.AddHistory;
var
  i: integer;
  Output: TextFile;
begin
  PosHistory += 1;
  Assign(Output, IntToStr(PosHistory));
  SaveToFile(Output);
  for i := PosHistory + 1 to MaxHistory do
    DeleteFile(IntToStr(i));
  if PosHistory > SizeMemory then
    DeleteFile(IntToStr(PosHistory - SizeMemory));
  MaxHistory := PosHistory;
  MinHistory := Max(MinHistory, MaxHistory - SizeMemory + 1);
end;

procedure T_ShapeList.DeleteShapes;
var
  i, j: integer;
begin
  for i := High(SelectList) downto 0 do
  begin
    Shapes[SelectList[i]].Free;
    for j := SelectList[i] to High(Shapes) - 1 do
      Shapes[j] := Shapes[j + 1];
    SetLength(Shapes, Length(Shapes) - 1);
  end;
  SelectList := nil;
end;

procedure T_ShapeList.UpShapes;
var
  ChangeShape: T_Shape;
  i: integer;
begin
  for i := 0 to High(SelectList) do
  begin
    if (High(SelectList) - i) = (High(Shapes) - SelectList[i]) then
      Exit;
    ChangeShape := Shapes[SelectList[i] + 1];
    Shapes[SelectList[i] + 1] := Shapes[SelectList[i]];
    Shapes[SelectList[i]] := ChangeShape;
    SelectList[i] += 1;
  end;
end;

procedure T_ShapeList.DownShapes;
var
  ChangeShape: T_Shape;
  i: integer;
begin
  for i := High(SelectList) downto 0 do
  begin
    if i = SelectList[i] then
      Exit;
    ChangeShape := Shapes[SelectList[i] - 1];
    Shapes[SelectList[i] - 1] := Shapes[SelectList[i]];
    Shapes[SelectList[i]] := ChangeShape;
    SelectList[i] -= 1;
  end;
end;

procedure T_ShapeList.CopyShapes;
var
  i: integer;
  CopyShape: string;
begin
  CopyShape := '';
  for i := 0 to High(SelectList) do
    CopyShape += Shapes[SelectList[i]].Save;
  Clipboard.AsText := CopyShape;
end;

function CopyFragment(Fragment: string): string;
begin
  Result := Copy(Fragment, Pos('=', Fragment) + 1, Pos('|', Fragment) -
    Pos('=', Fragment) - 1);
end;

procedure T_ShapeList.PasteShapes;
var
  i: integer;
  Class_Name: string;
begin
  Parameters := Clipboard.AsText;
  if Copy(Parameters, 1, Pos('=', Parameters) - 1) <> 'ClassName' then
    exit;
  SelectList := nil;
  repeat
    Class_Name := CopyFragment(Parameters);
    Delete(Parameters, 1, Pos('|', Parameters));
    for i := 0 to Length(ListShapes) - 1 do
      if ListShapes[i].ClassNameIs(Class_Name) then
      begin
        ShapeList.NextShapes(ListShapes[i].Create);
        ShapeList.Shapes[High(Shapes)].Load;
        SetLength(SelectList, Length(SelectList) + 1);
        SelectList[High(SelectList)] := High(Shapes);
      end;
  until Length(Parameters) = 0;
end;

procedure NextShape(Member: ClassShape);
begin
  SetLength(ListShapes, Length(ListShapes) + 1);
  ListShapes[high(ListShapes)] := Member;
end;

procedure T_ShapeList.NextShapes(CurShape: T_Shape);
begin
  SetLength(Shapes, Length(Shapes) + 1);
  Shapes[High(Shapes)] := CurShape;
end;

procedure T_ShapeList.DrawAll(ScreenCanvas: TCanvas);
var
  i: integer;
begin
  for i := 0 to High(ShapeList.Shapes) do
  begin
    Shapes[i].Draw(ScreenCanvas);
    Shapes[i].GetRect;
    AllRect := GetAllRect(Shapes[i]);
  end;
end;

procedure T_ShapeList.DrawRect(ScreenCanvas: TCanvas);
var
  i: integer;
begin
  ControlPoints:=nil;
  for i := 0 to High(SelectList) do begin
    Shapes[SelectList[i]].DrawRect(ScreenCanvas);
    Shapes[SelectList[i]].GetControlPoints (SelectList[i]);
  end;
 DrawControlPoints(ScreenCanvas);
end;

function T_ShapeList.GetAllRect(CurShape: T_Shape): TRect;
begin
  Result.Left := Min(AllRect.Left, Round((CurShape.SizeRect.Left + Offset.X) / Scale));
  Result.Top := Min(AllRect.Top, Round((CurShape.SizeRect.Top + Offset.Y) / Scale));
  Result.Right := Max(AllRect.Right, Round(
    (CurShape.SizeRect.Right + Offset.X) / Scale));
  Result.Bottom := Max(AllRect.Bottom, Round(
    (CurShape.SizeRect.Bottom + Offset.Y) / Scale));
end;

function T_ShapeList.UpdateAllRect: TRect;
var
  i: integer;
begin
  for i := 0 to High(SelectList) do
    Shapes[SelectList[i]].GetRect;
  Result.Left := Shapes[SelectList[0]].SizeRect.Left;
  Result.Top := Shapes[SelectList[0]].SizeRect.Top;
  Result.Right := Shapes[SelectList[0]].SizeRect.Right;
  Result.Bottom := Shapes[SelectList[0]].SizeRect.Bottom;
  for i := 1 to High(SelectList) do
  begin
    Result.Left := Min(Result.Left, Round(
      (Shapes[SelectList[i]].SizeRect.Left + Offset.X) / Scale));
    Result.Top := Min(Result.Top, Round(
      (Shapes[SelectList[i]].SizeRect.Top + Offset.Y) / Scale));
    Result.Right := Max(Result.Right, Round(
      (Shapes[SelectList[i]].SizeRect.Right + Offset.X) / Scale));
    Result.Bottom := Max(Result.Bottom, Round(
      (Shapes[SelectList[i]].SizeRect.Bottom + Offset.Y) / Scale));
  end;
end;

procedure T_ShapeList.SaveToFile(var Output: TextFile);
var
  i: integer;
begin
  Rewrite(Output);
  for i := 0 to High(Shapes) do
  begin
    Writeln(Output, Shapes[i].Save);
  end;
  CloseFile(Output);
end;

procedure T_ShapeList.LoadFromFile(var Input: TextFile);
var
  i: integer;
  Class_Name: string;
begin
  ShapeList.Destroy;
  ShapeList := T_ShapeList.Create;
  Scale := 1;
  Reset(Input);
  repeat
    Readln(Input, Parameters);
    Class_Name := CopyFragment(Parameters);
    Delete(Parameters, 1, Pos('|', Parameters));
    for i := 0 to Length(ListShapes) - 1 do
      if ListShapes[i].ClassNameIs(Class_Name) then
      begin
        ShapeList.NextShapes(ListShapes[i].Create);
        ShapeList.Shapes[High(Shapes)].Load;
      end;
  until EOF(Input);
  CloseFile(Input);
end;

constructor T_Shape.Create;
begin
end;

procedure T_Shape.AddPoint(ScreenCanvas: TCanvas);
begin
end;

procedure T_FreeHand.AddPoint(ScreenCanvas: TCanvas);
begin
  //if length(Points) = 0 then
  //  ScreenCanvas.MoveTo(W2S(StartPoint).X, W2S(StartPoint).Y);
  SetLength(Points, length(Points) + 1);
  Points[High(Points)] := EndPoint;
end;

procedure T_Spray.IncCounts;
begin
  Inc(FCounts[High(FCounts)]);
end;

procedure T_Spray.AddPoint(ScreenCanvas: TCanvas);
begin
  SetLength(Points, length(Points) + 1);
  Points[High(Points)] := EndPoint;
  SetLength(FCounts, Length(FCounts) + 1);
  FCounts[High(FCounts)] := 0;
  SetLength(FRandSeeds, Length(FRandSeeds) + 1);
  FRandSeeds[High(FRandSeeds)] := RandSeed;
end;

procedure T_FreeHand.Draw(ScreenCanvas: TCanvas);
var
  i: integer;
begin
  SetPapameters(ScreenCanvas);
  ScreenCanvas.Pen.Style := psSolid;
  ScreenCanvas.MoveTo(W2S(Points[0]).X, W2S(Points[0]).Y); //(W2S(StartPoint).X, W2S(StartPoint).Y);
  for i := 0 to High(Points) do
    ScreenCanvas.LineTo(W2S(Points[i]).X, W2S(Points[i]).Y);
end;

procedure T_Spray.Draw(ScreenCanvas: TCanvas);
var
  Rad, i, j, RandX, RandY: integer;
begin
  Rad := Round(FRadius * Scale);
  for i := 0 to High(Points) do
  begin
    RandSeed := FRandSeeds[i];
    for j := 0 to FCounts[i] * FDensity do
    begin
      RandX := RandomRange(-Rad, Rad);
      RandY := RandomRange(-Rad, Rad);
      if Round(Sqrt(Sqr(RandX) + Sqr(RandY))) < Rad then
        ScreenCanvas.Pixels[RandX + W2S(Points[i]).X, RandY + W2S(Points[i]).Y] :=
          FPenColor;
    end;
  end;
end;

procedure T_Line.Draw(ScreenCanvas: TCanvas);
begin
  SetPapameters(ScreenCanvas);
  ScreenCanvas.MoveTo(W2S(StartPoint).X, W2S(StartPoint).Y);
  ScreenCanvas.LineTo(W2S(EndPoint).X, W2S(EndPoint).Y);
end;

procedure T_Rectangle.Draw(ScreenCanvas: TCanvas);
begin
  SetPapameters(ScreenCanvas);
  ScreenCanvas.Rectangle(
    W2S(StartPoint).X, W2S(StartPoint).Y,
    W2S(EndPoint).X, W2S(EndPoint).Y);
end;

procedure T_RoundRect.Draw(ScreenCanvas: TCanvas);
begin
  SetPapameters(ScreenCanvas);
  ScreenCanvas.RoundRect(W2S(StartPoint).X, W2S(StartPoint).Y, W2S(
    EndPoint).X, W2S(EndPoint).Y, FCornerRadius, FCornerRadius);
end;

procedure T_Ellipse.Draw(ScreenCanvas: TCanvas);
begin
  SetPapameters(ScreenCanvas);
  ScreenCanvas.Ellipse(W2S(StartPoint).X, W2S(StartPoint).Y, W2S(
    EndPoint).X, W2S(EndPoint).Y);
  //ScreenCanvas.EllipseC(W2S(StartPoint).X, W2S(StartPoint).Y, 30,50);
end;

procedure T_Shape.SetPapameters(ScreenCanvas: TCanvas);
begin
  ScreenCanvas.Pen.Color := FPenColor;
  ScreenCanvas.Pen.Width := FPenWidth;

end;

procedure T_Line.SetPapameters(ScreenCanvas: TCanvas);
begin
  inherited;
  ScreenCanvas.Pen.Style := FPenStyle;
end;

procedure T_Rectangle.SetPapameters(ScreenCanvas: TCanvas);
begin
  inherited;
  ScreenCanvas.Brush.Color := FBrushColor;
  ScreenCanvas.Brush.Style := FBrushStyle;
end;

function T_Shape.Save: string;
begin
  Result := 'ClassName=' + ClassName + delim + 'PenWidth=' +
    IntToStr(FPenWidth) + delim + 'PenColor=' + ColorToString(FPenColor) +
    delim + 'Start.X=' + FloatToStr(StartPoint.X) + delim + 'Start.Y=' +
    FloatToStr(StartPoint.Y) + delim;
end;

function T_FreeHand.Save: string;
var
  i: integer;
begin
  Result := inherited;
  Result := Result + 'PointsCount=' + IntToStr(High(Points) + 1) + delim;
  for i := 0 to High(Points) do
    Result := Result + 'X' + IntToStr(i) + '=' + FloatToStr(Points[i].X) +
      delim + 'Y' + IntToStr(i) + '=' + FloatToStr(Points[i].Y) + delim;
end;

function T_Spray.Save: string;
var
  i: integer;
begin
  Result := inherited;
  Result += 'FRadius=' + IntToStr(FRadius) + delim + 'FDensity=' +
    IntToStr(FDensity) + delim;
  Result += 'FRandSeeds=' + IntToStr(High(FRandSeeds) + 1) + delim;
  for i := 0 to High(FRandSeeds) do
    Result += 'FRandSeeds' + IntToStr(i) + '=' + FloatToStr(FRandSeeds[i]) + delim;
  Result += 'FCounts=' + IntToStr(High(FCounts) + 1) + delim;
  for i := 0 to High(FRandSeeds) do
    Result += 'FCounts' + IntToStr(i) + '=' + FloatToStr(FCounts[i]) + delim;
end;

function T_Line.Save: string;
begin
  Result := inherited;
  Result := Result + 'End.X=' + FloatToStr(EndPoint.X) + delim +
    'End.Y=' + FloatToStr(EndPoint.Y) + delim + 'PenStyle=' +
    IntToStr(integer(FPenStyle)) + delim;
end;

function T_Rectangle.Save: string;
begin
  Result := inherited;
  Result := Result + 'BrushColor=' + ColorToString(FBrushColor) +
    delim + 'BrushStyle=' + IntToStr(integer(FBrushStyle)) + delim;
end;

function T_RoundRect.Save: string;
begin
  Result := inherited;
  Result := Result + 'RoundValue=' + IntToStr(FCornerRadius) + delim;
end;

constructor T_Shape.Load;
begin
  FPenWidth := StrToInt(CopyFragment(Parameters));
  Delete(Parameters, 1, Pos('|', Parameters));
  FPenColor := StringToColor(CopyFragment(Parameters));
  Delete(Parameters, 1, Pos('|', Parameters));
  StartPoint.X := StrToFloat(CopyFragment(Parameters));
  Delete(Parameters, 1, Pos('|', Parameters));
  StartPoint.Y := StrToFloat(CopyFragment(Parameters));
  Delete(Parameters, 1, Pos('|', Parameters));
end;

constructor T_FreeHand.Load;
var
  i: integer;
begin
  inherited;
  SetLength(Points, StrToInt(CopyFragment(Parameters)));
  Delete(Parameters, 1, Pos('|', Parameters));
  for i := 0 to High(Points) do
  begin
    Points[i].X := StrToFloat(CopyFragment(Parameters));
    Delete(Parameters, 1, Pos('|', Parameters));
    Points[i].Y := StrToFloat(CopyFragment(Parameters));
    Delete(Parameters, 1, Pos('|', Parameters));
  end;
end;

constructor T_Spray.Load;
var
  i: integer;
begin
  inherited;
  FRadius := StrToInt(CopyFragment(Parameters));
  Delete(Parameters, 1, Pos('|', Parameters));
  FDensity := StrToInt(CopyFragment(Parameters));
  Delete(Parameters, 1, Pos('|', Parameters));
  SetLength(FRandSeeds, StrToInt(CopyFragment(Parameters)));
  Delete(Parameters, 1, Pos('|', Parameters));
  for i := 0 to High(FRandSeeds) do
  begin
    FRandSeeds[i] := StrToInt(CopyFragment(Parameters));
    Delete(Parameters, 1, Pos('|', Parameters));
  end;
  SetLength(FCounts, StrToInt(CopyFragment(Parameters)));
  Delete(Parameters, 1, Pos('|', Parameters));
  for i := 0 to High(FCounts) do
  begin
    FCounts[i] := StrToInt(CopyFragment(Parameters));
    Delete(Parameters, 1, Pos('|', Parameters));
  end;
end;

constructor T_Line.Load;
begin
  inherited;
  EndPoint.X := StrToFloat(CopyFragment(Parameters));
  Delete(Parameters, 1, Pos('|', Parameters));
  EndPoint.Y := StrToFloat(CopyFragment(Parameters));
  Delete(Parameters, 1, Pos('|', Parameters));
  FPenStyle := TPenStyle(StrToInt(CopyFragment(Parameters)));
  Delete(Parameters, 1, Pos('|', Parameters));
end;

constructor T_Rectangle.Load;
begin
  inherited;
  FBrushColor := StringToColor(CopyFragment(Parameters));
  Delete(Parameters, 1, Pos('|', Parameters));
  FBrushStyle := TBrushStyle(StrToInt(CopyFragment(Parameters)));
  Delete(Parameters, 1, Pos('|', Parameters));
end;

constructor T_RoundRect.Load;
begin
  inherited;
  FCornerRadius := StrToInt(CopyFragment(Parameters));
  Delete(Parameters, 1, Pos('|', Parameters));
end;

function T_FreeHand.FindRegion(SelectRect: TRect): integer;
var
  i: integer;
  LinePos: array [0 .. 4] of TPoint;
  RectRegion, FreeHandRegion: HRGN;
  CofX, CofY: integer;
begin
  CofX := 5 + FPenWidth div 2;
  CofY := 5 + FPenWidth div 2;
  if (W2S(StartPoint).X) > (W2S(EndPoint).X) then
    CofX := -CofX;
  if (W2S(StartPoint).Y) > (W2S(EndPoint).Y) then
    CofY := -CofY;
  Result := 0;
  RectRegion := CreateRectRgnIndirect(SelectRect);
  LinePos[0] := Point(W2S(StartPoint).X - CofX, W2S(StartPoint).Y - CofY);
  LinePos[1] := Point(W2S(StartPoint).X + CofX, W2S(StartPoint).Y + CofY);
  for i := 0 to High(Points) do
  begin
    LinePos[2] := Point(W2S(Points[i]).X - CofX, W2S(Points[i]).Y - CofY);
    LinePos[3] := Point(W2S(Points[i]).X + CofX, W2S(Points[i]).Y + CofY);
    FreeHandRegion := CreatePolygonRgn(LinePos, 4, ALTERNATE);
    if CombineRgn(FreeHandRegion, FreeHandRegion, RectRegion, rgn_And) <> nullRegion then
    begin
      Result := 1;
    end
    else
    begin
      LinePos[0] := Point(W2S(Points[i]).X - CofX, W2S(Points[i]).Y - CofY);
      LinePos[1] := Point(W2S(Points[i]).X + CofX, W2S(Points[i]).Y + CofY);
    end;
  end;
  DeleteObject(FreeHandRegion);
  DeleteObject(RectRegion);
end;

function T_Spray.FindRegion(SelectRect: TRect): integer;
var
  R, i: integer;
  RectRegion, SprayRegion: HRGN;
begin
  R := Round(FRadius * Scale);
  Result := 0;
  RectRegion := CreateRectRgnIndirect(SelectRect);
  for i := 0 to High(Points) do
  begin
    SprayRegion := CreateEllipticRgn(W2S(Points[i]).X - R, W2S(Points[i]).Y -
      R, W2S(Points[i]).X + R, W2S(Points[i]).Y - R);
    if CombineRgn(SprayRegion, SprayRegion, RectRegion, rgn_And) <> nullRegion then
    begin
      Result := 1;
      break;
    end;
  end;
  DeleteObject(SprayRegion);
  DeleteObject(RectRegion);
end;

function T_Line.FindRegion(SelectRect: TRect): integer;
var
  LinePos: array [0 .. 3] of TPoint;
  LineRegion, RectRegion: HRGN;
  CofX, CofY: integer;
begin
  CofX := 5 + FPenWidth div 2;
  CofY := 5 + FPenWidth div 2;
  if (W2S(StartPoint).X) > (W2S(EndPoint).X) then
    CofX := -CofX;
  if (W2S(StartPoint).Y) > (W2S(EndPoint).Y) then
    CofY := -CofY;
  Result := 0;
  RectRegion := CreateRectRgnIndirect(SelectRect);
  LinePos[0] := Point(W2S(StartPoint).X - CofX, W2S(StartPoint).Y - CofY);
  LinePos[1] := Point(W2S(StartPoint).X - CofX, W2S(StartPoint).Y + CofY);
  LinePos[2] := Point(W2S(EndPoint).X + CofX, W2S(EndPoint).Y + CofY);
  LinePos[3] := Point(W2S(EndPoint).X + CofX, W2S(EndPoint).Y - CofY);
  LineRegion := CreatePolygonRgn(LinePos, 4, WINDING);
  if CombineRgn(LineRegion, LineRegion, RectRegion, rgn_And) <> nullRegion then
    Result := 1;
  DeleteObject(LineRegion);
  DeleteObject(RectRegion);
end;

function T_Rectangle.FindRegion(SelectRect: TRect): integer;
var
  RectangleRegion, RectRegion: HRGN;
begin
  Result := 0;
  RectRegion := CreateRectRgnIndirect(SelectRect);
  RectangleRegion := CreateRectRgnIndirect(SizeRect);
  if CombineRgn(RectangleRegion, RectangleRegion, RectRegion, rgn_And) <>
    nullRegion then
    Result := 1;
  DeleteObject(RectangleRegion);
  DeleteObject(RectRegion);
end;

function T_RoundRect.FindRegion(SelectRect: TRect): integer;
var
  RectRegion, RoundRectRegion: HRGN;
begin
  Result := 0;
  RectRegion := CreateRectRgnIndirect(SelectRect);
  RoundRectRegion := CreateRoundRectRgn(SizeRect.Left, SizeRect.Top,
    SizeRect.Right, SizeRect.Bottom, FCornerRadius, FCornerRadius);
  if CombineRgn(RoundRectRegion, RoundRectRegion, RectRegion, rgn_And) <>
    nullRegion then
    Result := 1;
  DeleteObject(RoundRectRegion);
  DeleteObject(RectRegion);
end;

function T_Ellipse.FindRegion(SelectRect: TRect): integer;
var
  RectRegion, EllipseRegion: HRGN;
begin
  Result := 0;
  RectRegion := CreateRectRgnIndirect(SelectRect);
  EllipseRegion := CreateEllipticRgnIndirect(SizeRect);
  if CombineRgn(EllipseRegion, EllipseRegion, RectRegion, rgn_And) <> nullRegion then
    Result := 1;
  DeleteObject(EllipseRegion);
  DeleteObject(RectRegion);
end;

procedure T_Shape.DrawRect(ScreenCanvas: TCanvas);
begin
  //ScreenCanvas.Pen.Cosmetic := False;
  //ScreenCanvas.Pen.Style := psPattern;
  //ScreenCanvas.Pen.SetPattern(Patr);
  ScreenCanvas.Pen.Width := 2;
  ScreenCanvas.Pen.Color := clRed;
  ScreenCanvas.Pen.Style := psDash;
  if Patr[1] = 0 then
    ScreenCanvas.Pen.Style := psDashDot
  else
    ScreenCanvas.Pen.Style := psDashDotDot;
  ScreenCanvas.Brush.Style := bsClear;
  ScreenCanvas.Rectangle(SizeRect);
end;

procedure T_FreeHand.GetRect;
var
  PointLT: TPoint;
  PointRB: TPoint;
  Cof, i: integer;
begin
  Cof := 5 + FPenWidth div 2;
  PointLT := W2S(Points[0]);
  PointRB := W2S(Points[0]);
  for i := 0 to High(Points) do
  begin
    PointLT.X := Min(PointLT.X, W2S(Points[i]).X);
    PointLT.Y := Min(PointLT.Y, W2S(Points[i]).Y);
    PointRB.X := Max(PointRB.X, W2S(Points[i]).X);
    PointRB.Y := Max(PointRB.Y, W2S(Points[i]).Y);
  end;
  SizeRect := Rect(PointLT.X - Cof, PointLT.Y - Cof, PointRB.X + Cof, PointRB.Y + Cof);
end;

procedure T_Spray.GetRect;
var
  R: integer;
begin
  inherited;
  R := Round(FRadius * Scale);
  SizeRect := Rect(SizeRect.Left - R, SizeRect.Top - R, SizeRect.Right + R,
    SizeRect.Bottom + R);
end;

procedure T_Line.GetRect;
var
  PointLT: TPoint;
  PointRB: TPoint;
  Cof: integer;
begin
  Cof := 5 + FPenWidth div 2;
  PointLT.X := Min(W2S(StartPoint).X, W2S(EndPoint).X);
  PointLT.Y := Min(W2S(StartPoint).Y, W2S(EndPoint).Y);
  PointRB.X := Max(W2S(StartPoint).X, W2S(EndPoint).X);
  PointRB.Y := Max(W2S(StartPoint).Y, W2S(EndPoint).Y);
  SizeRect := Rect(PointLT.X - Cof, PointLT.Y - Cof, PointRB.X + Cof, PointRB.Y + Cof);
end;

procedure T_FreeHand.Move(Old, Now: TPoint);
var
  i: integer;
begin
  StartPoint += (Now - Old) / Scale;
  for i := 0 to High(Points) do
    Points[i] += (Now - Old) / Scale;
end;

procedure T_Line.Move(Old, Now: TPoint);
begin
  StartPoint += (Now - Old) / Scale;
  EndPoint += (Now - Old) / Scale;
end;

procedure T_FreeHand.GetControlPoints (NumShape: Integer);
var
  i: integer;
begin
  for i := 0 to High(Points) do
  begin
    with ShapeList do
    begin
      SetLength(ControlPoints, Length(ControlPoints) + 1);
      ControlPoints[High(ControlPoints)].ConPoint := Points[i];
      ControlPoints[High(ControlPoints)].Index := i;
      ControlPoints[High(ControlPoints)].PerShape:= NumShape;
    end;
  end;
end;

procedure T_Line.GetControlPoints (NumShape: Integer);
begin
  with ShapeList do
  begin
    SetLength(ControlPoints, Length(ControlPoints) + 2);
    ControlPoints[High(ControlPoints)-1].ConPoint := StartPoint;
    ControlPoints[High(ControlPoints)-1].Index := 1;
    ControlPoints[High(ControlPoints)-1].PerShape:= NumShape;
    ControlPoints[High(ControlPoints)].ConPoint := EndPoint;
    ControlPoints[High(ControlPoints)].Index := 2;
    ControlPoints[High(ControlPoints)].PerShape:= NumShape;
  end;
end;

procedure T_ShapeList.DrawControlPoints(ScreenCanvas: TCanvas);
var
  i: integer;
begin
  for i := 0 to High(ShapeList.ControlPoints) do
  with ScreenCanvas do
  begin
    Pen.Width := 1;
    Pen.Color := clHighlight;
    Pen.Style := psSolid;
    Pen.Mode := pmCopy;
    Brush.Color := clPurple;
    Brush.Style := bsSolid;
    with ShapeList.ControlPoints[i] do
    Ellipse(W2S(ConPoint).X - 5, W2S(ConPoint).Y - 5, W2S(ConPoint).X + 5, W2S(ConPoint).Y + 5);
  end;
end;

procedure T_FreeHand.MovePoint(Ind: integer; Old, Now: TPoint);
begin
    Points[Ind] += (Now - Old) / Scale;
end;

procedure T_Line.MovePoint(Ind: integer; Old, Now: TPoint);
begin
   If Ind = 1 then
    StartPoint += (Now - Old) / Scale
   else
    EndPoint += (Now - Old) / Scale;
end;

function TurnPoint(Fi: double; CurPoint: TDoublePoint): TDoublePoint;
begin
  Fi:= DegToRad(fi);
  with Result do
  begin
    x := CurPoint.X * cos(Fi) - CurPoint.y * sin(Fi);
    y := CurPoint.Y * cos(Fi) + CurPoint.X * sin(Fi);
  end;
end;

procedure SaveFileToSVG(Name: string);
var
  XML: TXMLDocument;
  SVG, defs, MainGroup, Figure: TDOMElement;
  i: integer;
begin
  if Name = '' then
    exit;
  XML := TXMLDocument.Create;
  try
    SVG := XML.CreateElement('svg');
    SVG.SetAttribute('xmlns', 'http://www.w3.org/2000/svg');
    SVG.SetAttribute('version', '1.1');
    SVG.SetAttribute('baseProfile', 'full');
    SVG.SetAttribute('width', Format('%dpx',
        [ShapeList.AllRect.Right - ShapeList.AllRect.Left]));
    SVG.SetAttribute('height', Format('%dpx',
        [ShapeList.AllRect.Bottom - ShapeList.AllRect.Top]));
    XML.Appendchild(SVG);
    defs := XML.CreateElement('defs');
    SVG.AppendChild(defs);
    MainGroup := XML.CreateElement('g');
    MainGroup.SetAttribute('transform', Format('translate(%d,%d)',
        [-ShapeList.AllRect.Left, -ShapeList.AllRect.Top]));
    svg.AppendChild(MainGroup);
    for i := 0 to high(ShapeList.Shapes) do
    begin
      Figure := ShapeList.Shapes[i].SaveToSVG(XML, MainGroup, defs);
      if Figure = nil then exit;
      MainGroup.AppendChild(Figure);
    end;
    writeXMLFile(XML, Name);
  finally
    XML.Free;
  end;
end;

function AddPattern(id: string; XML: TXMLDocument): TDomElement;
begin
  Result := XML.CreateElement('pattern');
  result.SetAttribute('id', id);
  result.SetAttribute('x', '0');
  result.SetAttribute('y', '0');
  result.SetAttribute('width', '5');
  result.SetAttribute('height', '5');
  result.SetAttribute('viewBox', '0 0 5 5');
  result.SetAttribute('patternUnits', 'userSpaceOnUse');
end;

function PatternBrush(Style: integer; Color: string; XML: TXMLDocument; defs: TDomElement): string;
var
  NewPattern, temp: TDomElement;
begin
  inc(PatternNumber);
  result := 'p' + IntToStr(PatternNumber);
  NewPattern := AddPattern(Result, XML);
  temp := XML.CreateElement('path');
  temp.SetAttribute('stroke', Color);
  case Style of
    integer(bsHorizontal):
    begin
      temp.SetAttribute('d', 'M 0 2 L 10 2z');
      NewPattern.AppendChild(temp);
    end;
    integer(bsVertical):
    begin
      temp.SetAttribute('d', 'M 2 0 L 2 10z');
      NewPattern.AppendChild(temp);
    end;
    integer(bsFDiagonal):
    begin
      temp.SetAttribute('d', 'M 0 0 L 15 15z');
      NewPattern.AppendChild(temp);
    end;
    integer(bsBDiagonal):
    begin
      temp.SetAttribute('d', 'M 0 5 L 5 0z');
      NewPattern.AppendChild(temp);
    end;
    integer(bsCross):
    begin
      temp := XML.CreateElement('path');
      temp.SetAttribute('stroke', Color);
      temp.SetAttribute('d', 'M 0 2 L 10 2z');
      NewPattern.AppendChild(temp);
      temp := XML.CreateElement('path');
      temp.SetAttribute('stroke', Color);
      temp.SetAttribute('d', 'M 2 0 L 2 10z');
      NewPattern.AppendChild(temp);
    end;
    integer(bsDiagCross):
    begin
      temp := XML.CreateElement('path');
      temp.SetAttribute('stroke', Color);
      temp.SetAttribute('d', 'M 0 0 L 5 5z');
      NewPattern.AppendChild(temp);
      temp := XML.CreateElement('path');
      temp.SetAttribute('stroke', Color);
      temp.SetAttribute('d', 'M 0 5 L 5 0z');
      NewPattern.AppendChild(temp);
    end;
  end;
  defs.AppendChild(NewPattern);
end;

function GetHexFrom (Col: TColor) :string;
var
  R, G, B: integer;
begin
  B:= Col div $10000;
  G:= (Col mod $10000) div $100;
  R:= Col mod $100;
  Result:= '#' + IntToHex(R, 2) + IntToHex(G, 2) + IntToHex(B, 2);
end;

procedure SetStrokeParams(FigureNode: TDOMElement; ColorBGR: TColor; Width: Integer; PenStyle: TPenStyle);
var
  ColorRGB: string;
begin
  ColorRGB := GetHexFrom(ColorBGR);
  FigureNode.SetAttribute('stroke', ColorRGB);
  FigureNode.SetAttribute('stroke-width', FloatToStr(Width));
  if PenStyle <> psSolid then
    FigureNode.SetAttribute('stroke-linecap', 'round');
  case integer(PenStyle) of
    1: FigureNode.SetAttribute('stroke-dasharray', '20, 20');
    2: FigureNode.SetAttribute('stroke-dasharray', '1, 20');
    3: FigureNode.SetAttribute('stroke-dasharray', '20, 20, 1, 20');
    4: FigureNode.SetAttribute('stroke-dasharray', '20, 20, 1, 20, 1, 20');
  end;
end;

procedure SetFillParams(XML: TXMLDocument; FigureNode, defs: TDomElement;
  FBColor: TColor; FBStyle: TBrushStyle);
var
  s: string;
begin
  Case integer(FBStyle) of
    0: TDOMElement(FigureNode).SetAttribute('fill', GetHexFrom(FBColor));
    1: TDOMElement(FigureNode).SetAttribute('fill', 'none');
    else
    begin
      s := 'url(#' + PatternBrush (integer(FBStyle), GetHexFrom(FBColor), XML, defs) + ')';
      TDOMElement(FigureNode).SetAttribute('fill', s);
    end;
  end;
end;

function T_FreeHand.SaveToSVG(XML: TXMLDocument; MainGroup, defs: TDomElement): TDomElement;
var
  i:integer;
  s: string;
begin
  Result := XML.CreateElement('polyline');
  Result.SetAttribute('fill', 'none');
  Result.SetAttribute('stroke', Format('%s',[GetHexFrom(FPenColor)]));
  Result.SetAttribute('stroke-width', FloatToStr(FPenWidth));
  for i:= 0 to High(Points) do
    s := s + Format('%g,%g ', [Points[i].X, Points[i].Y]);
  Result.SetAttribute('points', s);
end;

function T_Spray.SaveToSVG(XML: TXMLDocument; MainGroup, defs: TDomElement
  ): TDomElement;
var
  tmp: TDOMElement;
  i, j, a, b, X, Y: integer;
begin
  Result := XML.CreateElement('g');
  Result.SetAttribute('stroke', GetHexFrom(FPenColor));
  for i := 0 to High(Points) do
  begin
    RandSeed := FRandSeeds[i];
    X := Round(Points[i].X);
    Y := Round(Points[i].Y);
    for j := 1 to FCounts[i] * FDensity do
    begin
      repeat
        a := X  + RandomRange(-FRadius, FRadius);
        b := Y  + RandomRange(-FRadius, FRadius);
      until sqr(a - X) + sqr(b - Y) <= sqr(FRadius);
      tmp := XML.CreateElement('ellipse');
      tmp.SetAttribute('cx', IntToStr(a));
      tmp.SetAttribute('cy', IntToStr(b));
      tmp.SetAttribute('rx', '1');
      tmp.SetAttribute('ry', '1');
      Result.AppendChild(tmp);
    end;
  end;
end;

function T_Line.SaveToSVG(XML: TXMLDocument; MainGroup, defs: TDomElement): TDomElement;
begin
  Result := XML.CreateElement('line');
  Result.SetAttribute('x1', FloatToStr(StartPoint.X));
  Result.SetAttribute('y1', FloatToStr(StartPoint.Y));
  Result.SetAttribute('x2', FloatToStr(EndPoint.X));
  Result.SetAttribute('y2', FloatToStr(EndPoint.Y));
  SetStrokeParams(Result, FPenColor, FPenWidth, FPenStyle);
end;

function T_Rectangle.SaveToSVG(XML: TXMLDocument; MainGroup, defs: TDomElement): TDomElement;
begin
  Result := XML.CreateElement('rect');
  with SizeRect do
  begin
    result.SetAttribute('x', IntToStr(left));
    result.SetAttribute('y', IntToStr(top));
    result.SetAttribute('width', IntToStr(right - left));
    result.SetAttribute('height', IntToStr(bottom - top));
  end;
  SetStrokeParams(Result, FPenColor, FPenWidth, FPenStyle);
  SetFillParams(XML, Result, defs, FBrushColor, FBrushStyle);
end;

function T_Ellipse.SaveToSVG(XML: TXMLDocument; MainGroup, defs: TDomElement): TDomElement;
var
  C, R: TPoint;
begin
  C := CenterPoint(SizeRect);
  with SizeRect do
    R := Point( Abs( Left - Right ) div 2, Abs( Top - Bottom ) div 2 );
  Result := XML.CreateElement('ellipse');
  Result.SetAttribute('cx', IntToStr(C.x));
  Result.SetAttribute('cy', IntToStr(C.y));
  Result.SetAttribute('rx', IntToStr(R.x));
  Result.SetAttribute('ry', IntToStr(R.Y));
  SetStrokeParams(Result, FPenColor, FPenWidth, FPenStyle);
  SetFillParams(XML, Result, defs, FBrushColor, FBrushStyle);
end;

function T_RoundRect.SaveToSVG(XML: TXMLDocument; MainGroup, defs: TDomElement): TDomElement;
begin
  result := XML.CreateElement('rect');
  with SizeRect do
  begin
    result.SetAttribute('x', IntToStr(Left));
    result.SetAttribute('y', IntToStr(Top));
    result.SetAttribute('width', IntToStr(Right - Left));
    result.SetAttribute('height', IntToStr(Bottom - Top));
    result.SetAttribute('rx', IntToStr(FCornerRadius));
  end;
  SetStrokeParams(Result, FPenColor, FPenWidth, FPenStyle);
  SetFillParams(XML, Result, defs, FBrushColor, FBrushStyle);
end;

procedure T_FreeHand.ConvertShape(Index: integer);
var
  i: Integer;
begin
  Case Index of
    0: exit;
    1: for i:= 0 to High(Points) do
        Points[i].Y := SizeRect.Bottom - Points[i].Y + SizeRect.Top;
    2: for i:= 0 to High(Points) do
        Points[i].X := SizeRect.Right - Points[i].X + SizeRect.Left;
    3: begin
      for i:= 0 to High(Points) do
        Points[i].Y := SizeRect.Bottom - Points[i].Y + SizeRect.Top;
      for i:= 0 to High(Points) do
        Points[i].X := SizeRect.Right - Points[i].X + SizeRect.Left;
    end;
  end;
end;

procedure T_Line.ConvertShape(Index: integer);
begin
  Case Index of
    0: exit;
    1: begin
        StartPoint.Y := SizeRect.Bottom - StartPoint.Y + SizeRect.Top;
        EndPoint.Y := SizeRect.Bottom - EndPoint.Y + SizeRect.Top;
    end;
    2: begin
        StartPoint.X := SizeRect.Right - StartPoint.X + SizeRect.Left;
        EndPoint.X := SizeRect.Right - EndPoint.X + SizeRect.Left;
    end;
    3: begin
        StartPoint.Y := SizeRect.Bottom - StartPoint.Y + SizeRect.Top;
        EndPoint.Y := SizeRect.Bottom - EndPoint.Y + SizeRect.Top;
        StartPoint.X := SizeRect.Right - StartPoint.X + SizeRect.Left;
        EndPoint.X := SizeRect.Right - EndPoint.X + SizeRect.Left;
    end;
  end;
end;

procedure T_FreeHand.TurnShape (Fi: Integer);
var
  i: integer;
begin
  for i := 1 to High(Points) do
  begin
    Points[i] -= Points[0];
    Points[i] := TurnPoint(Fi , Points[i]);
    Points[i] += Points[0];
  end;
end;

procedure T_Line.TurnShape (Fi: Integer);
begin
  EndPoint -= StartPoint;
  EndPoint := TurnPoint(Fi , EndPoint);
  EndPoint += StartPoint;
end;

procedure T_Rectangle.TurnShape (Fi: Integer);
begin
  if (fi mod 90) <> 0 then exit;
  inherited;
end;

initialization

  NextShape(T_FreeHand);
  NextShape(T_Line);
  NextShape(T_Rectangle);
  NextShape(T_RoundRect);
  NextShape(T_Ellipse);
  NextShape(T_Spray);

end.

