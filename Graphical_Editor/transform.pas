unit transform;

{$mode objfpc}{$H+}

interface

uses Windows, Classes, Controls, Math;

type
  TDoublePoint = record
    X, Y: double;
  end;

var
  OldScale: real;
  Scale: real;
  Offset: TPoint;

function S2W(sx, sy: integer): TDoublePoint;
function W2S(w: TDoublePoint): TPoint;
procedure SetScale(Zoom: boolean; X, Y: integer);
procedure ChangeOffset(X, Y: integer);

implementation

function S2W(sx, sy: integer): TDoublePoint;
begin
  Result.X := (sx + Offset.X) / Scale;
  Result.Y := (sy + Offset.Y) / Scale;
end;

function W2S(w: TDoublePoint): TPoint;
begin
  Result.X := Round(w.X * Scale) - Offset.X;
  Result.Y := Round(w.Y * Scale) - Offset.Y;
end;

procedure ChangeOffset(X, Y: integer);
begin
  Offset.X := Round((OldScale - Scale) * X - Offset.X);
  Offset.Y := Round((OldScale - Scale) * Y - Offset.Y);
end;

procedure SetScale(Zoom: boolean; X, Y: integer);
const
  StepScale = 0.05;
  MinScale = 0.15;
  MaxScale = 3.5;
var
  Fixed: TDoublePoint;
  newScale: double;
begin
  newScale := Scale + StepScale * IfThen(Zoom, +1, -1);
  if not InRange(newScale, MinScale, MaxScale) then
    exit;
  Fixed := S2W(X, Y);
  OldScale := Scale;
  Scale := newScale;
  Offset.X := Offset.X + (W2S(Fixed).X - X);
  Offset.Y := Offset.Y + (W2S(Fixed).Y - Y);
end;

initialization

  Scale := 1;

end.

