unit ExpBMP;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Spin,
  StdCtrls, ComCtrls, Shape, Math;

type

  { TFExports }

  TFExports = class(TForm)
    B_Save: TButton;
    B_Cansel: TButton;
    L_Width: TLabel;
    L_Height: TLabel;
    SE_Width: TSpinEdit;
    SE_Height: TSpinEdit;
    procedure B_SaveClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  FExports: TFExports;
  P_Width, P_Height: Integer;

implementation

{$R *.lfm}

{ TFExports }

procedure TFExports.B_SaveClick(Sender: TObject);
begin
  P_Height := SE_Height.Value;
  P_Width := SE_Width.Value;
  ModalResult := mrOK;
  FExports.Close;
end;

procedure TFExports.FormShow(Sender: TObject);
begin
  SE_Height.Value:= Min (P_Height, 2000);
  SE_Width.Value:= Min (P_Width, 2000);
end;

end.

