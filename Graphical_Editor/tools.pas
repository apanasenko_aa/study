unit tools;

{$mode objfpc}{$H+}

interface

uses
  Windows, Classes, Types, SysUtils, Shape, Graphics, Controls, Transform,
  ExtCtrls, StdCtrls, ComCtrls, Spin, Dialogs, Math, Menus;

type
  T_Tool = class
  private
    PosTop: integer;
    CurShape: T_Shape;
    ChildTool: T_Tool;
    PropertyTool: TPanel;
    PaintBox: TPaintBox;
    procedure SetShapesParams; virtual; abstract;
  public
    SCanvas: TCanvas;
    procedure MouseDown(X, Y: integer); virtual; abstract;
    procedure MouseMove(X, Y: integer); virtual; abstract;
    procedure MouseUp(X, Y: integer; aShift: TShiftState); virtual; abstract;
    procedure ParamsInit; virtual; abstract;
    procedure ParamsFree; virtual; abstract;
    class function GetTypeShape: ClassShape; virtual; abstract;
    procedure ColorChange(Pen, Brush: TColor);
    constructor Create(ScreenCanvas: TCanvas; P_Tool: TPanel; PBox: TPaintBox);
  end;

  T_PaintTool = class(T_Tool)
  private
    L_LineWidth: TLabel;
    LineWidth: TTrackBar;
    procedure SetShapesParams; override;
    procedure LineWidthChange(Sender: TObject);
  public
    procedure MouseDown(X, Y: integer); override;
    procedure MouseMove(X, Y: integer); override;
    procedure MouseUp(X, Y: integer; aShift: TShiftState); override;
    procedure ParamsInit; override;
    procedure ParamsFree; override;
    class function GetTypeShape: ClassShape; override;
  end;

  T_FreeHandTool = class(T_PaintTool)
    procedure MouseDown(X, Y: integer); override;
    class function GetTypeShape: ClassShape; override;
  end;

  T_LineTool = class(T_PaintTool)
  private
    L_LineStyle: TLabel;
    LineStyle: TComboBox;
    procedure SetShapesParams; override;
    procedure LineStyleChange(Sender: TObject);
  public
    procedure MouseDown(X, Y: integer); override;
    procedure ParamsInit; override;
    procedure ParamsFree; override;
    class function GetTypeShape: ClassShape; override;
  end;

  T_RectangleTool = class(T_LineTool)
  private
    L_BrushStyle: TLabel;
    BrushStyle: TComboBox;
    procedure SetShapesParams; override;
    procedure BrushStyleChange(Sender: TObject);
  public
    procedure MouseDown(X, Y: integer); override;
    procedure ParamsInit; override;
    procedure ParamsFree; override;
    class function GetTypeShape: ClassShape; override;
  end;

  T_RoundRectTool = class(T_RectangleTool)
  private
    L_CornerRadius: TLabel;
    CornerRadius: TTrackBar;
    procedure SetShapesParams; override;
    procedure CornerRadiusChange(Sender: TObject);
  public
    procedure MouseDown(X, Y: integer); override;
    procedure ParamsInit; override;
    procedure ParamsFree; override;
    class function GetTypeShape: ClassShape; override;
  end;

  T_EllipseTool = class(T_RectangleTool)
  public
    procedure MouseDown(X, Y: integer); override;
    class function GetTypeShape: ClassShape; override;
  end;

  T_SprayTool = class(T_Tool)
  private
    SprayRadius  : TSpinEdit;
    L_SprayRadius: TLabel;
    SpraySpeed   : TSpinEdit;
    L_SpraySpeed : TLabel;
    SprayTimer   : TTimer;
    procedure On_Timer(Sender: TObject);
    procedure SetShapesParams; override;
    procedure SprayChange(Sender: TObject);
  public
    procedure MouseDown(X, Y: integer); override;
    procedure MouseMove(X, Y: integer); override;
    procedure MouseUp(X, Y: integer; aShift: TShiftState); override;
    procedure ParamsInit; override;
    procedure ParamsFree; override;
    class function GetTypeShape: ClassShape; override;
  end;

  T_ControlTool = class(T_Tool)
  private
    L_Convert : TLabel;
    CB_Convert: TComboBox;
    L_Turn    : TLabel;
    SE_Turn   : TSpinEdit;
    CB_Turn   : TComboBox;
    fi        : Integer;
    HaveRect  : boolean;
    procedure ChangeConvert (Sender: TObject);
    procedure ChangeSE_Turn (Sender: TObject);
    procedure ChangeCB_Turn (Sender: TObject);
  public
    procedure MouseDown(X, Y: integer); override; abstract;
    procedure MouseMove(X, Y: integer); override; abstract;
    procedure MouseUp  (X, Y: integer; aShift: TShiftState); override; abstract;
    procedure ParamsInit; override;
    procedure ParamsFree; override;
  end;

  T_ExtentTool = class(T_ControlTool)
  public
    procedure MouseWheel(Zoom: boolean; X, Y: integer);
  end;

  T_HandTool = class(T_ControlTool)
  private
    OldX, OldY: Integer;
  public
    procedure MouseDown(X, Y: integer); override;
    procedure MouseMove(X, Y: integer); override;
  end;

  T_SelectTool = class(T_ControlTool)
  private
    StartSelect: TPoint;
    EndSelect  : TPoint;
  public
    procedure MouseDown(X, Y: integer); override;
    procedure MouseMove(X, Y: integer); override;
    procedure MouseUp(X, Y: integer; aShift: TShiftState); override;
  end;

  T_EditTool = class(T_ControlTool)
  private
    Old   : TPoint;
    PtEdit: Boolean;
    PtMove: TControlPoint;
  public
    procedure MouseDown(X, Y: integer); override;
    procedure MouseMove(X, Y: integer); override;
    procedure MouseUp(X, Y: integer; aShift: TShiftState); override;
  end;

  TToolClass = class of T_Tool;

var
  RegistrTool: array of TToolClass;
  HaveChanged: boolean;
  Select     : boolean;

implementation

procedure NewToolAdd(NTool: TToolClass);
begin
  SetLength(RegistrTool, Length(RegistrTool) + 1);
  RegistrTool[High(RegistrTool)] := NTool;
end;

//=========================== Tool ===========================//

constructor T_Tool.Create(ScreenCanvas: TCanvas; P_Tool: TPanel; PBox: TPaintBox);
begin
  PaintBox := PBox;
  PropertyTool := P_Tool;
  SCanvas := ScreenCanvas;
end;

procedure T_Tool.ColorChange(Pen, Brush: TColor);
var
  I: integer;
begin
  for i := 0 to High(SelectList) do
  begin
    ShapeList.Shapes[SelectList[i]].FPenColor := Pen;
    if ShapeList.Shapes[SelectList[i]].InheritsFrom(T_Rectangle) then
      (ShapeList.Shapes[SelectList[i]] as T_Rectangle).FBrushColor := Brush;
  end;
  ShapeList.AddHistory;
end;

//======================== Paint Tool ========================//

procedure T_PaintTool.MouseDown(X, Y: integer);
begin
  CurShape.StartPoint := S2W(X, Y);
  CurShape.EndPoint := S2W(X, Y);
  ShapeList.NextShapes(CurShape);
  SetShapesParams;
  HaveChanged := True;
end;

procedure T_PaintTool.MouseMove(X, Y: integer);
begin
  PaintBox.Invalidate;
  CurShape.AddPoint(SCanvas);
  CurShape.EndPoint := S2W(X, Y);
  CurShape.Draw(SCanvas);
end;

procedure T_PaintTool.MouseUp(X, Y: integer; aShift: TShiftState);
begin
  CurShape.GetRect;
  ShapeList.AllRect := ShapeList.GetAllRect(CurShape);
  CurShape := nil;
end;

procedure T_PaintTool.ParamsInit;
begin
  PosTop := 5;
  PropertyTool.Visible := True;
  LineWidth := TTrackBar.Create(PropertyTool);
  L_LineWidth := TLabel.Create(PropertyTool);
  with L_LineWidth do
  begin
    Parent := PropertyTool;
    SetBounds(0, PosTop, 95, 20);
    Caption := ' Ширина Кисти : ';
    PosTop += 20;
  end;
  with LineWidth do
  begin
    Parent := PropertyTool;
    ShowSelRange := False;
    SetBounds(1, PosTop, 93, 20);
    Min := 1;
    Max := 25;
    Frequency := 5;
    PosTop += 20;
    OnChange := @LineWidthChange;
  end;
end;

procedure T_PaintTool.ParamsFree;
begin
  FreeAndNil(LineWidth);
  FreeAndNil(L_LineWidth);
end;

procedure T_PaintTool.LineWidthChange(Sender: TObject);
var
  I: integer;
begin
  for i := 0 to High(SelectList) do
    ShapeList.Shapes[SelectList[i]].FPenWidth := LineWidth.Position;
  ShapeList.AddHistory;
  PaintBox.Invalidate;
end;

procedure T_PaintTool.SetShapesParams;
begin
  CurShape.FPenColor := SCanvas.Pen.Color;
  CurShape.FPenWidth := LineWidth.Position;
end;

class function T_PaintTool.GetTypeShape: ClassShape;
begin
  Result := T_Shape;
end;

//====================== FreeHand Tool ======================//

procedure T_FreeHandTool.MouseDown(X, Y: integer);
begin
  CurShape := T_FreeHand.Create;
  inherited;
end;

class function T_FreeHandTool.GetTypeShape: ClassShape;
begin
  Result := T_FreeHand;
end;

//======================== Line Tool ========================//

procedure T_LineTool.MouseDown(X, Y: integer);
begin
  if CurShape = nil then
    CurShape := T_Line.Create;
  inherited;
end;

procedure T_LineTool.ParamsInit;
begin
  inherited;
  L_LineStyle := TLabel.Create(PropertyTool);
  with L_LineStyle do
  begin
    Parent := PropertyTool;
    SetBounds(0, PosTop, 95, 20);
    Caption := ' Стиль линии : ';
    PosTop += 20;
  end;
  LineStyle := TComboBox.Create(PropertyTool);
  with LineStyle do
  begin
    Parent := PropertyTool;
    SetBounds(5, PosTop, 85, 20);
    PosTop += 25;
    Style := csDropDownList;
    with Items do
    begin
      Add('Сплошная');
      Add('Штрих');
      Add('Пунктир');
      Add('Штрих-Пунктир');
      Add('Штрих-пунктир двойной');
    end;
    OnChange := @LineStyleChange;
    ItemIndex := 0;
  end;
end;

procedure T_LineTool.ParamsFree;
begin
  inherited;
  FreeAndNil(L_LineStyle);
  FreeAndNil(LineStyle);
end;

procedure T_LineTool.SetShapesParams;
begin
  inherited;
  (CurShape as T_Line).FPenStyle := TPenStyle(LineStyle.ItemIndex);
end;

procedure T_LineTool.LineStyleChange(Sender: TObject);
var
  I: integer;
begin
  for i := 0 to High(SelectList) do
    (ShapeList.Shapes[SelectList[i]] as T_Line).FPenStyle :=
      TPenStyle(LineStyle.ItemIndex);
  ShapeList.AddHistory;
  PaintBox.Invalidate;
end;

class function T_LineTool.GetTypeShape: ClassShape;
begin
  Result := T_Line;
end;

//====================== Rectangle Tool ======================//

procedure T_RectangleTool.MouseDown(X, Y: integer);
begin
  if CurShape = nil then
    CurShape := T_Rectangle.Create;
  inherited;
end;

procedure T_RectangleTool.ParamsInit;
begin
  inherited;
  L_BrushStyle := TLabel.Create(PropertyTool);
  with L_BrushStyle do
  begin
    Parent := PropertyTool;
    SetBounds(0, PosTop, 95, 20);
    Caption := ' Стиль заливки : ';
    PosTop += 20;
  end;
  BrushStyle := TComboBox.Create(PropertyTool);
  with BrushStyle do
  begin
    Parent := PropertyTool;
    SetBounds(5, PosTop, 85, 20);
    PosTop += 25;
    Style := csDropDownList;
    with Items do
    begin
      Add('Сплошная');
      Add('Без Заливки');
      Add('Горизонтальная');
      Add('Вертикальная');
      Add('Диагональная');
      Add('Другая');
      Add('Клетка');
      Add('Сетка');
    end;
    OnChange := @BrushStyleChange;
    ItemIndex := 0;
  end;
end;

procedure T_RectangleTool.ParamsFree;
begin
  inherited;
  FreeAndNil(L_BrushStyle);
  FreeAndNil(BrushStyle);
end;

procedure T_RectangleTool.SetShapesParams;
begin
  inherited;
  (CurShape as T_Rectangle).FBrushColor := SCanvas.Brush.Color;
  (CurShape as T_Rectangle).FBrushStyle := TBrushStyle(BrushStyle.ItemIndex);
end;

procedure T_RectangleTool.BrushStyleChange(Sender: TObject);
var
  I: integer;
begin
  for i := 0 to High(SelectList) do
    (ShapeList.Shapes[SelectList[i]] as T_Rectangle).FBrushStyle :=
      TBrushStyle(BrushStyle.ItemIndex);
  ShapeList.AddHistory;
  PaintBox.Invalidate;
end;

class function T_RectangleTool.GetTypeShape: ClassShape;
begin
  Result := T_Rectangle;
end;

//====================== Round Rect Tool ======================//

procedure T_RoundRectTool.MouseDown(X, Y: integer);
begin
  if CurShape = nil then
    CurShape := T_RoundRect.Create;
  inherited;
end;

procedure T_RoundRectTool.ParamsInit;
begin
  inherited;
  L_CornerRadius := TLabel.Create(PropertyTool);
  with L_CornerRadius do
  begin
    Parent := PropertyTool;
    SetBounds(0, PosTop, 95, 20);
    Caption := ' Радиус угла : ';
    PosTop += 20;
  end;
  CornerRadius := TTrackBar.Create(PropertyTool);
  with CornerRadius do
  begin
    Parent := PropertyTool;
    ShowSelRange := False;
    SetBounds(1, PosTop, 93, 20);
    Min := 1;
    Max := 100;
    Frequency := 10;
    PosTop += 20;
    OnChange := @CornerRadiusChange;
  end;
end;

procedure T_RoundRectTool.ParamsFree;
begin
  inherited;
  FreeAndNil(L_CornerRadius);
  FreeAndNil(CornerRadius);
end;

procedure T_RoundRectTool.SetShapesParams;
begin
  inherited;
  (CurShape as T_RoundRect).FCornerRadius := CornerRadius.Position;
end;

procedure T_RoundRectTool.CornerRadiusChange(Sender: TObject);
var
  I: integer;
begin
  for i := 0 to High(SelectList) do
    (ShapeList.Shapes[SelectList[i]] as T_RoundRect).FCornerRadius :=
      CornerRadius.Position;
  ShapeList.AddHistory;
  PaintBox.Invalidate;
end;

class function T_RoundRectTool.GetTypeShape: ClassShape;
begin
  Result := T_RoundRect;
end;

//======================= Ellipse Tool =======================//

procedure T_EllipseTool.MouseDown(X, Y: integer);
begin
  if CurShape = nil then
    CurShape := T_Ellipse.Create;
  inherited;
end;

class function T_EllipseTool.GetTypeShape: ClassShape;
begin
  Result := T_Ellipse;
end;

//========================= Spray Tool =========================//

procedure T_SprayTool.MouseDown(X, Y: integer);
begin
  CurShape := T_Spray.Create;
  CurShape.StartPoint := S2W(X, Y);
  SprayTimer.Enabled := True;
  CurShape.EndPoint := S2W(X, Y);
  SetLength((CurShape as T_Spray).Points, 1);
  SetLength((CurShape as T_Spray).FCounts, 1);
  SetLength((CurShape as T_Spray).FRandSeeds, 1);
  (CurShape as T_Spray).FRandSeeds[0] := RandSeed;
  (CurShape as T_Spray).Points[0] := S2W(X, Y);
  SetShapesParams;
  ShapeList.NextShapes(CurShape);
  HaveChanged := True;
end;

procedure T_SprayTool.MouseMove(X, Y: integer);
begin
  CurShape.AddPoint(SCanvas);
  CurShape.EndPoint := S2W(X, Y);
end;

procedure T_SprayTool.MouseUp(X, Y: integer; aShift: TShiftState);
begin
  CurShape.GetRect;
  ShapeList.AllRect := ShapeList.GetAllRect(CurShape);
  CurShape := nil;
  SprayTimer.Enabled := False;
end;

procedure T_SprayTool.On_Timer(Sender: TObject);
begin
  (CurShape as T_Spray).IncCounts;
  (CurShape as T_Spray).Draw(SCanvas);
end;

procedure T_SprayTool.ParamsInit;
begin
  PosTop := 5;
  PropertyTool.Visible := True;
  L_SprayRadius := TLabel.Create(PropertyTool);
  with L_SprayRadius do
  begin
    Parent := PropertyTool;
    SetBounds(0, PosTop, 95, 20);
    Caption := ' Радиус : ';
    PosTop += 20;
  end;
  SprayRadius := TSpinEdit.Create(PropertyTool);
  with SprayRadius do
  begin
    Parent := PropertyTool;
    SetBounds(5, PosTop, 85, 20);
    PosTop += 20;
    MinValue := 10;
    MaxValue := 200;
    Value := MinValue;
    Increment := 1;
    OnChange := @SprayChange;
  end;
  L_SpraySpeed := TLabel.Create(PropertyTool);
  with L_SpraySpeed do
  begin
    Parent := PropertyTool;
    SetBounds(0, PosTop, 95, 20);
    Caption := ' Плотность : ';
    PosTop += 20;
  end;
  SpraySpeed := TSpinEdit.Create(PropertyTool);
  with SpraySpeed do
  begin
    Parent := PropertyTool;
    SetBounds(5, PosTop, 85, 20);
    PosTop += 20;
    MinValue := 50;
    MaxValue := 500;
    Value := MinValue;
    Increment := 1;
    OnChange := @SprayChange;
  end;
  SprayTimer := TTimer.Create(nil);
  with SprayTimer do
  begin
    Enabled := False;
    Interval := 30;
    OnTimer := @On_Timer;
  end;
end;

procedure T_SprayTool.ParamsFree;
begin
  FreeAndNil(L_SprayRadius);
  FreeAndNil(SprayRadius);
  FreeAndNil(L_SpraySpeed);
  FreeAndNil(SpraySpeed);
  FreeAndNil(SprayTimer);
end;

procedure T_SprayTool.SetShapesParams;
begin
  (CurShape as T_Spray).FPenColor := SCanvas.Pen.Color;
  (CurShape as T_Spray).FDensity := SpraySpeed.Value;
  (CurShape as T_Spray).FRadius := SprayRadius.Value;
end;

procedure T_SprayTool.SprayChange(Sender: TObject);
var
  I: integer;
begin
  for i := 0 to High(SelectList) do
  begin
    (ShapeList.Shapes[SelectList[i]] as T_Spray).FDensity := SpraySpeed.Value;
    (ShapeList.Shapes[SelectList[i]] as T_Spray).FRadius := SprayRadius.Value;
  end;
  ShapeList.AddHistory;
  PaintBox.Invalidate;
end;

class function T_SprayTool.GetTypeShape: ClassShape;
begin
  Result := T_Spray;
end;

//======================== Control Tool ========================//

procedure T_ControlTool.ParamsInit;
  function GetCommonParent(Class1, Class2: ClassShape): ClassShape;
  begin
    while (Class2 <> T_Shape) and not Class1.InheritsFrom(Class2) do
      Class2 := ClassShape(Class2.ClassParent);
    Result := Class2;
    HaveRect:= Class1.InheritsFrom(T_Rectangle) or HaveRect;
  end;

var
  i: integer;
  CommonParent: ClassShape;
begin
  fi:= 0;
  if SelectList = nil then exit;
  HaveRect:= false;
  PosTop := 0;
  PropertyTool.Visible := True;
  CommonParent := ClassShape(ShapeList.Shapes[SelectList[0]].ClassType);
  for i := 0 to High(SelectList) do
    CommonParent := GetCommonParent(
      ClassShape(ShapeList.Shapes[SelectList[i]].ClassType), CommonParent);
  for i := 0 to High(RegistrTool) do
    if RegistrTool[i].GetTypeShape = CommonParent then
    begin
      ChildTool := RegistrTool[i].Create(SCanvas, PropertyTool, PaintBox);
      break;
    end;
  ChildTool.ParamsInit;
  L_Convert := TLabel.Create(PropertyTool);
  with L_Convert do
  begin
    Parent := PropertyTool;
    SetBounds(0, ChildTool.PosTop, 95, 20);
    Caption := ' Преобразовать : ';
    ChildTool.PosTop += 20;
  end;
  CB_Convert := TComboBox.Create(PropertyTool);
  with CB_Convert do
  begin
    Parent := PropertyTool;
    SetBounds(5, ChildTool.PosTop, 85, 20);
    ChildTool.PosTop += 25;
    Style := csDropDownList;
    with Items do
    begin
      Add('Без изменений');
      Add('Перевернуть');
      Add('Отразить');
      Add('Развернуть');
    end;
    OnChange := @ChangeConvert;
    ItemIndex := 0;
  end;
  L_Turn := TLabel.Create(PropertyTool);
  with L_Turn do
  begin
    Parent := PropertyTool;
    SetBounds(0, ChildTool.PosTop, 95, 20);
    Caption := ' Повернуть : ';
    ChildTool.PosTop += 20;
  end;
  If not HaveRect then
  begin
    SE_Turn := TSpinEdit.Create(PropertyTool);
    with SE_Turn do
    begin
      Parent := PropertyTool;
      SetBounds(5, ChildTool.PosTop, 85, 20);
      Caption := ' Поверуть : ';
      ChildTool.PosTop += 20;
      MinValue := -360;
      MaxValue := 360;
      Value := 0;
      Increment := 1;
      OnChange := @ChangeSE_Turn;
    end;
  end
  else
  begin
    CB_Turn := TComboBox.Create(PropertyTool);
    with CB_Turn do
      begin
      Parent := PropertyTool;
      SetBounds(5, ChildTool.PosTop, 85, 20);
      ChildTool.PosTop += 25;
      Style := csDropDownList;
      with Items do
      begin
        Add('Без поворота');
        Add('90');
        Add('180');
        Add('270');
      end;
      OnChange := @ChangeCB_Turn;
      ItemIndex := 0;
    end;
  end;
end;

procedure T_ControlTool.ParamsFree;
begin
  if ChildTool = nil then
    exit;
  ChildTool.ParamsFree;
  FreeAndNil(ChildTool);
  FreeAndNil(CB_Convert);
  FreeAndNil(L_Convert);
  FreeAndNil(L_Turn);
  FreeAndNil(SE_Turn);
  FreeAndNil(CB_Turn);
end;

procedure T_ControlTool.ChangeConvert(Sender: TObject);
var
  I: integer;
begin
  for i := 0 to High(SelectList) do
    ShapeList.Shapes[SelectList[i]].ConvertShape(CB_Convert.ItemIndex);
  ShapeList.AddHistory;
  PaintBox.Invalidate;
end;

procedure T_ControlTool.ChangeSE_Turn(Sender: TObject);
var
  I: integer;
begin
  fi -= SE_Turn.Value;
  for i := 0 to High(SelectList) do
    ShapeList.Shapes[SelectList[i]].TurnShape(fi);
  fi := SE_Turn.Value;
  if SE_Turn.Value < 0 then
    SE_Turn.Value := SE_Turn.Value + 360;
  SE_Turn.Value := SE_Turn.Value mod 360;
  ShapeList.AddHistory;
  PaintBox.Invalidate;
end;

procedure T_ControlTool.ChangeCB_Turn(Sender: TObject);
var
  I: integer;
begin
  fi -= CB_Turn.ItemIndex * 90;
  for i := 0 to High(SelectList) do
    ShapeList.Shapes[SelectList[i]].TurnShape(fi);
  fi := CB_Turn.ItemIndex * 90;
  ShapeList.AddHistory;
  PaintBox.Invalidate;
end;

//======================== Extent Tool ========================//

procedure T_ExtentTool.MouseWheel(Zoom: boolean; X, Y: integer);
begin
  SetScale(Zoom, X, Y);
end;

//========================= Hand Tool =========================//

procedure T_HandTool.MouseDown(X, Y: integer);
begin
  OldX := X;
  OldY := Y;
end;

procedure T_HandTool.MouseMove(X, Y: integer);
begin
  Offset.X := Offset.X + OldX - X;
  Offset.Y := Offset.Y + OldY - Y;
  OldX := X;
  OldY := Y;
  PaintBox.Invalidate;
end;

//======================== Select Tool ========================//

procedure T_SelectTool.MouseDown(X, Y: integer);
begin
  StartSelect := Point(X, Y);
  Select := True;
end;

procedure T_SelectTool.MouseMove(X, Y: integer);
begin
  if not Select then
    exit;
  PaintBox.Invalidate;
  EndSelect := Point(X, Y);
  SCanvas.Pen.Width := 1;
  SCanvas.Pen.Color := clRed;
  SCanvas.Pen.Style := psDot;
  SCanvas.Brush.Style := bsClear;
  SCanvas.Rectangle(StartSelect.X, StartSelect.Y, EndSelect.X, EndSelect.Y);
end;

procedure T_SelectTool.MouseUp(X, Y: integer; aShift: TShiftState);
var
  i, j: integer;
  SelectRect: TRect;
  Remember: boolean;
begin
  if (StartSelect.X = X) and (StartSelect.Y = Y) then
  begin
    StartSelect.X -= 5;
    StartSelect.Y -= 5;
    X += 5;
    Y += 5;
  end;
  if aShift <> [ssCtrl] then
  begin
    SelectList := nil;
  end;
  SelectRect := Rect(StartSelect.X, StartSelect.Y, X, Y);
  for i := 0 to High(ShapeList.Shapes) do
  begin
    if ShapeList.Shapes[i].FindRegion(SelectRect) = 1 then
    begin
      if aShift = [ssCtrl] then
        for j := 0 to High(SelectList) do
          if i = SelectList[j] then
          begin
            SelectList[j] := SelectList[High(SelectList)];
            SetLength(SelectList, High(SelectList));
            Remember := False;
          end;
      if Remember then
      begin
        SetLength(SelectList, Length(SelectList) + 1);
        SelectList[High(SelectList)] := i;
      end;
      Remember := True;
    end;
  end;
  SetLength(ShapeList.ControlPoints, 0);
  for i := 0 to High(SelectList) do
    ShapeList.Shapes[SelectList[i]].GetControlPoints (SelectList[i]);
  ParamsFree;
  ParamsInit;
  Select := False;
end;

//========================= Edit Tool =========================//

procedure T_EditTool.MouseDown(X, Y: integer);
var
  i: integer;
begin
  if SelectList = nil then
    exit;
  for i := 0 to High(ShapeList.ControlPoints) do
    if PtInRegion(CreateEllipticRgn(
      W2S(ShapeList.ControlPoints[i].ConPoint).X - 5, W2S(ShapeList.ControlPoints[i].ConPoint).Y - 5,
      W2S(ShapeList.ControlPoints[i].ConPoint).X + 5, W2S(ShapeList.ControlPoints[i].ConPoint).Y + 5), X, Y)
    then
    begin
      PtEdit:= True;
      PtMove:= ShapeList.ControlPoints[i];
      break;
    end;
  Old := Point(X, Y);
end;

procedure T_EditTool.MouseMove(X, Y: integer);
var
  i: integer;
  Now: TPoint;
begin
  if SelectList = nil then
    exit;
  Now := Point(X, Y);
  if PtEdit then
    ShapeList.Shapes[PtMove.PerShape].MovePoint(PtMove.Index, Old, Now)
  else
    for i := 0 to High(SelectList) do
      ShapeList.Shapes[SelectList[i]].Move(Old, Now);
  Old := Point(X, Y);
  PaintBox.Invalidate;
end;

procedure T_EditTool.MouseUp(X, Y: integer; aShift: TShiftState);
begin
  PtEdit := False;
end;


initialization
  NewToolAdd(T_PaintTool);      // 0
  NewToolAdd(T_FreeHandTool);   // 1
  NewToolAdd(T_LineTool);       // 2
  NewToolAdd(T_RectangleTool);  // 3
  NewToolAdd(T_RoundRectTool);  // 4
  NewToolAdd(T_EllipseTool);    // 5
  NewToolAdd(T_SprayTool);      // 6
  NewToolAdd(T_HandTool);       // 7
  NewToolAdd(T_SelectTool);     // 8
  NewToolAdd(T_EditTool);       // 9
  NewToolAdd(T_ExtentTool);     // 10

end.

