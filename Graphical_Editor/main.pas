unit main;
// обрезка
{$mode objfpc}{$H+}// выбор - отмена
// TLabek - в функцию;
interface                                    //W3.org

uses
  Windows, Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Colorbox, ComCtrls, Grids, Menus, ActnList,
  Shape, Tools, Transform, About, types, Math, Clipbrd, ExtDlgs, ExpBMP;

type

  { TGraphical_Editor }

  TGraphical_Editor = class(TForm)
    ColorDialog: TColorDialog;
    Color_Line: TColorBox;
    MainMenu: TMainMenu;
    M_ExportsSVG: TMenuItem;
    M_AllBMP: TMenuItem;
    M_ExportsBMP: TMenuItem;
    M_CurBMP: TMenuItem;
    M_EditShapes: TMenuItem;
    M_Delete: TMenuItem;
    M_DownShapes: TMenuItem;
    M_Paste: TMenuItem;
    M_UpShapes: TMenuItem;
    M_UpDo: TMenuItem;
    M_ReDo: TMenuItem;
    M_Centralization: TMenuItem;
    M_File: TMenuItem;
    M_EditForm: TMenuItem;
    M_Close: TMenuItem;
    M_Clear: TMenuItem;
    M_Copy: TMenuItem;
    M_Cut: TMenuItem;
    M_About: TMenuItem;
    M_New: TMenuItem;
    M_Save: TMenuItem;
    M_Save_as: TMenuItem;
    M_Open: TMenuItem;
    Open: TOpenDialog;
    Palette: TDrawGrid;
    M_PaintBox: TPaintBox;
    ColorBar: TPanel;
    Box: TPanel;
    Property_Tool: TPanel;
    Save: TSaveDialog;
    SaveBMP: TSaveDialog;
    SaveSVG: TSaveDialog;
    SaveJPG: TSaveDialog;
    SB_Spray: TSpeedButton;
    SB_HandTool: TSpeedButton;
    SB_Pen: TSpeedButton;
    SB_Line: TSpeedButton;
    SB_Rectangle: TSpeedButton;
    SB_RoundRect: TSpeedButton;
    SB_Ellipse: TSpeedButton;
    Horizontal: TScrollBar;
    SB_Select: TSpeedButton;
    SB_Edit: TSpeedButton;
    Timer: TTimer;
    ZoomText: TStaticText;
    Vertical: TScrollBar;
    Ordinata: TStaticText;
    ColorPen: TShape;
    ColorBrush: TShape;
    ToolBar: TPanel;
    procedure ColorBrushMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure ColorPenMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure HorizontalChange(Sender: TObject);
    procedure HorizontalScroll(Sender: TObject; ScrollCode: TScrollCode;
      var ScrollPos: integer);
    procedure M_AllBMPClick(Sender: TObject);
    procedure M_CentralizationClick(Sender: TObject);
    procedure M_ClearClick(Sender: TObject);
    procedure M_AboutClick(Sender: TObject);
    procedure M_CloseClick(Sender: TObject);
    procedure M_CopyClick(Sender: TObject);
    procedure M_CutClick(Sender: TObject);
    procedure M_DeleteClick(Sender: TObject);
    procedure M_DownShapesClick(Sender: TObject);
    procedure M_CurBMPClick(Sender: TObject);
    procedure M_ExportsSVGClick(Sender: TObject);
    procedure M_NewClick(Sender: TObject);
    procedure M_OpenClick(Sender: TObject);
    procedure M_PaintBoxMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: boolean);
    procedure M_PaintBoxMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: boolean);
    procedure M_PasteClick(Sender: TObject);
    procedure M_UpDoClick(Sender: TObject);
    procedure M_SaveClick(Sender: TObject);
    procedure M_Save_asClick(Sender: TObject);
    procedure M_ReDoClick(Sender: TObject);
    procedure M_UpShapesClick(Sender: TObject);
    procedure PaletteDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure M_PaintBoxMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure M_PaintBoxMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: integer);
    procedure M_PaintBoxMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure M_PaintBoxPaint(Sender: TObject);
    procedure PaletteDrawCell(Sender: TObject; aCol, aRow: integer;
      aRect: TRect; aState: TGridDrawState);
    procedure PaletteMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure SB_ToolClick(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure VerticalChange(Sender: TObject);
    procedure PropertyScroll;
    procedure VerticalScroll(Sender: TObject; ScrollCode: TScrollCode;
      var ScrollPos: integer);
    procedure NewName;
    procedure ZoomStep(Zoom: boolean; X, Y: integer);
    procedure UpdateEnabled;
    procedure BMPSave(FileName: string);
  private
    { private declarations }
  public
    {}
  end;

var
  Graphical_Editor: TGraphical_Editor;
  DrawNow: boolean;
  CurTool: T_Tool;
  FName: string;

  PaletteColors: array[0..15] of
  TColor = (clWhite, clBlack, clMaroon, clGreen, clOlive, clNavy,
    clPurple, clTeal, clGray, clSilver, clRed, clLime, clYellow,
    clFuchsia, clAqua, clBlue);

implementation

{$R *.lfm}

{ TGraphical_Editor }


procedure TGraphical_Editor.FormCreate(Sender: TObject);
begin
  DrawNow := False;
  HaveChanged := False;
  FName := 'Безымянный';
  ColorPen.Brush.Color := clBlack;
  CurTool := T_FreeHandTool.Create(M_PaintBox.Canvas, Property_Tool, M_PaintBox);
  CurTool.ParamsInit;
  Graphical_Editor.Caption := FName;
  ZoomText.Caption := ('   Масштаб: ' + FloatToStr(Scale * 100) + '%');
  MinHistory := 1;
  SetLength(Patr, 6);
  Patr[0] := 0;
  Patr[1] := 5;
  Patr[2] := 7;
  Patr[3] := 5;
  Patr[4] := 7;
  Patr[5] := 0;
  UpdateEnabled;
end;

procedure TGraphical_Editor.NewName;
begin
  Graphical_Editor.Caption := ExtractFileName(FName) + '*';
end;

procedure TGraphical_Editor.M_PaintBoxMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  if Button = mbLeft then
  begin
    M_PaintBox.Canvas.Pen.Color := ColorPen.Brush.Color;
    M_PaintBox.Canvas.Brush.Color := ColorBrush.Brush.Color;
  end
  else
  begin
    M_PaintBox.Canvas.Pen.Color := ColorBrush.Brush.Color;
    M_PaintBox.Canvas.Brush.Color := ColorPen.Brush.Color;
  end;
  CurTool.MouseDown(X, Y);
  DrawNow := True;
  if HaveChanged then
    Graphical_Editor.NewName;
end;

procedure TGraphical_Editor.M_PaintBoxMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: integer);
begin
  if DrawNow then
    CurTool.MouseMove(X, Y);
end;

procedure TGraphical_Editor.M_PaintBoxMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  if DrawNow then
  begin
    if CurTool is T_HandTool then
    begin
      DrawNow := False;
      exit;
    end
    else
      CurTool.MouseUp(X, Y, Shift);
    DrawNow := False;
  end;
  M_PaintBox.Invalidate;
  if HaveChanged then
    ShapeList.AddHistory;
  UpdateEnabled;
end;

procedure TGraphical_Editor.M_PaintBoxPaint(Sender: TObject);
begin
  if ShapeList = nil then
  begin
    ShapeList := T_ShapeList.Create;
    PosHistory := 0;
    ShapeList.AddHistory;
  end;
  M_PaintBox.Canvas.Brush.Color := clWhite;
  M_PaintBox.Canvas.Brush.Style := bsSolid;
  M_PaintBox.Canvas.FillRect(Rect(0, 0, M_PaintBox.Width, M_PaintBox.Height));
  ShapeList.DrawAll(M_PaintBox.Canvas);
  ShapeList.DrawRect(M_PaintBox.Canvas);
  Ordinata.Caption := ('    ' + IntToStr(ShapeList.AllRect.Left) +
    ',' + IntToStr(ShapeList.AllRect.Top) + '    ' +
    IntToStr(ShapeList.AllRect.Right) + ',' + IntToStr(ShapeList.AllRect.Bottom));
  PropertyScroll;
  M_UpDo.Enabled := PosHistory > Max(1, MaxHistory - SizeMemory);
  M_ReDo.Enabled := PosHistory < MaxHistory;
end;

procedure TGraphical_Editor.PaletteDrawCell(Sender: TObject;
  aCol, aRow: integer; aRect: TRect; aState: TGridDrawState);
var
  i: integer;
begin
  for i := 0 to 15 do
  begin
    Palette.Canvas.Brush.Color := PaletteColors[i];
    Palette.Canvas.FillRect(Palette.CellRect(i mod 8, i div 8));
  end;
end;

procedure TGraphical_Editor.PaletteMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
var
  col, row: integer;
begin
  Palette.MouseToCell(x, y, col, row);
  if Button = mbLeft then
    ColorPen.Brush.Color := PaletteColors[8 * row + col]
  else
    ColorBrush.Brush.Color := PaletteColors[8 * row + col];
  CurTool.ColorChange(ColorPen.Brush.Color, ColorBrush.Brush.Color);
  invalidate;
end;

procedure TGraphical_Editor.SB_ToolClick(Sender: TObject);
begin
  CurTool.ParamsFree;
  CurTool.Free;
  CurTool := RegistrTool[(Sender as TSpeedButton).Tag].Create(M_PaintBox.Canvas,
    Property_Tool, M_PaintBox);
  if InRange((Sender as TSpeedButton).Tag, 1, 6) then
  begin
    SelectList := nil;
    ShapeList.ControlPoints := nil;
  end;
  CurTool.ParamsInit;
  invalidate;
  UpdateEnabled;
end;

procedure TGraphical_Editor.TimerTimer(Sender: TObject);
begin
  If SelectList = nil
    then exit;
  {if (patr[1] <> 0) then
  begin
    patr[1] -= 1;
    patr[5] += 1;
  end
  else
  begin
    patr[1] := 5;
    patr[5] := 0;
  end;      }
  if (patr[1] = 0) then
    patr[1] := 1
  else
    patr[1] := 0;
  M_PaintBox.invalidate;
end;

procedure TGraphical_Editor.VerticalChange(Sender: TObject);
begin
  Offset.Y := Vertical.Position;
  invalidate;
end;

procedure TGraphical_Editor.ColorPenMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  ColorDialog.Execute;
  ColorPen.Brush.Color := ColorDialog.Color;
end;

procedure TGraphical_Editor.FormClose(Sender: TObject; var CloseAction: TCloseAction);
var
  i: integer;
begin
  for i := 1 to MaxHistory do
    DeleteFile(IntToStr(i));
  {if not HaveChanged then              //          БЕСИТ!!!
    exit;
  if MessageDlg('Сохранить изменения?', mtCustom,
    [mbYes, mbNo], 0) = mrYes then
    Graphical_Editor.M_SaveClick(nil);   }

end;

procedure TGraphical_Editor.HorizontalChange(Sender: TObject);
begin
  Offset.X := Horizontal.Position;
  invalidate;
end;

procedure TGraphical_Editor.HorizontalScroll(Sender: TObject;
  ScrollCode: TScrollCode; var ScrollPos: integer);
begin
  with Horizontal do
  begin
    if (Position <= Min) and (ScrollCode = scLineUp) then
      Min := Min - 5;
    if (Position >= Max - Pagesize) and (ScrollCode = scLineDown) then
      Max := Max + 5;
  end;
end;

procedure TGraphical_Editor.M_AllBMPClick(Sender: TObject);
var
  FileName: string;
  O_Scale: real;
  O_Offset: TPoint;
begin
  P_Height := ShapeList.AllRect.Bottom - ShapeList.AllRect.Top;
  P_Width := ShapeList.AllRect.Right - ShapeList.AllRect.Left;
  if not SaveBMP.Execute then
    exit;
  O_Scale := Scale;
  O_Offset := Offset;
  if FExports.ShowModal = mrOk then
    exit;
  Scale :=  Min(
    Round(P_Width  / (ShapeList.AllRect.Right - ShapeList.AllRect.Left) * 100),
    Round(P_Height / (ShapeList.AllRect.Bottom - ShapeList.AllRect.Top) * 100)) / 100;
  Offset.X := Round(ShapeList.AllRect.Left * Scale);
  Offset.Y := Round(ShapeList.AllRect.Top * Scale);
  BMPSave(FileName);
  Scale := O_Scale;
  Offset := O_Offset;
end;

procedure TGraphical_Editor.M_CurBMPClick(Sender: TObject);
var
  FileName: string;
begin
  P_Width := M_PaintBox.Width;
  P_Height := M_PaintBox.Height;
  if not SaveBMP.Execute then
    exit;
  if FExports.ShowModal = mrOk then
    exit;
  BMPSave(FileName);
end;

procedure TGraphical_Editor.M_ExportsSVGClick(Sender: TObject);
var
  FileName: string;
begin
  SaveSVG.Execute;
  FileName:= SaveSVG.FileName;
  if pos('.', FileName) = 0 then
    FileName += '.svg';
  SaveFileToSVG(FileName);
end;

{procedure TGraphical_Editor.M_AllJPGClick(Sender: TObject);
var
  FileName: string;
  O_Scale: real;
  O_Offset: TPoint;
  jpg: TJpegImage;
begin
  if not SaveJPG.Execute then
    exit;
  P_Height := ShapeList.AllRect.Bottom - ShapeList.AllRect.Top;
  P_Width := ShapeList.AllRect.Right - ShapeList.AllRect.Left;
  O_Scale := Scale;
  O_Offset := Offset;
  TB_Qual:= True;
  if FExports.ShowModal = mrOk then
    exit;
  Scale :=  Min(
    Round(P_Width  / ShapeList.AllRect.Right - ShapeList.AllRect.Left * 100),
    Round(P_Height / ShapeList.AllRect.Bottom - ShapeList.AllRect.Top * 100));
  Offset.X := Round(ShapeList.AllRect.Left * Round(Scale / 100));
  Offset.Y := Round(ShapeList.AllRect.Top *  Round(Scale / 100));
  jpg := TJpegImage.Create;
  jpg.Width := Round (P_Width);
  jpg.Height := Round (P_Height);
  jpg.Canvas.FillRect(jpg.Canvas.ClipRect);
  ShapeList.DrawAll(jpg.Canvas);
  FileName := SaveJPG.FileName;
  if pos('.', Filename) = 0 then
    FileName += '.jpg'
  else
    ChangeFileExt(FileName, '.jpg');;
  jpg.CompressionQuality := P_Qual;
    //Compress;
  jpg.SaveToFile(FileName);
  jpg.Free;

  Scale := O_Scale;
  Offset := O_Offset;
end;   }

procedure TGraphical_Editor.M_CentralizationClick(Sender: TObject);
begin
  Scale := Scale * Min(Round(Horizontal.Width / (Horizontal.Max - Horizontal.Min) * 100),
    Round(Vertical.Height / (Vertical.Max - Vertical.Min) * 100)) / 100;
  if Round(Scale * 100) < 15 then
    Scale := 0.15;
  PropertyScroll;
  Vertical.Position := Vertical.Min + 25;
  Horizontal.Position := Horizontal.Min + 25;
  ZoomText.Caption := ('   Масштаб: ' + FloatToStr(Scale * 100) + '%');
  M_PaintBox.Invalidate;
end;

procedure TGraphical_Editor.M_ClearClick(Sender: TObject);
begin
  M_PaintBox.Canvas.Brush.Color := clWhite;
  M_PaintBox.Canvas.Brush.Style := bsSolid;
  M_PaintBox.Canvas.FillRect(Rect(0, 0, M_PaintBox.Width, M_PaintBox.Height));
  ShapeList.Destroy;
  ShapeList := T_ShapeList.Create;
  ColorPen.Brush.Color := clBlack;
  ColorBrush.Brush.Color := clWhite;
  SelectList := nil;
  Scale := 1;
  ZoomText.Caption := ('   Масштаб: ' + FloatToStr(Scale * 100) + '%');
  OldScale := 0;
  Offset := Point(0, 0);
  ShapeList.AddHistory;
  invalidate;
end;

procedure TGraphical_Editor.M_AboutClick(Sender: TObject);
begin
  FormAbout.Show;
end;

procedure TGraphical_Editor.M_CloseClick(Sender: TObject);
begin
  Graphical_Editor.Close;
end;

procedure TGraphical_Editor.M_CopyClick(Sender: TObject);
begin
  ShapeList.CopyShapes;
end;

procedure TGraphical_Editor.M_CutClick(Sender: TObject);
begin
  ShapeList.CopyShapes;
  M_DeleteClick(nil);
  Invalidate;
end;

procedure TGraphical_Editor.M_DeleteClick(Sender: TObject);
begin
  ShapeList.DeleteShapes;
  ShapeList.AddHistory;
  invalidate;
end;

procedure TGraphical_Editor.M_DownShapesClick(Sender: TObject);
begin
  ShapeList.DownShapes;
  ShapeList.AddHistory;
  invalidate;
end;



procedure TGraphical_Editor.M_NewClick(Sender: TObject);
begin
  if MessageDlg('Создать новый файл. Вы уверены?',
    mtCustom, mbOKCancel, 0) = mrOk then
  begin
    DrawNow := False;
    HaveChanged := False;
    FName := 'Безымянный';
    Graphical_Editor.Caption := FName;
    M_ClearClick(nil);
    PosHistory := 0;
    ShapeList.AddHistory;
  end;
end;

procedure TGraphical_Editor.M_OpenClick(Sender: TObject);
var
  Input: TextFile;
begin
  if HaveChanged then
    case MessageDlg('Сохранить изменения ' + FName +
        '?', mtCustom, mbYesNoCancel, 0) of
      mrYes:
        Graphical_Editor.M_SaveClick(nil);
      mrCancel:
        exit;
    end;
  Open.Execute;
  if Open.FileName = '' then
    exit;
  Save.FileName := Open.FileName;
  FName := Open.FileName;
  AssignFile(Input, Utf8ToAnsi(FName));
  ShapeList.LoadFromFile(Input);
  Graphical_Editor.Caption := ExtractFileName(FName);
  HaveChanged := False;
  ShapeList.DrawAll(M_PaintBox.Canvas);
  PropertyScroll;
  M_CentralizationClick(nil);
  ZoomText.Caption := ('   Масштаб: ' + FloatToStr(Scale * 100) + '%');
  PosHistory := 0;
  ShapeList.AddHistory;
  invalidate;
end;

procedure TGraphical_Editor.M_PaintBoxMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: boolean);
var
  Zoom: boolean;
begin
  Zoom := True;
  ZoomStep(Zoom, MousePos.X, MousePos.Y);
end;

procedure TGraphical_Editor.M_PasteClick(Sender: TObject);
var
  MousePt, CenterPt: TPoint;
  i: integer;
begin
  ShapeList.PasteShapes;
  if SelectList = nil then
    exit;
  MousePt := M_PaintBox.ScreenToClient(Mouse.CursorPos);
  CenterPt := CenterPoint(ShapeList.UpdateAllRect);
  CenterPt := Point(-CenterPt.X, -CenterPt.Y);
  MousePt := Point(-MousePt.X, -MousePt.Y);
  for i := 0 to High(SelectList) do
    ShapeList.Shapes[SelectList[i]].Move(MousePt, CenterPt);
  ShapeList.AddHistory;
  invalidate;
end;

procedure TGraphical_Editor.M_UpDoClick(Sender: TObject);
begin
  if PosHistory = MinHistory then
    exit;
  PosHistory -= 1;
  ShapeList.ReadHistory;
  invalidate;
end;

procedure TGraphical_Editor.M_ReDoClick(Sender: TObject);
begin
  if PosHistory = MaxHistory then
    exit;
  PosHistory += 1;
  ShapeList.ReadHistory;
  invalidate;
end;

procedure TGraphical_Editor.M_UpShapesClick(Sender: TObject);
begin
  ShapeList.UpShapes;
  ShapeList.AddHistory;
  invalidate;
end;

procedure TGraphical_Editor.M_PaintBoxMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: boolean);
var
  Zoom: boolean;
begin
  Zoom := False;
  ZoomStep(Zoom, MousePos.X, MousePos.Y);
end;

procedure TGraphical_Editor.ZoomStep(Zoom: boolean; X, Y: integer);
var
  ZoomTool: T_ExtentTool;
begin
  ZoomTool := T_ExtentTool.Create(M_PaintBox.Canvas, Property_Tool, M_PaintBox);
  ZoomTool.MouseWheel(Zoom, X, Y);
  ZoomTool.Destroy;
  ZoomText.Caption := ('   Масштаб: ' + FloatToStr(Scale * 100) + '%');
  invalidate;
end;

procedure TGraphical_Editor.M_SaveClick(Sender: TObject);
var
  Output: TextFile;
begin
  if not HaveChanged then
    exit;
  if FName = 'Безымянный' then
    Save.Execute;
  if Save.FileName = '' then
    exit;
  FName := Save.FileName;
  AssignFile(Output, Utf8ToAnsi(FName));
  ShapeList.SaveToFile(Output);
  Graphical_Editor.Caption := ExtractFileName(FName);
  HaveChanged := False;
end;

procedure TGraphical_Editor.M_Save_asClick(Sender: TObject);
begin
  Save.Execute;
  Graphical_Editor.M_SaveClick(nil);
end;

procedure TGraphical_Editor.PaletteDblClick(Sender: TObject);
var
  i: integer;
begin
  if ColorDialog.Execute then
    PaletteColors[8 * Palette.row + Palette.col] := ColorDialog.Color;
  for i := 0 to 15 do
  begin
    Palette.Canvas.Brush.Color := PaletteColors[i];
    Palette.Canvas.FillRect(Palette.CellRect(i mod 8, i div 8));
  end;
end;

procedure TGraphical_Editor.ColorBrushMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  ColorDialog.Execute;
  ColorBrush.Brush.Color := ColorDialog.Color;
end;

procedure TGraphical_Editor.PropertyScroll;
begin
  Horizontal.Min := Round(ShapeList.AllRect.Left * Scale) - 50;
  Horizontal.Max := Round(ShapeList.AllRect.Right * Scale) + 50;
  Horizontal.PageSize := M_PaintBox.Width;
  Horizontal.Position := Offset.X;
  Vertical.Min := Round(ShapeList.AllRect.Top * Scale) - 50;
  Vertical.Max := Round(ShapeList.AllRect.Bottom * Scale) + 50;
  Vertical.PageSize := M_PaintBox.Height;
  Vertical.Position := Offset.Y;
end;

procedure TGraphical_Editor.VerticalScroll(Sender: TObject;
  ScrollCode: TScrollCode; var ScrollPos: integer);
begin
  with Vertical do
  begin
    if (Position <= Min) and (ScrollCode = scLineUp) then
      Min := Min - 5;
    if (Position >= Max - Pagesize) and (ScrollCode = scLineDown) then
      Max := Max + 5;
  end;
end;

procedure TGraphical_Editor.BMPSave(FileName: string);
var
  BitMap: TBitMap;
begin
  BitMap := TBitMap.Create;
  BitMap.Width := P_Width;
  BitMap.Height := P_Height;
  BitMap.Canvas.FillRect(BitMap.Canvas.ClipRect);
  ShapeList.DrawAll(BitMap.Canvas);
  FileName := SaveBMP.FileName;
  if pos('.', Filename) = 0 then
    FileName += '.bmp';
  ChangeFileExt(FileName, '.bmp');
  BitMap.SaveToFile(FileName);
  BitMap.Free;
end;

procedure TGraphical_Editor.UpdateEnabled;
begin
  M_Cut.Enabled := Length(SelectList) <> 0;
  M_Copy.Enabled := Length(SelectList) <> 0;
  M_Delete.Enabled := Length(SelectList) <> 0;
  M_DownShapes.Enabled := Length(SelectList) <> 0;
  M_UpShapes.Enabled := Length(SelectList) <> 0;
end;

end.

