program paint;

{$mode objfpc}{$H+}

uses {$IFDEF UNIX} {$IFDEF UseCThreads}
  cthreads, {$ENDIF} {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  main,
  shape,
  Transform,
  tools,
  about,
  ExpBMP;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TGraphical_Editor, Graphical_Editor);
  Application.CreateForm(TFormAbout, FormAbout);
  Application.CreateForm(TFExports, FExports);
  Application.Run;
end.

