unit Pattern;

{$mode objfpc}{$H+}

interface

uses
  DOM, XMLWrite, SysUtils, Graphics, Classes;

function PatternBrush(Style: integer; Color: string; XML: TXMLDocument; defs: TDOMElement): string;
implementation

var
  PatternNumber: integer;

function AddPattern(id: string; XML: TXMLDocument): TDomElement;
begin
  Result := XML.CreateElement('pattern');
  result.SetAttribute('id', id);
  result.SetAttribute('x', '0');
  result.SetAttribute('y', '0');
  result.SetAttribute('width', '5');
  result.SetAttribute('height', '5');
  result.SetAttribute('viewBox', '0 0 5 5');
  result.SetAttribute('patternUnits', 'userSpaceOnUse');
end;

function PatternBrush(Style: integer; Color: string; XML: TXMLDocument; defs: TDomElement): string;
var
  NewPattern, temp: TDomElement;
begin
  inc(PatternNumber);
  result := 'p' + IntToStr(PatternNumber);
  NewPattern := AddPattern(Result, XML);
  temp := XML.CreateElement('path');
  temp.SetAttribute('stroke', Color);
  case Style of
    integer(bsHorizontal):
    begin
      temp.SetAttribute('d', 'M 0 2 L 10 2z');
      NewPattern.AppendChild(temp);
    end;
    integer(bsVertical):
    begin
      temp.SetAttribute('d', 'M 2 0 L 2 10z');
      NewPattern.AppendChild(temp);
    end;
    integer(bsFDiagonal):
    begin
      temp.SetAttribute('d', 'M 0 0 L 15 15z');
      NewPattern.AppendChild(temp);
    end;
    integer(bsBDiagonal):
    begin
      temp.SetAttribute('d', 'M 0 5 L 5 0z');
      NewPattern.AppendChild(temp);
    end;
    integer(bsCross):
    begin
      temp := XML.CreateElement('path');
      temp.SetAttribute('stroke', Color);
      temp.SetAttribute('d', 'M 0 2 L 10 2z');
      NewPattern.AppendChild(temp);
      temp := XML.CreateElement('path');
      temp.SetAttribute('stroke', Color);
      temp.SetAttribute('d', 'M 2 0 L 2 10z');
      NewPattern.AppendChild(temp);
    end;
    integer(bsDiagCross):
    begin
      temp := XML.CreateElement('path');
      temp.SetAttribute('stroke', Color);
      temp.SetAttribute('d', 'M 0 0 L 5 5z');
      NewPattern.AppendChild(temp);
      temp := XML.CreateElement('path');
      temp.SetAttribute('stroke', Color);
      temp.SetAttribute('d', 'M 0 5 L 5 0z');
      NewPattern.AppendChild(temp);
    end;
  end;
  defs.AppendChild(NewPattern);
end;

end.

