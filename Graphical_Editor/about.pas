unit about;
// Ключевые точк
// Рамка
// -Разбить по классам
// -История
// -Моменты перерисовки и Сохранения  *

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TFormAbout }

  TFormAbout = class(TForm)
    ButtonOK: TButton;
    LabelAbout: TLabel;
    procedure ButtonOKClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  FormAbout: TFormAbout;

implementation

{$R *.lfm}

{ TFormAbout }

procedure TFormAbout.ButtonOKClick(Sender: TObject);
begin
  FormAbout.Close;
end;

initialization

end.

