﻿def BronKerbosch(R: set, P: set, X: set, matrix: list):
	"""
	http://en.wikipedia.org/wiki/Bron%E2%80%93Kerbosch_algorithm
	"""
	result = []
	if not P and not X:
		return [R]

	for v in P.copy():
		result.extend(BronKerbosch(R | {v}, P & matrix[v], X & matrix[v], matrix))
		P -= {v}
		X |= {v}

	return result


if __name__ == "__main__":

	import sys

	in_file_name = 'input.txt' if len(sys.argv) == 1 else sys.argv[1]

	with open(in_file_name, 'r') as in_file:
		n, m = list(map(int, in_file.readline().strip().split()))
		matrix = [set() for j in range(n + 1)]
		for i in range(m):
			a, b = list(map(int, in_file.readline().strip().split()))
			matrix[a] |= {b}
			matrix[b] |= {a}

		init_P = set(range(1, n + 1))
		for i in range(len(matrix)):
			if not matrix[i]:
				init_P -= {i}

		if len(sys.argv) == 3:
			out_file = open(sys.argv[2], 'w')
			for i in BronKerbosch(set(), init_P, set(), matrix):
				out_file.write(' '.join(map(str, i)) + '\n')
			out_file.close()

		else:
			for i in BronKerbosch(set(), init_P, set(), matrix):
				print(' '.join(map(str, i)))
