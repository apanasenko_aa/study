class Vertex:

	def __init__(self, id, x, y):
		self.id = int(id)
		self.x = float(x)
		self.y = float(y)

	def __str__(self):
		return "Vertex id = %d, x = %f, y = %f " % (self.id, self.x, self.y)


class Edge:

	def __init__(self, a: Vertex, b: Vertex):
		self.a = a
		self.b = b

	def __str__(self):
		return "%d -- %d" % (self.a.id, self.b.id)


class Graph:

	board = 10

	def __init__(self, V: list, E: list):
		self.V = V
		self.E = E

	def set_screen_size(self, screen_width: int, screen_height: int):
		self.screen_width = screen_width - 2 * self.board
		self.screen_height = screen_height - 2 * self.board

	def __str__(self):
		x_min = min(map(lambda v: v.x, self.V))
		y_min = min(map(lambda v: v.y, self.V))

		x_max = max(map(lambda v: v.x, self.V))
		y_max = max(map(lambda v: v.y, self.V))

		result = 'graph A {\n'
		for v in self.V:
			x = (v.x - x_min) * self.screen_width / (x_max - x_min) + self.board
			y = (v.y - y_min) * self.screen_height / (y_max - y_min) + self.board
			result += '\t %d[pos = "%f, %f!"];\n' % (v.id, x, y)

		for e in self.E:
			result += '\t %s;\n' % str(e)

		return result + '}'


if __name__ == "__main__":

	import sys
	import os

	file_nodes_name = 'in.nodes' if len(sys.argv) < 2 else sys.argv[1]
	file_edges_name = 'in.eds' if len(sys.argv) < 3 else sys.argv[2]
	file_graph_name = 'graph.gv' if len(sys.argv) < 4 else sys.argv[3]

	V = {}
	with open(file_nodes_name, 'r') as nodes:
		for line in nodes:
			id, x, y = line.strip().split()
			V[id] = Vertex(id, x, y)

	E = []
	with open(file_edges_name, 'r') as edges:
		edges.readline()
		edges.readline()
		for line in edges:
			a, b, *end = line.strip().split()
			E.append(Edge(V[a], V[b]))

	G = Graph(V.values(), E)
	G.set_screen_size(1280, 1024)

	with open(file_graph_name, 'w') as graph:
		graph.write(str(G))

	os.system('neato -n -s -O -Tpng %s' % file_graph_name);