$path = 'D:\Programming\6_semester\OS\Windows\tests\';
get-childitem -literalpath $path *.txt | foreach {
    $str = $_.name.substring(0, 4).toUpper();
    $year = (([int]$_.name.substring(4, 4) + 1) % 10000).ToString("0000");
    rename-item -path ($path + $_.name) -newname ($str + $year + $_.name.substring(8, 4) + ".txt");
} | fl