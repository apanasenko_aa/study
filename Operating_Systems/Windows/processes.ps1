get-process | select name, id, path, starttime | foreach {
    if ($_.path -ne $NULL)
    {
        get-childitem -literalpath $_.path
    }
    $_
} | format-table -autosize