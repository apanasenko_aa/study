* [N.c](N.c) - вывод N чисел без условий
* [kitten.c](kitten.c) - RSQ. [Дерево отрезков.](https://ru.wikipedia.org/wiki/Дерево_отрезков) ([Задача](https://imcs.dvfu.ru/cats/static/problem_text-cpid-531806.html))
* [hSort.c](hSort.c) - [Heapsort](https://ru.wikipedia.org/wiki/Пирамидальная_сортировка)
* [qSort.c](qSort.c) - [Quicksort](https://ru.wikipedia.org/wiki/Быстрая_сортировка)
* [mSort.py](mSort.py) - [Mergesort](https://ru.wikipedia.org/wiki/Сортировка_слиянием)