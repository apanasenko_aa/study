#include <stdio.h>
#include <stdlib.h>

void qSort(int start, int end, int a[])
{
    int l = start;
    int r = end;
    int m = a[(r + l) / 2];
    //int m = a[start + rand() % (r - l)];
    int k;
    do {
        for (;(a[l] < m); l++);
        for (;(a[r] > m); r--);
        if (l > r) 
            break;
        k = a[l];
        a[l] = a[r];
        a[r] = k;
        l++;
        r--;
    } while (l < r);
    if (r > start)
        qSort(start, r, a);
    if (l < end)
        qSort(l, end, a);
}

int main() 
{
    FILE *in = fopen("input.txt", "r");
    FILE *out = fopen("output.txt", "w");
    int a[100000];
    int i, n;
    fscanf(in, "%d", &n);
    for (i = 0; i < n; i++)
        fscanf(in, "%d", &a[i]);
    qSort(0, n - 1, a);
    for (i = 0; i < n; i++)
        fprintf(out, "%d ", a[i]);
    return 0;
}
