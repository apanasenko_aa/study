# Merge sort
with open('input.txt', 'r') as inFile, open('output.txt', 'w') as outFile:
    array = list(map(int, inFile.read().strip ().split()))
    n = array.pop(0)

    def sort(in_arr = []):
        len_in_arr = len(in_arr)
        if len_in_arr == 1 :
            return in_arr
        left = sort(in_arr[:len_in_arr // 2])
        right = sort(in_arr[len_in_arr // 2:])
        sorted_arr = []
        l_ind = 0
        r_ind = 0
        l_len = len(left)
        r_len = len(right)
        while l_ind != l_len and r_ind != r_len :
            if left[l_ind] < right[r_ind] :
                sorted_arr.append(left[l_ind])
                l_ind += 1
            else:
                sorted_arr.append(right[r_ind])
                r_ind += 1
        sorted_arr += left[l_ind:] + right[r_ind:]
        return sorted_arr

    array = sort(array)
    #print(array)
    outFile.write(" ".join(map(str, array)))