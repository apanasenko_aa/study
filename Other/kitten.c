#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int OFFSET = 1000000;
const int N = 2000000;
const int L_SIZE = 256;
int a[2000000];
int b[2000000];

int rsq (int x) {
	int result = 0;
	while (x >= 0) {
		result += b[x];
		x = (x & (x + 1)) - 1;
	}
	return result;
}

void update (int x) {
	if (a[x] == 1)
		return;
	a[x] = 1;
	while (x < N) {
		b[x] += 1;
		x = (x | (x + 1));
	}
	return;
}

int main() {
	FILE *in = fopen("kitten.in", "r");
	FILE *out = fopen("kitten.out", "w");
	int i, x, y;
	for (i = 0; i < 2000000; i++)
        a[i] = b[i] = 0;
	int result;
	char *line, *token;
	char buf[256];
	while (!feof(in)){
		line = fgets(buf, L_SIZE, in);
		token = strtok(line, " ");
		x = atoi(token) + OFFSET;
		if ((token = strtok(NULL, " ")) != NULL) {
			y = atoi(token) + OFFSET;
			result = x > y ? rsq(x) - rsq(y - 1) : rsq(y) - rsq(x - 1);
			fprintf (out, "%d\r\n", result);
		} else
			update(x);


	}                                       
	return 0;
}
