// print N with out "if"
#include <stdio.h>

void My_Print(void);
void My_Exit(void);
int i = 0;

int main()
{
    int n = 15;
    void (*ArF[n])();
    for (;;){
        ArF[i] = My_Print;
        ArF[n] = My_Exit;
        ArF[i]();
    }
    return 0;
}

void My_Print(void)
{
    i++;
    printf ("%d\n", i);
    return;
}

void My_Exit(void)
{
    exit(0);
}
