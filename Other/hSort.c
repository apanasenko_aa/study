#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

const int N = 400000;

int max_index(int arr[], int i, int j)
{
    return arr[i] < arr[j] ? i : j;
}

void xxx (int array[], int idx)
{
    int max_idx = idx;
    idx = 0;
    while (max_idx != idx)
    {
        idx = max_idx;
        max_idx = max_index(array, idx, max_index(array, idx*2, idx*2+1));
        int tmp = array[idx];
        array[idx] = array[max_idx];
        array[max_idx] = tmp;
    }
}


int main() {
    FILE *in = fopen("input.txt", "r");
    FILE *out = fopen("output.txt", "w");
    int i, n, tmp, max_idx;
    fscanf(in, "%d", &n);
    int arr[N];
    for(i = 0; i < N; i++)
        arr[i] = INT_MAX;
    for(i = 1; i <= n; i++)
    {
        fscanf(in, "%d", &arr[i]);
    }
    for(i = n; i > 0; i--)
    {
        xxx(arr, i);
    }
    for(i = n; i > 1; i--)
    {
        fprintf(out, "%d ", arr[1]);
        arr[1] = arr[i];
        arr[i] = INT_MAX;
        xxx(arr, 1);
    }
    fprintf(out, "%d ", arr[1]);
    return 0;
}
