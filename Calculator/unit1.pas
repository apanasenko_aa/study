unit unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Menus, unit2;

type

  { TForm1 }

  TForm1 = class(TForm)
    ButNumber1: TButton;
    ButNumbe0: TButton;
    But_CE: TButton;
    But_Beak: TButton;
    But_C: TButton;
    Button14: TButton;
    Button15: TButton;
    Button16: TButton;
    Button17: TButton;
    Button18: TButton;
    Button19: TButton;
    ButNumbe2: TButton;
    Button20: TButton;
    ButDel: TButton;
    ButYmn: TButton;
    Button24: TButton;
    Button25: TButton;
    ButMinus: TButton;
    ButPlus: TButton;
    ButRavno: TButton;
    ButNumbe3: TButton;
    ButNumbe4: TButton;
    ButNumbe5: TButton;
    ButNumbe6: TButton;
    ButNumbe7: TButton;
    ButNumbe8: TButton;
    ButNumbe9: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    MainMenu1: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;

    procedure But_CEClick(Sender: TObject);
    procedure But_BeakClick(Sender: TObject);
    procedure But_CClick(Sender: TObject);
    procedure Button14Click(Sender: TObject);
    procedure Button15Click(Sender: TObject);
    procedure Button16Click(Sender: TObject);
    procedure Button17Click(Sender: TObject);
    procedure Button18Click(Sender: TObject);
    procedure Button19Click(Sender: TObject);
    procedure Button20Click(Sender: TObject);
    procedure Button24Click(Sender: TObject);
    procedure Button25Click(Sender: TObject);
    procedure AddNumber(Sender: TObject);
    procedure ButPlusClick(Sender: TObject);
    procedure ButRavnoClick(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: char);
    procedure FormKeyPress(Sender: TObject; var Key: char);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;
  a,a1, MS, Resylt: extended;
  Oper,Oper2: string;
  qqq, qqq2, qqq3, qqq4, Er: boolean;

implementation

{$R *.lfm}

{ TForm1 }
procedure TForm1.AddNumber(Sender: TObject);
begin
  If Er then Exit;
  if Pos(DecimalSeparator, Edit1.Text) = Length(Edit1.Text)
  then begin
     Edit1.Text := FloatToStr(a);
     Edit1.Text := Edit1.Text + DecimalSeparator + (Sender as TButton).Caption;
     a := StrToFloat(Edit1.Text);
  end
  else begin
     Edit1.Text := FloatToStr(a);
     if (Edit1.Text = '0') or (qqq3 = false)
     then
        Edit1.Text := (Sender as TButton).Caption
     else
        Edit1.Text := Edit1.Text + (Sender as TButton).Caption;
  end;
  a := StrToFloat(Edit1.Text);
  qqq:= true;
  qqq3:= true;
end;

procedure TForm1.ButPlusClick(Sender: TObject);
begin
  If Er then Exit;
  if qqq4 = true
  then begin
     Resylt:= StrToFloat(Edit1.Text);
     qqq4:= false;
  end;
  If Oper = ''
  then begin
     Oper := (Sender as TButton).Caption;
     If qqq2 = true
     then
        Resylt:=Resylt
     else
        Resylt:=a;
  end
  else begin
     Oper := (Sender as TButton).Caption;
     if oper2 ='+'
     then
        resylt:=resylt+a;
     if oper2 ='*'
     then
        resylt:=resylt*a;
     if oper2 ='/'
     then begin
        try
           Resylt:= Resylt/a;
        except
           But_CClick(Sender);
           Edit1.Text:= 'Деление на ноль невозможно';
           Er:= true;
           Exit;
           Beep;
        end;
     end;
     if oper2 ='-'
     then
        resylt:=resylt-a;
  end;
  Oper2:=Oper;
  Edit1.Text:= FloatToStr(Resylt);
  a1:=a;
  a:=0;
  qqq:=false;
end;

procedure TForm1.ButRavnoClick(Sender: TObject);
begin
  If Er then Exit;
  if qqq = true
  then begin

     if oper2 ='+'
     then
        resylt:=resylt+a;
     if oper2 ='*'
     then
        resylt:=resylt*a;
     if oper2 ='/'
     then begin
        try
           Resylt:= Resylt/a;
        except
           But_CClick(Sender);
           Edit1.Text:= 'Деление на ноль невозможно';
           Er:= true;
           Exit;
           Beep;
        end;
     end;
     if oper2 ='-'
     then
        resylt:=resylt-a;
  a1:=a;
  end;
  if qqq = false
  then begin
     if oper2 =''
     then
        resylt:=resylt;
     if oper2 ='+'
     then
        resylt:=resylt+a1;
     if oper2 ='*'
     then
        resylt:=resylt*a1;
     if oper2 ='/'
     then begin
        try
           Resylt:= Resylt/a1;
        except
           But_CClick(Sender);
           Edit1.Text:= 'Деление на ноль невозможно';
           Er:= true;
           Exit;
           Beep;
        end;
     end;
     if oper2 ='-'
     then
        resylt:=resylt-a1;

  end;
  oper:='';
  Edit1.Text:= FloatToStr(resylt);
  a:=0;
  qqq := false;
  qqq2 := true;
end;

procedure TForm1.Edit1KeyPress(Sender: TObject; var Key: char);
begin
  If Er then Exit;
  if Ord(key) = $08 then
  begin
    key := #0;
    But_BeakClick(nil); //But_Beak.Click;
    exit;
  end;
  if not (Key in ['0'..'9']) then
    Key := #0;
end;

procedure TForm1.FormKeyPress(Sender: TObject; var Key: char);
begin
  If Er then Exit;
  Edit1.SetFocus;
  Edit1.SelStart := length(Edit1.Text);
  Edit1.SelLength := 0;
end;

procedure TForm1.MenuItem2Click(Sender: TObject);
begin
  Halt(0);
end;

procedure TForm1.MenuItem3Click(Sender: TObject);
begin
  Form2.Show;
  form1.enabled:=false;
end;

procedure TForm1.Button20Click(Sender: TObject);
begin
  If Er then Exit;
  if StrToFloat(Edit1.Text) < 0 then
  begin
    beep;
    But_CClick(Sender);
    Edit1.Text := 'Недопустимый ввод';
    Er:= true;
    Exit;
  end
  else
     a := sqrt (StrToFloat(Edit1.Text));
  Edit1.Text := FloatToStr(a);
  a:= StrToFloat(Edit1.Text);
  a1:=a;
  qqq4:=true;
  qqq3:= false;
end;

procedure TForm1.Button24Click(Sender: TObject);
begin
  If Er then Exit;
  If StrToFloat(Edit1.Text) = 0
  then begin
     Edit1.Text := 'Недопустимый ввод';
     Er:= true;
     Exit;
  end;
  a:= 1 / StrToFloat(Edit1.Text);
  Edit1.Text := FloatToStr(a);
  a1:=a;
  qqq4:= true;
  qqq3:= false;
end;

procedure TForm1.Button25Click(Sender: TObject);
begin
  If Er then Exit;
  if Pos(DecimalSeparator, Edit1.Text) = 0
  then
      Edit1.Text := Edit1.Text + DecimalSeparator
  else
      Beep;
  a:= StrToFloat(Edit1.Text);
end;


procedure TForm1.But_CEClick(Sender: TObject);
begin
  If Er then Exit;
  Edit1.Text := '0';
  a := StrToFloat(Edit1.Text);
  Button25.Enabled := True;
end;

procedure TForm1.But_BeakClick(Sender: TObject);
var
  q: string;
begin
  If Er then Exit;
  if qqq2 = true
  then
     beep
  else begin
     q := copy(Edit1.Text, Length(Edit1.Text), 1);
     if q = ',' then
        Button25.Enabled := True;
        q := copy(Edit1.Text, 1, 1);
     if (Length(Edit1.Text) = 1) or ((Length(Edit1.Text) = 2) and (q = '-'))
     then
        Edit1.Text := '0'
     else
       Edit1.Text := Copy(Edit1.Text, 1, Length(Edit1.Text) - 1);
     a := StrToFloat(Edit1.Text);
  end;

end;

procedure TForm1.But_CClick(Sender: TObject);
begin
  a:=0;
  a1:=0;
  Resylt:=0;
  Oper:='';
  Oper2:='';
  qqq:=false;
  Edit1.Text:='0';
  qqq2 := false;
  Er:= False;
end;


procedure TForm1.Button14Click(Sender: TObject);
begin
  If Er then Exit;
  MS := 0;
  Edit2.Text:= '';
  qqq3 := false;
end;

procedure TForm1.Button15Click(Sender: TObject);
begin
  If Er then Exit;
  Edit1.Text := FloatToStr(MS);
  Edit2.Text:= 'M';
  a:=MS;
  qqq3 := false;
end;

procedure TForm1.Button16Click(Sender: TObject);
begin
  If Er then Exit;
  MS := StrToFloat(Edit1.Text);
  Edit2.Text:= 'M';
  qqq3 := false;
end;

procedure TForm1.Button17Click(Sender: TObject);
begin
  If Er then Exit;
  MS := MS + StrToFloat(Edit1.Text);
  Edit2.Text:= 'M';
  qqq3 := false;
end;

procedure TForm1.Button18Click(Sender: TObject);
begin
  If Er then Exit;
  MS := MS - StrToFloat(Edit1.Text);
  Edit2.Text:= 'M';
  qqq3 := false;
end;

procedure TForm1.Button19Click(Sender: TObject);
begin
  If Er then Exit;
  a := StrToFloat(Edit1.Text);
  if a <> 0
  then begin
     a := a * (-1);
     Edit1.Text := FloatToStr(a);
  end;
end;

end.
