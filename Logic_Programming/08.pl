less(0, s(N)).
less(s(N), s(M)) :- less(N, M).

less_or_equal(0, N).
less_or_equal(s(N), s(M)) :- less_or_equal(N, M). 

plus(0, N, N).
plus(s(N), M, s(K)) :- plus(N, M, K).

gcd(X, 0, X).
gcd(0, X, X).
gcd(X, Y, Z) :- less_or_equal(X, Y), plus(X, T, Y), gcd(T, X, Z);
				less(Y, X), plus(Y, T, X), gcd(T, Y, Z).