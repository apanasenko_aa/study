getSubList([H | C], I1, I2, R):-
		I1 > 1, 
		II1 is I1 - 1,
		II2 is I2 - 1,
		getSubList(C, II1, II2, R).

getSubList([H | C], I1, I2, [H | R]):-		
		I1 = 1,
		I2 > 0,
		II2 is I2 - 1,
		getSubList(C, I1, II2, R). 

getSubList(_, I1, I2, []):-		
		I1 = 1,
		I2 = 0.

