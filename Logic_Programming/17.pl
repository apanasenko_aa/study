position(Term, Term, []).
position(Sub, Term, Result) :-
	compound(Term),
	functor(Term, _, N),
	position(Sub, Term, N, Result).

position(Sub, Term, N, Result) :-
	N > 1,
	N1 is N - 1,
	position(Sub, Term, N1, Result).

position(Sub, Term, N, [N | Result]) :-
	arg(N, Term, Arg),
	position(Sub, Arg, Result).