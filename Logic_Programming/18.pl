between(I, J, K) :- nonvar(K), I =< K, K =< J.
between(I, J, K) :- var(K), I =< J, K is I.
between(I, J, K) :- var(K), I =< J, between(I + 1, J, K).

natural(0).
natural(X) :- natural(Y), X is Y + 1.

plus(X, Y, Z) :- nonvar(X), nonvar(Y), Z is X + Y.
plus(X, Y, Z) :- nonvar(X), var(Y), nonvar(Z), Y is Z - X.
plus(X, Y, Z) :- var(X), nonvar(Y), nonvar(Z), X is Z - Y.
plus(X, Y, Z) :- var(X), var(Y), nonvar(Z), between(0, Z, X), Y is Z - X.
plus(X, Y, Z) :- var(X), nonvar(Y), var(Z), natural(X), Z is X + Y.
plus(X, Y, Z) :- nonvar(X), var(Y), var(Z), natural(Y), Z is X + Y.
plus(X, Y, Z) :- var(X), var(Y), var(Z), natural(X), natural(Y), Z is X + Y.