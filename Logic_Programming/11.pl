ordered_l(_, void).                               
ordered_l(X, tree(H, L, R)) :- >(X, H),
                               ordered(tree(H, L, R)),
                               ordered(tree(X, R, void)).

ordered_r(_, void).                               
ordered_r(X, tree(H, L, R)) :- <(X, H),
                               ordered(tree(H, L, R)),
                               ordered(tree(X, void, L)).

ordered(void).
ordered(tree(H, L, R)) :- ordered_l(H, L),
                          ordered_r(H, R).
