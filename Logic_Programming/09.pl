sum([], 0).
sum([s(X) | Xs], s(Y)) :- sum([X | Xs], Y).
sum([0 | Xs], Y) :- sum(Xs, Y).
