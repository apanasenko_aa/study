owner(basil).
married(sybil, basil).

employe(polly).
employe(manuel).

guest(smith).
guest(jones).

dislike(basil, manuel).
dislike(X, Y) :- owner(Y), employe(X).

serve(X, Y) :- employe(X), guest(Y);
			   owner(X), guest(Y);
			   married(X, Z), owner(Z), guest(Y).
