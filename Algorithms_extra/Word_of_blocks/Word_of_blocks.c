#include <stdio.h>
#include <string.h>

#define COUNT_LETTER 6
#define MAX_COUNT_CUBE 13
#define MAX_COUNT_CHAR 128

typedef struct Edge {
	int used;
	int adjacent_vertex;
	struct Edge* next;
} Edge;


char blocks[MAX_COUNT_CUBE][COUNT_LETTER + 1];
char word[MAX_COUNT_CUBE];
Edge* connect[2][MAX_COUNT_CUBE];
int used[2][MAX_COUNT_CUBE];
int use[MAX_COUNT_CUBE];


Edge* createNewEdge (int adjacent_vertex, Edge* next)
{
	Edge* new_edge = (Edge*) malloc(sizeof(Edge));
	if (!new_edge)
	{
		return NULL;  //bo-bo
	}
	new_edge->used = 0;
	new_edge->adjacent_vertex = adjacent_vertex;
	new_edge->next = next;
	return new_edge;
}


void addEdge (int index_in_1, int index_in_0)
{
	if (connect[1][index_in_1]
			&& connect[1][index_in_1]->adjacent_vertex == index_in_0
		|| connect[0][index_in_0]
			&& connect[0][index_in_0]->adjacent_vertex == index_in_1)
		return;
	connect[1][index_in_1] = createNewEdge(index_in_0, connect[1][index_in_1]);
	connect[0][index_in_0] = createNewEdge(index_in_1, connect[0][index_in_0]);
}


int findEmptyBlock(int index_in_0)
{
	use[index_in_0] = 1;
		int is_find = 0;
	Edge* e = connect[0][index_in_0];
	while (!is_find && e)
	{
		if (!used[1][e->adjacent_vertex]
			|| e->adjacent_vertex != used[0][index_in_0] - 1
				&& !use[used[1][e->adjacent_vertex] - 1]
				&& findEmptyBlock(used[1][e->adjacent_vertex] - 1))
		{
			is_find = 1;
			used[0][index_in_0] = e->adjacent_vertex + 1;
			used[1][e->adjacent_vertex] = index_in_0 + 1;
		}
		e = e->next;
	}
	use[index_in_0] = 0;
	return is_find;
}


int main()
{

	int i, j, k, n;
	for (i = 0; i < MAX_COUNT_CUBE; i++)
		for (j = 0; j < 2; j++)
		{
			connect[j][i] = NULL;
			used[j][i] = 0;
		}

	FILE* in = fopen("input.txt", "r");
	FILE* out = fopen("output.txt", "w");
	fscanf(in, "%d", &n);
	fscanf(in, "%s", word);
	for (i = 0; i < n; i++)
	{
		fscanf(in, "%s", blocks[i]);
		for (j = 0; j < COUNT_LETTER; j++)
			for (k = 0; k < strlen(word); k++)
				if (blocks[i][j] == word[k])
					addEdge(i, k);
	}

	int all_use = 1;
	for (i = 0; i < strlen(word); ++i)
	{
		if (used[0][i]) continue;
		all_use *= findEmptyBlock(i);
	}

	if (all_use)
		for(i = 0; i < strlen(word); i++)
			fprintf(out, "%d ", used[0][i]);
	else
		fprintf(out, "0");
	return 0;
}
