#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

const double LEFT = 0;
const double RIGHT = 100;
const double TOP = 0;
const double BOTTOM = 100;
const double ZOOM = 10;
const double INF = 10000;
const double EPS = 0.0001;

class Point {
public :
	double x;
	double y;
	string color;
	string info;
	void Print(FILE* outFile);
	bool operator == (Point* p)	{ return abs(x - p->x) < EPS && abs(y - p->y) < EPS; }
	bool operator != (Point* p)	{ return abs(x - p->x) > EPS || abs(y - p->y) > EPS; }
	Point () {}
	Point(double X, double Y)
		: x(abs(X) < 0.001 ? 0 : X)
		, y(abs(Y) < 0.001 ? 0 : Y)
		, color("#FF0000")
	{}
};

class Poligon
{
public:
	vector<Point*> points;
	void Print(FILE* outFile);
	Poligon(vector<Point*> p)
		: points(p)
	{}
};

class Line
{ // y = kx + b
public :
	Point* start;
	Point* end;
	Point* virtual_start;
	Point* virtual_end;
	string color;
	bool isVertical;

	double k;
	double b;
	double Y(double x) {return k * x + b; }
	bool valid() { return !(start->x > end->x|| isVertical && start->y > end->y); }
	void Print(FILE* outFile);

	Line(Point* A, Point* B, string c)
		: start(A)
		, end(B)
		, color(c)
	{
		isVertical = A->x == B->x;
		if (isVertical) {
			k = 999999999;
			b = A->x;
			virtual_start = new Point(b, -INF);
			virtual_end = new Point(b, INF);
		} else {
			k = 0;
			b = A->y;
			virtual_start = new Point(-INF, Y(-INF));
			virtual_end = new Point(INF, Y(INF));
		}
	}

	Line(Point* A, Point* B) : color("#0000FF")
	{
		isVertical = A->y == B->y;
		if (!isVertical)
		{
			k = (A->x - B->x) / (B->y - A->y);
			b = (B->x - A->x) * (A->x + B->x) / (B->y - A->y) / 2 + (A->y + B->y) / 2;
			start = new Point(LEFT, Y(LEFT));
			end = new Point(RIGHT, Y(RIGHT));
			virtual_start = new Point(-INF, Y(-INF));
			virtual_end = new Point(INF, Y(INF));
		}
		else
		{
			k = 999999999;
			b = (A->x + B->x) / 2;
			start = new Point(b, TOP);
			end = new Point(b, BOTTOM);
			virtual_start = new Point(b, -INF);
			virtual_end = new Point(b, INF);
		}
	}
};



double STrianle(Point* A, Point* B, Point* C)
{
	return abs(A->x * (B->y - C->y) + B->x * (C->y - A->y) + C->x * (A->y - B->y)) / 2;
}

bool isPointInTriangle(Point* P, Point* A, Point* B, Point* C)
{
	return abs(STrianle(A, B, C) - (STrianle(A, B, P) + STrianle(A, P, C) + STrianle(P, B, C))) < 0.0001 && STrianle(A, B, C) > EPS;
}

void update(Line* l, Point* p, bool changeStart)
{
	if (!l->isVertical && changeStart && l->start->x >= p->x
		|| !l->isVertical && !changeStart && l->end->x <= p->x
		|| l->isVertical && changeStart && l->start->y >= p->y
		|| l->isVertical && !changeStart && l->end->y <= p->y)
		return;

	if (changeStart)
		l->start = p;
	else
		l->end = p;
}

Point* crossLine(Line* l1, Line* l2)
{
	double x;
	if (abs(l1->k - l2->k) < EPS)
		return 0;

	if (l1->isVertical || l2->isVertical)
	{
		if (l1->isVertical)
		{
			Line* l = l1;
			l1 = l2;
			l2 = l;
		}
		x = l2->b;
	}
	else
		x = (l1->b - l2->b) / (l2->k - l1->k);

	Point* crossPoint = new Point(x, l1->Y(x));
	return crossPoint;
}

void checkLine(Point* curPoint, Point* crossPoint, Line* l1, Line* l2)
{

	if (isPointInTriangle(curPoint, crossPoint, l1->virtual_start, l2->virtual_start))
	{
		update(l1, crossPoint, false);
		update(l2, crossPoint, false);
	}
	else if (isPointInTriangle(curPoint, crossPoint, l1->virtual_start, l2->virtual_end))
	{
		update(l1, crossPoint, false);
		update(l2, crossPoint, true);
	}
	else if (isPointInTriangle(curPoint, crossPoint, l1->virtual_end, l2->virtual_start))
	{
		update(l1, crossPoint, true);
		update(l2, crossPoint, false);
	}
	else if (isPointInTriangle(curPoint, crossPoint, l1->virtual_end, l2->virtual_end))
	{
		update(l1, crossPoint, true);
		update(l2, crossPoint, true);
	}
}

void Print(vector <Line*> v);
void Print(vector <Point*> v);
void Print(Poligon* v);

int main()
{
	freopen("test2.in", "r", stdin);
	FILE* outFile = fopen("output2.svg", "w");
	std::ofstream fout(outFile);

	fout << "<?xml version=\"1.0\"?>" << endl;
	fout << "<svg width=\"1251px\" xmlns=\"http://www.w3.org/2000/svg\" height=\"751px\" version=\"1.1\" baseProfile=\"full\">" << endl;
	fout << "<defs/>" << endl;
	fout << "<g transform=\"translate(10,10)\">" << endl;

	int n;
	cin >> n;

	vector <Point*> points;
	for (int i = 0; i < n; i++)
	{
		double x, y;
		cin >> x >> y;
		points.push_back(new  Point(x, y));
	}

	vector<Poligon*> result;
	for (int k = 0; k < n; k++)
	{
		Line* top = new Line(new Point(LEFT, TOP), new Point(RIGHT, TOP), "#00FF00");
		Line* bottom = new Line(new Point(LEFT, BOTTOM), new Point(RIGHT, BOTTOM), "#00FF00");
		Line* right = new Line(new Point(RIGHT, TOP), new Point(RIGHT, BOTTOM), "#00FF00");
		Line* left = new Line(new Point(LEFT, TOP), new Point(LEFT, BOTTOM), "#00FF00");

		vector<Line*> lines;
		lines.push_back(top);
		lines.push_back(left);
		lines.push_back(right);
		lines.push_back(bottom);

		for (int i = 0; i < n; i++)
			if (k != i)
				lines.push_back(new Line(points[k], points[i]));

		for (int i = 0; i < lines.size(); i++)
			for (int j = i + 1; j < lines.size(); j++)
			{
				Point* cp = crossLine(lines[i], lines[j]);
				if (!cp) continue;
				checkLine(points[k], cp, lines[i], lines[j]);
			}
		Print(lines);
		vector<Point*> res;
		for (int i = 0; i < lines.size() && !res.size(); i++)
		{
			if (lines[i]->valid())
			{
				res.push_back(lines[i]->start);
				res.push_back(lines[i]->end);
				bool add = true;
				while (add && *res[0] != res.back())
				{
					add = false;
					for (int j = 0; j < lines.size(); j++)
					{
						if (lines[j]->valid() && *lines[j]->start == res.back() && *lines[j]->end != res[res.size() - 2])
						{
							res.push_back(lines[j]->end);
							add = true;
							break;
						}
						if (lines[j]->valid() && *lines[j]->end == res.back() && *lines[j]->start != res[res.size() - 2])
						{
							res.push_back(lines[j]->start);
							add = true;
							break;
						}
					}
				}
				if (!add)
					res.resize(0);
			}
		}
		Print(res);
		Print(new Poligon(res));
		result.push_back(new Poligon(res));
	}

	for (int i = 0; i < result.size(); i++)
		result[i]->Print(outFile);
	for (int i = 0; i < n; i++)
		points[i]->Print(outFile);

	fout << "</g>" << endl;
	fout << "</svg>" << endl;
}

void Point::Print(FILE* outFile)
{
	std::ofstream fout(outFile);
	fout << "<ellipse "
		<< " cx=\"" << x * ZOOM << "\""
		<< " cy=\"" << y * ZOOM << "\""
		<< " rx=\"" << 1 << "\""
		<< " ry=\"" << 1 << "\""
		<< " fill=\"#FFFFFF\" stroke=\"" << color << "\" stroke-width=\"1\"/>" << "<!--" << info << "-->"
		<< endl;
}


void Poligon::Print(FILE* outFile)
{
	std::ofstream fout(outFile);
	fout << "<polygon points=\"";
	for (int i = 0; i < points.size(); i++)
		fout << points[i]->x * ZOOM << "," << points[i]->y * ZOOM << ' ';

	fout << "\" fill=\"rgb(" << rand() % 255 << "," << rand() % 255 << "," << rand() % 255 << ") "
		 << "\" stroke=\"rgb(" << rand() % 255 << "," << rand() % 255 << "," << rand() % 255 << ") " 
		 << "\" stroke-width=\"1\"/>"
		 << endl;
}

void Line::Print(FILE* outFile)
{
	std::ofstream fout(outFile);
	fout << "<line "
		<< " x1=\"" << start->x * ZOOM << "\""
		<< " x2=\"" << end->x * ZOOM << "\""
		<< " y1=\"" << start->y * ZOOM << "\""
		<< " y2=\"" << end->y * ZOOM << "\""
		<< " stroke=\"" << color << "\" stroke-width=\"1\"/>"
		<< endl;
}

void Print(vector <Line*> v)
{
	FILE* outFile = fopen("out.svg", "w");
	std::ofstream fout(outFile);

	fout << "<?xml version=\"1.0\"?>" << endl;
	fout << "<svg width=\"1251px\" xmlns=\"http://www.w3.org/2000/svg\" height=\"751px\" version=\"1.1\" baseProfile=\"full\">" << endl;
	fout << "<defs/>" << endl;
	fout << "<g transform=\"translate(338,313)\">" << endl;

	for (int i = 0; i < v.size(); i++)
		if (v[i]->valid())
			v[i]->Print(outFile);

	fout << "</g>" << endl;
	fout << "</svg>" << endl;
	fclose(outFile);
}

void Print(vector <Point*> v)
{
	FILE* outFile = fopen("out.svg", "w");
	std::ofstream fout(outFile);

	fout << "<?xml version=\"1.0\"?>" << endl;
	fout << "<svg width=\"1251px\" xmlns=\"http://www.w3.org/2000/svg\" height=\"751px\" version=\"1.1\" baseProfile=\"full\">" << endl;
	fout << "<defs/>" << endl;
	fout << "<g transform=\"translate(338,313)\">" << endl;

	for (int i = 0; i < v.size(); i++)
		v[i]->Print(outFile);

	fout << "</g>" << endl;
	fout << "</svg>" << endl;
	fclose(outFile);
}

void Print(Poligon* v)
{
	FILE* outFile = fopen("out.svg", "w");
	std::ofstream fout(outFile);

	fout << "<?xml version=\"1.0\"?>" << endl;
	fout << "<svg width=\"1251px\" xmlns=\"http://www.w3.org/2000/svg\" height=\"751px\" version=\"1.1\" baseProfile=\"full\">" << endl;
	fout << "<defs/>" << endl;
	fout << "<g transform=\"translate(338,313)\">" << endl;

	v->Print(outFile);

	fout << "</g>" << endl;
	fout << "</svg>" << endl;
	fclose(outFile);
}
