#include <iostream>
#include <string>
#include <vector>
#include <cstdio>

using namespace std;

struct Edge;
string text;

typedef struct Node
{
	Edge* to_parent;
	Node* suf_ref;
	vector<Edge*> to_child;
	int deep;
	Node()
		: deep(0)
		, suf_ref(nullptr)
		, to_parent(nullptr)
	{};
} Node;

typedef struct Edge
 {
	Node* parent;
	Node* child;
	int l;
	int r;
	int size() { return r - l + 1;}
	Edge(Node* p, Node* c, int _l, int _r)
		: parent(p)
		, child(c)
		, l(_l)
		, r(_r)
	{}
} Edge;

Node* curNode;
Node* preNode;

bool isRoot(Node* n)
{
	return n->to_parent == nullptr;
}

int addSubstr(int l, int r)
{
	for (int i = 0; i < curNode->to_child.size(); i++)
	{
		Edge* curEdge = curNode->to_child[i];
		int posInText = curNode->deep + l;
		int posInEdge = curEdge->l;
		while (posInEdge <= curEdge->r && posInText <= r && text[posInEdge] == text[posInText])
		{
			posInText++;
			posInEdge++;
		}
		if (posInText > r)
			return 3;
		if (posInEdge > curEdge->r)
		{
			if (preNode && curEdge->child->deep + 1 == preNode->deep)
			{
				preNode->suf_ref = curEdge->child;
				preNode = nullptr;
			}
			curNode = curEdge->child;
			return addSubstr(l, r);
		}
		if (posInEdge > curEdge->l)
		{
			Node* newNode = new Node();
			Edge* newEdge = new Edge(newNode, curEdge->child, posInEdge, curEdge->r);
			newEdge->child->to_parent = newEdge;
			curEdge->child = newNode;
			curEdge->r = posInEdge - 1;
			newNode->to_parent = curEdge;
			newNode->to_child.push_back(newEdge);
			newNode->deep = curEdge->parent->deep + curEdge->size();
			Node* newChild = new Node();
			newEdge = new Edge(newNode, newChild, posInText, r);
			newNode->to_child.push_back(newEdge);
			newChild->to_parent = newEdge;
			newChild->deep = newNode->deep + newEdge->size();
			if (preNode)
			{
				preNode->suf_ref = newNode;
			}
			preNode = newNode;
			if (isRoot(newNode->to_parent->parent) && newNode->to_parent->size() == 1)
			{
				newNode->suf_ref = newNode->to_parent->parent;
				preNode = nullptr;
			}
			return 2;
		}
	}
	Node* newNode = new Node();
	Edge* newEdge = new Edge(curNode, newNode, l + curNode->deep, r);
	newNode->to_parent = newEdge;
	newNode->deep = newEdge->parent->deep + newEdge->size();
	curNode->to_child.push_back(newEdge);
	if (preNode)
	{
		preNode->suf_ref = curNode;
		preNode = nullptr;
	}
	return 1;
}

int haveWord(string word, int posInWord)
{
	for (int i = 0; i < curNode->to_child.size(); i++)
	{
		Edge* curEdge = curNode->to_child[i];
		int posInEdge = curEdge->l;
		while (posInEdge <= curEdge->r && posInWord < word.length() && text[posInEdge] == word[posInWord])
		{
			posInWord++;
			posInEdge++;
		}
		if (posInWord == word.length())
			return 1;
		if (posInEdge > curEdge->r)
		{
			curNode = curEdge->child;
			return haveWord(word, posInWord);
		}
		if (posInEdge > curEdge->l)
			return 0;
	}
	return 0;
}

int main()
{
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	cin >> text;
	int n = text.length();
	Node* root = new Node();
	curNode = root;
	root->suf_ref = root;
	preNode = nullptr;
	int rule = 0;
	for (int l = 0; l < n && rule != 3; l++)
	{
		rule = addSubstr(l, n - 1);
		curNode = curNode->suf_ref;
	}
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		curNode = root;
		string word;
		cin >> word;
		cout << haveWord(word, 0) << " ";
	}
	return 0;
}
