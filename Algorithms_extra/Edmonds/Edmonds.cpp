#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <algorithm>

using namespace std;

typedef enum {
	NOT_VISIT,
	IN_VERTEX,
	OUT_VERTEX
} ClassV;

vector<vector<int>> matrix;
vector<int> pairs;

vector<int> getListOfParents(vector<int>& parents, int root, int v) {
	vector<int> result;
	while (v != root) {
		result.push_back(v);
		result.push_back(pairs[v]);
		v = parents[pairs[v]];
	}
	result.push_back(v);
	reverse(result.begin(), result.end());
	return result;
}

vector<int> getFlower(vector<int> &v1, vector<int> &v2){
	vector<int> result;
	int i = 0;
	while (i < v1.size() && i < v2.size() && v1[i] == v2[i])
		i++;
	for (int j = i - 1; j < v1.size(); j++)
		result.push_back(v1[j]);
	for (int j = v2.size() - 1; v2[j] != result[0]; j--)
		result.push_back(v2[j]);
	return result;
}

void edmonds (int root) {
	int n = matrix.size();
	queue <int> outV;
	vector<int> parent (n, 0);
	vector<bool> used(n, false);
	vector<ClassV> classV (n, NOT_VISIT);
	classV[root] = OUT_VERTEX;
	outV.push(root);
	while (!outV.empty()) {
		int curV = outV.front();
		outV.pop();
		if (used[curV]) continue;
		used[curV] = true;
		for (int i = 1; i < n; i++)
			if (matrix[curV][i]) {
				if (!pairs[i] && i != root) { // reverse
					parent[i] = curV;
					curV = i;
					while (parent[curV] != root) {
						int newEnd = curV;
						curV = pairs[parent[curV]];
						pairs[newEnd] = pairs[curV];
						pairs[pairs[curV]] = newEnd;
					}
					pairs[curV] = root;
					pairs[root] = curV;
					return;
				} else if (classV[i] == OUT_VERTEX) { // flower
					vector<int> wayTo_curV = getListOfParents(parent, root, curV);
					vector<int> wayTo_i = getListOfParents(parent, root, i);
					vector<int> flower = getFlower(wayTo_curV, wayTo_i);
					for (int j = 2; j < flower.size(); j += 2)
						parent[flower[j]] = flower[(j + 1) %  flower.size()];
					for (int j = flower.size() - 2; j > 0 ; j -= 2)
						parent[flower[j]] = flower[j - 1];
					for (int j = 0; j < flower.size(); j ++)
						outV.push(flower[j]);
				} else if (classV[i] == NOT_VISIT ) { // add new pair
					classV[i] = IN_VERTEX;
					classV[pairs[i]] = OUT_VERTEX;
					parent[i] = curV;
					outV.push(pairs[i]);
				} // else (classV[i] == IN_VERTEX) - do nothing
			}
	}
	pairs[root] = -1;
}


int main(int argc, char* argv[]){
	freopen("11.in", "r", stdin);
	freopen("output.txt", "w", stdout);
	//freopen(argv[1], "r", stdin);
	//freopen(argv[2], "w", stdout);
	int n, m;
	cin >> n >> m;
	n++;
	matrix.resize(n);
	pairs.resize(n);
	for (int i = 1; i < n; i++){
		pairs[i] = 0;
		matrix[i].resize(n);
		for (int j = 1; j < n; j++)
			matrix[i][j] = 0;
	}
	for (int i = 0; i < m; i++) {
		int a, b;
		cin >> a >> b;
		matrix[a][b] = 1;
		matrix[b][a] = 1;
	}
	for (int i = 1; i < n; i++)
		for (int j = 1; j < n && !pairs[i]; j++)
			if (matrix[i][j] && !pairs[j]) {
				pairs[i] = j;
				pairs[j] = i;
			}
	for (int i = 1; i < n; i++)
		if (!pairs[i])
			edmonds(i);
	int count = 0;
	for (int i = 1; i < n; i++)
		count += pairs[i] > 0;
	cout << count / 2 << endl;
	for (int i = 1; i < n; i++)
		cout << i << " " << pairs[i] << endl;
	return 0;
}
