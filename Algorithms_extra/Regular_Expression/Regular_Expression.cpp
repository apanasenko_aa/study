#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <stack>
#include <hash_map>

using namespace std;

static const int MAX_SIZE = 256;
static const char EPS = '\0';

class Node
{
public:
    vector<vector<Node*>> transitions;
    Node()
		: transitions(vector<vector<Node*>>(MAX_SIZE, vector<Node*>())) 
	{}
    void AddTransition(char c, Node* n) { transitions[c].push_back(n); }
};

class BaseRegex
{
public:
    Node* firstNode;
    Node* lastNode;
    BaseRegex() : firstNode(new Node()), lastNode(new Node()) {}
};

class SimpleRegex : public BaseRegex
{
public:
    SimpleRegex(char c)
		: BaseRegex() { firstNode->AddTransition(c, lastNode); }
};

class ChoiceRegex : public BaseRegex
{
public:
    BaseRegex* l;
    BaseRegex* r;
    ChoiceRegex(BaseRegex* L, BaseRegex* R);
};

ChoiceRegex::ChoiceRegex(BaseRegex* L, BaseRegex* R) : BaseRegex(), l(L), r(R)
{
    firstNode->AddTransition(EPS, l->firstNode);
    firstNode->AddTransition(EPS, r->firstNode);
    l->lastNode->AddTransition(EPS, lastNode);
    r->lastNode->AddTransition(EPS, lastNode);
}

class AnyCountRegex : public BaseRegex
{
public:
    AnyCountRegex(BaseRegex* r);
};

AnyCountRegex::AnyCountRegex(BaseRegex* r)
{
    firstNode = r->firstNode;
    lastNode = r->lastNode;
    firstNode->AddTransition(EPS, lastNode);
    lastNode->AddTransition(EPS, firstNode);
}

class Regex : public BaseRegex
{
private:
    hash_map<Node*, bool> added;
    BaseRegex* parseTerm(const string& str, int& idx);
    BaseRegex* parseRegex(const string& str, int& idx);
    bool isOp(char c) { return c == '(' || c == ')' || c == '*' || c == '|'; }
    void mergeStates(vector<Node*>& arr, Node* curNode);
    void parse(const string& str);
    void joinRegex(vector<BaseRegex*>& rs);
public:
    Regex(const string& str) { parse(str + EPS); }
    Regex(vector<BaseRegex*>& rs) { joinRegex(rs); }
    bool findIn(const string& pattern);
};

void Regex::joinRegex(vector<BaseRegex*>& rs)
{
    firstNode = rs[0]->firstNode;
    Node* cur = rs[0]->lastNode;
    for (int i = 1; i < rs.size(); i++)
    {
        cur->AddTransition(EPS, rs[i]->firstNode);
        cur = rs[i]->lastNode;
    }
    lastNode = cur;
}

void Regex::mergeStates(vector<Node*>& arr, Node* curNode)
{
    vector<Node*> t = curNode->transitions[EPS];
    for (int i = 0; i < t.size(); i++)
    {
        if (added.count(t[i]) == 0)
        {
            arr.push_back(t[i]);
            added[t[i]] = true;
            mergeStates(arr, t[i]);
        }
    }
}

bool Regex::findIn(const string& pattern)
{
    vector<Node*> nextStates;
    int i = 0;
    while (i < pattern.length() || nextStates.size() > 0)
    {
        vector<Node*> currStates = nextStates;
        nextStates.clear();
        if (i < pattern.length())
            currStates.push_back(firstNode);
        int n = currStates.size();
        for (int j = 0; j < n; j++)
            mergeStates(currStates, currStates[j]);
        added.clear();
        for (int j = 0; j < currStates.size(); j++)
        {
            auto cur = currStates[j];
            if (cur == lastNode)
                return true;
            else if (i < pattern.length())
                for (const auto& state : cur->transitions[pattern[i]])
                    nextStates.push_back(state);
        }
        i++;
    }
    return false;
}


BaseRegex* Regex::parseTerm(const string& str, int& idx)
{
    BaseRegex* r = nullptr;
    if (!isOp(str[idx]))
    {
        vector<BaseRegex*> rs;
        while (!isOp(str[idx]) && idx < str.length())
            rs.push_back(new SimpleRegex(str[idx++]));
        r = new Regex(rs);
    } else if (str[idx] == '(')
        r = parseRegex(str, ++idx);
    if (str[idx] == '*')
    {
        r = new AnyCountRegex(r);
        idx++;
    }
    return r;
}

BaseRegex* Regex::parseRegex(const string& str, int& idx)
{
    BaseRegex* l = parseTerm(str, idx);
    if (str[idx] == '|')
    {
        BaseRegex* r = parseRegex(str, ++idx);
        return new ChoiceRegex(l, r);
    }
    if (str[idx] == ')')
        ++idx;
    return l;
}

void Regex::parse(const string& str)
{
    int idx = 0;
    vector<BaseRegex*> exprs;
    while (idx < str.length())
        exprs.push_back(parseRegex(str, idx));
    joinRegex(exprs);
}

int main(int argc, char* argv[]){
	//freopen("input.txt", "r", stdin);
	//freopen("output.txt", "w", stdout);
	freopen(argv[1], "r", stdin);
	freopen(argv[2], "w", stdout);
    int k = 0;
    string pattern;
    cin >> pattern;
    Regex regex(pattern);
    int n;
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        string text;
        cin >> text;
        cout << regex.findIn(text) << endl;
    }
    cout << endl << endl;
    return 0;
}
