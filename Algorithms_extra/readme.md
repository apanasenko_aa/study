﻿Алгоритмы: дополнительные главы
===============================

[Страница курса](http://imcs.dvfu.ru/works/marks?group=27579;course=14471;year=2014;order_by=n)

* [Максимальный поток минимальной стоимости](Min_cost_max_flow/) ([Задача](http://imcs.dvfu.ru/cats/static/problem_text-cpid-934202.html))
* [Паросочетание в двудольном графе](Word_of_blocks/) ([Задача](http://imcs.dvfu.ru/cats/static/problem_text-cpid-935938.html))
* [Алгоритм Эдмонса](Edmonds/)
* [Алгоритм Укконена](Ukkonen_algorithm/) ([Задача](http://imcs.dvfu.ru/cats/static/problem_text-cpid-939661.html))
* [Регулярные выражения](Regular_Expression/)
* [Диаграмы Вороного](Voronoi_diagram/)
