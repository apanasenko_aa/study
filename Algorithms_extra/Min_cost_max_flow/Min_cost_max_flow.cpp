#include <stdio.h>
#include <string>
#include <stdlib.h>
#include <stdio.h>

#define MAX_COUNT_VERTEX 201
#define MAX_COUNT_EDGE 2001
#define MAX_COUNT_FLOW 101

typedef struct Edge {
	int to;
	int flow;
	int cost;
	int number;
	struct Edge* next;
	struct Edge* back;
} Edge;

typedef struct Vertex {
	Edge* road_to_city;
	int time_to_city;
	int previous_city;
	int fi;
} Vertex;

typedef struct Answer {
	std::string way;
	int length;
} Answer;


Edge* roads[MAX_COUNT_VERTEX];
Vertex cities[MAX_COUNT_VERTEX];
Answer ways[MAX_COUNT_FLOW];

Edge* createNewEdge (Edge* next, int to, int cost, int number, int flow)
{
	Edge* new_edge = (Edge*) malloc(sizeof(Edge));
	if (!new_edge) return NULL;
	new_edge->cost = cost;
	new_edge->number = number;
	new_edge->flow = flow;
	new_edge->to = to;
	new_edge->next = next;
	return new_edge;
}


void addEdge (int from, int to, int cost, int number)
	{
	if (from == to) return;
	roads[from] = createNewEdge(roads[from], to, cost, number, 1);
	roads[to] = createNewEdge(roads[to], from, -cost, number, 0);
	roads[from]->back = roads[to];
	roads[to]->back = roads[from];
}


void prepareGraph(int count_cities)
{
	int i;
	for (i = 1; i <= count_cities; i++)
	{
		cities[i].fi += cities[i].time_to_city;
		cities[i].time_to_city = -1;
	}
	cities[1].time_to_city = 0;
}


int findRoad(int n)
{
	int queue_cities[MAX_COUNT_VERTEX];
	int q_start = 0;
	int q_end = 1;
	queue_cities[0] = 1;
	while (q_start < q_end)
	{
		int cur_city = queue_cities[q_start++];
		for (Edge* e = roads[cur_city]; e != 0; e = e->next)
		{
			if (!e->flow) continue;
			Vertex* to = &cities[e->to];
			Vertex* from = &cities[cur_city];
			if (to->time_to_city == -1
				|| to->time_to_city > from->time_to_city + e->cost - to->fi + from->fi)
			{
				if (to->time_to_city == -1)
					queue_cities[q_end++] = e->to;
				to->time_to_city = from->time_to_city + e->cost - to->fi + from->fi;
				to->road_to_city = e;
				to->previous_city = cur_city;
			}
		}
		int i, near_city = q_start;
		for (i = q_start + 1; i < q_end; i++)
			near_city = cities[queue_cities[i]].time_to_city < cities[queue_cities[near_city]].time_to_city ? i : near_city;
		queue_cities[q_start] = (i = queue_cities[q_start] + queue_cities[near_city]) - (queue_cities[near_city] = queue_cities[q_start]); // swap
	}
	return cities[n].time_to_city != -1;
}


void incFlow(int cur_city)
{
	while (cur_city != 1)
	{
		cities[cur_city].road_to_city->flow--;
		cities[cur_city].road_to_city->back->flow++;
		cur_city = cities[cur_city].previous_city;
	}
}


std::string toString(int k)
{
	return k ? toString(k / 10) + char((k % 10) + '0') : "";
}


int checkRoad(int length, int cost, int index_city, int i, int n, std::string way)
{
	if (index_city == n)
	{
		ways[i].length = length;
		ways[i].way = way;
		return cost;
	}
	for (Edge* e = roads[index_city]; e != 0; e = e->next)
	{
		if (e->flow || e->cost < 0) continue;
		e->flow++;
		return checkRoad(length + 1, cost + e->cost, e->to, i, n, way + toString(e->number) + " ");
	}
}


int main()
{
	FILE* in = fopen("brides.in", "r");
	FILE* out = fopen("brides.out", "w");
	int i, n, m, k;
	fscanf(in, "%d %d %d", &n, &m, &k);
	memset(roads, 0, n + 1);
	for (i = 1; i <= m; i++)
	{
		int a, b, c;
		fscanf(in, "%d %d %d", &a, &b, &c);
		addEdge(a, b, c, i);
		addEdge(b, a, c, i);
	}
	int have_road = 1;
	for (i = 0; i < k && have_road; i++)
	{
		prepareGraph(n);
		if (have_road *= findRoad(n))
			incFlow(n);
	}
	if (have_road)
	{
		double sum = 0;
		for (i = 0; i < k; i++)
			sum += checkRoad(0, 0, 1, i, n, "");
		fprintf(out, "%.5f \n", sum / k);
		for (i = 0; i < k; i++)
			fprintf(out, "%d %s\n", ways[i].length, ways[i].way.c_str());
	} else
		fprintf(out, "-1\n");
	return 0;
}
