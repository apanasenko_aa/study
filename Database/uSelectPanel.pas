unit uSelectPanel;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, StdCtrls, ExtCtrls, uFieldNames;

type

  TSelectPanel = class
    P_ConditSelect: TPanel;
    CB_Column: TComboBox;
    CB_Operations: TComboBox;
    CB_LogicOper: TComboBox;
    SearchString: TEdit;
    constructor Create(CurTable: TTableDesc);
  private
    Table: TTableDesc;
  end;

implementation

constructor TSelectPanel.Create(CurTable: TTableDesc);
var
  i: integer;
begin
  Table := CurTable;
  P_ConditSelect := TPanel.Create(nil);
  CB_Column := TComboBox.Create(P_ConditSelect);
  with CB_Column do
  begin
    Parent := P_ConditSelect;
    SetBounds(12, 12, 150, 23);
    for i := 0 to High(CurTable.Fields) do
      Items.Add(CurTable.Fields[i].Caption);
    Style := csDropDownList;
    ItemIndex := 1;
  end;
  CB_Operations := TComboBox.Create(P_ConditSelect);
  with CB_Operations do
  begin
    Parent := P_ConditSelect;
    SetBounds(182, 12, 90, 23);
    for i := 0 to 7 do
      Items.Add(Operations[i]);
    Style := csDropDownList;
    ItemIndex := 0;
  end;
  SearchString := TEdit.Create(P_ConditSelect);
  with SearchString do
  begin
    Parent := P_ConditSelect;
    SetBounds(292, 12, 90, 23);
  end;
  CB_LogicOper := TComboBox.Create(P_ConditSelect);
  with CB_LogicOper do
  begin
    Parent := P_ConditSelect;
    SetBounds(402, 12, 50, 23);
    Items.Add('И');
    Items.Add('ИЛИ');
    Style := csDropDownList;
    ItemIndex := 0;
    Visible := False;
  end;
end;

end.

