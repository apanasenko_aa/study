unit uCard;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Buttons,
  DbCtrls, StdCtrls, ExtCtrls, uFieldNames, sqldb, IBConnection, db,
  uDataModule;

type

  { TCardForm }

  TCardForm = class(TForm)
    Card_DS: TDatasource;
    DBText_ID: TDBText;
    SB_Save: TSpeedButton;
    SB_Delete: TSpeedButton;
    SB_Cansel: TSpeedButton;
    Card_Query: TSQLQuery;
    Panel: TPanel;
    Card_Transaction: TSQLTransaction;
    Text_ID: TStaticText;
    constructor Create(ID: integer; Table: TTableDesc; isEdit: boolean); reintroduce;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure SB_CanselClick(Sender: TObject);
    procedure SB_DeleteClick(Sender: TObject);
    procedure SB_SaveClick(Sender: TObject);
    function GetUpdateSQLText: string;
    function GetDeleteSQLText: string;
    function GetSelectSQLText: string;
    function GetInsertSQLText: string;
    procedure CreateDBEdit(ind: integer);
    procedure CreateLabel(ind: integer);
    procedure CreateLookupCB(ind_field, ind_table: integer);
  private
    CardTable: TTableDesc;
    L_Fields: array of TLabel;
    DBE_Fields: array of TDBEdit;
    DBLookups: array of TDBLookupComboBox;
  public
    CardID: String;
  end; 

const
  Height_Comp = 30;

var
  CardForm: TCardForm;
  Top_Comp: integer;

implementation

{$R *.lfm}

constructor TCardForm.Create(ID: integer; Table: TTableDesc; isEdit: boolean);
const
  Height_SB = 90;
var
  i, TabInd: integer;
begin
  inherited Create(nil);
  Top_Comp := 10;
  CardID := IntToStr(ID);
  CardTable := Table;
  with Card_Query do
  begin
    if not isEdit then
    begin
      SQL.Text := 'SELECT GEN_ID(GEN_DB, 1) FROM RDB$DATABASE';
      Open;
      CardID := Fields[0].AsString;
      Close;
      SQL.Clear;
    end;
    SQL.Add(GetSelectSQLText);
    UpdateSQL.Add(GetUpdateSQLText);
    DeleteSQL.Add(GetDeleteSQLText);
    InsertSQL.Add(GetInsertSQLText);
    Open;
    if isEdit then
    begin
      Edit;
      Caption := Table.Caption + ' Карточка №' + CardID;
    end
    else
    begin
      Insert;
      FieldByName('ID').AsString := CardID;
      Caption := Table.Caption + ' Вставка...';
    end;
  end;
  for i := 0 to High(Table.Fields) do
  begin
    CreateLabel(i);
    TabInd := Table.Fields[i].TableIndex;
    if TabInd = -1 then
      CreateDBEdit(i)
    else
      CreateLookupCB(i, TabInd);
    Top_Comp += Height_Comp;
  end;
  Height:= Top_Comp + Height_SB;
  Show;
end;

function TCardForm.GetUpdateSQLText: string;
var
  i: integer;
  values: string;
begin
  Values := '';
  for i := 1 to High(CardTable.Fields) - 1 do
    Values += Format(' %s = :%s, ', [CardTable.Fields[i].Name, CardTable.Fields[i].Name]);
  Values += Format(' %s = :%s ', [CardTable.Fields[High(CardTable.Fields)].Name,
    CardTable.Fields[High(CardTable.Fields)].Name]);
  Result := Format('UPDATE %s SET %s WHERE ID = %s', [CardTable.Name, Values, CardID]);
end;

function TCardForm.GetDeleteSQLText: string;
begin
  Result := Format('DELETE FROM %s WHERE ID = %s', [CardTable.Name, CardID])
end;

function TCardForm.GetSelectSQLText: string;
begin
  Result := Format('SELECT * FROM %s WHERE ID = %s', [CardTable.Name, CardID ]);
end;

function TCardForm.GetInsertSQLText: string;
var
  Values, SFields: string;
  i: integer;
begin
  SFields:= '';
  for i := 0 to High(CardTable.Fields) - 1 do
    SFields += Format(' %s, ', [CardTable.Fields[i].Name]);
  SFields += Format(' %s ', [CardTable.Fields[High(CardTable.Fields)].Name]);
  Values := '';
  for i := 0 to High(CardTable.Fields) - 1 do
    Values += Format(' :%s, ', [CardTable.Fields[i].Name]);
  Values += Format(' :%s ', [CardTable.Fields[High(CardTable.Fields)].Name]);
  Result := Format('INSERT INTO %s (%s) VALUES(%s)', [CardTable.Name, SFields, Values]);
end;

procedure TCardForm.CreateDBEdit(ind: integer);
begin
  SetLength(DBE_Fields, Length(DBE_Fields) + 1);
  DBE_Fields[High(DBE_Fields)] := TDBEdit.Create(CardForm);
  with DBE_Fields[High(DBE_Fields)] do
  begin
    Parent := Panel;
    SetBounds(80, Top_Comp, 200, 25);
    Datasource := Card_DS;
    DataField := CardTable.Fields[ind].Name;
    if CardTable.Fields[ind].Name = 'ID' then
      ReadOnly:= True;
  end;
end;

procedure TCardForm.CreateLabel(ind: integer);
begin
  SetLength(L_Fields, Length(L_Fields) + 1);
  L_Fields[High(L_Fields)] := TLabel.Create(CardForm);
  with L_Fields[High(L_Fields)] do
  begin
    Parent := Panel;
    SetBounds(80, Top_Comp, 200, 25);
    Caption := CardTable.Fields[ind].Caption + ':';
  end;
  Top_Comp += Height_Comp;
end;

procedure TCardForm.CreateLookupCB(ind_field, ind_table: integer);
var
  CurQwery: TSQLQuery;
  CurDS : TDataSource;
begin
  CurQwery := TSQLQuery.Create(self);
  with CurQwery do begin
    Database := D_Module.D_Mod_Connection;
    Transaction := D_Module.D_Mod_Transaction;
    SQL.Text := Format('SELECT * FROM %s ORDER BY %s', [Tables[ind_table].Name, Tables[ind_table].Fields[0].Name]);
    Open;
  end;
  CurDS := TDataSource.Create(self);
  CurDS.DataSet := CurQwery;
  SetLength(DBLookups, Length(DBLookups) + 1);
  DBLookups[High(DBLookups)] := TDBLookupComboBox.Create(CardForm);
  with DBLookups[High(DBLookups)] do
  begin
    Parent := Panel;
    SetBounds(80, Top_Comp, 200, 25);
    Style := csDropDownList;
    DataSource := Card_DS;
    DataField := CardTable.Fields[ind_field].Name;
    ListSource := CurDS;
    KeyField := 'ID';
    ListField := Tables[ind_table].Fields[1].Name;
  end;
end;

procedure TCardForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  CloseAction := caFree;
end;

procedure TCardForm.SB_CanselClick(Sender: TObject);
begin
  Card_Transaction.Rollback;
  Card_Transaction.EndTransaction;
  Close;
end;

procedure TCardForm.SB_DeleteClick(Sender: TObject);
begin
  Card_Query.Delete;
  Card_Query.ApplyUpdates;
  Card_Transaction.Commit;
  Close;
end;

procedure TCardForm.SB_SaveClick(Sender: TObject);
begin
  Card_Query.Post;
  Card_Query.ApplyUpdates;
  Card_Transaction.Commit;
  Close;
end;

end.

