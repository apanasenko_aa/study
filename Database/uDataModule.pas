unit uDataModule;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, IBConnection, sqldb, FileUtil;

type

  { TD_Module }

  TD_Module = class(TDataModule)
    D_Mod_Connection: TIBConnection;
    D_Mod_Transaction: TSQLTransaction;
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  D_Module: TD_Module;

implementation

{$R *.lfm}

end.

