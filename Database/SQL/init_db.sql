CREATE DATABASE 'C:\Database\university_UTF8_2.fdb'
USER 'SYSDBA' PASSWORD 'masterkey'
DEFAULT CHARACTER SET UTF8;

CREATE TABLE groups(
		id INT NOT NULL,
		Name VARCHAR(150) NOT NULL,
		PRIMARY KEY(id));

CREATE TABLE students(
		id INT NOT NULL,
		Name VARCHAR(150) NOT NULL,
		Group_id INT REFERENCES  groups(id)
			ON DELETE CASCADE,
		Birhday DATE NOT NULL,
		PRIMARY KEY (id));

CREATE TABLE teachers(
		id INT NOT NULL,
		Name VARCHAR(150) NOT NULL,
		PRIMARY KEY ("id"));

CREATE TABLE subjects(
			id INT NOT NULL,
			Name VARCHAR(150) NOT NULL,
			Hours INT,
			PRIMARY KEY (id));

CREATE TABLE teachers_subjects(
			Teacher_id INT REFERENCES teachers(id),
			Subject_id INT REFERENCES subjects(id),
			Group_id INT REFERENCES groups(id));

CREATE TABLE classrooms(
		id INT NOT NULL,
		Auditory VARCHAR(50) NOT NULL,
		PRIMARY KEY (id));

CREATE TABLE time_lessons(
	id INT NOT NULL,
	Time TIME,
	PRIMARY KEY (id));

CREATE TABLE week_day(
	id INT NOT NULL,
	Day VARCHAR(100),
	PRIMARY KEY(id));

CREATE TABLE schedule(
	Week_id INT REFERENCES week_day(id),
	Time_id INT REFERENCES time_lessons(id),
	Subject_id INT REFERENCES subjects(id),
	Classroom_id INT REFERENCES classrooms(id),
	Group_id INT REFERENCES groups(id),
	Teacher_id INT REFERENCES teachers(id));

INSERT INTO groups VALUES(1, 'B8103a-1');
INSERT INTO groups VALUES(2, 'B8103a-2');
INSERT INTO groups VALUES(3, 'B8103б');

INSERT INTO students VALUES (1, 'Андриенко Максим', 2, '02/04/93');
INSERT INTO students VALUES (2, 'Апанасенко Александр',  1,  '06/28/93');
INSERT INTO students VALUES (3, 'Арнаут Ярослав', 2, '08/01/94');
INSERT INTO students VALUES (4, 'Белова Анна', 2, '01/17/93');
INSERT INTO students VALUES (5, 'Беспалов Данила', 1, '02/26/92');
INSERT INTO students VALUES (6, 'Бронов Александр', 2, '12/17/91');
INSERT INTO students VALUES (7, 'Голов Сергей', 2, '09/15/93');
INSERT INTO students VALUES (8, 'Горнак Дарья', 1, '07/18/93');
INSERT INTO students VALUES (9, 'Гура Ярослава', 2, '05/09/93');
INSERT INTO students VALUES (10, 'Долгополова Ирина', 2, '05/19/93');
INSERT INTO students VALUES (11, 'Духно Владимир', 2, '07/03/93');
INSERT INTO students VALUES (12, 'Захарова Анастасия', 2, '04/24/93');
INSERT INTO students VALUES (13, 'Зинов Михаил', 1,  '06/21/93');
INSERT INTO students VALUES (14, 'Злотников Антон', 1, '07/02/93');
INSERT INTO students VALUES (15, 'Кувялов Владимир', 2, '08/29/93');
INSERT INTO students VALUES (16, 'Ландышев Константин', 1, '09/02/93');
INSERT INTO students VALUES (17, 'Майбородин Денис', 1, '03/01/93');
INSERT INTO students VALUES (19, 'Молданова Ульяна', 2, '02/03/93');
INSERT INTO students VALUES (20, 'Момотюк Иван', 1, '09/26/93');
INSERT INTO students VALUES (21, 'Мухин Алексей', 1, '09/20/93');
INSERT INTO students VALUES (22, 'Никитенков Олег', 2, '10/10/93');
INSERT INTO students VALUES (23, 'Овчиникова Александра', 2, '05/13/93');
INSERT INTO students VALUES (24, 'Полушкин Василий', 2, '12/06/93');
INSERT INTO students VALUES (25, 'Раскатова Александра', 2, '03/16/93');
INSERT INTO students VALUES (26, 'Сафронов Александр', 1, '12/03/92');
INSERT INTO students VALUES (27, 'Столетняя Мария', 2, '05/25/93');
INSERT INTO students VALUES (28, 'Тертышный Марк', 1, '09/01/93');
INSERT INTO students VALUES (29, 'Франковский Кирилл', 1, '06/23/93');
INSERT INTO students VALUES (30, 'Хощенко Артем', 1, '02/22/93');
INSERT INTO students VALUES (31, 'Щурова Юлия', 1, '05/30/93');
INSERT INTO students VALUES (32, 'Якуба Максим', 2, '10/20/90');
INSERT INTO students VALUES (33, 'Данилова Вера', 3, '02/05/93');
INSERT INTO students VALUES (34, 'Шумина Ольга', 3, '06/29/93');
INSERT INTO students VALUES (35, 'Дьяконова Ольга', 3, '01/18/93');
INSERT INTO students VALUES (36, 'Астраханцева Алена', 3, '12/13/93');
INSERT INTO students VALUES (37, 'Святенко Карина', 3, '02/27/94');
INSERT INTO students VALUES (38, 'Кукина Татьяна', 3, '12/18/91');
INSERT INTO students VALUES (39, 'Кучкова Ксения', 3, '09/16/93');
INSERT INTO students VALUES (40, 'Спивак Юлия', 3, '07/19/93');
INSERT INTO students VALUES (41, 'Темненко Евгения', 3, '05/13/94');
INSERT INTO students VALUES (42, 'Тарасенко Александр', 3, '06/08/93');
INSERT INTO students VALUES (43, 'Ермоленко Никита', 3, '04/25/93');
INSERT INTO students VALUES (44, 'Пасполит Никита', 3, '06/22/93');
INSERT INTO students VALUES (45, 'Девликамов Глеб', 3, '07/03/93');
INSERT INTO students VALUES (46, 'Рогозин Денис', 3, '08/29/93');
INSERT INTO students VALUES (47, 'Каменьщиков Стас', 3, '09/03/93');
INSERT INTO students VALUES (48, 'Веренин Федор', 3, '03/02/93');
INSERT INTO students VALUES (49, 'Кузьмин Тимур', 3, '02/04/93');
INSERT INTO students VALUES (50, 'Локайчук Андрей', 3, '09/27/93');
INSERT INTO students VALUES (51, 'Утин Артем', 3, '09/21/93');
INSERT INTO students VALUES (52, 'Сарицкая Жанна', 3, '10/11/93');
INSERT INTO students VALUES (53, 'Лагин Максим', 3, '05/14/93');

INSERT INTO teachers VALUES(1, 'Дымченко Юрий Викторович');
INSERT INTO teachers VALUES(2, 'Дьяконова Тамара Михайловна');
INSERT INTO teachers VALUES(3, 'Жуплев Антон Сергеевич');
INSERT INTO teachers VALUES(4, 'Кленин Александр Сергеевич');
INSERT INTO teachers VALUES(5, 'Кленина Надежда Викторовна');
INSERT INTO teachers VALUES(6, 'Коробецкая Юлия Ивановна');
INSERT INTO teachers VALUES(7, 'Лудов Игорь Юрьевич');
INSERT INTO teachers VALUES(8, 'Никольская Татьяна Владимировна');
INSERT INTO teachers VALUES(9, 'Пак Геннадий Константинович');
INSERT INTO teachers VALUES(10, 'Прудникова Лариса Ивановна');
INSERT INTO teachers VALUES(11, 'Туфанов Игорь');
INSERT INTO teachers VALUES(12, 'Фролов Николай Николаевич');

INSERT INTO subjects VALUES(1, 'Алгебра и Геометрия', 114);
INSERT INTO subjects VALUES(2, 'Базы данных', 120);
INSERT INTO subjects VALUES(3, 'Дискретная математика', 114);
INSERT INTO subjects VALUES(4, 'Иностранный язык', 72);
INSERT INTO subjects VALUES(5, 'Математический анализ', 144);
INSERT INTO subjects VALUES(6, 'Практика на ЭВМ', 144);
INSERT INTO subjects VALUES(7, 'Физкультура', 68);
INSERT INTO subjects VALUES(8, 'Языки и методы программирования', 82);

INSERT INTO teachers_subjects VALUES(2,7,1);
INSERT INTO teachers_subjects VALUES(2,7,2);
INSERT INTO teachers_subjects VALUES(3,6,1);
INSERT INTO teachers_subjects VALUES(3,2,2);
INSERT INTO teachers_subjects VALUES(4,2,1);
INSERT INTO teachers_subjects VALUES(4,2,2);
INSERT INTO teachers_subjects VALUES(4,8,1);
INSERT INTO teachers_subjects VALUES(4,8,2);
INSERT INTO teachers_subjects VALUES(5,6,2);
INSERT INTO teachers_subjects VALUES(6,1,1);
INSERT INTO teachers_subjects VALUES(6,1,2);
INSERT INTO teachers_subjects VALUES(6,1,3);
INSERT INTO teachers_subjects VALUES(7,8,1);
INSERT INTO teachers_subjects VALUES(7,8,2);
INSERT INTO teachers_subjects VALUES(8,4,1);
INSERT INTO teachers_subjects VALUES(8,4,2);
INSERT INTO teachers_subjects VALUES(8,4,3);
INSERT INTO teachers_subjects VALUES(9,1,1);
INSERT INTO teachers_subjects VALUES(9,1,2);
INSERT INTO teachers_subjects VALUES(9,1,3);
INSERT INTO teachers_subjects VALUES(9,3,1);
INSERT INTO teachers_subjects VALUES(9,3,2);
INSERT INTO teachers_subjects VALUES(9,3,3);
INSERT INTO teachers_subjects VALUES(10,8,1);
INSERT INTO teachers_subjects VALUES(10,8,2);
INSERT INTO teachers_subjects VALUES(10,6,3);
INSERT INTO teachers_subjects VALUES(11,8,1);
INSERT INTO teachers_subjects VALUES(12,5,1);
INSERT INTO teachers_subjects VALUES(12,5,2);
INSERT INTO teachers_subjects VALUES(12,5,3);

INSERT INTO classrooms VALUES(1, '233');
INSERT INTO classrooms VALUES(2, '327');
INSERT INTO classrooms VALUES(3, '329');
INSERT INTO classrooms VALUES(4, '332');
INSERT INTO classrooms VALUES(5, '338');
INSERT INTO classrooms VALUES(6, '356');
INSERT INTO classrooms VALUES(7, 'Спорт. Зал');
INSERT INTO classrooms VALUES(8, '200ц');

INSERT INTO time_lessons VALUES(1, '8:00');
INSERT INTO time_lessons VALUES(2, '9:40');
INSERT INTO time_lessons VALUES(3, '11:20');
INSERT INTO time_lessons VALUES(4, '13:00');
INSERT INTO time_lessons VALUES(5, '16:10');

INSERT INTO week_day VALUES(1, 'Понедельник');
INSERT INTO week_day VALUES(2, 'Вторник');
INSERT INTO week_day VALUES(3, 'Среда');
INSERT INTO week_day VALUES(4, 'Четверг');
INSERT INTO week_day VALUES(5, 'Пятница');
INSERT INTO week_day VALUES(6, 'Суббота');

INSERT INTO schedule VALUES(1, 1, 3, 2, 1, 9);
INSERT INTO schedule VALUES(1, 1, 3, 2, 2, 9);
INSERT INTO schedule VALUES(1, 1, 4, 3, 3, 8);
INSERT INTO schedule VALUES(1, 2, 1, 1, 1, 9);
INSERT INTO schedule VALUES(1, 2, 1, 1, 2, 9);
INSERT INTO schedule VALUES(1, 2, 1, 1, 3, 9);
INSERT INTO schedule VALUES(1, 3, 6, 5, 1, 3);
INSERT INTO schedule VALUES(1, 3, 8, 8, 3, 10);
INSERT INTO schedule VALUES(1, 4, 6, 4, 1, 3);
INSERT INTO schedule VALUES(1, 4, 6, 8, 3, 10);
INSERT INTO schedule VALUES(1, 5, 6, 8, 3, 10);
INSERT INTO schedule VALUES(2, 1, 5, 6, 1, 1);
INSERT INTO schedule VALUES(2, 1, 5, 6, 2, 1);
INSERT INTO schedule VALUES(2, 2, 7, 7, 1, 2);
INSERT INTO schedule VALUES(2, 2, 7, 7, 2, 2);
INSERT INTO schedule VALUES(2, 2, 7, 7, 3, 2);
INSERT INTO schedule VALUES(2, 3, 6, 4, 2, 5);
INSERT INTO schedule VALUES(2, 3, 6, 8, 3, 10);
INSERT INTO schedule VALUES(2, 4, 6, 5, 2, 5);
INSERT INTO schedule VALUES(2, 4, 8, 8, 3, 10);
INSERT INTO schedule VALUES(3, 1, 5, 6, 1, 1);
INSERT INTO schedule VALUES(3, 1, 5, 6, 2, 1);
INSERT INTO schedule VALUES(3, 1, 3, 2, 3, 9);
INSERT INTO schedule VALUES(3, 2, 3, 2, 1, 9);
INSERT INTO schedule VALUES(3, 2, 3, 2, 2, 9);
INSERT INTO schedule VALUES(3, 2, 3, 2, 3, 9);
INSERT INTO schedule VALUES(3, 3, 5, 2, 1, 12);
INSERT INTO schedule VALUES(3, 3, 5, 2, 2, 12);
INSERT INTO schedule VALUES(3, 3, 5, 2, 3, 12);
INSERT INTO schedule VALUES(3, 4, 2, 5, 1, 4);
INSERT INTO schedule VALUES(4, 1, 4, 3, 1, 8);
INSERT INTO schedule VALUES(4, 1, 4, 3, 2, 8);
INSERT INTO schedule VALUES(4, 1, 1, 2, 3, 6);
INSERT INTO schedule VALUES(4, 2, 1, 3, 1, 6);
INSERT INTO schedule VALUES(4, 2, 1, 3, 2, 6);
INSERT INTO schedule VALUES(4, 2, 5, 6, 3, 1);
INSERT INTO schedule VALUES(4, 3, 2, 2, 1, 4);
INSERT INTO schedule VALUES(4, 3, 2, 2, 2, 4);
INSERT INTO schedule VALUES(4, 3, 2, 2, 3, 4);
INSERT INTO schedule VALUES(4, 4, 2, 8, 3, 10);
INSERT INTO schedule VALUES(5, 1, 8, 2, 1, 10);
INSERT INTO schedule VALUES(5, 1, 8, 2, 2, 10);
INSERT INTO schedule VALUES(5, 1, 8, 2, 3, 10);
INSERT INTO schedule VALUES(5, 2, 7, 7, 1, 2);
INSERT INTO schedule VALUES(5, 2, 7, 7, 2, 2);
INSERT INTO schedule VALUES(5, 2, 7, 7, 3, 2);
INSERT INTO schedule VALUES(5, 3, 5, 2, 1, 12);
INSERT INTO schedule VALUES(5, 3, 5, 2, 2, 12);
INSERT INTO schedule VALUES(5, 3, 5, 2, 3, 12);
INSERT INTO schedule VALUES(5, 3, 5, 2, 3, 1);
INSERT INTO schedule VALUES(5, 4, 2, 4, 2, 10);
INSERT INTO schedule VALUES(6, 1, 8, 4, 1, 11);
INSERT INTO schedule VALUES(6, 1, 8, 4, 2, 11);
INSERT INTO schedule VALUES(6, 2, 8, 4, 1, 11);
INSERT INTO schedule VALUES(6, 2, 8, 4, 2, 11);


INSERT INTO schedule VALUES(1, 1, 1, 3, 2, 1, 9);
INSERT INTO schedule VALUES(2, 1, 1, 3, 2, 2, 9);
INSERT INTO schedule VALUES(3, 1, 1, 4, 3, 3, 8);
INSERT INTO schedule VALUES(4, 1, 2, 1, 1, 1, 9);
INSERT INTO schedule VALUES(5, 1, 2, 1, 1, 2, 9);
INSERT INTO schedule VALUES(6, 1, 2, 1, 1, 3, 9);
INSERT INTO schedule VALUES(7, 1, 3, 6, 5, 1, 3);
INSERT INTO schedule VALUES(8, 1, 3, 8, 8, 3, 10);
INSERT INTO schedule VALUES(9, 1, 4, 6, 4, 1, 3);
INSERT INTO schedule VALUES(10,1, 4, 6, 8, 3, 10);
INSERT INTO schedule VALUES(11,1, 5, 6, 8, 3, 10);
INSERT INTO schedule VALUES(12,2, 1, 5, 6, 1, 1);
INSERT INTO schedule VALUES(13,2, 1, 5, 6, 2, 1);
INSERT INTO schedule VALUES(14,2, 2, 7, 7, 1, 2);
INSERT INTO schedule VALUES(15,2, 2, 7, 7, 2, 2);
INSERT INTO schedule VALUES(16,2, 2, 7, 7, 3, 2);
INSERT INTO schedule VALUES(17,2, 3, 6, 4, 2, 5);
INSERT INTO schedule VALUES(18,2, 3, 6, 8, 3, 10);
INSERT INTO schedule VALUES(19,2, 4, 6, 5, 2, 5);
INSERT INTO schedule VALUES(20,2, 4, 8, 8, 3, 10);
INSERT INTO schedule VALUES(21,3, 1, 5, 6, 1, 1);
INSERT INTO schedule VALUES(22,3, 1, 5, 6, 2, 1);
INSERT INTO schedule VALUES(23,3, 1, 3, 2, 3, 9);
INSERT INTO schedule VALUES(24,3, 2, 3, 2, 1, 9);
INSERT INTO schedule VALUES(25,3, 2, 3, 2, 2, 9);
INSERT INTO schedule VALUES(26,3, 2, 3, 2, 3, 9);
INSERT INTO schedule VALUES(27,3, 3, 5, 2, 1, 12);
INSERT INTO schedule VALUES(28,3, 3, 5, 2, 2, 12);
INSERT INTO schedule VALUES(29,3, 3, 5, 2, 3, 12);
INSERT INTO schedule VALUES(30,3, 4, 2, 5, 1, 4);
INSERT INTO schedule VALUES(31,4, 1, 4, 3, 1, 8);
INSERT INTO schedule VALUES(32,4, 1, 4, 3, 2, 8);
INSERT INTO schedule VALUES(33,4, 1, 1, 2, 3, 6);
INSERT INTO schedule VALUES(34,4, 2, 1, 3, 1, 6);
INSERT INTO schedule VALUES(35,4, 2, 1, 3, 2, 6);
INSERT INTO schedule VALUES(36,4, 2, 5, 6, 3, 1);
INSERT INTO schedule VALUES(37,4, 3, 2, 2, 1, 4);
INSERT INTO schedule VALUES(38,4, 3, 2, 2, 2, 4);
INSERT INTO schedule VALUES(39,4, 3, 2, 2, 3, 4);
INSERT INTO schedule VALUES(40,4, 4, 2, 8, 3, 10);
INSERT INTO schedule VALUES(41,5, 1, 8, 2, 1, 10);
INSERT INTO schedule VALUES(42,5, 1, 8, 2, 2, 10);
INSERT INTO schedule VALUES(43,5, 1, 8, 2, 3, 10);
INSERT INTO schedule VALUES(44,5, 2, 7, 7, 1, 2);
INSERT INTO schedule VALUES(45,5, 2, 7, 7, 2, 2);
INSERT INTO schedule VALUES(46,5, 2, 7, 7, 3, 2);
INSERT INTO schedule VALUES(47,5, 3, 5, 2, 1, 12);
INSERT INTO schedule VALUES(48,5, 3, 5, 2, 2, 12);
INSERT INTO schedule VALUES(49,5, 3, 5, 2, 3, 12);
INSERT INTO schedule VALUES(50,5, 3, 5, 2, 3, 1);
INSERT INTO schedule VALUES(51,5, 4, 2, 4, 2, 10);
INSERT INTO schedule VALUES(52,6, 1, 8, 4, 1, 11);
INSERT INTO schedule VALUES(53,6, 1, 8, 4, 2, 11);
INSERT INTO schedule VALUES(54,6, 2, 8, 4, 1, 11);
INSERT INTO schedule VALUES(55,6, 2, 8, 4, 2, 11);