unit uTables;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb, DB, FileUtil, Forms, Controls, Graphics, Dialogs,
  DBGrids, ExtCtrls, Buttons, Menus, IniPropStorage, uFieldNames, uSelectPanel,
  uCard, Math, uDataModule;

type

  { TFTable }

  TFTable = class(TForm)
    ImageList: TImageList;
    IniPropStorage: TIniPropStorage;
    MI_Def_Settings: TMenuItem;
    MI_Settings: TMenuItem;
    Table_Transaction: TSQLTransaction;
    Table_DS: TDatasource;
    DBGrid: TDBGrid;
    MainMenu: TMainMenu;
    AddRecord: TMenuItem;
    Panel: TPanel;
    SB_AddPanel: TSpeedButton;
    SB_DelPanel: TSpeedButton;
    SB_Select: TSpeedButton;
    Table_Query: TSQLQuery;
    procedure AddRecordClick(Sender: TObject);
    constructor Create(Table: TTableDesc); overload;
    procedure DBGridDblClick(Sender: TObject);
    procedure DBGridTitleClick(Column: TColumn);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure IniPropStorageRestoreProperties(Sender: TObject);
    procedure IniPropStorageSaveProperties(Sender: TObject);
    procedure MI_Def_SettingsClick(Sender: TObject);
    procedure SB_AddPanelClick(Sender: TObject);
    procedure SB_DelPanelClick(Sender: TObject);
    procedure SB_SelectClick(Sender: TObject);
    procedure GenerateRequest;
  private
    ColumnName : string;
    CurColumn: TColumn;
    OrderIndex: integer;
    CurTable: TTableDesc;
  public
    SelectList: array of TSelectPanel;
  end;

var
  FTable: TFTable;

implementation

{$R *.lfm}

const
  PanelHeight = 50;
  MinWidth = 500;
  DefWidth = 50;

constructor TFTable.Create(Table: TTableDesc);
var
  i: integer;
  CurCol: TColumn;
begin
  inherited Create(nil);
  IniPropStorage.IniSection:= Table.Name;
  OrderIndex := 0;
  DBGrid.Width := 0;
  CurTable := Table;
  Caption := CurTable.Caption;
  for i := 0 to High(CurTable.Fields) do
  begin
    CurCol := DBGrid.Columns.Add;
    CurCol.FieldName := CurTable.Fields[i].Name;
    CurCol.Width := CurTable.Fields[i].Width;
    DBGrid.Width := DBGrid.Width + CurCol.Width;
    CurCol.Title.Caption := CurTable.Fields[i].Caption;
  end;
  CurColumn := DBGrid.Columns[0];
  DBGrid.Width := max(DBGrid.Width + defWidth, MinWidth);
  Width := DBGrid.Width;
  GenerateRequest;
  Show;
end;

procedure TFTable.AddRecordClick(Sender: TObject);
begin
  Application.InsertComponent(TCardForm.Create(Table_Query.FieldByName('ID').AsInteger,
    CurTable, False));
end;

procedure TFTable.DBGridDblClick(Sender: TObject);
var
  i: integer;
begin
  for i := 0 to Application.ComponentCount - 1 do
    if (Application.Components[i] is TCardForm) and
      ((Application.Components[i] as TCardForm).CardID =
      Table_Query.FieldByName('ID').AsString) then
    begin
      (Application.Components[i] as TCardForm).Show;
      exit;
    end;
  Application.InsertComponent(TCardForm.Create(Table_Query.FieldByName('ID').AsInteger,
    CurTable, True));
end;

procedure TFTable.DBGridTitleClick(Column: TColumn);
begin
  if (ColumnName <> Column.FieldName) then
  begin
    OrderIndex := 0;
    CurColumn.Title.ImageIndex := 0;
    ColumnName := Column.FieldName;
  end;
  OrderIndex := (OrderIndex + 1) mod 3;
  CurColumn := Column;
  GenerateRequest;
  CurColumn.Title.ImageIndex := OrderIndex;
end;

procedure TFTable.FormActivate(Sender: TObject);
var
  CurRecNo: integer;
begin
  CurRecNo := Table_Query.RecNo;
  Table_Query.Close;
  Table_Transaction.Commit;
  Table_Query.Open;
  Table_Query.RecNo:= min(CurRecNo, Table_Query.RecordCount);
end;

procedure TFTable.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  CloseAction := caFree;
end;

procedure TFTable.IniPropStorageRestoreProperties(Sender: TObject);
var
  Len_SelectList, i: integer;
begin
  With IniPropStorage do
  begin
    IniSection:= CurTable.Name;
    ColumnName := ReadString('ColumnName', 'ID');
    Height := ReadInteger('Height', 400);
    Left := ReadInteger ('Left', 150);
    Top := ReadInteger('Top', 100);
    Width := ReadInteger('Width', 520);
    OrderIndex := ReadInteger ('OrderIndex', 0);
    Len_SelectList := ReadInteger ('Len_SelectList', 0);
    if Len_SelectList <> 0 then
    begin
      for i := 0 to Len_SelectList - 1 do begin
        SB_AddPanelClick(nil);
        With SelectList[i] do begin
          CB_Column.ItemIndex := ReadInteger(Format('SL_%d_CB_Col', [i]), 0);
          CB_LogicOper.ItemIndex := ReadInteger(Format('SL_%d_CB_LOp', [i]), 0);
          CB_Operations.ItemIndex := ReadInteger(Format('SL_%d_CB_Ope', [i]), 0);
          SearchString.Text := ReadString (Format('SL_%d_Search', [i]), '');
        end;
      end;
    end;
  end;
  SB_SelectClick(nil);
end;

procedure TFTable.IniPropStorageSaveProperties(Sender: TObject);
var
  i: integer;
begin
  With IniPropStorage do
  begin
    IniSection:= CurTable.Name;
    WriteString('ColumnName', ColumnName);
    WriteInteger('Height', Height);
    WriteInteger('Left', Left);
    WriteInteger('Height', Height);
    WriteInteger('Top', Top);
    WriteInteger('Width', Width);
    WriteInteger('OrderIndex', OrderIndex);
    WriteInteger('Len_SelectList', Length(SelectList));
    For i:= 0 to High(SelectList) do
    begin
      WriteInteger(Format('SL_%d_CB_Col', [i]), SelectList[i].CB_Column.ItemIndex);
      WriteInteger(Format('SL_%d_CB_LOp', [i]), SelectList[i].CB_LogicOper.ItemIndex);
      WriteInteger(Format('SL_%d_CB_Ope', [i]), SelectList[i].CB_Operations.ItemIndex);
      WriteString(Format('SL_%d_Search', [i]), SelectList[i].SearchString.Text);
    end;
  end;
end;

procedure TFTable.MI_Def_SettingsClick(Sender: TObject);
var
  Len_Sel_List, i: integer;
begin
  IniPropStorage.IniFileName:= 'Admin_DataBase_Standart.ini';
  IniPropStorageRestoreProperties(nil);
  Len_Sel_List := Length(SelectList);
  for i := 1 to Len_Sel_List do
    SB_DelPanelClick(nil);
  IniPropStorage.IniFileName:= '';
end;

procedure TFTable.SB_AddPanelClick(Sender: TObject);
begin
  if (length(SelectList) = 5) then
    exit;
  SB_DelPanel.Visible := True;
  SB_Select.Visible := True;
  SetLength(SelectList, length(SelectList) + 1);
  SelectList[High(SelectList)] := TSelectPanel.Create(CurTable);
  with SelectList[High(SelectList)].P_ConditSelect do
  begin
    Parent := Panel;
    BevelOuter := bvLowered;
    SetBounds(0, Panel.Height, Panel.Width, PanelHeight);
  end;
  if (length(SelectList) > 1) then
    SelectList[High(SelectList)].CB_LogicOper.Visible := True;
  Panel.Height := Panel.Height + PanelHeight;
end;

procedure TFTable.SB_DelPanelClick(Sender: TObject);
begin
  if (length(SelectList) = 1) then
  begin
    SB_DelPanel.Visible := False;
    SB_Select.Visible := False;
  end;
  SelectList[High(SelectList)].P_ConditSelect.Destroy;
  SetLength(SelectList, length(SelectList) - 1);
  Panel.Height := Panel.Height - PanelHeight;
  GenerateRequest;
end;

procedure TFTable.SB_SelectClick(Sender: TObject);
begin
  GenerateRequest;
end;

procedure TFTable.GenerateRequest;
const
  LogicOperat: array[0..1] of string = (' AND ', ' OR ');
  OrderBy: array[1..2] of string = (' ASC ', ' DESC ');
var
  Query: string;
  CurFieldSelect: string;
  CurFieldIndex: integer;
  i: integer;
begin
  Query := CurTable.SQLText;
  Query += 'WHERE 1 = 1';
  for i := Low(SelectList) to High(SelectList) do
  begin
    CurFieldIndex := CurTable.Fields[SelectList[i].CB_Column.ItemIndex].TableIndex;
    if CurFieldIndex = -1 then
      CurFieldSelect := CurTable.Name + '.' +
        CurTable.Fields[SelectList[i].CB_Column.ItemIndex].Name
    else
      CurFieldSelect := Tables[CurFieldIndex].Name + '.' +
        Tables[CurFieldIndex].Fields[1].Name;
    Query += LogicOperat[SelectList[i].CB_LogicOper.ItemIndex];
    case SelectList[i].CB_Operations.ItemIndex of
      0..5: Query += Format(' %s %s :par%d ', [CurFieldSelect,
          OperationsSQL[SelectList[i].CB_Operations.ItemIndex], i]);
      6: Query += Format(' position (:par%d in %s) <> 0 ', [i, CurFieldSelect]);
      7: Query += Format(' position (:par%d in %s) = 1 ', [i, CurFieldSelect]);

    end;
  end;
  if OrderIndex > 0 then
    Query += Format(' ORDER BY %s.%s%s',
      [CurTable.Name, ColumnName, OrderBy[OrderIndex]]);
  with Table_Query do
  begin
    Active := False;
    SQL.Clear;
    try
      SQL.Add(Query);
      //ShowMessage(SQL.Text);
      for i := Low(SelectList) to High(SelectList) do
        Params[i].AsString := SelectList[i].SearchString.Text;
      Active := True;
    except
      ShowMessage('Неправильный запрос');
      Active := False;
      SQL.Clear;
      SQL.Add(CurTable.SQLText);
      Active := True;
    end;
  end;
end;

end.

