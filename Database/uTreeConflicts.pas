unit uTreeConflicts;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb, FileUtil, Forms, Controls, Graphics, Dialogs,
  ComCtrls, uDataModule, uConflicts, uCard, uFieldNames;

type

  { TFConflicts }

  TFConflicts = class(TForm)
    TreeConflicts: TTreeView;
    procedure FormActivate(Sender: TObject);
    procedure TreeConflictsDblClick(Sender: TObject);
  private
    ConflictController: TConflictController;
  public
    constructor Create(AConflictController: TConflictController); reintroduce;
    procedure UpdateTree;
    procedure GoByIndex(ID, NumConf: integer);
  end; 

var
  FConflicts: TFConflicts;

implementation

{$R *.lfm}

{ TFConflicts }

procedure TFConflicts.TreeConflictsDblClick(Sender: TObject);
var
   Node: TTreeNode;
   id, i: integer;
begin
  Node := TreeConflicts.Selected;
  if Node.Data = nil then exit;
  ID := Integer(Node.Data);
  for i := 0 to Application.ComponentCount - 1 do
    if (Application.Components[i] is TCardForm) and
      ((Application.Components[i] as TCardForm).CardID = IntToStr(ID)) then
    begin
      (Application.Components[i] as TCardForm).Show;
      exit;
    end;
  Application.InsertComponent(TCardForm.Create(ID, Tables[7], True));
end;

procedure TFConflicts.FormActivate(Sender: TObject);
begin
  UpdateTree;
end;

constructor TFConflicts.Create(AConflictController: TConflictController);
begin
  inherited Create(nil);
  ConflictController := AConflictController;
  UpdateTree;
  Show;
end;

procedure TFConflicts.UpdateTree;
var
  Root, Node, ChildNode: TTreeNode;
  i, j, k, m, ID: integer;
  Conflict: TConflict;
  s: string;
begin
  TreeConflicts.Items.Clear;
  ConflictController.UpdateConflicts;
  Root := TTreeNode.Create(TreeConflicts.Items);
  for i := 0 to HIGH_CONFLICT do
  begin
    Conflict := ConflictController.Conflicts[i];
    Node := TreeConflicts.Items.Add(Root, Conflict.Name);
    for j := 0 to High(Conflict.ConflictValues) do
    begin
      ChildNode := TreeConflicts.Items.AddChild(Node, Conflict.ConflictValues[j].GeneralField);
      for k := 0 to High(Conflict.ConflictValues[j].ConflictRec) do
      begin
        s := '';
        for m := 0 to 4 do
          s += Conflict.ConflictValues[j].ConflictRec[k].Values[m] + ', ';
        ID := StrToInt(Conflict.ConflictValues[j].ConflictRec[k].ID);
        TreeConflicts.Items.AddChildObject(Childnode, s, Pointer(ID));
      end;
    end;
  end;
  Root.Expanded := True;
end;

procedure TFConflicts.GoByIndex(ID, NumConf: integer);
var
   Node, ChildNode: TTreeNode;
   count, i, j: integer;
begin
  Node := TreeConflicts.Items[0];
  for i := 0 to NumConf - 1 do
    Node := Node.GetNextSibling;
  Node.Expanded := True;
  Node := Node.GetFirstChild;
  count := Node.Parent.Count - 1;
  for i := 0 to count do
  begin
    ChildNode := Node.GetNext;
    for j := 0 to Node.Count - 1 do
    begin
      if Integer(ChildNode.Data) = ID then
      begin
        ChildNode.Selected := True;
        Exit;
      end;
      ChildNode := ChildNode.GetNextSibling;
    end;
    Node := Node.GetNextSibling;
  end;
end;

end.
