unit uMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Menus,
  uFieldNames, uTables, IBConnection, sqldb, uSchedule, uTreeConflicts,
  uConflicts;

type

  { TAdminDB }

  TAdminDB = class(TForm)
    MI_Conflicts: TMenuItem;
    MainMenu: TMainMenu;
    MI_Schedule: TMenuItem;
    T_SCHEDULE: TMenuItem;
    T_TIME_LESSONS: TMenuItem;
    T_WEEK_DAY: TMenuItem;
    T_SUBJECTS: TMenuItem;
    T_STUDENTS: TMenuItem;
    T_TEACHERS: TMenuItem;
    T_CLASSROOMS: TMenuItem;
    T_GROUPS: TMenuItem;
    MI_Exit: TMenuItem;
    MI_Tables: TMenuItem;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure MI_ConflictsClick(Sender: TObject);
    procedure MI_ScheduleClick(Sender: TObject);
    procedure MI_ExitClick(Sender: TObject);
    procedure MI_TableClick(Sender: TObject);
  private
    ConflictController: TConflictController;
  public
    { public declarations }
  end;

var
  AdminDB: TAdminDB;

implementation

{$R *.lfm}

{ TAdminDB }

procedure TAdminDB.MI_TableClick(Sender: TObject);
var
  Table: TTableDesc;
begin
  Table := Tables[(Sender as TMenuItem).Tag];
  Application.InsertComponent(TFTable.Create(Table));
end;

procedure TAdminDB.MI_ExitClick(Sender: TObject);
begin
  AdminDB.Close;
end;

procedure TAdminDB.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  CloseAction := caFree;
end;

procedure TAdminDB.FormCreate(Sender: TObject);
begin
  ConflictController := TConflictController.Create;
end;

procedure TAdminDB.MI_ConflictsClick(Sender: TObject);
begin
  Application.InsertComponent(TFConflicts.Create(ConflictController));
end;

procedure TAdminDB.MI_ScheduleClick(Sender: TObject);
begin
  Application.InsertComponent(TScheduleForm.Create(ConflictController));
end;

end.

