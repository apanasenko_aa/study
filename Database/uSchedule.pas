unit uSchedule;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Grids,
  ExtCtrls, StdCtrls, Buttons, Menus, sqldb, uFieldNames, Math, uDataModule,
  uCard, uTables, Comobj, uConflicts, uTreeConflicts, variants;

type

  TTitle = record
    ID: integer;
    Caption: string;
  end;

  TDGridCell = record
    Fields: array [0..5] of string;
    ID: Integer;
    CellHeight: integer;
    CellWidth : integer;
  end;

  TTitles = array of TTitle;
  TDGridCells = array of array of array of TDGridCell;

  { TScheduleForm }

  TScheduleForm = class(TForm)
    CB_Horizontal: TComboBox;
    CB_Vertical: TComboBox;
    CG_Fields: TCheckGroup;
    CB_Titles: TCheckBox;
    CB_FilterField: TComboBox;
    CB_Operation: TComboBox;
    DrawGrid: TDrawGrid;
    E_FindText: TEdit;
    GB_Horizontal: TGroupBox;
    GB_Vertical: TGroupBox;
    GB_Filter: TGroupBox;
    ButtonImages: TImageList;
    L_Export: TLabel;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    Panel: TPanel;
    PM_Conflicts: TPopupMenu;
    SaveToExcel: TSaveDialog;
    SaveToHTML: TSaveDialog;
    SB_ChangeRec: TSpeedButton;
    SB_ShowAll: TSpeedButton;
    SB_DelRec: TSpeedButton;
    SB_Select: TSpeedButton;
    Schedule_Query: TSQLQuery;
    SB_AddRec: TSpeedButton;
    Schedule_Transaction: TSQLTransaction;
    SB_ExpHTML: TSpeedButton;
    SB_ExpExcel: TSpeedButton;
    SB_Conflict: TSpeedButton;
    procedure CB_HorizontalChange(Sender: TObject);
    procedure CB_TitlesChange(Sender: TObject);
    procedure CB_VerticalChange(Sender: TObject);
    procedure CG_FieldsItemClick(Sender: TObject; Index: integer);
    constructor Create(AConflictController: TConflictController); overload;
    procedure DrawGridDblClick(Sender: TObject);
    procedure DrawGridDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure DrawGridDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure DrawGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure DrawGridHeaderSizing(sender: TObject; const IsColumn: boolean;
      const aIndex, aSize: Integer);
    procedure DrawGridMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DrawGridSelection(Sender: TObject; aCol, aRow: Integer);
    procedure FormActivate(Sender: TObject);
    procedure SB_AddRecClick(Sender: TObject);
    procedure SB_ChangeRecClick(Sender: TObject);
    procedure SB_ConflictClick(Sender: TObject);
    procedure SB_DelRecClick(Sender: TObject);
    procedure SB_ExpExcelClick(Sender: TObject);
    procedure SB_ExpHTMLClick(Sender: TObject);
    procedure SB_SelectClick(Sender: TObject);
    procedure SB_ShowAllClick(Sender: TObject);
    procedure PopupMenuClick(Sender: TObject);
    procedure ExpExcel;
    procedure ExpHTML;
    procedure ExpOO;
  private
    DropID: Integer;
    Select: Boolean;
    SButtons: array of TSpeedButton;
    SButtonsConf: array of TSpeedButton;
    DGrid_Cols : TTitles;               //Vertical
    DGrid_Rows : TTitles;               //Horizontal
    CurCanvas: TCanvas;
    GridValues: TDGridCells;
    ConflictController: TConflictController;
    procedure DrawTitlesFields (Rect: TRect; outText: string);
    procedure GetDGridCells;
    procedure DrawCells (Rect: TRect; Row, Col: integer);
    function GetTitles (Table_Name, Field_Name: string) : TTitles;
    function GetSelectSQL : string;
    function isConflictRecord(ID: Integer) : Boolean;
    procedure DrawTriangle(BRPoint: TPoint);
    procedure SetDefault;
    procedure DrawButtons (Cell: TRect);
  public
    { public declarations }
  end; 



var
  ScheduleForm: TScheduleForm;

implementation

const
  Standard_Indentation = 5;
  Legs_triangle = 20;
  Text_Height = 15;
  SCHEDULE = 7;

{$R *.lfm}

{ TScheduleForm }

//report
//lazReport
//fastCube

constructor TScheduleForm.Create(AConflictController: TConflictController);
var
  i: integer;
  Field: string;
begin
  inherited Create(nil);
  ConflictController := AConflictController;
  CurCanvas := DrawGrid.Canvas;
  Select := False;
  CB_FilterField.Items.Add('Без фильтра');
  for i := 0 to 5 do
  begin
    Field:= Tables[i].Fields[1].Caption;
    CB_Horizontal.Items.Add(Field);
    CB_Vertical.Items.Add(Field);
    CG_Fields.Items.Add(Field);
    CG_Fields.Checked[i] := True;
    CB_FilterField.Items.Add(Field);
    CB_Operation.Items.Add(Operations[i]);
  end;
  CB_FilterField.ItemIndex := 0;
  CB_Horizontal.ItemIndex := 3;
  CB_Vertical.ItemIndex := 1;
  CB_HorizontalChange(nil);
  CB_VerticalChange(nil);
  Select := True;
  GetDGridCells;
  DrawGrid.RowHeights[0] := Text_Height * 4;
  DrawGrid.ColWidths[0] := 120;
  SetLength(SButtons, 4);
  SButtons[0] := SB_AddRec;
  SButtons[1] := SB_ChangeRec;
  SButtons[2] := SB_DelRec;
  SButtons[3] := SB_ShowAll;
  for i:= 0 to 3 do
    SButtons[i].Parent := DrawGrid;
  SetLength(SButtonsConf, 1);
  SB_Conflict.Parent := DrawGrid;
  Show;
end;

function TScheduleForm.isConflictRecord(ID: Integer) : Boolean;
begin
  With Schedule_Query do
  begin
    Close;
    SQL.Text:= Format('SELECT ID FROM conflicts where Id = %d', [ID]);
    Open;
    Result := Fields[0].AsInteger = ID;
  end;
end;

procedure TScheduleForm.DrawGridDblClick(Sender: TObject);
var
  Col, Row: integer;
begin
  Col := DrawGrid.Col - 1;
  Row := DrawGrid.Row - 1;
  if Length(GridValues[Row][Col]) = 0 then exit;
  DrawGrid.CellRect(DrawGrid.Col, DrawGrid.Row);
  if (DrawGrid.RowHeights[DrawGrid.Row] < GridValues[Row][Col][0].CellHeight) or
    (DrawGrid.ColWidths[DrawGrid.Col] < GridValues[Row][Col][0].CellWidth) then
  begin
    DrawGrid.RowHeights[DrawGrid.Row] := GridValues[Row][Col][0].CellHeight;
    DrawGrid.ColWidths[DrawGrid.Col] := GridValues[Row][Col][0].CellWidth;
  end
  else
  begin
    DrawGrid.RowHeights[DrawGrid.Row] := DrawGrid.DefaultRowHeight;
    DrawGrid.ColWidths[DrawGrid.Col] := DrawGrid.DefaultColWidth;
  end;
  DrawButtons(DrawGrid.CellRect(DrawGrid.Col, DrawGrid.Row));
end;

procedure TScheduleForm.DrawGridDragDrop(Sender, Source: TObject; X, Y: Integer);

  procedure UpdateGrid (Col, Row: integer);
  var
    CurQwery: TSQLQuery;
  begin
    CurQwery := TSQLQuery.Create(self);
    with CurQwery do
    begin
      Database := D_Module.D_Mod_Connection;
      Transaction := D_Module.D_Mod_Transaction;
      SQL.Text := Format('UPDATE SCHEDULE_1 SET %s = %d, %s = %d WHERE ID = %d',
        [Tables[SCHEDULE].Fields[CB_Horizontal.ItemIndex + 1].Name, DGrid_Rows[Col].ID,
         Tables[SCHEDULE].Fields[CB_Vertical.ItemIndex + 1].Name, DGrid_Cols[Row].ID, DropID]);
      ExecSQL;
    end;
    D_Module.D_Mod_Transaction.Commit;
    CurQwery.Free;
  end;

var
  Row, Col, i: integer;
begin
  DrawGrid.MouseToCell(X, Y, Col, Row);
  if (Col = -1) or (Row = -1) then exit;
  for i := 0 to High(GridValues[Row - 1][Col - 1]) do
    if GridValues[Row - 1][Col - 1][i].ID = DropID then exit;
  UpdateGrid(Col - 1, Row - 1);
  Schedule_Query.Close;
  Schedule_Transaction.Commit;
  GetDGridCells;
  DrawButtons(DrawGrid.CellRect(DrawGrid.Col, DrawGrid.Row));
end;

procedure TScheduleForm.DrawGridDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
var
  Row, Col: integer;
begin
  DrawGrid.MouseToCell(X, Y, Col, Row);
  Accept := (Row <> 0) and (Col <> 0);
end;

procedure TScheduleForm.DrawGridDrawCell(Sender: TObject; aCol, aRow: Integer;
  aRect: TRect; aState: TGridDrawState);
begin
  if (ACol = 0) and (ARow = 0) then exit;
  if aRow = 0 then
    DrawTitlesFields(aRect, DGrid_Rows[aCol - 1].Caption)
  else if aCol = 0 then
    DrawTitlesFields(aRect, DGrid_Cols[aRow - 1].Caption)
  else if Length(GridValues[aRow - 1, aCol - 1]) > 0 then
    DrawCells(aRect, aRow - 1, aCol - 1);
end;

procedure TScheduleForm.DrawGridHeaderSizing(sender: TObject;
  const IsColumn: boolean; const aIndex, aSize: Integer);
begin
  DrawButtons(DrawGrid.CellRect(DrawGrid.Col, DrawGrid.Row));
end;

procedure TScheduleForm.DrawGridMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  NumRec, Col, Row: integer;
  HeightOneRec: Integer;
  CurCell: TRect;
begin
  DrawGrid.MouseToCell(X, Y, Col, Row);
  if (Col <= 0) or (Row <= 0) or (Length(GridValues[Row - 1][Col - 1]) = 0) then exit;
  HeightOneRec := GridValues[Row - 1][Col - 1][0].CellHeight div Length(GridValues[Row - 1][Col - 1]);
  CurCell := DrawGrid.CellRect(Col, Row);
  NumRec := (Y - CurCell.Top) div HeightOneRec;
  if (NumRec) >= Length(GridValues[Row - 1][Col - 1]) then exit;
  DropID := GridValues[Row - 1][Col - 1][NumRec].ID;
  //show ID;
  DrawGrid.BeginDrag(True);
end;

procedure TScheduleForm.DrawGridSelection(Sender: TObject; aCol, aRow: Integer);
begin
  DrawButtons(DrawGrid.CellRect(DrawGrid.Col, DrawGrid.Row));
end;

procedure TScheduleForm.FormActivate(Sender: TObject);
begin
  Schedule_Query.Close;
  Schedule_Transaction.Commit;
  GetDGridCells;
  DrawButtons(DrawGrid.CellRect(DrawGrid.Col, DrawGrid.Row));
end;

procedure TScheduleForm.SB_AddRecClick(Sender: TObject);
var
  CurCard: TCardForm;
begin
  CurCard := TCardForm.Create(0, Tables[SCHEDULE], False);
  CurCard.Card_Query.FieldByName(Tables[SCHEDULE].Fields[CB_Horizontal.ItemIndex + 1].Name).AsInteger:= DGrid_Rows[DrawGrid.Col - 1].ID;
  CurCard.Card_Query.FieldByName(Tables[SCHEDULE].Fields[CB_Vertical.ItemIndex + 1].Name).AsInteger:= DGrid_Cols[DrawGrid.Row  - 1].ID;
  Application.InsertComponent(CurCard);
end;

procedure TScheduleForm.SB_ChangeRecClick(Sender: TObject);
var
  Col, Row: Integer;
begin
  Col := DrawGrid.Col - 1;
  Row := DrawGrid.Row - 1;
  Application.InsertComponent(TCardForm.Create(
    GridValues[Row][Col][(Sender as TSpeedButton).Tag].ID, Tables[SCHEDULE], True));
end;

procedure TScheduleForm.SB_ConflictClick(Sender: TObject);
var
  i: Integer;
  cMenu: TMenuItem;
begin
  With Schedule_Query do
  begin
    Close;
    SQL.Text:= Format('SELECT * FROM conflicts where Id = %d',
        [GridValues[DrawGrid.Row - 1][DrawGrid.Col - 1][(Sender as TSpeedButton).Tag].ID]);
    Open;
  end;
  PM_Conflicts.Items.Clear;
  for i := 0 to HIGH_CONFLICT do
    if Schedule_Query.Fields[i + 1].AsInteger = 1 then
      with PM_Conflicts do
      begin
        cMenu := TMenuItem.Create(self);
        cMenu.Caption := CONF_NAME[i];
        cMenu.Tag := i;
        DropID := GridValues[DrawGrid.Row - 1][DrawGrid.Col - 1][(Sender as TSpeedButton).Tag].ID;
        cMenu.OnClick := @PopupMenuClick;
        Items.Add(cMenu);
      end;
  PM_Conflicts.PopUp(Mouse.CursorPos.X, Mouse.CursorPos.Y);
end;

procedure TScheduleForm.SB_DelRecClick(Sender: TObject);
var
  Col, Row: Integer;
  CurQwery: TSQLQuery;
begin
  if MessageDlg('Удалить запись?', mtWarning, mbYesNo, 0) = mrNo then exit;
  Col := DrawGrid.Col - 1;
  Row := DrawGrid.Row - 1;
  CurQwery := TSQLQuery.Create(self);
  with CurQwery do
  begin
    Transaction := D_Module.D_Mod_Transaction;
    SQL.Text := (Format('DELETE FROM %s WHERE ID = %d',
      [Tables[SCHEDULE].Name, GridValues[Row][Col][(Sender as TSpeedButton).Tag].ID]));
    ExecSQL;
  end;
  D_Module.D_Mod_Transaction.Commit;
  Schedule_Transaction.Commit;
  CurQwery.Free;
  GetDGridCells;
  DrawButtons(DrawGrid.CellRect(DrawGrid.Col, DrawGrid.Row));
end;

procedure TScheduleForm.SB_ExpExcelClick(Sender: TObject);
begin
  ExpExcel;
end;

procedure TScheduleForm.SB_ExpHTMLClick(Sender: TObject);
begin
  ExpHTML;
end;

procedure TScheduleForm.DrawTitlesFields(Rect: TRect; outText: string);
var
  outSL: TStringList;
  i, TextTop: integer;
begin
  outSL := TStringList.Create;
  CurCanvas.Brush.Color := DrawGrid.FixedColor;
  CurCanvas.FillRect(Rect);
  outSL.Delimiter:= ' ';
  outSL.DelimitedText:= outText;
  TextTop:= 5;
  for i:= 0 to outSL.Count - 1 do
  begin
    CurCanvas.TextOut(Rect.Left + Standard_Indentation,
                    Rect.Top + TextTop, outSL[i]);
    TextTop += Text_Height;
  end;
end;

procedure TScheduleForm.GetDGridCells;
var
  i, j, Col, Row: integer;
begin
  ConflictController.UpdateConflicts;
  SetDefault;
  DGrid_Cols := GetTitles(Tables[CB_Vertical.ItemIndex].Name,
                          Tables[CB_Vertical.ItemIndex].Fields[1].Name);
  DrawGrid.RowCount:= Length(DGrid_Cols) + 1;
  DGrid_Rows := GetTitles(Tables[CB_Horizontal.ItemIndex].Name,
                          Tables[CB_Horizontal.ItemIndex].Fields[1].Name);
  DrawGrid.ColCount:= Length(DGrid_Rows) + 1;
  with Schedule_Query do
  begin
    Active := False;
    SQL.Clear;
    try
      SQL.Add(GetSelectSQL);
      if CB_FilterField.ItemIndex > 0 then
        Params[0].AsString := E_FindText.Text;
      Active := True;
    except
      ShowMessage(SQL.Text);
      Active := False;
      SQL.Clear;
    end;
    SetLength(GridValues, Length(DGrid_Cols), Length(DGrid_Rows));
    i := 0;
    While not EOF do
    begin
      Row := i div Length(DGrid_Rows);
      Col := i mod Length(DGrid_Rows);
      if (DGrid_Rows[Col].ID = Fields[1].AsInteger) and
         (DGrid_Cols[Row].ID = Fields[0].AsInteger) then
      begin
        SetLength(GridValues[Row,Col], Length(GridValues[Row,Col]) + 1);
        for j:= 0 to 5 do
          GridValues[Row,Col][High(GridValues[Row,Col])].Fields[j] := Fields[j + 3].AsString;
        GridValues[Row,Col][High(GridValues[Row,Col])].ID := Fields[2].AsInteger;
        Next;
      end
      else
        inc(i)
    end;
  end;
  DrawGrid.Repaint;
  //ShowMessage(IntToStr(Length(DGrid_Rows)));
  //ShowMessage(IntToStr(Length(DGrid_Cols)));
end;

procedure TScheduleForm.DrawCells(Rect: TRect; Row, Col: integer);
var
  i, j, TextTop, MaxWidth: integer;
  Values: string;
begin
  MaxWidth := 0;
  CurCanvas.Brush.Color := $EEEEEE;
  CurCanvas.FillRect(Rect);
  TextTop :=0;
  for i := 0 to High(GridValues[Row, Col]) do
  begin
    If isConflictRecord(GridValues[Row][Col][i].ID) then
      CurCanvas.Font.Color:= clRed
    else
      CurCanvas.Font.Color:= clBlack;
    for j := 0 to 5 do
    begin
      Values := '';
      if not CG_Fields.Checked[j] then Continue;
      if CB_Titles.Checked then
        Values := Tables[j].Fields[1].Caption + ':  ';
      Values += GridValues[Row][Col][i].Fields[j];
      CurCanvas.TextOut(Rect.Left + Standard_Indentation, Rect.Top + TextTop, Values);
      TextTop += CurCanvas.TextHeight(Values);
      MaxWidth := Max(MaxWidth, CurCanvas.TextWidth(Values));
    end;
    TextTop += Standard_Indentation * 3;
  end;
  TextTop -= Standard_Indentation * 2;
  MaxWidth += Standard_Indentation * 2;
  MaxWidth := Max(MaxWidth, DrawGrid.DefaultColWidth );
  GridValues[Row][Col][0].CellHeight:= TextTop;
  GridValues[Row][Col][0].CellWidth:= MaxWidth;
  if (DrawGrid.RowHeights[Row + 1] < TextTop) or
     (DrawGrid.ColWidths[Col + 1] < MaxWidth)
  then
    DrawTriangle(Rect.BottomRight);
end;

procedure TScheduleForm.CB_HorizontalChange(Sender: TObject);
begin
  if Select then GetDGridCells;
end;

procedure TScheduleForm.CB_TitlesChange(Sender: TObject);
begin
  DrawGrid.Repaint;
end;

procedure TScheduleForm.CB_VerticalChange(Sender: TObject);
begin
  if Select then GetDGridCells;
end;

procedure TScheduleForm.CG_FieldsItemClick(Sender: TObject; Index: integer);
begin
  DrawGrid.Repaint;
  DrawButtons(DrawGrid.CellRect(DrawGrid.Col, DrawGrid.Row));
end;

function TScheduleForm.GetTitles (Table_Name, Field_Name: string) : TTitles;
var
  CurQwery: TSQLQuery;
begin
  CurQwery := TSQLQuery.Create(self);
  with CurQwery do
  begin
    Database := D_Module.D_Mod_Connection;
    Transaction := D_Module.D_Mod_Transaction;
    SQL.Text := Format('SELECT a.ID, a.%s FROM %s a ORDER BY a.ID', [Field_Name, Table_Name]);
    Open;
    while not EOF do
    begin
      SetLength(Result, Length(Result) + 1);
      Result[High(Result)].ID:= Fields[0].AsInteger;
      Result[High(Result)].Caption:= Fields[1].AsString;
      Next;
    end;
  end;
  CurQwery.Free;
end;

function TScheduleForm.GetSelectSQL: string;
const
  Cond_Fiels: array [1..6] of string = (' CLASSROOMS.AUDITORY ',
                                        ' TIME_LESSONS.TIME_LESSON ',
                                        ' GROUPS.NAME ',
                                        ' WEEK_DAY.NAME_DAY ',
                                        ' SUBJECTS.NAME ',
                                        ' TEACHERS.NAME ');
var
  Ord, Condition: string;
begin
  Ord := Format(' %s.ID, %s.ID', [Tables[CB_Vertical.ItemIndex].Name,
                                  Tables[CB_Horizontal.ItemIndex].Name]);
  if CB_FilterField.ItemIndex > 0 then
    Condition := Format(' WHERE %s %s :par1 ',[Cond_Fiels[CB_FilterField.ItemIndex],
                                               OperationsSQL[CB_Operation.ItemIndex]])
  else
    Condition := '';
  Result := StringReplace(Select_Sched, 'SELECT', 'SELECT' + Ord + ', ', []) +
        Format(' %s ORDER BY %s' , [Condition, Ord]);
end;

procedure TScheduleForm.DrawTriangle(BRPoint: TPoint);
begin
  CurCanvas.Brush.Color := clYellow;
  CurCanvas.Polygon([BRPoint, Point(BRPoint.X - Legs_triangle, BRPoint.Y),
                              Point(BRPoint.X, BRPoint.Y - Legs_triangle)]);
end;

procedure TScheduleForm.SetDefault;
var
  i: integer;
begin
  SetLength(GridValues, 0, 0, 0);
  for i:= 1 to Length(DGrid_Cols) - 1 do
    DrawGrid.RowHeights[i] := DrawGrid.DefaultRowHeight;
  for i:= 1 to Length(DGrid_Rows) - 1 do
    DrawGrid.ColWidths[i] := DrawGrid.DefaultColWidth;
  for i := 0 to 5 do
    CG_Fields.Checked[i] := True;
  CG_Fields.Checked[CB_Horizontal.ItemIndex] := False;
  CG_Fields.Checked[CB_Vertical.ItemIndex] := False;
end;

procedure TScheduleForm.DrawButtons(Cell: TRect);

  function CopyBotton(OldSB: TSpeedButton): TSpeedButton;
  begin
    Result := TSpeedButton.Create(Self);
    Result.BoundsRect := OldSB.BoundsRect;
    Result.Parent := OldSB.Parent;
    Result.OnClick:= OldSB.OnClick;
    Result.Glyph := OldSB.Glyph;
  end;

  procedure SetParamsButton(Button: TSpeedButton; ALeft, ATop, ATag: Integer);
  begin
    With Button do
    begin
      Top := ATop;
      Left := ALeft;
      Tag := ATag;
    end;
  end;
const
  BUTTON_HEIGHT = 17;
  TOP_INDENT = 2;
var
  i, j, n: Integer;
  Col, Row, CountRec, TopSB: Integer;
begin
  //Разделить создание и движение!!!
  for i := High(SButtons) downto 4 do
    SButtons[i].Free;
  SetLength(SButtons, 4);
  for i := 0 to Length(SButtonsConf) - 1 do
    SButtonsConf[i].Free;
  SetLength(SButtonsConf, 0);
  TopSB := 0;
  for i := 0 to 5 do
    if CG_Fields.Checked[i] then
      TopSB += Text_Height;
  Col := DrawGrid.Col - 1;
  Row := DrawGrid.Row - 1;
  CountRec := Length(GridValues[Row][Col]);
  n := (CountRec) * 4 ;
  for i:= 0 to 3 do
    SetParamsButton(SButtons[i], Cell.Right - BUTTON_HEIGHT * (i + 1), Cell.Top + TOP_INDENT, 0);
  If (CountRec > 0) and isConflictRecord(GridValues[Row][Col][0].ID) then
  begin
    SetLength(SButtonsConf, 1);
    SButtonsConf[0] := CopyBotton(SB_Conflict);
    SetParamsButton(SButtonsConf[0], Cell.Right - 30, SButtons[0].Top + BUTTON_HEIGHT, 0);
  end;
  Cell.Top := Cell.Top + TopSB + Text_Height;
  for i := 2 to CountRec do
  begin
    SetLength(SButtons, Length(SButtons) + 2);
    SButtons[High(SButtons)- 1] := CopyBotton(SB_ChangeRec);
    SButtons[High(SButtons)] := CopyBotton(SB_DelRec);
    for j := High(SButtons)- 1 to High(SButtons) do
      SetParamsButton(SButtons[j], Cell.Right - BUTTON_HEIGHT * (j mod 2 + 1), Cell.Top + TOP_INDENT, i - 1);
    If isConflictRecord(GridValues[Row][Col][i - 1].ID) then
    begin
      SetLength(SButtonsConf, Length(SButtonsConf) + 1);
      SButtonsConf[High(SButtonsConf)] := CopyBotton(SB_Conflict);
      SetParamsButton(SButtonsConf[High(SButtonsConf)], Cell.Right - 30, SButtons[High(SButtons)].Top + BUTTON_HEIGHT, i - 1);
    end;
    Cell.Top := Cell.Top + TopSB + Text_Height;
  end;
  for i:= 0 to High(SButtons) do
    SButtons[i].Visible:= (Cell.Bottom > SButtons[i].Top + SButtons[i].Height) and
       (Cell.Left < SButtons[i].Left) and (i <= n);
  for i:= 0 to High(SButtonsConf) do
    SButtonsConf[i].Visible:= (Cell.Bottom > SButtonsConf[i].Top + SButtonsConf[i].Height) and
       (Cell.Left < SButtonsConf[i].Left);

end;

procedure TScheduleForm.SB_SelectClick(Sender: TObject);
begin
  Schedule_Query.Close;
  Schedule_Transaction.Commit;
  Schedule_Query.Open;
  GetDGridCells;
end;

procedure TScheduleForm.SB_ShowAllClick(Sender: TObject);

  procedure AddPanel (ColIndex, OperIndex: integer; Search: String; Table: TFTable);
  begin
    with Table do
    begin
      SB_AddPanelClick(nil);
      with SelectList[High(SelectList)] do
      begin
        CB_Column.ItemIndex:= ColIndex;
        CB_Operations.ItemIndex:= OperIndex;
        SearchString.Text := Search;
      end;
    end;
  end;

const
  LIKE = 4;
var
  Table: TFTable;
begin
  Table := TFTable.Create(Tables[SCHEDULE]);
  with Table do
  begin
    MI_Def_SettingsClick(nil);
    AddPanel(CB_Vertical.ItemIndex + 1, LIKE, DGrid_Cols[DrawGrid.Row  - 1].Caption, Table);
    if CB_Vertical.ItemIndex <> CB_Horizontal.ItemIndex then
      AddPanel(CB_Horizontal.ItemIndex + 1, LIKE, DGrid_Rows[DrawGrid.Col  - 1].Caption, Table);
    if CB_FilterField.ItemIndex > 0 then
      AddPanel(CB_FilterField.ItemIndex + 1, CB_Operation.ItemIndex, E_FindText.Text, Table);
    SB_SelectClick(nil);
  end;
  Application.InsertComponent(Table);
end;

procedure TScheduleForm.PopupMenuClick(Sender: TObject);
var
  Conflicts: TFConflicts;
begin
  Conflicts := TFConflicts.Create(ConflictController);
  Application.InsertComponent(Conflicts);
  Conflicts.GoByIndex(DropID, (Sender as TMenuItem).Tag);
end;

{procedure TScheduleForm.ExpExcel;
var
  Excel: Variant;
  i : Integer;
begin
  Excel := CreateOleObject('Excel.Application');
  Excel.Workbooks.Add;
  Excel.Visible := False;
  for i := 0 to High(DGrid_Rows) do
    Excel.Cells(1,i + 2) := (DGrid_Rows[i].Caption);
  for i := 0 to High(DGrid_Cols) do
    Excel.Cells(i + 2, 1) := (DGrid_Cols[i].Caption);
  Excel.Visible := True;
end;    }

procedure TScheduleForm.ExpExcel;
var
  Excel, ArrayData, Workbook : Variant;
  s: String;
  i, j, k, l, ColWidth: integer;
  RowCount, ColCount : integer;
begin
  RowCount := High(DGrid_Cols) + 2;
  ColCount := High(DGrid_Rows) + 2;
  Excel := CreateOleObject('Excel.Application');
  Workbook := Excel.WorkBooks.Add;
  //ArrayData := VarArrayCreate([1, RowCount, 1, ColCount], varVariant);
  ArrayData := VarArrayCreate([1, 40, 1, 40], varVariant);
  for i := 0 to High(DGrid_Rows) do
    ArrayData[1,i + 2] := Utf8ToAnsi(DGrid_Rows[i].Caption);
  for i := 0 to High(DGrid_Cols) do
    ArrayData[i + 2, 1] := Utf8ToAnsi(DGrid_Cols[i].Caption);
  for i := 0 to High(DGrid_Cols) do
  begin
    //Excel.WorkSheets[1].Columns.Columns[col].ColumnWidth := ExcelColWidth + 4;
    //ColWidth := Excel.WorkSheets[1].Columns.Columns[i + 2].ColumnWidth;
    ColWidth:= Workbook.WorkSheets[1].Columns[i + 2].ColumnWidth;
    for j := 0 to High(DGrid_Rows) do
    begin
      s := '';
      for k := 0 to High(GridValues[i][j]) do
      begin
        for l := 0 to 5 do
        begin
          if not CG_Fields.Checked[l] then Continue;
          if CB_Titles.Checked then
            s += Tables[l].Fields[1].Caption + ':  ';
          s += GridValues[i][j][k].Fields[l] + #13#10;
         //ColWidth:= Max(ColWidth, Length(GridValues[i][j][k].Fields[l]));
        end;
      end;
      ArrayData[i + 2, j + 2]:= Utf8ToAnsi(s);
    end;
    //Excel.WorkSheets[1].Columns.Columns[i + 2].ColumnWidth:= ColWidth;
  end;
  //s := Format('%s%d', [Chr(Ord('A') + ColCount - 1) ,RowCount]);
  Excel.Range('A1','AN40') := ArrayData;

  Excel.Visible := True;
end;

procedure TScheduleForm.ExpHTML;
const
   START_HTML = '<html>'#13#10'<head>'#13#10'<title>Расписание</title>'#13#10'</head>'#13#10'<table>'#13#10'<tbody>'
    + #13#10'<style>table {border-collapse: collapse;} .war {color: red;} td, th {vertical-align: top; white-space: nowrap;  '
    + 'padding: 0;border:1px black solid;}.fill{background: #FD9D8A;}th{text-align: center; background: silver; padding: 7;}'
    + '* {padding: 0; margin: 0;}div {padding: 5px 8px; border-bottom:1px black solid;}td div:last-child{border: 0;} #war{color:#55000} </style>';
   END_HTML = '</table>'#13#10'</body>'#13#10'</html>';
var
  HTML: TStringList;
  FileName, s: string;
  i, j, k, l: Integer;
begin
  if not SaveToHTML.Execute then exit;
  FileName := SaveToHTML.FileName;
  HTML := TStringList.Create;
  HTML.Add(START_HTML);
  s := '<tr><th></th>';
  for i := 0 to High(DGrid_Rows) do
    s += #13#10 + '<th>' + DGrid_Rows[i].Caption + '</th>';
  HTML.Add(s + '</tr>');
  for i := 0 to High(DGrid_Cols) do
  begin
    s := '<tr><th>' + DGrid_Cols[i].Caption  + '</th>' + #13#10;
    for j := 0 to High(DGrid_Rows) do
    begin
      s += '<td>';
      for k := 0 to High(GridValues[i][j]) do
      begin
        s += '<div ';
        If isConflictRecord(GridValues[i][j][k].ID) then
          s += 'Class = "war" ';
        s += '>';
        for l := 0 to 5 do
        begin
          if not CG_Fields.Checked[l] then Continue;
          if CB_Titles.Checked then
            s += Tables[l].Fields[1].Caption + ':  ';
          s += GridValues[i][j][k].Fields[l] + '<br>';
        end;
        s += '</div>';
      end;
      s += '</td>' + #13#10;
    end;
    HTML.Add(s + '</tr>')
  end;
  HTML.Add(END_HTML);
  HTML.SaveToFile(FileName);
  HTML.Free;
end;

procedure TScheduleForm.ExpOO;
var
  Doc, Manager, Desktop, Sheets, Sheet: Variant;
begin
  Manager := CreateOleObject('com.sun.star.ServiceManager');
  Desktop := Manager.CreateInstance('com.sun.star.frame.Desktop');
  Doc := Desktop.LoadComponentFromURL('private:factory/scalc','_blank', 0, VarArrayCreate([0, - 1], varVariant));
  Sheets := Doc.GetSheets;
  Sheet := Sheets.GetByIndex(0);
end;

end.

