unit uFieldNames;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type

  TFieldDesc = record
    Caption: string;
    Name: string;
    Width: integer;
    TableIndex: integer;
  end;

  TTableDesc = class
  public
    Caption: string;
    Name: string;
    SQLText: string;
    Fields: array of TFieldDesc;
    function AddField(CurCaption, CurName: string; CurWidth, Index: integer): TTableDesc;
  end;

  TTablesArray = array of TTableDesc;

const
  HIGH_CONFLICT = 6;
  Select_All = 'SELECT * FROM ';
  Select_Stud = ('SELECT STUDENTS.ID, STUDENTS.NAME, GROUPS.NAME as GROUP_ID, '
    + ' STUDENTS.BIRHDAY FROM STUDENTS INNER JOIN GROUPS '
    + ' ON STUDENTS.GROUP_ID = GROUPS.ID ');
  Select_Sched = (' SELECT SCHEDULE_1.ID, ' +
                  ' CLASSROOMS.AUDITORY as CLASSROOM_ID, ' +
                  ' TIME_LESSONS.TIME_LESSON as TIME_ID, ' +
                  ' GROUPS.NAME as GROUP_ID,' +
                  ' WEEK_DAY.NAME_DAY as WEEK_ID, ' +
                  ' SUBJECTS.NAME as SUBJECT_ID,' +
                  ' TEACHERS.NAME as TEACHER_ID' +
                  ' FROM SCHEDULE_1 ' +
                  ' INNER JOIN WEEK_DAY ON SCHEDULE_1.WEEK_ID = WEEK_DAY.ID ' +
                  ' INNER JOIN TIME_LESSONS ON SCHEDULE_1.TIME_ID = TIME_LESSONS.ID ' +
                  ' INNER JOIN SUBJECTS ON SCHEDULE_1.SUBJECT_ID = SUBJECTS.ID ' +
                  ' INNER JOIN CLASSROOMS ON SCHEDULE_1.CLASSROOM_ID = CLASSROOMS.ID ' +
                  ' INNER JOIN GROUPS ON SCHEDULE_1.GROUP_ID = GROUPS.ID ' +
                  ' INNER JOIN TEACHERS ON SCHEDULE_1.TEACHER_ID = TEACHERS.ID ');
  Operations: array[0..7] of string = (
                  'Больше', 'Меньше', 'Не Меньше', 'Не больше',
                  'Равно', 'Не равно', 'Содержит', 'Начинается с');
  OperationsSQL: array[0..5] of string = ('>', '<', '>=', '<=', '=', '!=');
  CONF_NAME : array [0..HIGH_CONFLICT] of string = (
          'Преподаватель находится в разных аудиториях в одно и тоже время',
          'Группа находится в разных аудиториях в одно и тоже время',
          'Разные группы в одной аудитории в одно и тоже время',
          'Два предмета в одной аудитории в одно и тоже время',
          'Два преподавателя в одной аудитории в одно и тоже время у одной группы',
          'У одной группы разные пары в одно и тоже время',
          'Преподаватель ведёт разные предметы в одно и тоже время');

var
  Tables: TTablesArray;

implementation

function AddTable(CurCaption, CurName, Text: string): TTableDesc;
begin
  SetLength(Tables, Length(Tables) + 1);
  Tables[High(Tables)] := TTableDesc.Create;
  Tables[High(Tables)].Caption := CurCaption;
  Tables[High(Tables)].Name := CurName;
  Tables[High(Tables)].SQLText:= Text;
  Result := Tables[High(Tables)];
end;

function TTableDesc.AddField(CurCaption, CurName: string; CurWidth, Index: integer): TTableDesc;
begin
  SetLength(Fields, Length(Fields) + 1);
  Fields[High(Fields)].Caption := CurCaption;
  Fields[High(Fields)].Name := CurName;
  Fields[High(Fields)].Width := CurWidth;
  Fields[High(Fields)].TableIndex:= Index;
  Result := self;
end;

initialization

{0}AddTable('Аудитории', 'CLASSROOMS', Select_All + ' CLASSROOMS CLASSROOMS ').
    AddField('№', 'ID', 50, -1).
    AddField('Номер аудитории', 'AUDITORY', 130, -1);
{1}AddTable('Время', 'TIME_LESSONS', Select_All + ' TIME_LESSONS TIME_LESSONS ').
    AddField('№ Пары', 'ID', 70, -1).
    AddField('Начало', 'TIME_LESSON', 100, -1);
{2}AddTable('Группы', 'GROUPS', Select_All + ' GROUPS GROUPS ').
    AddField('№', 'ID', 50, -1).
    AddField('№ группы', 'NAME', 100, -1);
{3}AddTable('Дни недели', 'WEEK_DAY', Select_All + ' WEEK_DAY WEEK_DAY ').
    AddField('№', 'ID', 50, -1).
    AddField('День недели', 'NAME_DAY', 100, -1);
{4}AddTable('Предметы', 'SUBJECTS', Select_All + ' SUBJECTS SUBJECTS ').
    AddField('№', 'ID', 50, -1).
    AddField('Название предмета', 'NAME', 210, -1).
    AddField('Кол-во часов', 'HOURS', 100, -1);
{5}AddTable('Преподаватели', 'TEACHERS', Select_All + ' TEACHERS TEACHERS ').
    AddField('№', 'ID', 50, -1).
    AddField('Ф.И.О. Преподавателя', 'NAME', 300, -1);
{6}AddTable('Студенты', 'STUDENTS', Select_Stud).
    AddField('№', 'ID', 40, -1).
    AddField('Ф.И.О. Студента', 'NAME', 250, -1).
    AddField('№ Группы', 'GROUP_ID', 70, 2).
    AddField('Дата рождения', 'BIRHDAY', 100, -1);
{7}AddTable('Расписание', 'SCHEDULE_1', Select_Sched).
    AddField('№', 'ID', 0, -1).
    AddField('Аудитория', 'CLASSROOM_ID', 80, 0).
    AddField('Время', 'TIME_ID', 50, 1).
    AddField('№ группы', 'GROUP_ID', 70, 2).
    AddField('День недели', 'WEEK_ID', 90, 3).
    AddField('Предмет', 'SUBJECT_ID', 220, 4).
    AddField('Преподователь', 'TEACHER_ID', 220, 5);

end.

