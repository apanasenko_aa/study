unit uConflicts;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb, uDataModule, Dialogs, uFieldNames;

type

   ConflictNames = array of string;

   TValue = record
      Values: array[0..4] of string;
      ID: string;
   end;

   TConflictValue = record
      ConflictRec: array of TValue;
      GeneralField: string;
   end;

   TConflict = class
   public
      Name, SQLText: string;
      ID: integer;
      ConflictValues: array of TConflictValue;
      constructor Create(AName, ASQLText: string);
      procedure UpdateConflict(Query: TSQLQuery; NumConf: integer);
      procedure DestroyConflict;
   end;

   TConflictController = class
   private
      Query: TSQLQuery;
   public
      Conflicts: array[0..HIGH_CONFLICT] of TConflict;
      constructor Create;
      procedure UpdateConflicts;
   end;


implementation

{ TConflict }

constructor TConflict.Create(AName, ASQLText: string);
begin
  Name := AName;
  SQLText := ASQLText;
end;

procedure InsertOrUpdateConflict(ID, NumConf: integer);
var
  CQuery: TSQLQuery;
begin
  CQuery := TSQLQuery.Create(nil);
  with CQuery do
  begin
    DataBase := D_Module.D_Mod_Connection;
    Transaction := D_Module.D_Mod_Transaction;
    SQL.Text:= Format('SELECT ID from conflicts where Id = %d', [ID]);
    Open;
    if Fields[0].AsInteger <> ID then
    begin
      Close;
      SQL.Text := Format('INSERT INTO CONFLICTS VALUES (%d, 0, 0, 0, 0, 0, 0, 0)', [ID]);
      ExecSQL;
    end;
    Close;
    SQL.Text := Format('UPDATE CONFLICTS SET CONFLICTS.CONFLICT_%d = 1 WHERE ID = %d', [NumConf + 1, ID]);
    ExecSQL;
  end;
  CQuery.Close;
  CQuery.Free;
end;

procedure TConflict.UpdateConflict(Query: TSQLQuery; NumConf: integer);
var
  Field: String;
  HighConf, i: Integer;
begin
  DestroyConflict;
  with Query do
  begin
    Close;
    SQL.Clear;
    SQL.Text := SQLText;
    Open;
    Field := '';
    while not EOF do
    begin
      if Field <> Fields[0].AsString then
      begin
        SetLength(ConflictValues, Length(ConflictValues) + 1);
        Field := Fields[0].AsString;
        ConflictValues[High(ConflictValues)].GeneralField := Field;
      end;
      HighConf := High(ConflictValues);
      SetLength(ConflictValues[HighConf].ConflictRec, Length(ConflictValues[HighConf].ConflictRec) + 1);
      with ConflictValues[HighConf].ConflictRec[High(ConflictValues[HighConf].ConflictRec)] do
      begin
        for i := 1 to FieldCount - 2 do
          Values[i - 1] := Fields[i].AsString;
        ID := Fields[FieldCount - 1].AsString;
        InsertOrUpdateConflict(StrToInt(ID), NumConf);
      end;
      Next;
    end;
  end;
end;

procedure TConflict.DestroyConflict;
var
  i: Integer;
begin
  for i := 0 to High(ConflictValues) do
    SetLength(ConflictValues[i].ConflictRec, 0);
  SetLength(ConflictValues, 0);
end;

constructor TConflictController.Create;
const
  PATTERN = '%s %s, %s, %s, %s, %s %s ';
  SELECT = 'SELECT DISTINCT ';
  TEACHERS = '''Преподаватель: '' || TEACHERS.NAME ';
  SUBJECTS = '''Предмет: '' || SUBJECTS.NAME ';
  CLASSROOMS = '''Аудитория: '' || CLASSROOMS.AUDITORY ';
  GROUPS = '''Группа: '' || GROUPS.NAME ';
  INEER_JOIN_TABLES = 'WEEK_DAY.NAME_DAY, TIME_LESSONS.TIME_LESSON, SCHEDULE_1.ID FROM SCHEDULE_1 ' +
                      'INNER JOIN WEEK_DAY ON SCHEDULE_1.WEEK_ID = WEEK_DAY.ID ' +
                      'INNER JOIN TIME_LESSONS ON SCHEDULE_1.TIME_ID = TIME_LESSONS.ID ' +
                      'INNER JOIN CLASSROOMS ON SCHEDULE_1.CLASSROOM_ID = CLASSROOMS.ID ' +
                      'INNER JOIN SUBJECTS ON SCHEDULE_1.SUBJECT_ID = SUBJECTS.ID ' +
                      'INNER JOIN GROUPS ON SCHEDULE_1.GROUP_id = GROUPS.ID ' +
                      'INNER JOIN TEACHERS ON SCHEDULE_1.TEACHER_id = TEACHERS.ID ' +
                      'INNER JOIN SCHEDULE_1 S ON SCHEDULE_1.WEEK_ID = S.WEEK_ID ' +
                      'AND SCHEDULE_1.TIME_ID = S.TIME_ID';
  SQL_TEXT : array [0..HIGH_CONFLICT] of string = (
         //* Преподаватель находится в разных аудиториях одновременно */
        'AND SCHEDULE_1.TEACHER_ID = S.TEACHER_ID ' +
        'AND SCHEDULE_1.CLASSROOM_ID <> S.CLASSROOM_ID ' +
        'ORDER BY TEACHERS.NAME',

         //* Группа находится в разных аудиториях в одно время */
        'AND SCHEDULE_1.GROUP_ID = S.GROUP_ID ' +
        'AND SCHEDULE_1.CLASSROOM_ID <> S.CLASSROOM_ID ' +
        'ORDER BY GROUPS.NAME, WEEK_DAY.NAME_DAY',

        //* Разные группы в одной аудитории в одно время */
        'AND SCHEDULE_1.CLASSROOM_ID = S.CLASSROOM_ID ' +
        'AND SCHEDULE_1.GROUP_ID <> S.GROUP_ID ' +
        'ORDER BY CLASSROOMS.AUDITORY, WEEK_DAY.NAME_DAY, TEACHERS.NAME, TIME_LESSONS.TIME_LESSON',

        //* Два предмета в одной аудитории в одно время */
        'AND SCHEDULE_1.CLASSROOM_ID = S.CLASSROOM_ID ' +
        'AND SCHEDULE_1.SUBJECT_ID <> S.SUBJECT_ID ' +
        'ORDER BY GROUPS.NAME, WEEK_DAY.NAME_DAY ',

        //* 'Два преподавателя в одной аудитории в одно время у одной группы' */
        'AND SCHEDULE_1.TEACHER_ID <> S.TEACHER_ID ' +
        'AND SCHEDULE_1.CLASSROOM_ID = S.CLASSROOM_ID ' +
        'AND SCHEDULE_1.GROUP_ID = S.GROUP_ID ' +
        'ORDER BY CLASSROOMS.AUDITORY, WEEK_DAY.NAME_DAY, TIME_LESSONS.TIME_LESSON',

        //* 'У одной группы разные пары в одно и тоже время' */
        'AND SCHEDULE_1.GROUP_ID = S.GROUP_ID ' +
        'AND (SCHEDULE_1.TEACHER_ID <> S.TEACHER_ID ' +
        'OR SCHEDULE_1.CLASSROOM_ID <> S.CLASSROOM_ID ' +
        'OR SCHEDULE_1.SUBJECT_ID <> S.SUBJECT_ID) ' +
        'ORDER BY GROUPS.NAME, WEEK_DAY.NAME_DAY, TIME_LESSONS.TIME_LESSON',

        //* 'Преподаватель ведёт разные предметы в одно и тоже время' */
        'AND SCHEDULE_1.TEACHER_ID = S.TEACHER_ID ' +
        'AND SCHEDULE_1.SUBJECT_ID <> S.SUBJECT_ID ' +
        'ORDER BY TEACHERS.NAME, SUBJECTS.NAME, WEEK_DAY.NAME_DAY, TIME_LESSONS.TIME_LESSON' );
begin
  Query := TSQLQuery.Create(nil);
  Query.DataBase := D_Module.D_Mod_Connection;
  Query.Transaction := D_Module.D_Mod_Transaction;
  Conflicts[0] := TConflict.Create(CONF_NAME[0],
      Format (PATTERN, [SELECT, TEACHERS, CLASSROOMS, SUBJECTS, GROUPS, INEER_JOIN_TABLES, SQL_TEXT[0]]));
  Conflicts[1] := TConflict.Create(CONF_NAME[1],
      Format (PATTERN, [SELECT, GROUPS, CLASSROOMS, SUBJECTS, TEACHERS, INEER_JOIN_TABLES, SQL_TEXT[1]]));
  Conflicts[2] := TConflict.Create(CONF_NAME[2],
      Format (PATTERN, [SELECT, CLASSROOMS, TEACHERS, SUBJECTS, GROUPS, INEER_JOIN_TABLES, SQL_TEXT[2]]));
  Conflicts[3] := TConflict.Create(CONF_NAME[3],
      Format (PATTERN, [SELECT, CLASSROOMS, SUBJECTS, GROUPS, TEACHERS, INEER_JOIN_TABLES, SQL_TEXT[3]]));
  Conflicts[4] := TConflict.Create(CONF_NAME[4],
      Format (PATTERN, [SELECT, CLASSROOMS, TEACHERS, SUBJECTS, GROUPS, INEER_JOIN_TABLES, SQL_TEXT[4]]));
  Conflicts[5] := TConflict.Create(CONF_NAME[5],
      Format (PATTERN, [SELECT, GROUPS, SUBJECTS, CLASSROOMS, TEACHERS, INEER_JOIN_TABLES, SQL_TEXT[5]]));
  Conflicts[6] := TConflict.Create(CONF_NAME[6],
      Format (PATTERN, [SELECT, TEACHERS, SUBJECTS, CLASSROOMS, GROUPS, INEER_JOIN_TABLES, SQL_TEXT[6]]));


end;

procedure TConflictController.UpdateConflicts;
var
  i: Integer;
begin
  D_Module.D_Mod_Transaction.Commit;
  with Query do
  begin
    Close;
    SQL.Text:= 'DELETE FROM CONFLICTS';
    ExecSQL;
  end;
  for i := 0 to HIGH_CONFLICT do
    Conflicts[i].UpdateConflict(Query, i);
  D_Module.D_Mod_Transaction.Commit;
end;

end.
